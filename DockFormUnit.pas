unit DockFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TDockForm = class(TForm)
  private
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormGetSiteInfo(Sender: TObject; DockClient: TControl; var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
    procedure FormDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    function ComputeDockingRect(var DockRect: TRect; MousePos: TPoint): TAlign;
    procedure CMDockClient(var Message: TCMDockClient); message CM_DOCKCLIENT;
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); virtual;
  end;

  TFolderDockForm = class(TDockForm)
  private
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0);
  end;

  TThumbnailDockForm = class(TDockForm)
  private
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure FormEndDock(Sender, Target: TObject; X, Y: Integer);
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0);
  end;

implementation

uses MainFormUnit;

constructor TDockForm.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
 inherited;
 FormCreate(Self);
end;

procedure TDockForm.FormCreate(Sender: TObject);
begin
 Left := 711;
 Top := 164;
 Width := 200;
 Height := 200;
 BorderIcons := [biSystemMenu];
 BorderStyle := bsSizeToolWin;
 Color := clBtnFace;
 Font.Charset := DEFAULT_CHARSET;
 Font.Color := clWindowText;
 Font.Height := -11;
 Font.Name := 'MS Sans Serif';
 Font.Style := [];
 OldCreateOrder := False;
 OnCreate := FormCreate;
 OnDockOver := FormDockOver;
 OnGetSiteInfo := FormGetSiteInfo;
 PixelsPerInch := 96;
 DockSite := true;
 DragKind := dkDock;
 DragMode := dmAutomatic;
 KeyPreview := true;
 FormStyle := fsStayOnTop;
end;

procedure TDockForm.FormGetSiteInfo(Sender: TObject;
  DockClient: TControl; var InfluenceRect: TRect; MousePos: TPoint;
  var CanDock: Boolean);
begin
 if (Sender as TForm).HostDockSite <> nil then
  CanDock := DockClient is TDockForm
 else
  CanDock := false;
end;

procedure TDockForm.FormDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  ARect: TRect;
begin
 Accept := Source.Control is TDockForm;
 if Accept then
  if (Self.HostDockSite = nil) or (Self.HostDockSite.DockClientCount = 1) then
   if (ComputeDockingRect(ARect, Point(X, Y)) <> alNone) then
    Source.DockRect := ARect
   else
  else
   begin
    ARect.TopLeft := ClientToScreen(Self.ClientRect.TopLeft);
    ARect.BottomRight := ClientToScreen(Self.ClientRect.BottomRight);
    Source.DockRect := ARect;
   end
end;

function TDockForm.ComputeDockingRect(var DockRect: TRect; MousePos: TPoint): TAlign;
var
  DockLeftRect,
  DockRightRect: TRect;
begin
  Result := alNone;

  DockLeftRect.TopLeft := Point(0, 0);
  DockLeftRect.BottomRight := Point(ClientWidth div 2, ClientHeight);

  DockRightRect.TopLeft := Point(ClientWidth div 2, 0);
  DockRightRect.BottomRight := Point(ClientWidth, ClientHeight);

  if PtInRect(DockLeftRect, MousePos) then
    begin
      Result := alLeft;
      DockRect := DockLeftRect;
      DockRect.Right := ClientWidth div 2;
    end
  else
      if PtInRect(DockRightRect, MousePos) then
        begin
          Result := alRight;
          DockRect := DockRightRect;
          DockRect.Left := ClientWidth div 2;
        end
      else
  if Result = alNone then Exit;

  DockRect.TopLeft := ClientToScreen(DockRect.TopLeft);
  DockRect.BottomRight := ClientToScreen(DockRect.BottomRight);
end;

procedure TDockForm.CMDockClient(var Message: TCMDockClient);
var
  ARect: TRect;
  DockType: TAlign;
  Pt: TPoint;
begin
 if Message.DockSource.Control is TForm then
  begin
   Pt.x := Message.MousePos.x;
   Pt.y := Message.MousePos.y;
   DockType := ComputeDockingRect(ARect, Pt);
   if (HostDockSite is TPanel) then
    begin
     Message.DockSource.Control.ManualDock(HostDockSite, nil, DockType);
     Exit;
    end;
  end;
end;

{ TFolderDockForm }

constructor TFolderDockForm.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
 inherited;
 OnClose := FormClose;
end;

procedure TFolderDockForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 inherited;
 ActionViewFolders.Checked := false;
 ActionViewFolders.Enabled := true;
end;

{ TThumbnailDockForm }

constructor TThumbnailDockForm.CreateNew(AOwner: TComponent;
  Dummy: Integer);
begin
 inherited;
 OnClose := FormClose;
 OnStartDock := FormStartDock;
 OnEndDock := FormEndDock;
end;

procedure TThumbnailDockForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 inherited;
 ActionViewThumbnail.Checked := false;
 ActionViewThumbnail.Enabled := true;
end;

procedure TThumbnailDockForm.FormEndDock(Sender, Target: TObject; X, Y: Integer);
var
 Img : TThumbnailImage;
begin
 if HostDockSite = nil then
  if ControlCount > 0 then
   begin
    Img := (Controls[0] as TThumbnailImage);
     if Img.Thumbnail then
      begin
       ClientWidth := Img.Picture.Width + 20;
       ClientHeight := Img.Picture.Height + 20;
      end;
    end;
end;

procedure TThumbnailDockForm.FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
var
 Img : TThumbnailImage;
begin
 if ControlCount > 0 then
  begin
   Img := (Controls[0] as TThumbnailImage);
    if Img.Thumbnail then
     begin
//      DragObject := TDragDockObject.Create(nil);
//      DragObject.DockRect := Img.ClientRect;
     end;
   end;
end;

procedure TThumbnailDockForm.WMPaint(var Message: TWMPaint);
begin
 inherited;
 if HostDockSite = nil then
  begin
   Canvas.Pen.Color := clActiveCaption;
   Canvas.Pen.Style := psSolid;
   Canvas.Brush.Style := bsClear;
   Canvas.Rectangle(Rect(ClientRect.Left + 5, ClientRect.Top + 5, ClientRect.Right - 5, ClientRect.Bottom - 5));
  end;
end;

procedure TDockForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if (HostDockSite is TFoldersDockPanel) then
  ExifDateMainForm.ShowFoldersDockPanel(HostDockSite as TPanel, False);
end;

end.
