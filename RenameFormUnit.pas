unit RenameFormUnit;

interface

uses
  Forms, Classes, Graphics, Windows, StdCtrls, Controls, ExtCtrls, ComCtrls,
  Exif, Messages, SysUtils, Dialogs, MyRegistry, DateUtils;

const

 ERROR_FILE_NAME = 'ERROR:FILE:NAME';

 TagsSigns: array[0..51, 0..1] of PChar = (
                                          ('ApertureValue',           '%A'),
                                          ('ExposureBiasValue',       '%B'),
                                          ('Compression',             '%C'),
                                          ('DateTime',                '%D'),
                                          ('ExposureTime',            '%E'),
                                          ('FNumber',                 '%F'),
                                          ('ImageType',               '%G'),
                                          ('',                        '%H'),
                                          ('ISO',                     '%I'),
                                          ('',                        '%J'),
                                          ('',                        '%K'),
                                          ('Model',                   '%L'),
                                          ('Make',                    '%M'),
                                          ('ImageNumber',             '%N'),
                                          ('DateTimeOriginal',        '%O'),
                                          ('ColorSpace',              '%P'),
                                          ('',                        '%Q'),
                                          ('ISOSpeedRatings',         '%R'),
                                          ('ShutterSpeedValue',       '%S'),
                                          ('MeteringMode',            '%T'),
                                          ('UserComment',             '%U'),
                                          ('ExifVersion',             '%V'),
                                          ('OwnerName',               '%W'),
                                          ('PixelXDimension',         '%X'),
                                          ('PixelYDimension',         '%Y'),
                                          ('DateTimeDigitized',       '%Z'),

                                          ('FocusMode',               '%1M'),
                                          ('FlashActivity',           '%2F'),
                                          ('ExposureMode',            '%1E'),
                                          ('DigitalZoomRatio',        '%1D'),
                                          ('EasyShootingMode',        '%2E'),
                                          ('WhiteBalance',            '%1W'),
                                          ('Self-timer',              '%1T'),
                                          ('Flash',                   '%1F'),

                                          ('FocalLength',             '%3F'),
                                          ('Orientation',             '%1O'),
                                          ('XResolution',             '%1X'),
                                          ('YResolution',             '%1Y'),
                                          ('ResolutionUnit',          '%1R'),
                                          ('Software',                '%1S'),
                                          ('Artist',                  '%1A'),
                                          ('Copyright',               '%1C'),
                                          ('ComponentsConfiguration', '%2C'),
                                          ('YCbCrPositioning',        '%3C'),
                                          ('MakerNote',               '%2M'),
                                          ('FlashpixVersion',         '%4F'),
                                          ('MakroMode',               '%3M'),
                                          ('FlashMode',               '%5F'),
                                          ('FlashDetail',             '%6F'),
                                          ('ImageSixe',               '%2S'),
                                          ('Contrast',                '%4C'),
                                          ('Saturation',              '%3S')
                                          );

type

  TTagListBox = class(TListBox)
  protected
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
  end;

  TMaskComboBox = class(TComboBox)
   procedure DoOnChange(Sender: TObject);
   constructor Create(AOwner: TComponent); override;
  end;

  TRenameListView = class(TListView)
  public
    constructor Create(AOwner: TComponent); override;
  protected
    Column0width: integer;
    Column1width: integer;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
  end;

  TProgressForm = class(TForm)
    Panel: TPanel;
    ProgressBar: TProgressBar;
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); virtual;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    procedure CreateParams(var Params: TCreateParams);
  private
  end;

  TRenameForm = class(TForm)
  private
    RenameResult: boolean; // ������������� �� �������������� ������ (���� �������������, �� ExitButtonClick ������ mrOk)
    Activate: boolean;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ExitButtonClick(Sender: TObject);
    procedure TagListBoxDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure MaskEditDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure MaskEditDragDrop(Sender, Source: TObject; X, Y: Integer);
    function  GetMask: integer;
    function  ExtractSign(s: string): string;
    function  ExtractTag(s: string): string;
    function  FindByTagFromListBox(Tag: string): integer;
    function  FindBySignFromListBox(Sign: string): integer;
    procedure FillMaskEdit;
    function  ReadFiles: boolean;

    function  UniqueCheckRenamedFileName(_FileName: string; _Count: integer): string;
    function  GetRenamedFileName(iExif: TExif; var res: boolean): string;

    procedure Rename(Sender: TObject);
    procedure PreviewFiles;

    procedure PreviewButtonClick(Sender: TObject);
    procedure HelpButtonClick(Sender: TObject);
    procedure FillTagListBox;
    procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
    procedure Enabler(b: boolean);
    procedure ExifAfterRenameCheckClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    ChangeEdit: boolean;
  public
    Cancel: boolean; // ����������� ������ �� ESC
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); virtual;
    destructor Destroy;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function  ReadCommonTags: boolean;
  end;

var
  SaveMaskCount: integer;
  RenameButtonEnabled: boolean;
  Tags: TParamList;                 // ������ ����� �����
  RenameForm: TRenameForm;
  ProgressForm: TProgressForm;
  PanelMain,
  PanelRight,
  PanelClient,
  PanelTop,
  PanelBottom,
  PanelTagListBox: TPanel;
  Splitter: TSplitter;
  TagListBox: TTagListBox;
  Mask: TMaskComboBox;
  RenameView: TRenameListView;
  PreviewButton,
  HelpButton,
  RenameButton,
  ExitButton: TButton;
  ExifAfterRenameCheck: TCheckBox;

function PhotoStrDateToFileName(Str: string; AddTime: integer; TimeType: char; var res: boolean): string;

implementation

uses MainFormUnit;

function CorrectTagSigns: boolean;
// ��������� �������� �������� ����� �� ������������
var
 i, j: integer;
begin
 result := true;
 for i := 0 to Length(TagsSigns) - 1 do
  for j := 0 to Length(TagsSigns) - 1 do
   if i <> j then
    if TagsSigns[i, 1] = TagsSigns[j, 1] then
     begin
      asm int 3 end;
      result := false;
      exit;
     end;
end;

function HaveTagCode(Code: string): boolean;
// ��������� ������� �� ��� � ����� ����� � ������
 var
  i: integer;
begin
 result := false;
 for i := 0 to Length(TagsSigns) - 1 do
  if TagsSigns[i, 1] = Code then
   begin
    result := true;
    exit;
   end;
end;

function GetTag(sign: string): string;
var
 i: integer;
begin
 result := '';
 for i := 0 to Length(TagsSigns) - 1 do
  if TagsSigns[i, 1] = sign then
   begin
    result := TagsSigns[i, 0];
    exit;
   end;
end;

constructor TRenameForm.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
 inherited;
 FormCreate(Self);
 Activate := false;
 OnActivate := FormActivate;
 ChangeEdit := false;
end;

procedure TRenameForm.FormActivate(Sender: TObject);
begin
 if not Activate then
  begin
   Activate := true;
   FillMaskEdit;
   Application.ProcessMessages;
   if ReadFiles then
    RenameView.Perform(WM_SIZE, 0, 0)
   else
    ShowMessage('TRenameForm.FormActivate: Error read files.');
  end;
end;

procedure TRenameForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
end;

procedure TRenameForm.PreviewButtonClick(Sender: TObject);
begin
 PreviewFiles;
end;

procedure TRenameForm.HelpButtonClick(Sender: TObject);
begin
 ShowMessage('�������� ���� � ����� � �������� ����� ���������������' + #10#13 +
             '�� �������-�� �����, ����� ��� ������ ������ ��� �����.' + #10#13 +
             '��� ������ ����������, ���� ������ ������, ��������, � ������ ������� �����.' + #10#13 +
             '��� ����� ����� ���� ���� ��������� "+" ��� "-",' + #10#13 +
             '���������� ������� (�������� ���������� �����) � ������:' + #10#13 +
             '  "h" - �����' + #10#13 +
             '  "m" - �����' + #10#13 +
             '  "s" - ������' + #10#13 +
             '�������:' + #10#13 +
             '   %D-2h    DateTime - 2 ���� �����' + #10#13 +
             '   %Z+1h    DateTimeDigitized + 1 ��� ������' + #10#13 +
             '   %O+4h    DateTimeOriginal + 4 ���� ������' + #10#13 +
             '   %O+7m    DateTimeOriginal + 7 ����� ������' + #10#13 +
             '   %Z-1s    DateTimeDigitized - 1 ������� �����');
end;

procedure TRenameForm.ExifAfterRenameCheckClick(Sender: TObject);
begin
 MyReg.WriteToReg('ExifAfterRenameCheck', ExifAfterRenameCheck.Checked);
end;

procedure TRenameForm.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
  PreviewButtonClick(Sender);
end;

procedure TRenameForm.FormCreate(Sender: TObject);
var
 b: boolean;
 i: integer;
begin
 RenameResult := false;
 Cancel := false;
 Width := 564;
 Height := 360;
 Position := poMainFormCenter;
 BorderIcons := [biSystemMenu];
 BorderStyle := bsSizeToolWin;
 Color := clBtnFace;
 DockSite := True;
 Font.Charset := DEFAULT_CHARSET;
 Font.Color := clWindowText;
 Font.Height := -11;
 Font.Name := 'MS Sans Serif';
 Font.Style := [];
 OldCreateOrder := False;
 OnClose := FormClose;
// OnCreate := FormCreate;
 PixelsPerInch := 96;
 DockSite := false;
 KeyPreview := true;
// FormStyle := fsStayOnTop;
 Constraints.MinHeight := 300;
 Constraints.MinWidth := 550;

 PanelMain := TPanel.Create(Self);
 PanelMain.Parent := Self;
 PanelMain.Align := alClient;
 PanelMain.BevelOuter := bvNone;

 PanelTop := TPanel.Create(Self);
 PanelTop.Parent := PanelMain;
 PanelTop.Height := 50;
 PanelTop.Align := alTop;
 PanelTop.BevelOuter := bvNone;

 PanelRight := TPanel.Create(Self);
 PanelRight.Parent := PanelMain;
 PanelRight.Align := alRight;
 PanelRight.Width := 200;
 PanelRight.BevelOuter := bvNone;

 Splitter := TSplitter.Create(Self);
 Splitter.Parent := PanelMain;
 Splitter.Align := alRight;
 Splitter.Width := 5;

 PanelBottom := TPanel.Create(Self);
 PanelBottom.Parent := PanelRight;
 PanelBottom.Align := alBottom;
 PanelBottom.Height := 96;//113;
 PanelBottom.BevelOuter := bvNone;

 PanelTagListBox := TPanel.Create(Self);
 PanelTagListBox.Parent := PanelRight;
 PanelTagListBox.Align := alClient;
 PanelTagListBox.BevelOuter := bvNone;

 PanelClient := TPanel.Create(Self);
 PanelClient.Parent := PanelMain;
 PanelClient.Align := alClient;
 PanelClient.BevelOuter := bvNone;

 TagListBox := TTagListBox.Create(Self);
 TagListBox.Parent := PanelTagListBox;
 TagListBox.Align :=alNone;
 TagListBox.Ctl3D := false;
 TagListBox.SetBounds(5, 0, TagListBox.Parent.ClientWidth - 15, TagListBox.Parent.ClientHeight - 5);
 TagListBox.Anchors := [akLeft,akTop,akRight,akBottom];
 TagListBox.Style := lbOwnerDrawFixed;
 TagListBox.OnDrawItem := TagListBoxDrawItem;
 TagListBox.DragMode := dmAutomatic;

 RenameView := TRenameListView.Create(Self);
 RenameView.Parent := PanelClient;
 RenameView.Align := alClient;

 with RenameView.Columns.Add do
  Caption := 'Renaming';
 with RenameView.Columns.Add do
  Caption := 'Original name';

 RenameView.ViewStyle := vsReport;
 RenameView.RowSelect := true;
 RenameView.FlatScrollBars := false;
// RenameView.ReadOnly := true;
 RenameView.HideSelection := false;
 RenameView.CheckBoxes := true;
 RenameView.MultiSelect := true;

 Mask := TMaskComboBox.Create(Self);
 Mask.Parent := PanelTop;
 Mask.Top := PanelTop.ClientHeight div 2 - Mask.Height div 2;
 Mask.OnClick := PreviewButtonClick;
 Mask.OnKeyPress := MaskEditKeyPress;
 Mask.OnDragOver := MaskEditDragOver;
 Mask.OnDragDrop := MaskEditDragDrop;

 if MyReg.ReadFromReg('SaveRenameMaskCount', i) then
  SaveMaskCount := i
 else
  begin
   SaveMaskCount := 5;
   MyReg.WriteToReg('SaveRenameMaskCount', SaveMaskCount);
  end;

 PreviewButton := TButton.Create(Self);
 PreviewButton.Parent := PanelTop;
 PreviewButton.Top := Mask.Top - 2;
 PreviewButton.Height := Mask.Height;
 PreviewButton.Width := 120;
 PreviewButton.Caption := 'Preview';
 PreviewButton.Left := PreviewButton.Parent.ClientRect.Right - PreviewButton.Width - 20;
 PreviewButton.Anchors := [akRight];
 PreviewButton.OnClick := PreviewButtonClick;

 HelpButton := TButton.Create(Self);
 HelpButton.Parent := PanelTop;
 HelpButton.Top := Mask.Top - 2;
 HelpButton.Height := Mask.Height;
 HelpButton.Width := 20;
 HelpButton.Caption := '?';
 HelpButton.Anchors := [akRight];
 HelpButton.OnClick := HelpButtonClick;

 Mask.Width := PreviewButton.Left - HelpButton.Width - 20 - PanelTop.ClientRect.Left - 20;
 Mask.Left := Mask.Parent.ClientRect.Left + 20;
 Mask.Anchors := [akLeft,akRight];

 HelpButton.Left := Mask.Left + Mask.Width + 10;

 ExifAfterRenameCheck := TCheckBox.Create(Self);
 ExifAfterRenameCheck.Parent := PanelBottom;
 ExifAfterRenameCheck.Left := 20;
 ExifAfterRenameCheck.Top := 0;
 ExifAfterRenameCheck.Width := PanelBottom.ClientWidth - ExifAfterRenameCheck.Left;
 ExifAfterRenameCheck.Caption := 'Close this window after rename';
 if MyReg.ReadFromReg('ExifAfterRenameCheck', b) then
  ExifAfterRenameCheck.Checked := b
 else
  ExifAfterRenameCheck.Checked := false;
 ExifAfterRenameCheck.OnClick := ExifAfterRenameCheckClick;

 RenameButton := TButton.Create(Self);
 RenameButton.Parent := PanelBottom;
 RenameButton.Top := ExifAfterRenameCheck.Top + ExifAfterRenameCheck.Height + 7;
 RenameButton.Width := RenameButton.Parent.ClientWidth - 20;
 RenameButton.Caption := 'Rename';
 RenameButton.Left := 10;
 RenameButton.Anchors := [akLeft, akRight];
 RenameButton.OnClick := Rename;

 ExitButton := TButton.Create(Self);
 ExitButton.Parent := PanelBottom;
 ExitButton.Top := RenameButton.Top + RenameButton.Height + 10;
 ExitButton.Width := ExitButton.Parent.ClientWidth - 20;
 ExitButton.Caption := 'Exit';
 ExitButton.Left := 10;
 ExitButton.OnClick := ExitButtonClick;
 ExitButton.Anchors := [akLeft, akRight];

// PanelBottom.Height := ExitButton.Top + ExitButton.Height + 10;;
 OnKeyDown := FormKeyDown;

 Tags := TParamList.Create; // ������ ����� �����
end;

function TRenameForm.FindByTagFromListBox(Tag: string): integer;
var
 i: integer;
begin
 result := -1;
 for i := 0 to TagListBox.Count - 1 do
  if ExtractTag(TagListBox.Items[i]) = Tag then
   result := i;
end;

function TRenameForm.FindBySignFromListBox(sign: string): integer;
var
 i: integer;
begin
 result := -1;
 for i := 0 to TagListBox.Count - 1 do
  if ExtractSign(TagListBox.Items[i]) = sign then
   result := i;
end;

procedure TRenameForm.MaskEditDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
 s, old: string;
 Start, Len: integer;
 i: integer;
begin
 if Source is TTagListBox then
  begin
{
    s := (Sender as TComboBox).Text;
    old := s;
    if X > (Sender as TComboBox).Canvas.TextWidth(s) then
     begin
      Start := Length(s);
      Len := Length(ExtractSign((Source as TTagListBox).Items[(Source as TTagListBox).ItemIndex]));
      s := s + ExtractSign((Source as TTagListBox).Items[(Source as TTagListBox).ItemIndex]);
      (Sender as TComboBox).Text := s;
      (Sender as TComboBox).SelStart := start;
      (Sender as TComboBox).SelLength := Len;
     end
    else
     For i := 0 to Length(s) - 1 do ;
//      if
   (Sender as TComboBox).Text := old;
   (Source as TComboBox).SetFocus;
}
   Accept := true;
  end;
end;

procedure TRenameForm.MaskEditDragDrop(Sender, Source: TObject; X, Y: Integer);
var
 s: string;
 start: integer;
begin
{
 s := ExtractSign((Source as TTagListBox).Items[(Source as TTagListBox).ItemIndex]);
 (Sender as TComboBox).SetTextBuf(PChar(s));
 exit;
}
 start := Length((Sender as TComboBox).Text) + 1;//(Sender as TComboBox).SelStart;
 s := (Sender as TComboBox).Text;
 Insert(ExtractSign((Source as TTagListBox).Items[(Source as TTagListBox).ItemIndex]), s, start);
 (Sender as TComboBox).Text := s;
 (Sender as TComboBox).SetFocus;
 (Sender as TComboBox).SelLength := 0;
 (Sender as TComboBox).SelStart := start + Length(Copy((Source as TTagListBox).Items[(Source as TTagListBox).ItemIndex], 0, Pos(' - ', (Source as TTagListBox).Items[(Source as TTagListBox).ItemIndex]) - 1));
 ChangeEdit := true;
 PreviewFiles;
end;

function TRenameForm.ExtractSign(s: string): string;
begin
 result := copy(s, 0, Pos(' - ', s) - 1);
end;

function TRenameForm.GetMask: integer;
var
 i: integer;
begin
 result := -1;
 for i := 0 to Mask.Items.Count - 1 do
  if Mask.Items[i] = Mask.Text then
   begin
    result := i;
    exit;
   end;
end;

function TRenameForm.ExtractTag(s: string): string;
begin
 result := Copy(s, Pos(' - ', s) + 3, Length(s) - Pos(' - ', s) + 3);
end;

procedure TRenameForm.TagListBoxDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
 s: ShortString;
begin
 with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    s := ExtractTag((Control as TListBox).Items[Index]);
    TextOut(Rect.Left + 3, Rect.Top + (Rect.Bottom - Rect.Top) div 2 - TextHeight(s) div 2, s);
    s := '- ' + ExtractSign((Control as TListBox).Items[Index]);
    TextOut(Rect.Right - 42, Rect.Top + (Rect.Bottom - Rect.Top) div 2 - TextHeight(s) div 2, s);
   end;
end;

function TRenameForm.GetRenamedFileName(iExif: TExif; var res: boolean): string;
var
 s, v, sign: string;
 CorrectTimeSignLength: byte;
 CorrectHours: byte;
 CorrectType: char;
 i, j, n: integer;
begin
 CorrectTimeSignLength := 0;
 res := true;
 result := '';
 if Length(Mask.Text) = 0 then
  exit;
 if FileExists(iExif.FileName) then
  begin
   s := Mask.Text;
   i := 1;
   repeat
    // ������ ��������� ���
    if (s[i] = '%') and
    ((Length(s) >= i + 1) and (s[i + 1] in ['A'..'Z'])) or
    ((Length(s) >= i + 2) and (s[i + 1] in ['1'..'9']) and (s[i + 2] in ['A'..'Z']))
    then
     begin
      if s[i + 1] in ['A'..'Z'] then
       sign := s[i] + s[i + 1]
      else
       sign := s[i] + s[i + 1] + s[i + 2];
      // ��������� ���� �� ����� ��� � ������ (-1 ���� ���)
      n := FindBySignFromListBox(sign);
      if (n <> -1) and (iExif.ExifData.FindTag(ExtractTag(TagListBox.Items[n])) <> nil) then
       begin
        try
         if pos('DateTime', ExtractTag(TagListBox.Items[n])) <> 0 then
          // ����������� �������� ����� �� ��������� �����, �����, ������ ������ ��� �����
          // ���� �������� ����� ���� � �������� "+" ��� "-" ���������� ������� (���� ��� ��� �������) � ������ "h", "m", "s"
          if ((length(s) >= i + 4) and ((s[i + 2] = '+') or (s[i + 2] = '-')) and (s[i + 3] in ['0'..'9']) and (s[i + 4] in ['h', 'm', 's'])) or
             ((length(s) >= i + 5) and ((s[i + 2] = '+') or (s[i + 2] = '-')) and (s[i + 3] in ['0'..'9']) and (s[i + 4] in ['0'..'9'])) and (s[i + 5] in ['h', 'm', 's']) then
           begin
            if s[i + 4] in ['h', 'm', 's'] then
             begin
              CorrectTimeSignLength := 3;
              CorrectHours := StrToInt(s[i + 3]);
              CorrectType := s[i + 4];
             end
            else
             begin
              CorrectTimeSignLength := 4;
              CorrectHours := StrToInt(s[i + 3] + s[i + 4]);
              CorrectType := s[i + 5];
             end;
            if (s[i + 2] = '+') then
             v := PhotoStrDateToFileName(string(iExif.ExifData.FindTag(GetTag(s[i] + s[i + 1])).Value), CorrectHours, CorrectType, res)
            else
             v := PhotoStrDateToFileName(string(iExif.ExifData.FindTag(GetTag(s[i] + s[i + 1])).Value), -CorrectHours, CorrectType, res);
           end
          else
           v := PhotoStrDateToFileName(string(iExif.ExifData.FindTag(GetTag(s[i] + s[i + 1])).Value), 0, CorrectType, res)
         else
          v := string(iExif.ExifData.FindTag(ExtractTag(TagListBox.Items[n])).Value);
        except
         res := false;
         v := '';
        end;
        delete(s, i, length(Sign) + CorrectTimeSignLength); // ������� �� ����� ��� � ��������� �������, ���� ��� ����
        insert(v, s, i);            // ��������� ������ ���� �������� ����
        i := i + Length(v);
        continue;
       end
      else
       i := i + 1;
     end
    else
     i := i + 1;
   until i > Length(s);
  end;
 result := s + ExtractFileExt(iExif.FileName);
end;

function TRenameForm.UniqueCheckRenamedFileName(_FileName: string; _Count: integer): string;
// ��������� ������������ ����� ����� � ������ RenameView.Items
// ���� ��� �� ��������� ��������� _������ 1..MaxIndex
// Count ���������� ������� RenameView ������� � ���� ��� �������� �� ������������
const
 MaxIndex = HIGH(longint);
var
 i: integer;

 function Check(_Name: string): boolean;
 // ��������� ������������ ����� � ������ RenameView.Items
 var
  i: integer;
 begin
  result := true;
  for i := 0 to _Count do
   if CompareStr(RenameView.Items[i].Caption, _Name) = 0 then
    begin
     result := false;
     break;
    end;
 end;

 function ExtractFileNameOnly(_FName: string): string;
 // ��������� ��� ����� ��� ����������
 var
  Ext: string;
 begin
  result := _FName;
  if Length(ExtractFileExt(_FName)) > 0 then
   begin
    Ext := ExtractFileExt(_FName);
    delete(result, Pos(Ext, _FName), Length(Ext));
   end;
 end;

 function ChangeFileNameOnly(_FName, _NewFName: string): string;
 // ������ ������ ��� ����� ��� ���������� �� �����
 begin
  result := _NewFName + ExtractFileExt(_FName);
 end;

begin
 if _Count > RenameView.Items.Count - 1 then
  raise Exception.Create('ERROR #7f9');
 result := _FileName;
 if not Check(result) then
  begin
   for i := 1 to MaxIndex do
    if Check(ChangeFileNameOnly(_FileName, ExtractFileNameOnly(_FileName) + '_' + IntToStr(i))) then
     begin
      result := ChangeFileNameOnly(_FileName, ExtractFileNameOnly(_FileName) + '_' + IntToStr(i));
      break;
     end
  end;
end;

function TRenameForm.ReadFiles: boolean;
var
 i, CountSelFiles: integer;
 Tick: longword;
 res: boolean;
 Exif: TExif;
begin
 result := false;
 if Cancel then Exit;
 RenameButtonEnabled := true;
 Enabler(false);
 Tick := GetTickCount;
 ProgressForm := TProgressForm.CreateNew(Self);
 ProgressForm.ProgressBar.Min := 0;
 ProgressForm.ProgressBar.Max := FileListView.SelCount;
 ProgressForm.ProgressBar.Position := 0;
 ProgressForm.Caption := 'Reading exif...';
 CountSelFiles := 0;
 with ExifDateMainForm do
  begin
   if FileListView.SelCount > 0 then
    for i := 0 to FileListView.Items.Count - 1 do
     if (FileListView.Items[i].Selected) and (not Cancel) then
      begin
       if (GetTickCount - Tick > 100) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
        if not ProgressForm.Visible then
         ProgressForm.Show;
       Application.ProcessMessages;
       CountSelFiles := CountSelFiles + 1;
       ProgressForm.ProgressBar.Position := CountSelFiles;
       with RenameView.Items.Add do
        begin
         Data := FileListView.Items[i];
         SubItems.Add(FileListView.Items[i].Caption);
         if RenameView.Canvas.TextWidth(FileListView.Items[i].Caption) + 20 > RenameView.Column1Width then
          RenameView.Column1width := RenameView.Canvas.TextWidth(FileListView.Items[i].Caption) + 20;
         Caption := UniqueCheckRenamedFileName(GetRenamedFileName(TExif(FileListView.Items[i].Data), res), RenameView.Items.Count - 1);
         Checked := res;
        end;
      end;
  end;
 ProgressForm.Close;
 ProgressForm.Free;
 Enabler(true);
 result := true and (not Cancel);
end;

procedure TRenameForm.PreviewFiles;
var
 l: integer;
 Tick: longword;
 res: boolean;
begin
 RenameButtonEnabled := true;
 Enabler(false);
 Tick := GetTickCount;
 ProgressForm := TProgressForm.CreateNew(Self);
 ProgressForm.ProgressBar.Min := 0;
 ProgressForm.ProgressBar.Max := RenameView.Items.Count;
 ProgressForm.ProgressBar.Position := 0;
 ProgressForm.Caption := 'Changing file names...';
 for l := 0 to RenameView.Items.Count - 1 do
  begin
// ���� ������ ����� ������� �������� ������ � progressbar .....................
   if (GetTickCount - Tick > 100) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
    if not ProgressForm.Visible then
     ProgressForm.Show;
// .............................................................................
   ProgressForm.ProgressBar.Position := l;
// .............................................................................
   if FileExists(Dir(FolderTreeView.Path) + RenameView.Items[l].SubItems[0]) then
    begin
     RenameView.Items[l].Caption := UniqueCheckRenamedFileName(GetRenamedFileName(TExif(TListItem(RenameView.Items[l].Data).Data), res), l - 1);
     if res then
      RenameView.Items[l].Checked := true
     else
      RenameView.Items[l].Checked := false;
    end
   else
    begin
     ShowMessage('File: ' + Dir(FolderTreeView.Path) + RenameView.Items[l].SubItems[0] + ' not found' + #13#10);
     ModalResult := mrCancel;
    end;
   Application.ProcessMessages;
  end;
 ChangeEdit := false;
 ProgressForm.Close;
 ProgressForm.Free;
 Enabler(true);
end;

procedure TRenameForm.Enabler(b: boolean);
var
 i: integer;
begin
 for i := 0 to ComponentCount - 1 do
  (Components[i] as TControl).Enabled := b;
 if not RenameButtonEnabled then
  RenameButton.Enabled := false;
end;

function TRenameForm.ReadCommonTags: boolean;
var
 iExif: TExif;
 i, j, l: integer;
 Temp: TParamList;
 Item: TParamItem;
 CountSelFiles: integer;
 Tick: Longword;
begin
 result := false;
 if Cancel then exit;
 Enabler(false);
// �������� ����� ..............................................................
 Tick := GetTickCount;
 ProgressForm := TProgressForm.CreateNew(Self);
 try
  ProgressForm.ProgressBar.Min := 0;
  ProgressForm.ProgressBar.Max := FileListView.SelCount;
  ProgressForm.ProgressBar.Position := 0;
  ProgressForm.Caption := 'Looking for common tags...';

  CountSelFiles := 0;
  with ExifDateMainForm do
   begin
    if FileListView.SelCount > 0 then ;
    // ��������� ����� �� ������ � ������� �����
     for l := 0 to FileListView.Items.Count - 1 do
      // ���� ���� �������!
      if (FileListView.Items[l].Selected) and (not Cancel) then
       begin
        // ���� ������ ����� ������� �������� ������ � progressbar .....................
        if (GetTickCount - Tick > 100) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
         if not ProgressForm.Visible then
          ProgressForm.Show;
        Application.ProcessMessages;
// .............................................................................
        CountSelFiles := CountSelFiles + 1;
// .............................................................................
        ProgressForm.ProgressBar.Position := CountSelFiles;
// .............................................................................
        if ProgressForm.Visible then
         Application.ProcessMessages;
        // ��������� � ������� ���� ���� ������ ........................................
        iExif := TExif(FileListView.Items[l].Data);
        if iExif.ValidExif then
         case CountSelFiles of
          1: begin
               // ���� ��� ������ ���� � ������ ������ ��������� ��� �������� ���������� �� ���� �� � ���
               for i := 0 to iExif.ExifData.Count - 1 do
                for j := 0 to iExif.ExifData.Items[i].Count - 1 do
                 begin
                  Item := iExif.ExifData[i].Items[j];
                  Tags.AddFrom(Item);
                 end;
              end;
         else
          // ��� ��������� ����� ���������� � �������� ...................................
          // ������� ��������� ������
          Temp := TParamList.Create;
          // ��������� � ���� ��� ���� �������� �����
          for i := 0 to iExif.ExifData.Count - 1 do
           for j := 0 to iExif.ExifData.Items[i].Count - 1 do
            begin
             Item := iExif.ExifData[i].Items[j];
             Temp.AddFrom(Item);
            end;
          // ������� �� ����� ����
          i := 0;
          while i < Tags.Count do
           begin
            if Temp.Find(Tags.Items[i]) = -1 then
             begin
              Tags.Delete(i);
              i := i - 1;
             end;
            i := i + 1;
           end;
          Temp.Free;
         end
        else
         exit;
       end;
   end;
  FillTagListBox;
  result := true and (not Cancel); // ���� �� Cancel ����� true
  ProgressForm.Close;
 finally
  ProgressForm.Free;
  Enabler(true);
 end;
end;

procedure TRenameForm.FillTagListBox;
var
 i, j: integer;
 Sign: string[5];
 Ch1, Ch2: Char;
 Find: boolean;

begin
 TagListBox.Clear;
 if Tags.Count = 0 then exit;
 Ch1 := '1';
 Ch2 := 'A';
 Sign := '%' + Ch1 + Ch2;
 for i := 0 to Tags.Count - 1 do
  begin
   Sign := '%' + Ch1 + Ch2;
   Find := false;
   // ��������� ���� �� ����� ��� � ������
   // ���� ���� ����������� ��� ��� �� ������
   for j := 0 to Length(TagsSigns) - 1 do
    if TagsSigns[j, 0] = Tags.Items[i].Name then
     begin
      Sign := TagsSigns[j, 1];
      Find := true;
      Break;
     end;
   // ��������� ���
   TagListBox.AddItem(Sign + ' - ' + Tags.Items[i].Name, Pointer(i));

   // ���� ��� ��� ����������� ��������� ���������
   // ���� �������������� ��� ���� � ������, �� ��������� ���������
   if Find = false then
    repeat
     Ch2 := Chr(Ord(Ch2) + 1);
     if Ch2 = Chr(Ord('Z') + 1) then
      begin
       Ch2 := 'A';
       Ch1 := Chr(Ord(Ch1) + 1);
        if Ch1 = Chr(Ord('9') + 1) then
         begin
          Application.MessageBox('Overflow codes for tags!', 'Rename files', MB_OK);
          exit;
         end;
      end;
    until not HaveTagCode('%' + Ch1 + Ch2);
  end;
end;

procedure TRenameForm.FillMaskEdit;
var
 i: integer;
 s: string;
begin
// ��������� ���� � ��������� ..................................................
 for i := 0 to SaveMaskCount - 1 do
  begin
   if MyReg.ReadFromReg('SaveMask' + IntToStr(i), s) then
    Mask.Items.Add(s);
  end;
 if Mask.Items.Count > 0 then
  Mask.ItemIndex := 0;
// ���� �� ������� ������� �����, �� �������� DateTimeOriginal .................
 if Mask.ItemIndex = -1 then
  if FindByTagFromListBox('DateTimeOriginal') <> -1 then
   Mask.Text := '%O'
  else
   // ���� � ������ ��� ���� DateTimeOriginal, �� �������� DateTime ............
   if FindByTagFromListBox('DateTime') <> -1 then
    Mask.Text := '%D'
   else
    // ���� ��� ����� � ������, �� ������ ������ � ������
    if TagListBox.Items.Count > 0 then
     Mask.Text := ExtractTag(TagListBox.Items[0]);

{
 for i := 0 to Length(TagsSigns) - 1 do
  if FindByTagFromListBox(TagsSigns[i, 0]) <> -1 then
   begin
    Mask.Items.Add(TagsSigns[i, 1]);
    if TagsSigns[i, 0] = 'DateTimeOriginal' then
     Mask.ItemIndex := Mask.Items.Count - 1;
   end;
// ���� �� ������ �������� ���, �� �������� DateTimeDigitized ..................
 if Mask.ItemIndex = -1 then
  for i := 0 to Mask.Items.Count - 1 do
   if Mask.Items[i] = '%Z' then
    begin
     Mask.ItemIndex := i;
     Break;
    end;
// ���� �� ������ �������� ���, �� �������� DateTime ...........................
 if Mask.ItemIndex = -1 then
  for i := 0 to Mask.Items.Count - 1 do
   if Mask.Items[i] = '%D' then
    begin
     Mask.ItemIndex := i;
     Break;
    end;
// ���� �� ������ �������� ���, �� �������� ImageNumber (only Canon) ...........
 if Mask.ItemIndex = -1 then
  for i := 0 to Mask.Items.Count - 1 do
   if Mask.Items[i] = '%N' then
    begin
     Mask.ItemIndex := i;
     Break;
    end;
// ���� �� ������ �������� ���, �� �������� ������ �� ������ ...................
 if Mask.ItemIndex = -1 then
  Mask.ItemIndex := 1;
}
end;

procedure TRenameForm.ExitButtonClick(Sender: TObject);
begin
 if RenameResult then
  ModalResult := mrOk
 else
  ModalResult := mrCancel;
end;

{ TTagListBox }

procedure TTagListBox.WMSize(var Msg: TWMSize);
begin
 inherited;
 Invalidate;
end;

{ TProgressForm }

procedure TProgressForm.CreateParams(var Params: TCreateParams);
begin
 inherited CreateParams(Params);
 with Params do
  begin
   Style := Style or ws_Overlapped;
   WndParent := GetActiveWindow;
  end;
end;

constructor TProgressForm.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
 inherited;
 FormCreate(Self);
end;

procedure TProgressForm.FormCreate(Sender: TObject);
begin
 Width := 200;
 Height := 60;
 Caption := '';
 Position := poMainFormCenter;
 BorderIcons := [];
 BorderStyle := bsToolWindow;
 Color := clBtnFace;
 DockSite := True;
 Font.Charset := DEFAULT_CHARSET;
 Font.Color := clWindowText;
 Font.Height := -11;
 Font.Name := 'MS Sans Serif';
 Font.Style := [];
 OldCreateOrder := False;
// OnCreate := FormCreate;
 PixelsPerInch := 96;
 DockSite := false;
 KeyPreview := true;
 FormStyle := fsNormal;

 Panel := TPanel.Create(Self);
 Panel.Parent := Self;
 Panel.Align := alClient;
 Panel.BevelOuter := bvNone;
 Panel.Caption := '';

 ProgressBar := TProgressBar.Create(Self);
 ProgressBar.Parent := Self;
 ProgressBar.SetBounds(10, 10, 175, 15);

 OnKeyDown := FormKeyDown;
end;

procedure TProgressForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 if Owner is TForm then
  (Owner as TForm).OnKeyDown(Sender, Key, Shift);
end;

{ TRenameListView }

constructor TRenameListView.Create(AOwner: TComponent);
begin
 inherited;
 Column0width := 0;
 Column1width := 0;
end;

procedure TRenameListView.WMSize(var Msg: TWMSize);
begin
 inherited;
 if Column1width = 0 then
  Self.Column[1].Width := ClientWidth div 2
 else
  Self.Column[1].Width := Column1width;
 if Column0width = 0 then
  Self.Column[0].Width := ClientWidth - Self.Column[1].Width
 else
  Self.Column[0].Width := Column0width;
end;

destructor TRenameForm.Destroy;
begin
 Tags.Free;
end;

function PhotoStrDateToFileName(Str: String; AddTime: integer; TimeType: char; var res: boolean): string;
// TimeType 'h' ����, 'm' ������, 's' �������
var
 lFS, fnFS: TFormatSettings;
 d: TDateTime;
begin
 res := false;
 fnFS.ShortDateFormat := 'yyyy.mm.dd';
 fnFS.ShortTimeFormat := 'hh-nn-ss';
{
 FS.CurrencyFormat := 1;
 FS.NegCurrFormat := 5;
 FS.ThousandSeparator := #0;
 FS.DecimalSeparator := '.';
 FS.CurrencyDecimals := 2;
 FS.DateSeparator := '.';
 FS.TimeSeparator := ':';
 FS.ListSeparator := '+';
 FS.CurrencyString := '�.';
 FS.ShortDateFormat := 'dd/mm/yyyy';
 FS.LongDateFormat := 'dd/mm/yyyy';
 FS.TimeAMString := 'am';
 FS.TimePMString := 'pm';
 FS.ShortTimeFormat := 'hh:nn:ss';
 FS.LongTimeFormat := 'hh:nn:ss';
 FS.TwoDigitYearCenturyWindow := 10;
}

 lFS.DateSeparator:=':';
 lFS.TimeSeparator:=':';
 lFS.ShortDateFormat:='yyyy/mm/dd hh:nn:ss';
 lFS.LongDateFormat:='yyyy/mm/dd hh:nn:ss';
 lFS.ShortTimeFormat:='hh:nn:ss';
 lFS.LongTimeFormat:='hh:nn:ss';
 lFS.TwoDigitYearCenturyWindow := 10;
 try
  d := StrToDateTime(Str, lFS);
  if AddTime <> 0 then
   case TimeType of
    'h': d := IncHour(d, AddTime);
    'm': d := IncMinute(d, AddTime);
    's': d := IncSecond(d, AddTime);
   end;
  res := true;
 except
  d := 0;
 end;
 if d > 0 then
  result := FormatDateTime(fnFS.ShortDateFormat + ' ' + fnFS.ShortTimeFormat, d)
 else
  begin
   result := ERROR_FILE_NAME;
   RenameButtonEnabled := false;
  end;
end;

procedure TRenameForm.Rename(Sender: TObject);
var
 i: integer;
 NotRenamedFiles, m: integer;
 Tick: Longword;
 s: string;
begin
 if ChangeEdit then
  if Application.MessageBox('Refresh names before rename files!', 'Rename files', MB_YESNO) = IDYES then
   exit;
//   PreviewFiles;

 Tick := GetTickCount;
 ProgressForm := TProgressForm.CreateNew(Self);
 ProgressForm.ProgressBar.Min := 0;
 ProgressForm.ProgressBar.Max := RenameView.Items.Count;
 ProgressForm.ProgressBar.Position := 0;
 ProgressForm.Caption := 'Renaming files...';

 NotRenamedFiles := 0;
 m := 0;
 for i := 0 to RenameView.Items.Count - 1 do
  begin
// ���� ������ ����� ������� �������� ������ � progressbar .....................
   if (GetTickCount - Tick > 300) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
    if not ProgressForm.Visible then
     ProgressForm.Show;
// .............................................................................
   ProgressForm.ProgressBar.Position := i;
   if RenameView.Items[i].Checked then
    if FileExists(DirLabel.Caption + '\' + RenameView.Items[i].SubItems[0]) then
     if MoveFile(PChar(DirLabel.Caption + '\' + RenameView.Items[i].SubItems[0]), PChar(DirLabel.Caption + '\' + RenameView.Items[i].Caption)) then
      begin
       RenameResult := true;
       RenameView.Items[i].SubItems[0] := RenameView.Items[i].Caption;
       TListItem(RenameView.Items[i].Data).Caption := RenameView.Items[i].Caption;
       TExif(TListItem(RenameView.Items[i].Data).Data).ReplaceFileName(PChar(DirLabel.Caption + '\' + RenameView.Items[i].Caption));
       m := m + 1;
      end
     else
      begin
       ShowMessage(DirLabel.Caption + '\' + RenameView.Items[i].SubItems[0] + #10#13 + DirLabel.Caption + '\' + RenameView.Items[i].Caption + #10#13#10#13 + SysErrorMessage(GetLastError));
       try asm int 3 end except end; // �� ���� �� ������ ���� ������� ���� ������
       inc(NotRenamedFiles);
      end
    else
     begin
      ShowMessage(DirLabel.Caption + '\' + RenameView.Items[i].SubItems[0] + #10#13#10#13 + 'File not found');
      try asm int 3 end except end; // �� ������ �������� ����! ���� �� �����?
      inc(NotRenamedFiles);
     end;
  end;
 ProgressForm.Close;
 ProgressForm.Free;

 // ��������� ����� � �������
 s := Mask.Text;
 m := GetMask;
 if m = -1 then
  begin
   if Mask.Items.Count < SaveMaskCount then
    Mask.Items.Add('');
   if Mask.Items.Count > 1 then
    for i := Mask.Items.Count - 1 downto 0 do
     Mask.Items[i] := Mask.Items[i - 1];
   Mask.Items[0] := s;
   for i := 0 to Mask.Items.Count - 1 do
    MyReg.WriteToReg('SaveMask' + IntToStr(i), Mask.Items[i]);
  end
 else
  if m > 0 then
   begin
    for i := m downto 0 do
     Mask.Items[i] := Mask.Items[i - 1];
    Mask.Items[0] := s;
    for i := 0 to m do
     MyReg.WriteToReg('SaveMask' + IntToStr(i), Mask.Items[i]);
   end;
 // ���������� ���������, ���� ���� �� ��������������� �����
 if NotRenamedFiles > 0 then
  ShowMessage(IntToStr(NotRenamedFiles) + ' files not renamed')
 else
  if ExifAfterRenameCheck.Checked then
   ModalResult := mrOk
  else
   ShowMessage('All selected files successfuly renamed');
end;

{ TMaskComboBox }

constructor TMaskComboBox.Create(AOwner: TComponent);
begin
 inherited;
 OnChange := DoOnChange
end;

procedure TMaskComboBox.DoOnChange(Sender: TObject);
begin
// if (Parent.Parent.Parent is TRenameForm) then
//  TRenameForm(Parent.Parent.Parent).ChangeEdit := true;
end;

procedure TRenameForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 case Key of
  27: Cancel := true;
 end;
end;

begin
 CorrectTagSigns;
end.
