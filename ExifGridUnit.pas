unit ExifGridUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Exif, Grids, ConstUnit;

type
  TExifGridForm = class(TForm)
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); virtual;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormStartDock(Sender: TObject;
      var DragObject: TDragDockObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ExifGridForm: TExifGridForm;
  ExifGrid : TExifGrid;

implementation

uses MainFormUnit;

constructor TExifGridForm.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
 inherited;
 FormCreate(Self);
end;

procedure TExifGridForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if (HostDockSite is TExifGridRightDockPanel) then
  ExifDateMainForm.ShowExifGridDockPanel(HostDockSite as TPanel, False, 0);
 Action := caHide;
 ActionViewExifGrid.Checked := false;
 ActionViewExifGrid.Enabled := true;
end;

procedure TExifGridForm.FormCreate(Sender: TObject);
begin
 Left := 520;
 Top := 286;
 Width := 504;
 Height := 360;
 BorderIcons := [biSystemMenu];
 BorderStyle := bsSizeToolWin;
 Color := clBtnFace;
 DockSite := True;
 DragKind := dkDock;
 DragMode := dmAutomatic;
 Font.Charset := DEFAULT_CHARSET;
 Font.Color := clWindowText;
 Font.Height := -11;
 Font.Name := 'MS Sans Serif';
 Font.Style := [];
 OldCreateOrder := False;
 OnClose := FormClose;
 OnCreate := FormCreate;
 OnStartDock := FormStartDock;
 PixelsPerInch := 96;
 DockSite := false;
 DragKind := dkDock;
 DragMode := dmAutomatic;
 KeyPreview := true;
 FormStyle := fsStayOnTop;
end;

procedure TExifGridForm.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
var
 Arect : TRect;
begin
 ARect.Left := 0;
 ARect.Right := 50;
 ARect.Top := 0;
 ARect.Bottom := 50;
// DragObject.DockRect := ARect;
end;

end.
