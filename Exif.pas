unit Exif;

interface

uses
  Classes, SysUtils, Variants, StdCtrls, Math, Grids, Forms, Types,
  Controls, Windows, Messages, FileCtrl, Menus, ImgList, CommCtrl, Graphics,
  LitheMenus, Contnrs

  ,Dialogs;

const
  Motorola = $4D4D;     // Motorola align
  Intel    = $4949;     // Intel align

// Types of files
  tfJPEG = 'JPEG';
  tfTIFF = 'TIFF';

// POPUP MENU ..................................................................
  PM_TITLE_ITEM         = 'Exif Reader  version 1.0';
  PM_SHOWUNKNOWN_ITEM   = 'Show unknown tags';
  PM_SHOWFILENAME_ITEM  = 'Show file name';
  PM_EXPANDINGTREE_ITEM = 'Expanding tree on begin';
  PM_VIEWLOG_ITEM       = 'View log';
  PM_ABOUT_ITEM         = 'About';


type

  TParamList = class;

  TParamItem = class
  private
  protected
    Number: Byte;
  public
    Name: Ansistring;
    ParamType: Byte;
    Value: Variant;
    Unknown: Boolean;
    Group: TParamList;      // ���������
  end;

  TParamList = class(TObjectList)
  private
    FName: string;
  protected
    function GetItem(Index: Integer): TParamItem;
    procedure AddItem(Name: string; Number, ParamType: Byte; Value: Variant; Unknown: boolean);
  public
    procedure AddFrom(Value: TParamItem);
    function Compare(Value1, Value2: TParamItem; WithValue: boolean): boolean;
    function Find(Value: TParamItem): integer;
    function GetByName(ParamName: string): Variant;
    property Items[Index: Integer]: TParamItem read GetItem;
    property Name: string read FName write FName;
  end;

  TParamListCollections = class
  private
   FCount: integer;
   function GetSubItemsCount: integer;
   function GetUnknownCount: integer;
   function GetItem(Index: integer): TParamList;
  protected
   FTitles: array of TParamList;
   function Add(iName: string): boolean;
  public
   function GetByName(iName: string): TParamList;
   function FindTag(Tag: string): TParamItem;
   function GetIndexByName(iName: string): integer;
   property Items[Index: integer]: TParamList read GetItem; default;
   constructor Create;
   destructor Destroy; override;
  published
   property Count: integer read FCount;
   property UnknownCount: integer read GetUnknownCount;
   property SubItemsCount: integer read GetSubItemsCount;
  end;

  TItemType = (ERROR,
               NoExif,                 // #255#255 No Exif
               TitleFileName,          // ��� ����� � ������ �����
               TitleBlank,             // #0#0 �������� ������ ���������
               TitleCollapsed,         // #1#1 �������� �������� ���������
               TitleExpanded,          // #2#2 �������� ��������� ���������
               RightOfTitle,           // ������ ������ �� �������� ���������
               UnknownSubTitle,        // ������������ ��������������� ���������
               LastUnknownSubTitle,    // ������������ ��������������� ��������� (��������� � ���������)
               SubTitle,               // ������������ ���������
               LastSubTitle,           // ������������ ��������� (��������� � ���������)
               Value,                  // ��������
               UnknownValue,           // �������� ��������������� ���������
               LastTitle);             // #3#3 ��������� �����

  TExifElement = Packed Record
    Key        : Word;
    Name       : string;
    Value      : Variant;
    TypeElement: TItemType;
    Unknown    : boolean;
  end;

  TExifGrid = class;

  TExifGridColors = class(TPersistent)
  private
    FTitleBgColor: TColor;
    FTitleBgColor3DLight: TColor;
    FTitleBgColor3DShadow: TColor;
    FTextBgColor: TColor;
    FLineColor: TColor;
    FPlusColor: TColor;
    FTitleColor: TColor;
    FSubTitleColor: TColor;
    FValueColor: TColor;
    FCursorColor: TColor;
    FCursorFrameColor: TColor;
    FFileNameBgColor: TColor;
    FFileNameColor: TColor;
    FUnknownBgColor: TColor;
    FUnknownSubTitleColor: TColor;
    FUnknownValueColor: TColor;
    FOnChange: TNotifyEvent;
    procedure SetColors(Index: Integer; Value: TColor);
    procedure Change;
  published
    property TitleBgColor: TColor index 0 read FTitleBgColor write SetColors;
    property TitleBgColor3DLight: TColor index 1 read FTitleBgColor3DLight write SetColors;
    property TitleBgColor3DShadow: TColor index 2 read FTitleBgColor3DShadow write SetColors;
    property TextBgColor: TColor index 3 read FTextBgColor write SetColors;
    property LineColor: TColor index 4 read FLineColor write SetColors;
    property PlusColor: TColor index 5 read FPlusColor write SetColors;
    property TitleColor: TColor index 6 read FTitleColor write SetColors;
    property SubTitleColor: TColor index 7 read FSubTitleColor write SetColors;
    property ValueColor: TColor index 8 read FValueColor write SetColors;
    property CursorColor: TColor index 9 read FCursorColor write SetColors;
    property CursorFrameColor: TColor index 10 read FCursorFrameColor write SetColors;
    property FileNameBgColor: TColor index 11 read FCursorFrameColor write SetColors;
    property FileNameColor: TColor index 12 read FCursorFrameColor write SetColors;
    property UnknownBgColor: TColor index 13 read FUnknownBgColor write SetColors;
    property UnknownSubTitleColor: TColor index 14 read FUnknownSubTitleColor write SetColors;
    property UnknownValueColor: TColor index 15 read FUnknownValueColor write SetColors;
  end;

  TIndention = 0..MaxInt;
  TExifGridIndentions = class(TPersistent)
  private
    FIndention: TIndention;
    FIndentionPlus: TIndention;
    FSubTitleIndention: TIndention;
    FValueIndention: TIndention;
    FTitleIndention: TIndention;
    FOnChange: TNotifyEvent;
    procedure SetIndentions(Index: Integer; Value: TIndention);
    procedure Change;
  published
    property Indention: TIndention index 0 read FIndention write SetIndentions;
    property IndentionPlus: TIndention index 1 read FIndentionPlus write SetIndentions;
    property SubTitleIndention: TIndention index 2 read FSubTitleIndention write SetIndentions;
    property ValueIndention: TIndention index 3 read FValueIndention write SetIndentions;
    property TitleIndention: TIndention index 4 read FTitleIndention write SetIndentions;
  end;

  TPlusSize = 5..25;
  TExifGridPlus = class(TPersistent)
  private
    FControl: TExifGrid;
    FPlusSize: TPlusSize;
    FPlusVisible: boolean;
    FOnChange: TNotifyEvent;
    procedure SetPlusSize(Value: TPlusSize);
    procedure SetPlusVisible(Value: boolean);
    procedure Change;
  published
    property PlusSize: TPlusSize read FPlusSize write SetPlusSize;
    property PlusVisible: boolean read FPlusVisible write SetPlusVisible;
  end;

  Z_QWE = class(TstringGrid);

  TExif = class;

  TSelectedRow = class
                  Name: string;
                  Value: string;
                  procedure Clear;
                 end;

  TSelectCellEvent = procedure (Sender: TObject; ACol, ARow: Longint; SelectItem: TSelectedRow) of object;

  TExifGrid = class(TCustomGrid)
  private
    FSelfExif: boolean;
    FExif: TExif;                     // ������ TExif
    FPopupMenu: TPopupMenu;
    FData: array of TExifElement;     // ������ ������
    FCells: array of TExifElement;    // ������ ��������� ������
    FTitleHeight: byte;               // ������ ������ � �����������
    FMaxColWidth: integer;            // ������ ������� ������� �� ������
    FColMove: boolean;                // ������� ��������� ������� �������� ������
    FColors: TExifGridColors;         // �����
    FIndentions: TExifGridIndentions; // �������
    FPlus: TExifGridPlus;             // ��������� �����
    FFileName: string;                // ��� �����
    FDefaultExpand: boolean;          // ������� ���������� ��������� ��� ������
    FFileNameVisible: boolean;        // ������� ������ ����� ����� � ������� ������
    FUnknownVisible: boolean;         // ������� ������ ����������� �����
    FAutoCatchFocus: boolean;         // ������ ����� ��� ��������� ������
    FLogAlwaysVisible: boolean;       // ���������� ����� ���� "view log" ��� ���������� EXIF
    FSelectedRow :TSelectedRow;
    FOnSelectCell: TSelectCellEvent;
    FMouseMove: TMouseMoveEvent;
    procedure Refresh(Sender: TObject);
    procedure ExpandData(Row: integer);
    procedure Write(s: string);
    function  GetData(Col,Row: integer):string;
    function  GetCells(ACol, ARow: Integer): string;
    procedure SetCells(ACol, ARow: Integer; const Value: string);
    procedure InitScrollBar;
    function  GetScrollTrackPos: integer;
    function  TypeOf(Col,Row: longint): TItemType;
    procedure Click(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ClickPlus(Col,Row: integer);
    procedure SetFileName(iFileName: string);
    procedure SetDefaultExpand(Value: boolean);
    procedure FillGrid;
    function  FillData(ReadDataFromFile: boolean = true): boolean;
    procedure SetPlus(Value: TExifGridPlus);
    procedure SetIndentions(Value: TExifGridIndentions);
    procedure SetFileNameVisible(Value: boolean);
    procedure SetUnknownVisible(Value: boolean);
    procedure SetColumnsSize;
    procedure CreatePopupMenu(PM: TPopupMenu);
    procedure ShowAboutClick(Sender: TObject);
    procedure ShowUnknownClick(Sender: TObject);
    procedure ShowFileNameClick(Sender: TObject);
    procedure SetDefaultExpandClick(Sender: TObject);
    procedure ShowLog(Sender: TObject);
    function  GetFirstColumnWidth: integer;
    procedure SetFirstColumnWidth(const Value: integer);
    procedure SetExif(const Value: TExif);
    procedure GetExifData(ReadDataFromFile: boolean);
  protected
    procedure WMNCPaint(var Msg: TWMNCPaint); message WM_NCPAINT;
    procedure WMLButtonDown(var Message: TWMLButtonDown); message WM_LBUTTONDOWN;
    procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message WM_LBUTTONDBLCLK;
    procedure WMMouseMove(var Message: TWMMouseMove); message WM_MOUSEMOVE;

    function  DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function  DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DrawCell(ACol, ARow: Longint; Rect: TRect; AState: TGridDrawState); override;
    function  SelectCell(ACol, ARow: Longint): Boolean; override;
  public
    property RowHeights;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property SelectedRow: TSelectedRow read FSelectedRow;// write FSelectedRow;
  published
    property Exif: TExif read FExif write SetExif;
    property PopupMenu;
    property Align;
    property Font;
    property BorderStyle;
//    property PopupMenu;
//    property Exif: TExif read FExif;
    property TitleHeight: byte read FTitleHeight write FTitleHeight;
    property MaxColWidth: integer read FMaxColWidth write FMaxColWidth;
    property FileName: string read FFileName write SetFileName;
    property Colors: TExifGridColors read FColors write FColors;
    property Indentions: TExifGridIndentions read FIndentions write SetIndentions;
    property Plus: TExifGridPlus read FPlus write SetPlus;
    property DefaultExpand: boolean read FDefaultExpand write SetDefaultExpand;
    property FileNameVisible: boolean read FFileNameVisible write SetFileNameVisible;
    property UnknownVisible: boolean read FUnknownVisible write SetUnknownVisible;
    property AutoCatchFocus: boolean read FAutoCatchFocus write FAutoCatchFocus;
    property LogAlwaysVisible: boolean read FLogAlwaysVisible write FLogAlwaysVisible;
    property OnMouseMove: TMouseMoveEvent read FMouseMove write FMouseMove;
    property OnSelectCell: TSelectCellEvent read FOnSelectCell write FOnSelectCell;
    property FirstColumnWidth: integer read GetFirstColumnWidth write SetFirstColumnWidth;
  end;

  TTIFFHeader = Packed record
    Align     : Word;        // II or MM
    bytes     : Word;        // 0x002A or 0x2A00
    OffsetIDF0: Cardinal;    // OffSet of the first IDF
  End;

  TTag = Packed record
    TagID  : Word;       //Tag number
    TagType: Word;       //Type tag
    Count  : Cardinal;   //tag length
    OffSet : Cardinal;   //Offset / Value
  end;

  TJFIF = Packed record
    Number    : Byte;
    Length    : Word;
    Version   : string;
    Units     : string;
    Xdensity  : Word;
    Ydensity  : Word;
    Xthumbnail: Byte;
    Ythumbnail: Byte;
  end;

  TSubIFDItem = Packed record
     Owner    : string;
     Length   : Cardinal;
     OffSet   : Cardinal;
  end;

  TThumbnail = Packed record
     Image    : array of byte;
     Offset   : Cardinal;
     Length   : Cardinal;
  end;

  TMarker = Packed record
    Identifier: Word;
    Offset    : Int64;
  end;

  TExif = class(TComponent)
    public
//      FileArray: array of Byte; // ����� ����������� ���������� ������
    private
      MakerNoteInformation: string; // �������� ��������� Maker Note
      F: File;
      FFileName                : Ansistring;  // ��� �����
      FFileExtention           : Ansistring;  // ���������� �����
      FLog                     : TstringList; // ��� ������ �����
      FLogTickCount            : Cardinal;    // ����� ����� �������� � ���
      FTickCount               : Cardinal;
      FExifData                : TParamListCollections; // ��������� ����������
// --------------------
      FOwner             : string;
      FTypeOfFile        : string;     // ��� �����: 'JPEG', 'TIFF'
      FTypeOfAlignTIFF   : string;     // ��� ������������ ������ � ���������� 'Intel', 'Motorola'
      FValidExif         : Boolean;    // Has valid Exif header
      FThumbnail         : TThumbnail;
      FJFIFmarker        : Boolean;    // Has valid JFIF marker
      FJFIF              : TJFIF;      // JFIF marker
// ��������� �� �������������� �������
      IFDpointer         : Cardinal;        // Exif IFD Pointer
      GPSpointer         : Cardinal;        // GPS Info IFD Pointer
      InteroperabilityIFDpointer: Cardinal;        // Interoperability IFD Pointer
      PrintIM            : TSubIFDItem;     // Print Image Matching IFD Pointer
      AdobeResource      : TSubIFDItem;     // Adobe resources
      XMLMetadata        : TSubIFDItem;     // XML Meta Data
      MakerNote          : TSubIFDItem;
// �������
      SOI                : TMarker;
      EOI                : TMarker;
{
      FAPP1              : Cardinal;  // Exif attribute information
      FAPP2              : Cardinal;  // Exif extended data
      FDQT               : Cardinal;  // Quantization table definition
      FDHT               : Cardinal;  // Huffman table definition
      FDRI               : Cardinal;  // Restart Interoperability definition
      FSOF               : Cardinal;  // Parameter data relating to frame
      FSOS               : Cardinal;  // Parameters relating to components
}
      FUnknownsVisible: boolean;

      procedure Write(s: string);
      procedure SetFileName(const iFileName: Ansistring);
      procedure Init;
      procedure ReadFromFile;
      function  DoubleToStr(Value: Double; Count: byte; TrimRightZero: boolean = true):string;
      function  ExtractDigit(var Str: string): Cardinal;
      function  InvertHex2(const Value:Word):Word;         // InvertHex2($1234)=$3412
      function  InvertHex4(const Value:Cardinal):Cardinal; // InvertHex4($12345678)=$78563412
      function  ReadValue(TagType: Word; Count, OOffset, Off0: Cardinal; const TIFFAlign: Word): Variant;
      procedure GetTagValue(const Tag: TTag; Align: Word; Off0: Cardinal; var ParamName: string; var Param, logParam: variant);
      procedure ReadOlympusMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadNikonMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadCanonMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadFujifilmMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadCasioMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadSanyoMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadMinoltaMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadSonyMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure ReadUnknownMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
      procedure BlockRead(var F: File; var Buf; Count: Integer);
      procedure ReadLeicaMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
    protected
    public
// �����������
      constructor Create;
      destructor Destroy; override;
      procedure ReplaceFileName(iFileName: Ansistring); // ������ FFileName ��� ������ Exif ���������� (������������ ��������� ��� �������������� ������, ��� �� �� ������ ������ ��� ���� ������)
    published
// ��� �����
      property FileName: Ansistring read FFileName write SetFileName;
// ��� �������� �����
      property Log: TstringList read FLog;
// ���������� ������������ ��������� �� ���������� EXIF ���������
      property ValidExif: Boolean read FValidExif;
// ��������� ����������
      property ExifData: TParamListCollections read FExifData;

      property Thumbnail: TThumbnail read FThumbnail;

// ��������� �� ��������� JFIF
      property JFIFmarker: Boolean read FJFIFmarker;
// ��������� JFIF (JPEG File Interchange Format)
      property JFIF: TJFIF read FJFIF;
// ��� ����� 'JPEG' ��� 'TIFF'
      property TypeOfFile: string read FTypeOfFile;
// ��� ������������ ���� � ����� 'Intel' - ������������ ������� (��� ����)
//                               'Motorola' - ������������ ����� �������
      property TypeOfAlignTIFF: string read FTypeOfAlignTIFF;
// ���� ������ ����������� �����
      property UnknownsVisible: Boolean read FUnknownsVisible write FUnknownsVisible;
  end;

{$R *.res}

procedure Register;
function RationalToDouble(str: string): Double;

implementation

type

  TAPP = Packed record
    MarkerPrefix: Byte;
    Number      : Byte;
    Length      : Word;
    Indefiner   : Array [1..5] of Char; //Indefiner - "Exif" 00, "JFIF" 00 and ets
    Padding     : Byte;
    TIFFHeader  : TTIFFHeader;
    Entries     : Word;

  end;

  TMarkerType = Packed record
    case Integer of
      0: (Word: Word);                 // ������ APPO (255,240)
      1: (FF, Number: Byte);
  end;

  TJFIFMarker = Packed record
    Marker: TMarkerType;
    Length: Word;
  end;

  TJFIFrec = Packed record
    Marker: TMarkerType;              // ������ APPO (255,240)
    Length    : Word;                 // ����� ������ ��������� +2
{
    Indefin   : Array [1..5] of Char; // �������������: JFIF#0
    Version   : Word;                 // ������ (1,2)
    Units     : Byte;                 // ������� ��������� ��������� �� ����������� � � Y
    Xdensity  : Word;                 // �(��������������) ���������
    Ydensity  : Word;                 // Y (������������) ���������
    Xthumbnail: Byte;                 // ������ ������������ �����������: �
    Ythumbnail: Byte;                 // ������ ������������ �����������: �
}    
  end;

  TIFDHeader = Packed record
    pad         : Byte;     //00h
    ByteOrder   : Word;     //II (4D4D)
    i42         : Word;     //2A00
    IFD0offSet  : Cardinal; //0th offset IFD
    Interoperabil: Byte;
  end;

Var
  Ch: Char;
{
  function RationalToDouble(str:string):Double;
  function DoubleToStr(V: Double; Count: byte):string;
  function InvertHex2(Value:Word):Word; // InvertHex2($1234)=$3412
  function InvertHex4(Value:Cardinal):Cardinal; // InvertHex4($12345678)=$78563412
}

procedure Register;
begin
 RegisterComponents('LITHE', [TExifGrid]);
 RegisterComponents('LITHE', [TExif]);
// RegisterPropertyEditor(TypeInfo(integer), TExifGrid, 'Tag', nil).
end;

procedure TExif.BlockRead(var F: File; var Buf; Count: Integer);
var
 i,j,k: Cardinal;
begin
{$R+}
{
 if Length(FileArray) > 0 then
  begin
   j := FilePos(F);
   if j + Count > FileSize(F) then
    asm int 3 end;
   for i := j to j + Count - 1 do
    FileArray[i] := Ord(Ch);
  end;
}
{$R-}
 System.BlockRead(F, Buf, Count);
end;

procedure TExifGridColors.Change;
begin
 if Assigned(FOnChange) then FOnChange(Self);
end;

procedure TExifGridColors.SetColors(Index: Integer; Value: TColor);
begin
 case Index of
   0: FTitleBgColor := Value;
   1: FTitleBgColor3DLight := Value;
   2: FTitleBgColor3DShadow := Value;
   3: FTextBgColor := Value;
   4: FLineColor := Value;
   5: FPlusColor := Value;
   6: FTitleColor := Value;
   7: FSubTitleColor := Value;
   8: FValueColor := Value;
   9: FCursorColor := Value;
  10: FCursorFrameColor := Value;
  11: FFileNameBgColor := Value;
  12: FFileNameColor := Value;
  13: FUnknownBgColor := Value;
  14: FUnknownSubTitleColor := Value;
  15: FUnknownValueColor := Value;
 end;
 Change;
end;

procedure TExifGridIndentions.Change;
begin
 if Assigned(FOnChange) then FOnChange(Self);
end;

procedure TExifGridIndentions.SetIndentions(Index: Integer; Value: TIndention);
begin
 case Index of
  0: if FIndention<>Value then
      begin
       FIndention := Value;
       Change;
      end;
  1: if FIndentionPlus<>Value then
      begin
       FIndentionPlus := Value;
       Change;
      end;
  2: if FSubTitleIndention<>Value then
      begin
       FSubTitleIndention := Value;
       Change;
      end;
  3: if FValueIndention<>Value then
      begin
       FValueIndention := Value;
       Change;
      end;
  4: if FTitleIndention<>Value then
      begin
       FTitleIndention := Value;
       Change;
      end;
 end;
end;

procedure TExifGridPlus.Change;
begin
 if Assigned(FOnChange) then FOnChange(Self);
end;

procedure TExifGridPlus.SetPlusSize(Value: TPlusSize);
begin
 FPlusSize := Value;
 if FPlusSize > FControl.TitleHeight then FPlusSize := FControl.TitleHeight-2;
 if FPlusSize mod 2 = 0 then FPlusSize := FPlusSize+1;
 Change;
end;

procedure TExifGridPlus.SetPlusVisible(Value: Boolean);
begin
 FPlusVisible := Value;
 Change;
end;

procedure TExifGrid.SetPlus(Value: TExifGridPlus);
begin
 FPlus.Assign(Value);
end;

procedure TExifGrid.SetIndentions(Value: TExifGridIndentions);
begin
 FIndentions.Assign(Value);
end;

procedure TExifGrid.Write(s: string);
begin
// Form1.Output2.Lines.Add(s)
end;

procedure TExifGrid.ShowAboutClick(Sender: TObject);
begin
 ShowMessage('Exif reader'+#13#13'version 1.0'#13#13'By lithe@mail.ru');
end;

procedure TExifGrid.ShowUnknownClick(Sender: TObject);
begin
 if UnknownVisible = true then UnknownVisible := false
 else UnknownVisible := true;
end;

procedure TExifGrid.ShowFileNameClick(Sender: TObject);
begin
 if FileNameVisible = true then FileNameVisible := false
 else FileNameVisible := true;
end;

procedure TExifGrid.SetDefaultExpandClick(Sender: TObject);
begin
 if DefaultExpand = true then DefaultExpand := false
 else DefaultExpand := true;
end;

procedure TExifGrid.ShowLog(Sender: TObject);
var
 LogForm: TForm;
 LogMemo: TMemo;
begin
 if FExif <> nil then
  begin
   LogForm := TForm.Create(nil);
   LogForm.Caption := 'Log '+ FFileName;
   LogForm.Height := 500;
   LogForm.Width := 500;
   LogForm.BorderStyle := bsSizeToolWin;
   LogForm.Left := ClientToScreen(Point((Left + Width div 2) - LogForm.Width div 2, 0)).X;
   LogForm.Top := ClientToScreen(Point(0, (Top + Height div 2) - LogForm.Height div 2)).Y;
   LogForm.Position := poMainFormCenter;
   LogMemo := TMemo.Create(LogForm);
   LogMemo.Parent := LogForm;
   LogMemo.Align := alClient;
   LogMemo.ReadOnly := true;
   LogMemo.ScrollBars := ssBoth;
   LogMemo.WordWrap := false;
   LogMemo.Lines.Assign(FExif.FLog);
   LogForm.ShowModal;
   Self.Invalidate;
   LogMemo.Free;
   LogForm.Free;
  end;
end;

procedure TExifGrid.CreatePopupMenu(PM: TPopupMenu);
var
 Item: TMenuItem;
begin
 PM.Items.Caption := 'Exif';
// ������� ����� ���� ��������� ................................................
 Item := TMenuItem.Create(PM);
 Item.Caption := PM_TITLE_ITEM;
 Item.Enabled := false;
 PM.Items.Add(Item);
{
// ������� ����� ���� ����������� ..............................................
 Item := TMenuItem.Create(PM);
 Item.Caption := '-';
 PM.Items.Add(Item);
}
// ������� ����� ���� SHOW UNKNOWN .............................................
 Item := TMenuItem.Create(PM);
 Item.Caption := PM_SHOWUNKNOWN_ITEM;
 Item.Checked := false;
 Item.OnClick := ShowUnknownClick;
 PM.Items.Add(Item);
// ������� ����� ���� SHOW FILE NAME ...........................................
 Item := TMenuItem.Create(PM);
 Item.Checked := true;
 Item.Caption := PM_SHOWFILENAME_ITEM;
 Item.OnClick := ShowFileNameClick;
 PM.Items.Add(Item);
// ������� ����� ���� EXPANDING TREE ON BEGIN ..................................
 Item := TMenuItem.Create(PM);
 Item.Checked := true;
 Item.Caption := PM_EXPANDINGTREE_ITEM;
 Item.OnClick := SetDefaultExpandClick;
 PM.Items.Add(Item);
// ������� ����� ���� ����������� ..............................................
 Item := TMenuItem.Create(PM);
 Item.Caption := '-';
 PM.Items.Add(Item);
// ������� ����� ���� VIEW LOG .................................................
 Item := TMenuItem.Create(PM);
 Item.Caption := PM_VIEWLOG_ITEM;
 if FLogAlwaysVisible then
  Item.Enabled := true
 else
  Item.Enabled := false;
 Item.OnClick := ShowLog;
 PM.Items.Add(Item);
// ������� ����� ���� ����������� ..............................................
 Item := TMenuItem.Create(PM);
 Item.Caption := '-';
 PM.Items.Add(Item);
// ������� ����� ���� ABOUT ....................................................
 Item := TMenuItem.Create(PM);
 Item.Caption := PM_ABOUT_ITEM;
 Item.OnClick := ShowAboutClick;
 PM.Items.Add(Item);
 LitheMenu.Add(PM, nil);
end;

constructor TExifGrid.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
 FSelfExif := false;
 ShowHint := true;
 FPopupMenu := TPopupMenu.Create(Self);
 FColors := TExifGridColors.Create;
 FIndentions := TExifGridIndentions.Create;
 FPlus := TExifGridPlus.Create;
 CreatePopupMenu(FPopupMenu);
 PopupMenu := FPopupMenu;
 FColors.FOnChange := Refresh;
 FIndentions.FOnChange := Refresh;
 FPlus.FOnChange := Refresh;
 FPlus.FControl := Self;
 FFileNameVisible := true;
 FUnknownVisible := true;
 FAutoCatchFocus := false;
 FLogAlwaysVisible := false;
 ColCount := 2;
 RowCount := 1;
 FixedCols := 1;
 Constraints.MinHeight := 90;
 Constraints.MinWidth := 90;
 BorderStyle := bsSingle;
 ColWidths[0] := Width;
 RowHeights[0] := Height;
 DefaultColWidth := 150;
 DefaultDrawing := false;
 DefaultRowHeight := 15;
 Font.Charset := 204;
 Font.Name := 'Arial';
 Font.Pitch := fpFixed;
 Font.Size := 8;
 Color := clWindow;
 GridLineWidth := 1;
 Options := [goColSizing, goEditing];
 ParentColor := false;
 ScrollBars := ssVertical;
 Visible := true;
 ControlStyle := [csCaptureMouse, csOpaque, csDoubleClicks, csNeedsBorderPaint, csClickEvents];
 FIndentions.FIndention := 5;
 FIndentions.FIndentionPlus := 6;
 FIndentions.FSubTitleIndention := 15;
 FIndentions.FValueIndention := 5;
 FIndentions.FTitleIndention := 13;
 FPlus.FPlusSize := 9;
 FColors.FTitleBgColor := clBtnFace;
 FColors.FTitleBgColor3DLight := clBtnHighlight;
 FColors.FTitleBgColor3DShadow := clBtnShadow;
 FColors.FTextBgColor := clWindow;
 FColors.FLineColor := FColors.TitleBgColor;
 FColors.FPlusColor := cl3DDkShadow;
 FColors.FTitleColor := clBlack;
 FColors.FSubTitleColor := clBlack;
 FColors.FValueColor := clBlack;
 FColors.FCursorColor := clCream;
 FColors.FCursorFrameColor := FColors.PlusColor;
 FColors.FFileNameBgColor := clCream;
 FColors.FFileNameColor := clGray;
 FColors.FUnknownBgColor := FColors.FTextBgColor;
 FColors.FUnknownSubTitleColor := clRed;
 FColors.FUnknownValueColor := clGray;

 FPlus.FPlusVisible := true;
 FTitleHeight := 20;
 FDefaultExpand := true;
 SetLength(FData,1);
 SetLength(FCells,RowCount);
 FCells[0].Name := 'No Exif';
 FCells[0].TypeElement := NoExif;
 FSelectedRow := TSelectedRow.Create;
 FSelectedRow.Clear;
end;

function TExifGrid.GetCells(ACol, ARow: Integer): string;
{
var
  ssl: TstringSparseList;
}
begin
{
  ssl := TstringSparseList(TSparseList(FData)[ARow]);
  if ssl = nil then Result := '' else Result := ssl[ACol];
}
end;

procedure TExifGrid.SetCells(ACol, ARow: Integer; const Value: string);
begin
{
  TstringGridstrings(EnsureDataRow(ARow))[ACol] := Value;
  EnsureColRow(ACol, True);
  EnsureColRow(ARow, False);
  Update(ACol, ARow);
}
end;

procedure TExifGrid.WMLButtonDblClk(var Message: TWMLButtonDblClk);
var
 Point: TPoint;
 hintWnd: THintWindow;

 procedure ActivateHint(x,y: Integer);
 var
  rect: TRect;
 begin
  {
  rect := hintWnd.CalcHintRect(Screen.Width, Hint, nil);
  }
  rect.Left := 0;
  rect.Right := 200;
  rect.Top := 0;
  rect.Bottom := 100;
  hintWnd.ActivateHint(rect, Hint);
 end;

begin
// inherited;
 if ComponentState = [csDesigning] then
  begin
   Align := alClient;
   Refresh(Self);
  end
 else
  begin
   Point := ScreenToClient(Mouse.CursorPos);
   if (Point.X >= ColWidths[0] - 1) and (Point.X <= ColWidths[0] + 1) then
    begin
     ColWidths[0] := FIndentions.FIndention + FIndentions.FSubTitleIndention + FMaxColWidth + 2;
     SetColumnsSize;
    end;
   if TypeOf(MouseCoord(Message.XPos, Message.YPos).X, MouseCoord(Message.XPos, Message.YPos).Y) in ([Value, UnknownValue]) then
    begin
     Hint := FSelectedRow.Name + #10 + #13 + FSelectedRow.Value;
     if not Assigned(hintWnd) then hintWnd := THintWindow.Create(nil);
     ActivateHint(10,10);
//     Application.ShowHint := true;
    end;
  end;
end;

procedure TExifGrid.SetColumnsSize;
var
 SBI: TScrollBarInfo;
 H: THandle;
 BorderSize: byte;
begin
// ��������� ������ ����� ������ ��������
 BorderSize := 0;
 case BorderStyle of
  bsNone  : BorderSize := 0;
  bsSingle: BorderSize := 4;
 end;
// �������� ������ � �������� ScrollBar
 SBI.cbSize := SizeOf(SBI);
 h := Handle;
 GetScrollBarInfo(H, OBJID_VSCROLL,SBI);
// ��������� ����� �� ScrollBar � �������������� ������������� ������ �������
 if (STATE_SYSTEM_INVISIBLE and SBI.rgstate[0])<>0 then
  ColWidths[1] := Width - ColWidths[0] - BorderSize
 else
  ColWidths[1] := Width-ColWidths[0] - SBI.dxyLineButton - BorderSize;
end;

procedure TExifGrid.WMSize(var Msg: TWMSize);
begin
 inherited;
 if Length(FData) > 1 then
  SetColumnsSize
 else
  begin
   ColWidths[0] := Width;
   RowHeights[0] := Height;
  end;
 InitScrollBar;
end;

procedure TExifGrid.WMPaint(var Msg: TWMPaint);
begin
 inherited;
 InitScrollBar;
 Write('WMPaint  FixedRows = ' + IntToStr(FixedRows) + ' TopRow = ' + IntToStr(TopRow) + ' Row = ' + IntToStr(Row) + ' LeftCol = ' + IntToStr(LeftCol) + ' ColWidths[0] = ' + IntToStr(ColWidths[0]));
end;

procedure TExifGrid.Refresh;
begin
 Invalidate;
end;

procedure TExifGrid.ExpandData(Row: integer);
begin
 if FData[FCells[Row].Key].TypeElement = TitleExpanded then FData[FCells[Row].Key].TypeElement := TitleCollapsed
 else FData[FCells[Row].Key].TypeElement := TitleExpanded;
end;

procedure TExifGrid.SetDefaultExpand(Value: boolean);
begin
 FDefaultExpand := Value;
 PopupMenu.Items.Find(PM_EXPANDINGTREE_ITEM).Checked := Value; 
 Invalidate;
end;

procedure TExifGrid.GetExifData(ReadDataFromFile: boolean);
begin
 // ���� ������ ������ �� ����� ���� ����� �� �� �������� ������� TExif
 if FillData(ReadDataFromFile) then
  FPopupMenu.Items.Find('View log').Enabled := true
 else
  if (FExif <> nil) and (FLogAlwaysVisible) then
   FPopupMenu.Items.Find('View log').Enabled := true
  else
   FPopupMenu.Items.Find('View log').Enabled := false;
 FillGrid;
 TopRow := FixedRows;
end;

procedure TExifGrid.SetFileName(iFileName: string);
begin
 if (FSelfExif) and (Assigned(FExif)) then FExif.Free;
 FSelfExif := true;
 FExif := TExif.Create;
 FFileName := iFileName;
 GetExifData(true);
 Write('SetFileName: ColWidths[0]=' + IntToStr(ColWidths[0]));
end;

function TExifGrid.FillData(ReadDataFromFile: boolean = true): boolean;
Var
 i,j,l: Word;
    DE: TItemType;
begin
 result := false;
 if FDefaultExpand then DE := TitleExpanded else DE := TitleCollapsed;
 FExif.UnknownsVisible := true;
 if ReadDataFromFile then
  FExif.FileName := FFileName;
 if (not ReadDataFromFile) or ((ReadDataFromFile) and (FileExists(FFileName))) then
  if FExif.ValidExif then
   begin
    // ������ Exif ���������
    SetLength(FData, FExif.FExifData.SubItemsCount);
    l := 0;
    if FExif.FExifData.Count > 0 then
     for i := 0 to FExif.FExifData.Count - 1 do   // ������ �� ������ ���������
      begin
       result := true;
       FData[l].Name := FExif.FExifData[i].Name;
       FData[l].Value := '';
       FData[l].Unknown := false;
       if FExif.FExifData[i].Count > 0 then
        FData[l].TypeElement := DE
       else
        FData[l].TypeElement := TitleBlank;
       l := l + 1;
       if FExif.FExifData[i].Count > 0 then
        for j := 0 to FExif.FExifData[i].Count - 1 do // ������ �� ������ ����� � ���������
         begin
          FData[l].Name := FExif.FExifData[i].Items[j].Name;
          FData[l].Value := FExif.FExifData[i].Items[j].Value;
          FData[l].Unknown := FExif.FExifData[i].Items[j].Unknown;
          if (j = FExif.FExifData[i].Count - 1) then
           FData[l].TypeElement := LastSubTitle
          else
           FData[l].TypeElement := SubTitle;
          l := l + 1;
         end;
      end
   end
  else
   begin
//  Exif ��������� �����������
    SetLength(FData, 1);
    FData[0].Name := 'no Exif';
    FData[0].TypeElement := NoExif;
   end
 else
  if ReadDataFromFile then
   begin
    // ���� �����������
    SetLength(FData, 1);
    FData[0].Name := 'file nof found';
    FData[0].TypeElement := NoExif;
   end;
 if Length(FData) > 0 then
  for i := 0 to Length(FData) - 1 do
   FData[i].Key := i;
end;

procedure TExifGrid.FillGrid;
Var
 Key: word;
 ReadPos, WritePos, i, j: integer;
 TempType: TItemType;
begin
 ColCount := 2;
 if Length(FData) = 1 then
  begin
   RowCount := 1;
   ColWidths[0] := Width;
   RowHeights[0] := Height;
   SetLength(FCells,RowCount);
   FCells[0] := FData[0];
  end
 else
  begin
   ColWidths[0] := (Width div 5) * 2;
   SetColumnsSize;
   if FFileNameVisible then
    begin
     RowCount := Length(FData)+2;
     FixedRows := 1;
     WritePos := 1;
    end
   else
    begin
     RowCount := Length(FData)+1;
     FixedRows := 0;
     WritePos := 0;
    end;
   SetLength(FCells, RowCount);
   FCells[0].Name := FFileName;
   FCells[0].Value := '';
   FCells[0].TypeElement := TitleFileName;
   FCells[0].Unknown := false;
   ReadPos := 0;
   while ReadPos < Length(FData) do
    begin
     if (FUnknownVisible = true) or ((FUnknownVisible = false) and (FData[ReadPos].Unknown = false)) then
      begin
       FCells[WritePos] := FData[ReadPos];
       if FData[ReadPos].TypeElement = TitleCollapsed then
        begin
         ReadPos := ReadPos + 1;
         while not (FData[ReadPos].TypeElement in ([TitleExpanded, TitleBlank, TitleCollapsed, LastTitle]))
               and (ReadPos < Length(FData)) do
          ReadPos := ReadPos + 1;
         ReadPos := ReadPos - 1;
        end;
       WritePos := WritePos + 1;
      end;
     ReadPos := ReadPos + 1;
    end;
   FCells[WritePos].Name := '';
   FCells[WritePos].Value := '';
   FCells[WritePos].TypeElement := LastTitle;
   RowCount := WritePos+1;
   for i := 0 to RowCount - 2 do
    begin
     if (FUnknownVisible = false) then
      if (FCells[i].TypeElement in ([TitleExpanded, TitleCollapsed])) and
         (FCells[i + 1].TypeElement in ([TitleBlank, TitleExpanded, TitleCollapsed, LastTitle])) then
       begin
        j := FCells[i].Key + 1;
        TempType := FCells[i].TypeElement;
        FCells[i].TypeElement := TitleBlank;
        while (j < Length(FData) - 1) do
         begin
          if (FData[j].TypeElement in ([TitleBlank, TitleExpanded, TitleCollapsed, LastTitle])) then Break;
          if FData[j].Unknown = false then FCells[i].TypeElement := TempType;
          j := j + 1;
         end;
       end;
     if (FCells[i+1].TypeElement in ([TitleBlank, TitleExpanded, TitleCollapsed, LastTitle])) and
        (FCells[i].TypeElement in ([SubTitle])) then
      FCells[i].TypeElement := LastSubTitle;
     case FCells[i].TypeElement of
      TitleFileName,
      Value,
      SubTitle,
      LastSubTitle  : RowHeights[i] := DefaultRowHeight;
      TitleBlank,
      TitleExpanded,
      TitleCollapsed: RowHeights[i] := TitleHeight;
     end;
    end;
   RowHeights[RowCount-1] := FIndentions.FIndention+2;
  end;
// ���� ������ ��������� �� ������ ���������� ��� �� ���������� ������� ������
// ��� �� ������ ���� ������ ���������� ������ ���  
 if not (TypeOf(1, Row) in ([Value, UnknownValue])) then
  begin
   Key := VK_DOWN;
   KeyDown(Key, []);
   Key := VK_UP;
   KeyDown(Key, []);
  end
 else
  SelectCell(Col, Row);
 Invalidate;
 InitScrollBar;
end;

procedure TExifGrid.InitScrollBar;
var
 SI: TSCROLLINFO;
begin
 SI.cbSize := SizeOf(SI);
 SI.fMask := (SIF_PAGE or SIF_RANGE or SIF_POS);
 GetScrollInfo(Handle,SB_VERT,SI);
 SI.nMin := FixedRows;
 SI.nMax := RowCount - 1;
 SI.nPage := VisibleRowCount;
 SI.nPos := TopRow;
// SI.nTrackPos := 0;
 SetScrollInfo(Handle,SB_VERT,SI,true);
// Write('InitScrollBar: RowCount='+IntToStr(RowCount)+' VisibleRowCount='+IntToStr(VisibleRowCount))
end;

function TExifGrid.GetScrollTrackPos: integer;
var
 SI: TSCROLLINFO;
begin
 SI.cbSize := SizeOf(SI);
 SI.fMask := SIF_ALL;
 GetScrollInfo(Handle,SB_VERT,SI);
 result := SI.nTrackPos;
// Write('Min='+IntToStr(SI.nMin)+' Max='+IntToStr(SI.nMax)+' Page='+IntToStr(SI.nPage)+' Pos='+IntToStr(SI.nPos)+' TrackPos='+IntToStr(SI.nTrackPos));
end;

procedure TExifGrid.WMVScroll(var Message: TWMVScroll);
begin
// inherited;
// Write('OnScroll');
// GetScrollTrackPos;
 case Message.ScrollCode of
  SB_LINEDOWN:   begin
                   if TopRow+VisibleRowCount<RowCount then
                    TopRow := TopRow+1;
                   SetScrollPos(Handle,SB_VERT,TopRow,true);
                  end;
  SB_LINEUP:     begin
                   if TopRow>FixedRows then
                    TopRow := TopRow-1;
                   SetScrollPos(Handle,SB_VERT,TopRow,true);
                  end;
  SB_THUMBTRACK: begin
                   TopRow := GetScrollTrackPos;
                   SetScrollPos(Handle,SB_VERT,TopRow,true);
                  end;
  SB_PAGEDOWN  : begin
                   if TopRow+VisibleRowCount<RowCount then
                    if RowCount-(TopRow+VisibleRowCount)<VisibleRowCount then
                     TopRow := RowCount-VisibleRowCount
                    Else
                     TopRow := TopRow+VisibleRowCount;
                   SetScrollPos(Handle,SB_VERT,TopRow,true);
                  end;
  SB_PAGEUP    : begin
                   if TopRow>FixedRows then
                    begin
                     if TopRow>VisibleRowCount then
                      TopRow := TopRow-VisibleRowCount
                     else
                      TopRow := FixedRows;
                     SetScrollPos(Handle,SB_VERT,TopRow,true);
                    end;
                  end;
 End;

end;

function TExifGrid.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
 if TopRow > FixedRows then
   TopRow := TopRow - 1;
 SetScrollPos(Handle, SB_VERT, TopRow, true);
end;

function TExifGrid.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
 if TopRow + VisibleRowCount < RowCount then
  TopRow := TopRow + 1;
 SetScrollPos(Handle, SB_VERT, TopRow, true);
end;

procedure TExifGrid.KeyDown(var Key: Word; Shift: TShiftState);
var
 i: integer;
 TopVisible: integer;
 iTopRow: integer;
begin
// �������� ����������� ������� �� �������
 if FFileNameVisible then
  iTopRow := 1
 else
  iTopRow := 0;
 if Key <> VK_F2 then
  begin
   if Key = VK_UP then
    if Row > 1 then
     if (TypeOf(1, Row - 1) in ([Value, UnknownValue])) then
      begin
       inherited;
       Exit;
      end
     else
      if Row > 2 then
       begin
        i := Row - 1;
        while i > 2 do
         if (TypeOf(1, i) in ([Value, UnknownValue])) then
          begin
           Row := i;
           Break;
          end
         else
          i := i - 1;
       end
      else
       if FFileNameVisible then
        TopRow := iTopRow
       else
        TopRow := iTopRow;
   if Key = VK_DOWN then
    if Row < RowCount - 1 then
     if (TypeOf(1, Row + 1) in ([Value, UnknownValue])) then
      begin
       inherited;
       Exit;
      end
     else
      if Row < RowCount - 2 then
       begin
        i := Row + 1;
        while i < RowCount - 2 do
         if (TypeOf(1, i) in ([Value, UnknownValue])) then
          begin
           Row := i;
           Break;
          end
         else
          i := i + 1;
       end;
  end;
{
// �������� ����������� ��������� ������ � ������� ����������
 if FFileNameVisible then
  TopVisible := 1
 else
  TopVisible := 0;
 if Key = VK_UP then
  if TopRow > TopVisible then
   TopRow := TopRow - 1;
 if Key = VK_DOWN then
  if TopRow + VisibleRowCount < RowCount then
   TopRow := TopRow + 1;
}
 SetScrollPos(Handle,SB_VERT,TopRow,true);
end;

procedure TExifGrid.MouseMove(Shift: TShiftState; X, Y: Integer);
var
 MiddleY: integer;
   Y1,Y2: integer;
begin
// inherited;
// Form1.Label2.Caption := 'ColCount='+IntToStr(ColCount)+' RowCount='+IntToStr(RowCount);
{
 if ComponentState <> [csDesigning] then
  if FAutoCatchFocus then
   if not Focused then SetFocus;
}
 if FColMove then
  if (X>30) and (X<Width-30) then
   begin
    ColWidths[0] := X;
    SetColumnsSize;
   end;

// Form1.Label2.Caption := 'ExifGridMouseMove ['+IntToStr(MouseCoord(X,Y).X)+','+IntToStr(MouseCoord(X,Y).Y)+']';
// Form1.Label2.Caption := Form1. Label2.Caption+' Y('+IntToStr(CellRect(MouseCoord(X,Y).X,MouseCoord(X,Y).Y).TopLeft.Y)+','+IntToStr(CellRect(MouseCoord(X,Y).X,MouseCoord(X,Y).Y).BottomRight.Y)+')';
// Form1.Label2.Caption := Form1.Label2.Caption+'   X='+IntToStr(X)+' Y='+IntToStr(Y);

// ��������� ���������� ������� ���� ��� ������
 if FPlus.FPlusVisible then
  begin
   Y1 := CellRect(MouseCoord(X,Y).X,MouseCoord(X,Y).Y).TopLeft.Y;
   Y2 := CellRect(MouseCoord(X,Y).X,MouseCoord(X,Y).Y).BottomRight.Y;
   MiddleY := (Y1+(Y2-Y1) div 2);
   Y1 := MiddleY-(FPlus.FPlusSize div 2);
   Y2 := MiddleY+(FPlus.FPlusSize div 2)+1;
  end;
  if (X>=ColWidths[0]-1) and (X<=ColWidths[0]+1) then
   Cursor := crHSplit
  else
   begin
    if (X>FIndentions.FIndentionPlus) and (X<FIndentions.FIndentionPlus+FPlus.FPlusSize) and
       (Y>Y1) and (Y<Y2) then
     if TypeOf(MouseCoord(X,Y).X,MouseCoord(X,Y).Y) in ([TitleExpanded,TitleCollapsed]) then
      Cursor := crHandPoint
     else Cursor := crDefault
    else Cursor := crDefault;
   end;
 if Assigned(FMouseMove) then FMouseMove(Self, Shift, X, Y);
end;

procedure TExifGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 inherited;
 if EditorMode = true then
  EditorMode := false;
 if (X >= ColWidths[0] - 1) and (X <= ColWidths[0] + 1) then FColMove := true;
end;

procedure TExifGrid.Click(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
 MiddleY: integer;
 Y1,Y2: integer;
begin
 if FPlus.FPlusVisible then
  begin
   Y1 := CellRect(MouseCoord(X,Y).X,MouseCoord(X,Y).Y).TopLeft.Y;
   Y2 := CellRect(MouseCoord(X,Y).X,MouseCoord(X,Y).Y).BottomRight.Y;
   MiddleY := (Y1+(Y2-Y1) div 2);
   Y1 := MiddleY-(FPlus.FPlusSize div 2);
   Y2 := MiddleY+(FPlus.FPlusSize div 2)+1;
   if TypeOf(MouseCoord(X,Y).X,MouseCoord(X,Y).Y) in ([TitleExpanded,TitleCollapsed]) then
    if (X>FIndentions.FIndentionPlus) and (X<FIndentions.FIndentionPlus+FPlus.FPlusSize) and
       (Y>Y1) and (Y<Y2) then
     ClickPlus(MouseCoord(X,Y).X,MouseCoord(X,Y).Y);
  end;
end;

procedure TExifGrid.ClickPlus(Col,Row: integer);
begin
// Write('ClickPlus '+IntToStr(Row));
 ExpandData(Row);
 FillGrid;
end;

procedure TExifGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Click(Button, Shift, X, Y);
 inherited;
 if FColMove then FColMove := false;
end;

function TExifGrid.TypeOf(Col,Row: longint): TItemType;
begin
 if Length(FData)=1 then
  begin
   result := NoExif;
   Exit;
  end;
 result := ERROR;
 if (Col<0) or (Col>1) then Exit;
 if (Row<0) or (Row>Length(FCells)-1) then Exit;
 case Col of
  0: result := FCells[Row].TypeElement;
  1: case FCells[Row].TypeElement of
        SubTitle,
        LastSubTitle: result := Value;
        TitleBlank,
        TitleExpanded,
        TitleCollapsed: result := RightOfTitle;
      else
       result := FCells[Row].TypeElement
      end
 end;
 if FCells[Row].Unknown then
  case result of
   SubTitle     : result := UnknownSubTitle;
   LastSubTitle : result := LastUnknownSubTitle;
   Value        : result := UnknownValue;
  end;
end;

function TExifGrid.GetData(Col,Row: integer):string;
begin
 result := '';
 if (Col<0) or (Col>1) then Exit;
 if (Row<0) or (Row>Length(FCells)-1) then Exit;
 case Col of
  0: result := FCells[Row].Name;
  1: result := FCells[Row].Value;
 end;

end;

procedure TExifGrid.DrawCell(ACol, ARow: Longint; Rect: TRect; AState: TGridDrawState);
var
  iRect: TRect;
  TopLineX,BottomLineX: integer;
  s:string;
  X: integer;
  MiddleY: word;
  i: integer;
  ItemType: TItemType;
begin
// if AState = [gdSelected{, gdFocused}] then asm int 3 end;
// �������������� ����������
 TopLineX := Rect.Left+FIndentions.FIndention;
 BottomLineX := Rect.Left+FIndentions.FIndention;
 iRect := Rect;
 ItemType := TypeOf(ACol,ARow);
 s := GetData(ACol,ARow);
// � ����������� �� ���� ������ �������������� ��������� �����������
 with Canvas do
  begin
   case TypeOf(ACol,ARow) of
    NoExif             : begin
                           Font.Style := [];
                          end;
    TitleBlank,
    TitleExpanded,
    TitleCollapsed      : begin
                           if FPlus.FPlusVisible then X := Rect.Left+FIndentions.FIndention+FIndentions.FTitleIndention
                            else X := Rect.Left+FIndentions.FIndentionPlus;
                           Brush.Color := FColors.FTitleBgColor;
                           Font.Style := [fsBold];
                           Font.Color := FColors.FTitleColor;
                           FillRect(Classes.Rect(Rect.Left,Rect.Top,X,Rect.Bottom));
                          end;
    TitleFileName      : begin
                           if ACol = 1 then
                            begin
                             Brush.Color := FColors.FFileNameBgColor;
                             Font.Style := [];
                             Font.Color := FColors.FFileNameColor;
                             iRect.Left := Rect.Left - ColWidths[0];
                             iRect.Bottom := Rect.Bottom - 1;
                             s := MinimizeName(FFileName, Canvas, iRect.Right - iRect.Left);
                             X := iRect.Right - TextWidth(s) - 2;
                            end;
                          end;
    RightOfTitle       : begin
                           if FPlus.FPlusVisible then
                            iRect.Left := Rect.Left-ColWidths[0]+FIndentions.FIndention+FIndentions.FTitleIndention
                           else
                            iRect.Left := Rect.Left-ColWidths[0]+FIndentions.FIndentionPlus;
                           iRect.Top := Rect.Top+1;
                           iRect.Bottom := Rect.Bottom-1;
                           X := iRect.Left;
                           Brush.Color := FColors.FTitleBgColor;
                           Font.Style := [fsBold];
                           Font.Color := FColors.FTitleColor;
                           s := GetData(0,ARow);
                          end;
    SubTitle           : begin
                           X := Rect.Left+FIndentions.FIndention+FIndentions.FSubTitleIndention;
                           Font.Color := FColors.FSubTitleColor;
                           Font.Style := [];
                           Brush.Color := FColors.FTextBgColor;
                          end;
    UnknownSubTitle    : begin
                           X := Rect.Left+FIndentions.FIndention+FIndentions.FSubTitleIndention;
                           Font.Color := FColors.FUnknownSubTitleColor;
                           Font.Style := [];
                           Brush.Color := FColors.FUnknownBgColor;
                          end;
    LastSubTitle       : begin
                           X := Rect.Left+FIndentions.FIndention+FIndentions.FSubTitleIndention;
                           Font.Color := FColors.FSubTitleColor;
                           Font.Style := [];
                           Brush.Color := FColors.FTextBgColor;
                          end;
    LastUnknownSubTitle: begin
                           X := Rect.Left+FIndentions.FIndention+FIndentions.FSubTitleIndention;
                           Font.Color := FColors.FUnknownSubTitleColor;
                           Font.Style := [];
                           Brush.Color := FColors.FUnknownBgColor;
                          end;
    Value              : begin
                           X := Rect.Left+FIndentions.FValueIndention;
                           Font.Color := FColors.FValueColor;
                           Font.Style := [fsBold];
                           Brush.Color := FColors.FTextBgColor;
                          end;
    UnknownValue       : begin
                           X := Rect.Left+FIndentions.FValueIndention;
                           Font.Color := FColors.FUnknownValueColor;
                           Font.Style := [fsBold];
                           Brush.Color := FColors.FUnknownBgColor;
                          end;
    LastTitle          : begin
                           Brush.Color := FColors.FTitleBgColor;
                           iRect.Right := Rect.Right+ColWidths[1];
                          end;
   end;
// ���� �� ��������� ������ ���������, ������ ������ ����� ������� �
   if TypeOf(ACol,ARow+1) in ([TitleBlank,TitleExpanded,TitleCollapsed,LastTitle]) then
    BottomLineX := Rect.Left+FIndentions.FIndention div 2;
// ���� �� ���������� ������ filename, ������ ������� ����� c������
   if TypeOf(ACol,ARow-1)=TitleFileName then TopLineX := Rect.Left;
// ���� �� ���������� ������ ���������, ������ ������� ����� ������� �
   if TypeOf(ACol,ARow-1) in ([TitleBlank,TitleExpanded,TitleCollapsed]) then TopLineX := Rect.Left+FIndentions.FIndention div 2;
// ���� ��� ��������� ������, �� ������ ������ ����� �������
   if ItemType=LastTitle then BottomLineX := Rect.Left;
// ���� ��� FileName, �� ������ ������ ����� �������
   if ItemType=TitleFileName then BottomLineX := iRect.Left;
// �������� ������� ������� �� �����
   for i := 1 to Length(s) do if s[i]<#32 then s[i] := '.';
// ��������� �������� ������ �� ���������
   MiddleY := (Rect.Top+(Rect.Bottom-Rect.Top) div 2);
// ��������� ������������ ������ ������� �� ������
   if ACol=0 then
    if ItemType in ([SubTitle, LastSubTitle]) then
     if TextWidth(s) > FMaxColWidth then FMaxColWidth := TextWidth(s);
// ������� �����
   if not (ItemType in ([NoExif, TitleBlank, TitleExpanded, TitleCollapsed])) then
    begin
     if (gdSelected in AState) and (ItemType in ([Value,UnknownValue])) then Brush.Color := FColors.FCursorColor;
//     s := '('+IntToStr(ACol)+','+IntToStr(ARow)+')  '+s; // ����� ������� � �������
     TextRect(iRect, X, (Rect.Top + (Rect.Bottom - Rect.Top) div 2) - TextHeight('A') div 2, s);
// ���� ������ ����������
     if (AState = [gdSelected, gdFocused]) and (ItemType in ([Value, UnknownValue])) then
      begin
       Brush.Color := FColors.FCursorFrameColor;
       iRect.Left := iRect.Left + 1;
       iRect.Right := iRect.Right;
       FrameRect(iRect);
      end;
    end;
// ������� ����� " NO EXIF "
   if ItemType=NoExif then
    begin
     Brush.Color := FColors.FTextBgColor;
     FillRect(Classes.Rect(0,0,Width,Height));
     iRect.Left := Rect.Left+10;
     iRect.Right := Rect.Right-10;
     iRect.Top := Rect.Top+10;
     iRect.Bottom := Rect.Bottom-10;
     Brush.Color := FColors.FTitleBgColor;
     FillRect(iRect);
     Brush.Style := bsClear;
     if Length(FileName)>0 then
      begin
       Font.Color := FColors.FTitleColor;
       TextRect(iRect,Width div 2 - TextWidth(FFileName) div 2, Height div 2 - TextHeight(s) - 2, FFileName);
       TextRect(iRect,Width div 2 - TextWidth(s) div 2, Height div 2 + 2, s);
      end
     else
      begin
       Font.Color := FColors.FTitleColor;
       TextRect(iRect,Width div 2 - TextWidth('EXIF reader') div 2,Height div 2 - TextHeight('EXIF reader')-2,'EXIF reader');
       TextRect(iRect,Width div 2 - TextWidth('version 1.0') div 2,Height div 2 + 2,'version 1.0');
       Font.Color := clGray;
       TextRect(iRect,iRect.Right - TextWidth('lithe@mail.ru')-2,iRect.Bottom - TextHeight('lithe@mail.ru')-1,'lithe@mail.ru');
      end;
     Brush.Color := FColors.FPlusColor;
     FrameRect(iRect);
    end;
// ������ ������
   Pen.Color := FColors.FTitleBgColor;
   if Acol=0 then
    begin
     if (ItemType<>NoExif) then
      begin
// ������ ������� � ������ ������
       Brush.Color := FColors.FTitleBgColor;
       FillRect(Classes.Rect(Rect.Left, Rect.Top, Rect.Left + FIndentions.FIndention, Rect.Bottom));
// ������ ����� � ������ ������
       if BorderStyle <> bsNone then
        begin
         Pen.Color := FColors.FTitleBgColor3DLight;
         Polyline([
                   Point(Rect.Left,Rect.Top),
                   Point(Rect.Left,Rect.Bottom)
                   ]);
        end;
       if not (ItemType in ([TitleBlank,TitleExpanded,TitleCollapsed,LastTitle])) then
        begin
// ������ ����� � ����� �������
         Pen.Color := FColors.FTitleBgColor3DShadow;
         Polyline([
                   Point(Rect.Left+FIndentions.FIndention,Rect.Top),
                   Point(Rect.Left+FIndentions.FIndention,Rect.Bottom)
                   ]);
        end;
      end;
     Pen.Color := FColors.FPlusColor;
     if (ItemType in ([TitleBlank,TitleExpanded,TitleCollapsed])) and (FPlus.FPlusVisible) then
      begin
// ������ ����
       Brush.Color := FColors.FTextBgColor;
       Rectangle(Classes.Rect(Rect.Left+FIndentions.FIndentionPlus,MiddleY-(FPlus.FPlusSize div 2),Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize,MiddleY+(FPlus.FPlusSize div 2)+1));
       if not (ItemType=TitleBlank) then
        begin
         if ItemType=TitleCollapsed then
          Polyline([
                    Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize div 2,MiddleY-(FPlus.FPlusSize div 2)+2),
                    Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize div 2,MiddleY+(FPlus.FPlusSize div 2)-2+1)
                    ]);

         Polyline([
                   Point(Rect.Left+FIndentions.FIndentionPlus+2,MiddleY),
                   Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize-2,MiddleY)
                   ]);
        end
       else
        begin
         Brush.Color := FColors.FTitleBgColor;
         FillRect(Classes.Rect(Rect.Left+FIndentions.FIndentionPlus+1,MiddleY-(FPlus.FPlusSize div 2)+1,Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize-1,MiddleY+(FPlus.FPlusSize div 2)));
        end;
      end;
// ������ ����� � ������������
     Pen.Color := FColors.FLineColor;
     if ItemType in ([SubTitle,UnknownSubTitle]) then
      Polyline([
                 Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize div 2,Rect.Bottom),
                 Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize div 2,Rect.Top),
                 Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize div 2,MiddleY),
                 Point(Rect.Left+FIndentions.FIndention+FIndentions.FSubTitleIndention-3,MiddleY)
                ]);
// ������ ����� ��� �����������
     if ItemType in ([LastSubTitle,LastUnknownSubTitle]) then
       Polyline([
                Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize div 2,Rect.Top),
                Point(Rect.Left+FIndentions.FIndentionPlus+FPlus.FPlusSize div 2,MiddleY),
                Point(Rect.Left+FIndentions.FIndention+FIndentions.FSubTitleIndention-3,MiddleY)
                ]);
    end;
// ������ ������������ �������������� �����
    if ItemType in ([Value,UnknownValue]) then
     begin
      Pen.Color := FColors.FLineColor;
      Polyline([
                Point(Rect.Left,Rect.Top),
                Point(Rect.Left,Rect.Bottom)
                ]);
     end;
// ������ 3D �����
    if (ItemType in ([TitleBlank,TitleExpanded,TitleCollapsed,LastTitle,TitleFileName])) or (ItemType=RightOfTitle) then
     begin
      if ACol=1 then
       begin
        TopLineX := Rect.Left;
        BottomLineX := Rect.Left;
       end;
      if ARow=0 then TopLineX := Rect.Right;
// ������� �����
      Pen.Color := FColors.FTitleBgColor3DLight;
      Polyline([
                Point(TopLineX,Rect.Top),
                Point(Rect.Right,Rect.Top)
                ]);
// ������ �����
      Pen.Color := FColors.FTitleBgColor3DShadow;
      Polyline([
                Point(BottomLineX,Rect.Bottom-1),
                Point(Rect.Right,Rect.Bottom-1)
                ]);
     end;
  end;
end;

constructor TExif.Create;
begin
 FLogTickCount := GetTickCount;
 FExifData := TParamListCollections.Create;
 FLog := TstringList.Create;
 MakerNoteinformation := 'Maker Note information';
 FUnknownsVisible := false;
 Init;
end;

procedure TExif.SetFileName(const iFileName: Ansistring);
begin
 if FileExists(iFileName) then
  begin
   FFileName := iFileName;
   FFileExtention := ExtractFileExt(iFileName);
   ReadFromFile;
  end
 else
  Write('File: "' + iFileName + '" not found');
end;

procedure TExif.Init;
begin
 IFDpointer := 0;
 GPSpointer := 0;
 InteroperabilityIFDpointer := 0;
 FJFIFmarker := false;
end;

function TExif.InvertHex4(const Value:Cardinal): Cardinal; // InvertHex4($12345678)=$78563412
Var
 Res: Cardinal;
 rr: array[1..4] of Byte absolute Res;
 vv: array[1..4] of Byte absolute Value;
begin
 rr[1] := vv[4];
 rr[2] := vv[3];
 rr[3] := vv[2];
 rr[4] := vv[1];
 Result := Res;
end;

function TExif.InvertHex2(const Value:Word): Word; // InvertHex2($1234)=$3412
Var
 Res: Word;
 rr: array[1..2] of Byte absolute Res;
 vv: array[1..2] of Byte absolute Value;
begin
 rr[1] := vv[2];
 rr[2] := vv[1];
 Result :=  Res;
end;

function TExif.DoubleToStr(Value: Double; Count: byte; TrimRightZero: boolean = true): string;
// ������� ��������� �� Count ������� ����� ������� � ����������� � ������
var
 s: string;
 j: byte;
 i: Cardinal;
 D: Double;
 b: boolean;
 v: Double;

 procedure _TrimRightZero(var _s: string);
 // ������� ��� ���� � ������ ������� ����� �������.
 // ���� ����� ����� ������� ���� - �� �������� 
 begin
  if (Count > 0) and (Length(_s) > Count) then
   while (_s[Length(_s)] = '0') and (_s[Length(_s) - 1] <> ',') do
    delete(_s, Length(_s), 1);
 end;

begin
 result := '';
 if Count > 0 then
  v := RoundTo(Value, -Count);
 b := false;
 s := '';
 if v < 0 then
  begin
   s := '-';
   d := Abs(v);
  end
 else
  d := v;
 i := Trunc(d);
 s := s + IntToStr(i);
 if d - i > 0 then
  begin
   s := s + ',';
   b := true;
   d := (d - i) * 10;
   i := Trunc(d);
   s := s + IntToStr(i);
   if Count > 0 then Count := Count - 1;
  end;
 if Count > 0 then
  for j := 1 to Count do
   begin
    if (b = false) and (j = 1) and (Copy(s, length(s), 1) <> ',') then
     begin
      s := s + ',';
      b := true;
     end;
    d := (d - i) * 10;
    if (d <> 0) or (j = 1) then
     begin
      i := Trunc(d);
      s := s + IntToStr(i);
     end
   end;
 if TrimRightZero then
  _TrimRightZero(s); 
 result := s;
end;

function RationalToDouble(str: string): Double;
begin
 if str = '' then
  result := 0
 else
  try
   if StrToInt(Copy(Str, Pos('/', Str) + 1, Length(Str) - Pos('/', Str))) <> 0 then
    result := StrToInt(Copy(Str, 1, Pos('/', Str) - 1)) / StrToInt(Copy(Str, Pos('/', Str) + 1, Length(Str) - Pos('/', Str)))
   else
    result := 0;
  except
   result := -1;
  end
end;

// ��������� ������ ����� �� ������ ����: "12,456,3,235,2,0,0,0..."
// � ���������� ����: cardinal � ������� ��� �� ������
function TExif.ExtractDigit(var Str: string): Cardinal;
begin
 Result := 4294967295;
 if (Pos(',',Str)=0) and (length(Str)>0) then
  begin
   try
    Result := StrToInt(Str);
   except
   end;
   Delete(Str,1,length(Str));
  end
 else
  begin
   try
    Result := StrToInt(Copy(Str,1,Pos(',',Str)-1));
   except
   end;
   Delete(Str,1,Pos(',',Str));
  end;
end;

function TExif.ReadValue(TagType: Word; Count, OOffset, Off0: Cardinal; const TIFFAlign: Word): Variant;
var   fp: LongInt;
 iOffSet: Cardinal;
//   iOff0: Cardinal;
     i,j: Cardinal;
       b: byte;
       w: Word;
      sw: Smallint;
       c: Cardinal;
      sc: Integer;
     Str: string;
      bb: array[1..4] of byte absolute iOffSet;
      ww: array[1..2] of word absolute iOffSet;
begin
  fp := FilePos(F); // Save file offset
  iOffSet := OOffSet;
  Result := Null;
  case TagType of
//------------------------------------------------------------------------------
    //  BYTE 8-bit unsigned integer.
    1: if 1*Count <= 4 then
        begin
         // �������� ������ ����� 4 ���� � ��������� � OffSet
         Result := '';
         for i := 1 to Count do
          begin
           Result := Result+IntToStr(bb[i]);
           if i <> Count then
            result := result + ',';
          end;
        end
       else
        begin
         // �������� ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign = $4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         try
          i := 1;
          repeat
           BlockRead(F, b, 1);
           Str := Str + IntToStr(b);
           if i < Count then
            Str := Str + ',';
           inc(i);
          until i > Count;
          result := Str;
         except
          result := Null;
         end;
        end;
//------------------------------------------------------------------------------
    // ASCII 8-bit byte that contains a 7-bit ASCII code; the last byte must be NUL (binary zero)
    2: if Count <= 4 then
        begin
         // �������� ������ ����� 4 � ��������� � OffSet
         if TIFFAlign = $4D4D then
          iOffSet := InvertHex4(OOffSet)
          else iOffSet := OOffSet;
         Str := '';
         for i := 1 to Count do
          Str := Str + Chr(bb[i]);
         Result := string(PChar(Str));
        end
       else
        begin
       // �������� ������ 4 � ��������� �� �������� OffSet
       {  if TIFFAlign=$4D4D then asm int 3 end; }
         SetLength(Str,Count);
         if TIFFAlign = $4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
//         if TIFFAlign=$4D4D then iOff0 := InvertHex4(Off0) else iOff0 := Off0;
//         ShowMessage('2 '+IntToHex(iOffset+Off0,4));
         Seek(F, iOffset + Off0);
         try
          i := 1;
          repeat
           BlockRead(F, Str[i], 1);
           inc(i);
          until (i >= Count);
          if i <= Count then
           Str := Copy(Str, 1, i - 1);
          while Str[Length(Str)] = #0 do
           begin
            Delete(Str,Length(Str), 1);
            if Length(Str) = 0 then
             break;
           end;
          Result := Str;
         except
          Result := Null;
         end;
        end;
//------------------------------------------------------------------------------
    // SHORT 16-bit (2-byte) unsigned integer.
    3: If 2 * Count <= 4 then
        begin
         // �������� ������ ����� 4 ���� � ��������� � OffSet
         Result := '';
         for i := Count downto 1 do
          begin
           if TIFFAlign=$4D4D then
            Result := Result+IntToStr(InvertHex2(ww[i]))
           else
            Result := Result+IntToStr(ww[i]);
           if i<>Count then Result := Result+',';
          end;
        end
       else
        begin
         // �������� ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign = $4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         try
          Str := '';
          i := 1;
          repeat
           BlockRead(F, w, 2);
           if TIFFAlign = $4D4D then
            w := InvertHex2(w);
           Str := Str + IntToStr(w);
           if i<Count then
            Str := Str+',';
           inc(i);
          until i > Count;
          Result := Str;
         except
          Result := Null;
         end;
        end;
//------------------------------------------------------------------------------
    // LONG 32-bit (4-byte) unsigned integer.
    4: If 4*Count<=4 then
        begin
         // �������� ������ ����� 4 ���� � ��������� � OffSet
         Result := OOffSet;
         if TIFFAlign=$4D4D then Result := InvertHex4(Result);
        end
       else
        begin
         // �������� ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign=$4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         try
          Str := '';
          i := 1;
          repeat
           BlockRead(F, c, 4);
           Str := Str+IntToStr(c);
           if i<Count then Str := Str+',';
           inc(i);
          until i>Count;
          Result := Str;
         except
          Result := Null;
         end;
        end;
//------------------------------------------------------------------------------
    // RATIONAL Two LONGs: the first represents the numerator of a fraction; the second, the denominator.
    5: begin
         // �������� ������ ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign=$4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         j := 0;
         Str := '';
         while j < Count do
          try
           BlockRead(F, c, 4);
           if TIFFAlign=$4D4D then c := InvertHex4(c);
           Str := Str+IntToStr(c)+'/';
           BlockRead(F, c, 4);
           if TIFFAlign=$4D4D then c := InvertHex4(c);
           Str := Str+IntToStr(c);
           j := j + 1;  if j < Count then Str := Str + ',';
          except
           Result := Null;
          end;
          Result := Str;
         end;
//------------------------------------------------------------------------------
// In TIFF 6.0, some new field types have been defined:
//------------------------------------------------------------------------------
    // SBYTE An 8-bit signed (twos-complement) integer.
    6: begin
        if 1*Count<=4 then
         Result := IntToStr(ShortInt(OOffSet))
        else
         Result := '!!!!!!!!!!!!!!!!!!!! TagType=6';
       end;
//------------------------------------------------------------------------------
    // UNDEFINED An 8-bit byte that may contain anything, depending on the definition of the field.
    7: if 1*Count<=4 then
        begin
         // �������� ������ ����� 4 ���� � ��������� � OffSet
         Result := '';
         for i := 1 to Count do
          begin
           Result := Result+IntToStr(bb[i]);
           if i<>Count then Result := Result+',';
          end;
        end
       else
        begin
         // �������� ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign=$4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         try
          i := 1;
          repeat
           BlockRead(F, b, 1);
           Str := Str+IntToStr(b);
           if i<Count then Str := Str+',';
           inc(i);
          until i>Count;
          Result := Str;
         except
          Result := Null;
         end;
        end;
//------------------------------------------------------------------------------
    // SSHORT A 16-bit (2-byte) signed (twos-complement) integer.
    8: If 2 * Count <= 4 then
        begin
         // �������� ������ ����� 4 ���� � ��������� � OffSet
         if TIFFAlign = $4D4D then iOffSet := InvertHex2(OOffSet) else iOffSet := OOffSet;
         Result := Smallint(OOffSet);
        end
       else
        begin
         // �������� ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign = $4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         try
          Str := '';
          i := 1;
          repeat
           BlockRead(F, sw, 2);
           if TIFFAlign = $4D4D then sw := InvertHex2(sw);
           Str := Str + IntToStr(sw);
           if i < Count then Str := Str+',';
           inc(i);
          until i > Count;
          Result := Str;
         except
          Result := Null;
         end;
        end;
//------------------------------------------------------------------------------
//  SLONG A 32-bit (4-byte) signed (twos-complement) integer.
    9: If 4 * Count <= 4 then
        begin
         // �������� ������ ����� 4 ���� � ��������� � OffSet
         if TIFFAlign=$4D4D then iOffSet := InvertHex2(OOffSet) else iOffSet := OOffSet;
         Result := Longint(iOffSet);
        end
       else
        begin
         // �������� ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign=$4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         try
          Str := '';
          i := 1;
          repeat
           BlockRead(F, sc, 4);
           Str := Str + IntToStr(sc);
           if i < Count then Str := Str + ',';
           inc(i);
          until i > Count;
          Result := Str;
         except
          Result := Null;
         end;
        end;
//------------------------------------------------------------------------------
//  SRATIONAL Two SLONG�s: the first represents the numerator of a fraction, the second the denominator.
   10: begin
         // �������� ������ ������ 4 ���� � ��������� �� �������� OffSet
         if TIFFAlign = $4D4D then iOffSet := InvertHex4(OOffSet) else iOffSet := OOffSet;
         Seek(F, iOffset + Off0);
         try
          Str := '';
          BlockRead(F, sc, 4);
          if TIFFAlign = $4D4D then c := InvertHex4(c);
          Str := Str + IntToStr(sc)+'/';
          BlockRead(F, sc, 4);
          if TIFFAlign = $4D4D then c := InvertHex4(c);
          Str := Str + IntToStr(sc);
          Result := Str;
         except
          Result := Null;
         end;
        end;
//------------------------------------------------------------------------------
   // FLOAT Single precision (4-byte) IEEE format.
   11: Result := '!!!!!!!!!!!FLOAT!!!!!!!!!!!!';
//------------------------------------------------------------------------------
   // DOUBLE Double precision (8-byte) IEEE format.
   12: Result := '!!!!!!!!!!!DOUBLE!!!!!!!!!!!!';
  else
   begin
//    Result := '';        ?????????????????? ���� ��? �� �����!
    Exit;
   end
  end;
{
  SetLength(Result,Count);
  Seek(f, Offset);
  try
    i := 1;
    repeat
      BlockRead(f,Result[i],1);
      inc(i);
    until (i>=Count) or (Result[i-1]=#0);
    if i<=Count then Result := Copy(Result,1,i-1);
  except
    Result := '';
  end;
}
  Seek(F, fp);     //Restore file offset
end;

procedure TExif.GetTagValue(const Tag: TTag; Align: Word; Off0: Cardinal; var ParamName: string; var Param, logParam: variant);
var
 s, ss: string;
 i, j: cardinal;
 n,m: cardinal;
 d: double;
 w: word;
 b: array[0..1] of byte absolute w;

 procedure CaseElse(const tag: TTag; var Param: Variant);
 var
  s: string;
 begin
  try
   s := '(Tag $' + IntToHex(tag.TagID, 4) + ') ' + '(!) ' + IntToStr(Param);
   Param := s;
  except
   Param := 'ERROR �8a9r3';
  end;
 end;

begin
 ParamName := '';
 Param := Null;
 logParam := Null;

    Param := ReadValue(tag.TagType, tag.Count, tag.OffSet, off0, Align);
    logParam := Param;

      case tag.TagID of

// .................................................. GPS information ..........
       $0000: begin
                 ParamName := 'Version';
                 s := Param;
                 ss := '';
                 for i := 1 to 2 do
                  begin
                   ss := ss + IntToStr(ExtractDigit(s));
                   if i < 2 then ss := ss + '.';
                  end;
                 Param := ss;
               end;
       $0001: begin ParamName := 'GPSLatitudeRef'; end;
       $0002: begin ParamName := 'GPSLatitude'; end;
       $0003: begin ParamName := 'GPSLongitudeRef'; end;
       $0004: begin ParamName := 'GPSLongitude'; end;
       $0005: begin ParamName := 'GPSAltitudeRef'; end;
       $0006: begin ParamName := 'GPSAltitude'; end;
       $0007: begin ParamName := 'GPSTimeStamp'; end;
       $0008: begin ParamName := 'GPSSatellites'; end;
       $0009: begin ParamName := 'GPSStatus'; end;
       $000A: begin ParamName := 'GPSMeasureMode'; end;
       $000B: begin ParamName := 'GPSDOP'; end;
       $000C: begin ParamName := 'GPSSpeedRef'; end;
       $000D: begin ParamName := 'GPSSpeed'; end;
       $000E: begin ParamName := 'GPSTrackRef'; end;
       $000F: begin ParamName := 'GPSTrack'; end;
       $0010: begin ParamName := 'GPSImgDirectionRef'; end;
       $0011: begin ParamName := 'GPSImgDirection'; end;
       $0012: begin ParamName := 'GPSMapDatum'; end;
       $0013: begin ParamName := 'GPSDestLatitudeRef'; end;
       $0014: begin ParamName := 'GPSDestLatitude'; end;
       $0015: begin ParamName := 'GPSDestLongitudeRef'; end;
       $0016: begin ParamName := 'GPSDestLongitude'; end;
       $0017: begin ParamName := 'GPSDestBearingRef'; end;
       $0018: begin ParamName := 'GPSDestBearing'; end;
       $0019: begin ParamName := 'GPSDestDistanceRef'; end;
       $001A: begin ParamName := 'GPSDestDistance'; end;
       $001B: begin ParamName := 'GPSProcessingMethod'; end;
       $001C: begin ParamName := 'GPSAreaInformation'; end;
       $001D: begin ParamName := 'GPSDateStamp'; end;
       $001E: begin ParamName := 'GPSDifferential'; end;

// .............................................................................
       $0100: begin ParamName := 'ImageWidth';  end;
       $0101: begin ParamName := 'ImageLength'; end;
       $0102: begin ParamName := 'BitsPerSample'; end;
       $0103: begin
                 ParamName := 'Compression';
                 case Integer(Param) of
                   1: Param := 'Uncompressed';
                   2: Param := 'CCITT 1D';
                   3: Param := 'CCITT Group 3';
                   4: Param := 'CCITT Group 4';
                   5: Param := 'LZW';
                   6: Param := '(OLD)JPEG';
                   7: Param := 'JPEG';
                   8: Param := 'ZIP';
                   32773: Param := 'PackBits';
                   32946: Param := 'ACDSEE Deflate';
                 else CaseElse(Tag,Param);
                 end;
               end;
       $0106: begin
                 ParamName := 'PhotometricInterpretation';
                 case Integer(Param) of
                   0: Param := 'WhiteIsZero';
                   1: Param := 'BlackIsZero';
                   2: Param := 'RGB';
                   3: Param := 'RGB Palette';
                   4: Param := 'Transparency mask';
                   5: Param := 'CMYK';
                   6: Param := 'YCbCr';
                   8: Param := 'CIELab';
                 else CaseElse(Tag,Param);
                 end;
               end;
       $0112: begin
                 ParamName := 'Orientation';
                 case Integer(Param) of
                   1: Param := 'Normal';
                   2: Param := 'The 0th row is at the visual top of the image, and the 0th column is the visual right-hand side';
                   3: Param := 'The 0th row is at the visual bottom of the image, and the 0th column is the visual right-hand side';
                   4: Param := 'The 0th row is at the visual bottom of the image, and the 0th column is the visual left-hand side';
                   5: Param := 'The 0th row is the visual left-hand side of the image, and the 0th column is the visual top';
                   6: Param := 'Rotated ContraClockWise';
                   7: Param := 'The 0th row is the visual right-hand side of the image, and the 0th column is the visual bottom';
                   8: Param := 'Rotated clockWise';
                 else CaseElse(Tag,Param);
                 end;
               end;
       $0115: begin ParamName := 'SamplesPerPixel'; end;
       $011C: begin
                 ParamName := 'PlanarConfiguration';
                 case Integer(Param) of
                   1: Param := 'chunky format';
                   2: Param := 'planar format';
                 else CaseElse(Tag,Param);
                 end;
               end;
       $0212: begin
                 ParamName := 'YCbCrSubSampling';
                 if string(Param)='2,1' then Param := 'YCbCr 4:2:2'
                 else
                  if string(Param)='2,2' then Param := 'YCbCr4:2:0'
                  else CaseElse(Tag,Param);
               end;
       $0213: begin
                 ParamName := 'YCbCrPositioning';
                 case Integer(Param) of
                   1: Param := 'centered';
                   2: Param := 'co-sited';
                 else CaseElse(Tag,Param);
                 end;
               end;
       $011A: begin
                 ParamName := 'XResolution';
                 if StrToInt(Copy(string(Param),Pos('/',string(Param))+1,Length(string(Param))-Pos('/',string(Param))))<>1 then
                  begin
                   s := string(Param);
                   Param := string(IntToStr(Trunc(StrToInt(Copy(string(Param),1,Pos('/',string(Param))-1))/StrToInt(Copy(string(Param),Pos('/',string(Param))+1,Length(string(Param))-Pos('/',string(Param))))))+'/1 ('+s+')');
                  end;
               end;
       $011B: begin
                 ParamName := 'YResolution';
                 if StrToInt(Copy(string(Param),Pos('/',string(Param))+1,Length(string(Param))-Pos('/',string(Param))))<>1 then
                  begin
                   s := string(Param);
                   Param := string(IntToStr(Trunc(StrToInt(Copy(string(Param),1,Pos('/',string(Param))-1))/StrToInt(Copy(string(Param),Pos('/',string(Param))+1,Length(string(Param))-Pos('/',string(Param))))))+'/1 ('+s+')');
                  end;
               end;
       $0128: begin
                 ParamName := 'ResolutionUnit';
                 case Integer(Param) of
                   2: Param := 'inch';
                   3: Param := 'centimetre';
                 else CaseElse(Tag,Param);
                 end;
               end;
// Recording offset ----------------------------------------
       $0111: begin ParamName := 'StripOffsets'; end;
       $0116: begin ParamName := 'RowsPerStrip'; end;
       $0117: begin ParamName := 'StripByteConunts'; end;
       $0201: begin // Offset of Thumbnail
                ParamName := 'JPEGInterchangeFormat';
                FThumbnail.Offset := Cardinal(Param);
               end;
       $0202: begin // Length of Thumbnail
                ParamName := 'JPEGInterchangeFormatLength';
                FThumbnail.Length := Cardinal(Param);
               end;
// Image data characteristics ------------------------------
       $012D: begin ParamName := '(!)TransferFunction'; end;
       $013E: begin ParamName := '(!)WhitePoint'; end;
       $013F: begin ParamName := '(!)PrimaryChromaticities'; end;
       $0211: begin ParamName := '(!)YCbCrCoefficients'; end;
       $0214: begin
                ParamName := '(!)ReferenceBlackWhite';
                 if string(Param)='0,255,0,255,0,255' then Param := 'PhotometricInterpretation is RGB'
                 else
                  if string(Param)='0/1,255/1,0/1,255/1,0/1,255/1' then Param := 'PhotometricInterpretation is RGB(!)'
                  else
                   if string(Param)='0,255,0,128,0,128' then Param := 'PhotometricInterpretation is YCbCr'
                   else CaseElse(Tag,Param);
               end;

// Other ---------------------------------------------------
       $0132: begin ParamName := 'DateTime';  end;
       $010E: begin ParamName := 'ImageDescription'; end;
       $010F: begin
                ParamName := 'Make';
                FOwner := UpperCase(Param);
                s := 'CANON';          if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'OLYMPUS';        if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'NIKON';          if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'CASIO';          if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'FUJIFILM';       if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'SANYO';          if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'MINOLTA';        if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'SONY';           if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'KODAK';          if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'RICOH';          if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'KONICA MINOLTA'; if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
                s := 'KYOCERA';        if Pos(s, UpperCase(string(Param))) <> 0 then Fowner := s;
               end;
       $0110: begin ParamName := 'Model'; end;
       $0131: begin ParamName := 'Software'; end;
       $013B: begin ParamName := 'Artist'; end;
       $8298: begin ParamName := 'Copyright'; end;
       $00FE: begin ParamName := 'NewSubfileType'; end;
       $00FF: begin ParamName := 'SubfileType'; end;
       $013D: begin
                 ParamName := 'Predictor';
                 case Integer(Param) of
                   1: Param := 'no prediction scheme used before coding';
                   2: Param := 'horizontal differencing';
                 else CaseElse(Tag,Param);
                 end;
               end;
       $83BB: begin ParamName := 'IPTC/NAA'; end;
// ������ �������������� �������� --------------------------
       $8769: begin
                 ParamName := #0#0#0'IFDpointer';
                 IFDpointer := Param;
               end;
       $8825: begin
                 ParamName := #0#0#0'GPSpointer';
                 GPSpointer := Param;
               end;
       $A005: begin
                 ParamName := #0#0#0'InteroperabilityIFDpointer';
                 InteroperabilityIFDpointer := Param; 
               end;
       $02BC: begin
                ParamName := 'XMLMetadata';
                XMLMetadata.Owner := FOwner;
                XMLMetadata.Length := tag.Count;
                if Align=$4D4D then XMLMetadata.OffSet := InvertHex4(tag.OffSet)
                else XMLMetadata.OffSet := tag.OffSet;
                Param := 'Length: '+IntToStr(XMLMetadata.Length)+' byte  OffSet=$'+IntToHex(XMLMetadata.OffSet,4)+' ('+IntToStr(XMLMetadata.OffSet)+')';
               end;
       $8649: begin
                ParamName := 'AdobeResource';
                AdobeResource.Owner := FOwner;
                AdobeResource.Length := tag.Count;
                if Align=$4D4D then AdobeResource.OffSet := InvertHex4(tag.OffSet)
                else AdobeResource.OffSet := tag.OffSet;
                Param := 'Length: '+IntToStr(AdobeResource.Length)+' byte  OffSet=$'+IntToHex(AdobeResource.OffSet,4)+' ('+IntToStr(AdobeResource.OffSet)+')';
               end;
       $C4A5: begin
                ParamName := 'PrintM IFD';
                PrintIM.Owner := FOwner;
                PrintIM.Length := tag.Count;
                if Align=$4D4D then
                 PrintIM.OffSet := InvertHex4(tag.OffSet)
                else
                 PrintIM.OffSet := tag.OffSet;
                Param := 'Length: '+IntToStr(PrintIM.Length)+' byte  OffSet=$'+IntToHex(PrintIM.OffSet,4)+' ('+IntToStr(PrintIM.OffSet)+')';
               end;
       $927C: begin
                ParamName := 'MakerNote';
                MakerNote.Owner := FOwner;
                MakerNote.Length := tag.Count;
                if Align = $4D4D then
                 MakerNote.OffSet := InvertHex4(tag.OffSet)
                else
                 MakerNote.OffSet := tag.OffSet;
                Param := '"' + MakerNote.Owner + '"  ' + IntToStr(MakerNote.Length) + ' byte  OffSet=$' + IntToHex(MakerNote.OffSet, 4) + ' (' + IntToStr(MakerNote.OffSet) + ')';
               end;
// ---------------------------------------------------------
       $9000: begin
                ParamName := 'ExifVersion';
                s := string(Param);
                ss := '';
                try
                 if (s[1] = '4') and (s[2] = '8') and (s[3] = ',') and (s[9] = ',') then
                  for i := 1 to 3 do
                   begin
                    ss := ss + IntToStr(StrToInt(Copy(s, 1, Pos(',', s) - 1)) - 48);
                    Delete(s, 1, 3);
                   end;
                 ss := ss + IntToStr(StrToInt(s) - 48);
                 Insert(',', ss, 3);
                 if ss[1] = '0' then Delete(ss, 1, 1);
                 if ss[Length(ss)] = '0' then Delete(ss, Length(ss), 1);
                 Param := ss;
                except
                end
               end;
       $A000: begin
                ParamName := 'FlashpixVersion';
                s := string(Param);
                if (s[1]='4') and (s[2]='8') and (s[3]=',') and (s[11]='8') and (s[10]='4') and (s[9]=',') then
                 begin
                  Delete(s,1,3); Delete(s,Length(s)-2,3);
                  s := IntToStr(StrToInt(Copy(s,1,Pos(',',s)-1))-48)+','+IntToStr(StrToInt(Copy(s,Pos(',',s)+1,Length(s)-Pos(',',s)))-48);
                  Param := s;
                 end;
               end;
       $A001: begin
                ParamName := 'ColorSpace';
                case Param of
                     1: Param := 'sRGB';
                 65535: Param := 'Uncalibrated';
               else CaseElse(Tag,Param);
                end;
               end;
       $9101: begin
                ParamName := 'ComponentsConfiguration';
                try
                  s := string(Param); ss := '';
                  for j := 1 to Tag.Count-1 do
                   begin
                    case StrToInt(Copy(s,1,Pos(',',s)-1)) of
                     0: ;
                     1: ss := ss + 'Y';
                     2: ss := ss + 'Cb';
                     3: ss := ss + 'Cr';
                     4: ss := ss + 'R';
                     5: ss := ss + 'G';
                     6: ss := ss + 'B';
                    else
                     ss := ss + ' (!) ';
                    end;
                    Delete(s, 1, Pos(',', s));
                    Param := ss;
                   end;
                except
                 CaseElse(Tag,Param);
                end;
               end;
       $9102: begin
                ParamName := 'CompressedBitsPerPixel';
                s := Copy(string(Param), Pos('/', string(Param)) + 1, Length(string(Param)) - Pos('/', string(Param)));
                if StrToInt(Copy(string(Param), Pos('/', string(Param)) + 1, Length(string(Param)) - Pos('/', string(Param)))) > 1 then
                 begin
                  s := string(Param);
                  Param := string(IntToStr(Trunc(StrToInt(Copy(string(Param), 1, Pos('/', string(Param)) - 1)) / StrToInt(Copy(string(Param), Pos('/', string(Param)) + 1, Length(string(Param)) - Pos('/', string(Param)))))) + '/1 (' + s + ')');
                 end;
                Param := string(Param)+' (bit/pixel)';
               end;
       $A002: begin ParamName := 'PixelXDimension'; end;
       $A003: begin ParamName := 'PixelYDimension'; end;
       $9286: begin
                ParamName := 'UserComment';
                s := ReadValue(2,tag.Count,tag.OffSet,off0, Align);
                ss := string(PChar(Copy(s,1,8)));
                Delete(s,1,8);
                if Length(ss)>0 then Param := s+' ('+ss+')' else Param := '';
               end;
       $A004: begin ParamName := 'RelatedSoundFile'; end;
       $9003: begin ParamName := 'DateTimeOriginal'; end;
       $9004: begin ParamName := 'DateTimeDigitized'; end;
       $9290,$9291,$9292: begin
                case tag.TagID of
                 $9290: begin ParamName := 'SubSecTime'; end;
                 $9291: begin ParamName := 'SubSecTimeOriginal'; end;
                 $9292: begin ParamName := 'SubSecTimeDigitized'; end;
                end;
                Param := ReadValue(1,tag.Count,tag.OffSet,off0, Align);
                s := string(Param); Param := '';
                while length(s)>0 do
                 begin
                  j := ExtractDigit(s);
                  if j>=48 then Param := Param+IntToStr(j-48);
                 end;
               end;
       $A420: begin ParamName := 'ImageUniqueID'; end;

       $829A: begin
               ParamName := 'ExposureTime';
               // �������� �������� ������� ����: X/Y
               // ��������� ���� X <> 1 (�������� ��������� ��������� � �����������
               // �.�. �������� ����� � ���� 1/M)
               if StrToInt(Copy(string(Param), 1, Pos('/',string(Param)) - 1)) <> 1 then
                begin
                 s := string(Param);
                 // ���� ����� ���������� ����������� ���������� �����
                 d := StrToInt(Copy(string(Param), Pos('/', string(Param)) + 1, Length(string(Param)) - Pos('/', string(Param)))) / StrToInt(copy(string(Param), 1, Pos('/', string(Param)) - 1));
                 if Frac(d) = 0 then
                  begin
                   j := Trunc(d);
                   if j = 0 then
                    Param := 'Error value: (' + string(Param) + ')'
                   else
                    if j = 1 then
                     Param := string('1 Sec')
                    else
                     Param := string('1/' + IntToStr(j) + ' Sec');
                  end
                 else
                  // ���� ����� ���������� ����������� ���������� �������
//                  Param := FloatToStr(RationalToDouble(string(Param))) + ' Sec';
                  Param := DoubleToStr(RationalToDouble(string(Param)), 5, true) + ' Sec';
                end
               else
                Param := string(Param) + ' Sec';
              end;
       $829D: begin
                ParamName := 'FNumber';
                Param := 'F ' + DoubleToStr(RationalToDouble(string(Param)), 1);
               end;
       $8822: begin
                ParamName := 'ExposureProgram';
                case Param of
                 0: Param := 'Not defined';
                 1: Param := 'Manual';
                 2: Param := 'Program';
                 3: Param := 'Aperture priority';
                 4: Param := 'Shutter priority';
                 5: Param := 'Creative program';
                 6: Param := 'Action program';
                 7: Param := 'Portrait mode';
                 8: Param := 'Landscape mode';
                else CaseElse(Tag,Param);
                end;
               end;
       $8824: begin ParamName := 'SpectralSensitivity';  end;
       $8827: begin ParamName := 'ISOSpeedRatings'; end;
       $8828: begin ParamName := 'OECF'; end;
       $9201: begin
{
               // �������� �������� ������� ����: X/Y
               // ��������� ���� X <> 1 (�������� ��������� ��������� � �����������
               // �.�. �������� ����� � ���� 1/M)
               if StrToInt(Copy(string(Param), 1, Pos('/',string(Param)) - 1)) <> 1 then
                begin
                 s := string(Param);
                 // ���� ����� ���������� ����������� ���������� �����
                 d := StrToInt(Copy(string(Param), Pos('/', string(Param)) + 1, Length(string(Param)) - Pos('/', string(Param)))) / StrToInt(copy(string(Param), 1, Pos('/', string(Param)) - 1));
                 if Frac(d) = 0 then
                  begin
                   j := Trunc(d);
                   if j = 0 then
                    Param := 'Error value: (' + string(Param) + ')'
                   else
                    if j = 1 then
                     Param := string('1 Sec')
                    else
                     Param := string('1/' + IntToStr(j) + ' Sec');
                  end
                 else
                  // ���� ����� ���������� ����������� ���������� �������
                  Param := DoubleToStr(RationalToDouble(string(Param)), 1) + ' Sec';
                end
               else
                Param := string(Param) + ' Sec';
}
                ParamName := 'ShutterSpeedValue';
                if Power(2, RationalToDouble(string(Param))) = 0 then
                 Param := 'Error value: (' + string(Param) + ')'
                else
                 if Power(2, RationalToDouble(string(Param))) = 1 then
                  Param := '1 Sec'
                 else
                  begin
                   d := Power(2, RationalToDouble(string(Param)));
                   if (Frac(d) = 0) or (d > 10) then
                    Param := string('1/' + FloatToStrF(d, ffFixed, 7, 2) + ' Sec')
                   else
                    // ���� ����� ���������� ����������� ���������� �������
                    Param := DoubleToStr(1 / d, 1) + ' Sec';
                  end
               end;
       $9202: begin
                ParamName := 'ApertureValue';
                Param := 'f/' + DoubleToStr(Power(SQRT(2), RationalToDouble(string(Param))), 1);
               end;
       $9203: begin
                ParamName := 'BrightnessValue';
                Param := DoubleToStr(RationalToDouble(string(Param)), 10);
               end;
       $9204: begin
                ParamName := 'ExposureBiasValue';
                Param := 'EV '+DoubleToStr(RationalToDouble(string(Param)), 10);
               end;
       $9205: begin
                ParamName := 'MaxApertureValue';
                Param := 'F ' + DoubleToStr(Power(SQRT(2), RationalToDouble(string(Param))), 1);
               end;
       $9206: begin
                ParamName := 'SubjectDistance';
                if RationalToDouble(string(Param))>0 then
                 Param := DoubleToStr(RationalToDouble(string(Param)),2)+' (m)'
                else Param := 'Unknown';
               end;
       $9207: begin
                ParamName := 'MeteringMode';
                case Param of
                   0: Param := 'unknown';
                   1: Param := 'Average';
                   2: Param := 'CenterWeightedAverage';
                   3: Param := 'Spot';
                   4: Param := 'MultiSpot';
                   5: Param := 'Pattern';
                   6: Param := 'Partial';
                 255: Param := 'other';
                else CaseElse(Tag,Param);
                end;
               end;
       $9208: begin
                ParamName := 'LightSource';
                case Param of
                   0: Param := 'unknown';
                   1: Param := 'Daylight';
                   2: Param := 'Fluorescent';
                   3: Param := 'Tungsten (incandescent light)';
                   4: Param := 'Flash';
                   9: Param := 'Fine weather';
                  10: Param := 'Cloudy weather';
                  11: Param := 'Shade';
                  12: Param := 'Daylight fluorescent (D 5700 � 7100K)';
                  13: Param := 'Day white fluorescent (N 4600 � 5400K)';
                  14: Param := 'Cool white fluorescent (W 3900 � 4500K)';
                  15: Param := 'White fluorescent (WW 3200 � 3700K)';
                  17: Param := 'Standard light A';
                  18: Param := 'Standard light B';
                  19: Param := 'Standard light C';
                  20: Param := 'D55';
                  21: Param := 'D65';
                  22: Param := 'D75';
                  23: Param := 'D50';
                  24: Param := 'ISO studio tungsten';
                 255: Param := 'other light source';
                else CaseElse(Tag,Param);
                end;
               end;
       $9209: begin
                ParamName := 'Flash';
                case Integer(Param) of
                  $0000: Param := 'Flash did not fire';
                  $0001: Param := 'Flash fired';
                  $0005: Param := 'Strobe return light not detected';
                  $0007: Param := 'Strobe return light detected';
                  $0009: Param := 'Flash fired, compulsory flash mode';
                  $000D: Param := 'Flash fired, compulsory flash mode, return light not detected';
                  $000F: Param := 'Flash fired, compulsory flash mode, return light detected';
                  $0010: Param := 'Flash did not fire, compulsory flash mode';
                  $0018: Param := 'Flash did not fire, auto mode';
                  $0019: Param := 'Flash fired, auto mode';
                  $001D: Param := 'Flash fired, auto mode, return light not detected';
                  $001F: Param := 'Flash fired, auto mode, return light detected';
                  $0020: Param := 'No flash function';
                  $0041: Param := 'Flash fired, red-eye reduction mode';
                  $0045: Param := 'Flash fired, red-eye reduction mode, return light not detected';
                  $0047: Param := 'Flash fired, red-eye reduction mode, return light detected';
                  $0049: Param := 'Flash fired, compulsory flash mode, red-eye reduction mode';
                  $004D: Param := 'Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected';
                  $004F: Param := 'Flash fired, compulsory flash mode, red-eye reduction mode, return light detected';
                  $0059: Param := 'Flash fired, auto mode, red-eye reduction mode';
                  $005D: Param := 'Flash fired, auto mode, return light not detected, red-eye reduction mode';
                  $005F: Param := 'Flash fired, auto mode, return light detected, red-eye reduction mode';
                else
                 Param := 'reserved';
                end;
               end;
       $920A: begin
                ParamName := 'FocalLength';
                if RationalToDouble(string(Param)) > 0 then
                 Param := DoubleToStr(RationalToDouble(string(Param)), 1) + ' (mm)'
                else Param := 'Unknown';
               end;
       $9214: begin
                ParamName := 'SubjectArea';
                ss := '';
                s := Param;
                n := ExtractDigit(s);
                ss := 'x=' + IntToStr(n) + ', ';
                n := ExtractDigit(s);
                ss := ss + 'y=' + IntToStr(n) + ', ';
                if tag.Count > 2 then
                 begin
                  n := ExtractDigit(s);
                  if tag.Count = 3 then
                   ss := ss + 'd=' + IntToStr(n)
                  else
                   begin
                    ss := ss + 'w=' + IntToStr(n) + ', ';
                    n := ExtractDigit(s);
                    ss := ss + 'h=' + IntToStr(n);
                   end;
                 end;
                Param := ss;
               end;
       $A20B: begin ShowMessage('A20B'); ParamName := 'FlashEnergy'; end;
       $A20C: begin ShowMessage('A20C'); ParamName := 'SpatialFrequencyResponse'; end;
       $A20E: begin
                ParamName := 'FocalPlaneXResolution';
                Param := DoubleToStr(RationalToDouble(string(Param)),2);
               end;
       $A20F: begin
                ParamName := 'FocalPlaneYResolution';
                Param := DoubleToStr(RationalToDouble(string(Param)),2);
               end;
       $A210: begin
                ParamName := 'FocalPlaneResolutionUnit';
                case Integer(Param) of
                 0: Param := 'no-unit';
                 1: Param := 'inch';
                 2: Param := 'centimeter';
                 3: Param := '8.3mm of Fujifilm''s';
                else CaseElse(Tag,Param);
                end;
               end;
       $A214: begin
                asm int 3 end;
                ParamName := 'SubjectLocation';
               end;
       $A215: begin
                ParamName := 'ExposureIndex';
               end;
       $A217: begin
                ParamName := 'SensingMethod';
                case Integer(Param) of
                 1: Param := 'Not defined';
                 2: Param := 'One-chip color area sensor';
                 3: Param := 'Two-chip color area sensor';
                 4: Param := 'Three-chip color area sensor';
                 5: Param := 'Color sequential area sensor';
                 7: Param := 'Trilinear sensor';
                 8: Param := 'Color sequential linear sensor';
                else CaseElse(Tag,Param);
                end;
               end;
       $A300: begin
                ParamName := 'FileSource';
                if Tag.Count > 1 then
                 Param := ReadValue(tag.TagType, 1, tag.OffSet, off0, Align);
                case Integer(Param) of
                 3: Param := 'DSC (digital still camera)';
                else CaseElse(Tag, Param);
                end;
               end;
       $A301: begin
                ParamName := 'SceneType';
                case Integer(Param) of
                 1: Param := 'A directly photographed image';
                else CaseElse(Tag,Param);
                end;
               end;
       $A302: begin
                ParamName := 'CFAPattern';
                try
                 ss := Param;
                 b[1] := ExtractDigit(ss);
                 b[0] := ExtractDigit(ss);
                 n := w;
                 b[1] := ExtractDigit(ss);
                 b[0] := ExtractDigit(ss);
                 m := w;
                 s := '';
                 for i := 1 to n do
                  begin
                   s := s+'(';
                   for j := 1 to m do
                    case ExtractDigit(ss) of
                     0: if j<>m then s := s+'R,' else s := s+'R';
                     1: if j<>m then s := s+'G,' else s := s+'G';
                     2: if j<>m then s := s+'B,' else s := s+'B';
                     3: if j<>m then s := s+'C,' else s := s+'C';
                     4: if j<>m then s := s+'M,' else s := s+'M';
                     5: if j<>m then s := s+'Y,' else s := s+'Y';
                     6: if j<>m then s := s+'W,' else s := s+'W';
                    else Break;
                    end;
                   s := s+')';
                  end;
                 Param := s;
                except
                 CaseElse(Tag,Param);
                end;
               end;
       $A401: begin
                ParamName := 'CustomRendered';
                case Integer(Param) of
                 0: Param := 'Normal process';
                 1: Param := 'Custom process';
                else CaseElse(Tag,Param);
                end;
               end;
       $A402: begin
                ParamName := 'ExposureMode';
                case Integer(Param) of
                 0: Param := 'Auto exposure';
                 1: Param := 'Manual exposure';
                 2: Param := 'Auto bracket';
                else CaseElse(Tag,Param);
                end;
               end;
       $A403: begin
                ParamName := 'WhiteBalance';
                case Integer(Param) of
                 0: Param := 'Auto white balance';
                 1: Param := 'Manual white balance';
                else CaseElse(Tag,Param);
                end;
               end;
       $A404: begin
                ParamName := 'DigitalZoomRatio';
                if (RationalToDouble(string(Param))=0) or (RationalToDouble(string(Param))=1) then Param := 'Off'
                else Param := DoubleToStr(RationalToDouble(string(Param)),2);
               end;
       $A405: begin
                ParamName := 'FocalLengthIn35mmFilm';
                Param := string(Param)+' (mm)';
               end;
       $A406: begin
                ParamName := 'SceneCaptureType';
                case Integer(Param) of
                 0: Param := 'Standard';
                 1: Param := 'Landscape';
                 2: Param := 'Portrait';
                 3: Param := 'Night scene';
                else CaseElse(Tag,Param);
                end;
               end;
       $A407: begin
                ParamName := 'GainControl';
                case Integer(Param) of
                 0: Param := 'None';
                 1: Param := 'Low gain up';
                 2: Param := 'High gain up';
                 3: Param := 'Low gain down';
                 4: Param := 'High gain down';
                else CaseElse(Tag,Param);
                end;
               end;
       $A408: begin
                ParamName := 'Contrast';
                case Integer(Param) of
                 0: Param := 'Normal';
                 1: Param := 'Soft';
                 2: Param := 'Hard';
                else CaseElse(Tag,Param);
                end;
               end;
       $A409: begin
                ParamName := 'Saturation';
                case Integer(Param) of
                 0: Param := 'Normal';
                 1: Param := 'Low saturation';
                 2: Param := 'High saturation';
                else CaseElse(Tag,Param);
                end;
               end;
       $A40A: begin
                ParamName := 'Sharpness';
                case Integer(Param) of
                 0: Param := 'Normal';
                 1: Param := 'Soft';
                 2: Param := 'Hard';
                else CaseElse(Tag,Param);
                end;
               end;
       $A40B: begin
                ParamName := 'DeviceSettingDescription';
               end;
       $A40C: begin
                ParamName := 'SubjectDistanceRange';
                case Integer(Param) of
                 0: Param := 'unknown';
                 1: Param := 'Macro';
                 2: Param := 'Close view';
                 3: Param := 'Distant view';
                else CaseElse(Tag,Param);
                end;
               end;
      end;

end;

procedure TExif.ReadUnknownMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
      SONY: Array[1..10] of Char;
   iOffset: Cardinal;
     i,j,l: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
 Tag, Tag2: TTag;
      s,s2: string;
    Create: boolean;

 procedure AssignValue;
 var
  writeOffset: Cardinal;
 begin
 // ���� �� ������� �� ������� ���������
  if not Create then
   if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
      ((UnknownsVisible = true) and ((Param <> Null))) then
    if FExifData.Add(MakerNoteinformation) then
     Create := true;
  if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
 // ��������� ��������� � ������� ���������� � ���
  if Param <> Null then
   if ParamName <> '' then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    if UnknownsVisible = true then
     begin
      FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
      Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
     end
    else
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
  else
   Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':'+' (!) TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(writeOffset));
 end;

 procedure CaseElse;
 var
  s: string;
 begin
  try
   s := '(Tag $' + IntToHex(tag.TagID, 4) + ') ' + '(!) ' + IntToStr(Param);
   ParamName := '';
   Param := s;
  except
   Param := 'ERROR �0a4r3';
  end;
 end;

begin
  if FExifData.GetByName('Main information') <> nil then s := FExifData.GetByName('Main information').GetByName('Make');
  MakerNoteInformation := s + ' Maker Note information (unknown)';
  Write(' ------------- Unknown MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;

  Seek(F, OffSet + Off0);
{
  mFile.Read(SONY,SizeOf(SONY));
  Write(IntToHex(mFile.Position - SizeOf(SONY), 8) + ': ' + SONY);
  mFile.Seek(mFile.Position + 2, soFromBeginning);
  iOffset := Off0;
  If SONY <> 'SONY DSC '#0 then Exit;
}
// .............................................................................

  i :=  OffSet + Off0;
  while i < OffSet + Off0 + MakerNote.Length - 1 do
   begin
    Seek(F, i);
    BlockRead(F, Entries, 2);
    if Align=$4D4D then Entries := InvertHex2(Entries);
    BlockRead(F, tag, 12);
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
    if (tag.TagType > 0) and
       (tag.TagType < 13) and
       (tag.Count < 999) then
     if (Entries > 0) and
        (Entries < 100) then
      if Entries > 1 then
       begin
        BlockRead(F, tag2, 12);
        if Align=$4D4D then
         begin
          tag2.TagID := InvertHex2(tag2.TagID);
          tag2.TagType := InvertHex2(tag2.TagType);
          tag2.Count := InvertHex4(tag2.Count);
         end;
        if (tag2.TagType > 0) and
           (tag2.TagType < 13) and
           (tag2.Count < 999) then
         Break
        else
         begin
          i := i + 1;
          Continue;
         end;

       end
      else Break;
    i := i + 1;
   end;
  if i < OffSet + Off0 + MakerNote.Length - 1 then
   begin
{
    if Entries < 2 then
     ShowMessage(FFileName + #13#13 + 'Unknown maker note!  MakerNote.Length=' + IntToStr(MakerNote.Length) + 'offset=' + IntToHex(OffSet + Off0, 8) + '   Entries=' + IntToStr(Entries) + '   �������=' + IntToStr(i - (OffSet + Off0)) + #13#13 + '   tag.ID=' + IntToHex(tag.TagID, 8))
    else
     ShowMessage(FFileName + #13#13 + 'Unknown maker note!  MakerNote.Length=' + IntToStr(MakerNote.Length) + 'offset=' + IntToHex(OffSet + Off0, 8) + '   Entries=' + IntToStr(Entries) + '   �������=' + IntToStr(i - (OffSet + Off0)) + #13 + '   tag.ID=' + IntToHex(tag.TagID, 8) + #13 + '   tag2.ID=' + IntToHex(tag2.TagID, 8));
}
    Write('Unknown maker note!  offset=' + IntToHex(OffSet + Off0, 8) + '   Entries=' + IntToStr(Entries) + '   difference = ' + IntToStr(i - (OffSet + Off0)) + '   tag.ID=' + IntToHex(tag.TagID, 8));
    Seek(F, i);
   end
  else
   begin
    Write('Unknown maker note not found.  offset=' + IntToHex(OffSet + Off0, 8));
    Exit;
   end;

// .............................................................................

  BlockRead(F, Entries, 2);
  if Align = $4D4D then Entries := InvertHex2(Entries);
  Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
//    if tag.TagID = $1015 then asm int 3 end;
    Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
//    if tag.TagID = $0200 then Param := '3,7,8,8';
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0E00: begin
                ParamName := 'PrintM';
                Param := 'Offset: $' + IntToHex(tag.OffSet,4) + '(' + IntToStr(tag.OffSet) + ')';
               end;
//    else CaseElse;          
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadLeicaMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
     LEICA: array[1..6] of Char;
   iOffset: Cardinal;
     i,j,l: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
 Tag, Tag2: TTag;
      s,s2: string;
    Create: boolean;

 procedure AssignValue;
 var
  writeOffset: Cardinal;
 begin
 // ���� �� ������� �� ������� ���������
  if not Create then
   if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
      ((UnknownsVisible = true) and ((Param <> Null))) then
    if FExifData.Add(MakerNoteinformation) then
     Create := true;
  if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
 // ��������� ��������� � ������� ���������� � ���
  if Param <> Null then
   if ParamName <> '' then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    if UnknownsVisible = true then
     begin
      FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
      Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
     end
    else
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
  else
   Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset));
 end;

 procedure CaseElse;
 var
  s: string;
 begin
  try
   s := '(Tag $' + IntToHex(tag.TagID, 4) + ') ' + '(!) ' + IntToStr(Param);
   ParamName := '';
   Param := s;
  except
   Param := 'ERROR �1a3r3';
  end;
 end;

begin
  MakerNoteInformation := 'Leica Maker Note information';
  Write(' ------------- Leica MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;

  Seek(F, OffSet + Off0);

  BlockRead(F, LEICA, SizeOf(LEICA));
  Write(IntToHex(FilePos(F) - SizeOf(LEICA), 8) + ': ' + LEICA);
  Seek(F, FilePos(F) + 2);
  iOffset := Off0;

  If LEICA <> 'LEICA'#0 then Exit;

  BlockRead(F, Entries, 2);
  if Align = $4D4D then Entries := InvertHex2(Entries);
  Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
//    if tag.TagID = $1015 then asm int 3 end;
    Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
//    if tag.TagID = $0200 then Param := '3,7,8,8';
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0E00: begin
                ParamName := 'PrintM';
                Param := 'Offset: $' + IntToHex(tag.OffSet,4) + '(' + IntToStr(tag.OffSet) + ')';
               end;
//    else CaseElse;          
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadSonyMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
      SONY: Array[1..10] of Char;
   iOffset: Cardinal;
     i,j,l: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
       Tag: TTag;
      s,s2: string;
    Create: boolean;

procedure AssignValue;
var
 writeOffset: Cardinal;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
 if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
 else
  Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset));
end;

procedure CaseElse;
var
 s:string;
begin
 s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
 ParamName := '';
 Param := s;
end;

begin
  MakerNoteinformation := 'Sony Maker Note information';
  Write(' ------------- Sony MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;
  Seek(F, OffSet + Off0);
  BlockRead(F, SONY, SizeOf(SONY));
  Write(IntToHex(FilePos(F) - SizeOf(SONY), 8) + ': ' + SONY);
  Seek(F, FilePos(F) + 2);
  iOffset := Off0;
  If SONY <> 'SONY DSC '#0 then Exit;

// .............................................................................
{
  i :=  OffSet + Off0;
  while i < OffSet + Off0 + 100 do
   begin
    mFile.Seek(i, soFromBeginning);
    mFile.Read(Entries,2);
    if Align=$4D4D then Entries := InvertHex2(Entries);
    mFile.Read(tag,12);
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
    if (Entries > 0) and
       (Entries < 100) and
       (tag.TagType > 0) and
       (tag.TagType < 13) and
       (tag.Count < 999)
       then Break;
    i := i + 1;
   end;
  if i < OffSet + Off0 + 100 then
   ShowMessage(IntToStr(OffSet + Off0) + '   ' + IntToStr(Entries) + '   ' + IntToStr(i - (OffSet + Off0)));
}
// .............................................................................

  BlockRead(F, Entries, 2);
  if Align = $4D4D then Entries := InvertHex2(Entries);
  Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
//    if tag.TagID = $1015 then asm int 3 end;
    Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
//    if tag.TagID = $0200 then Param := '3,7,8,8';
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0000: begin
                ParamName := 'Macro'; Pn := 3;
                case Param of
                 0: Param := 'normal';
                 1: Param := 'macro';
                 2: Param := 'view';
                 3: Param := 'manual';
                else CaseElse;
                end;
               end;
       $0E00: begin
                ParamName := 'PrintM';
                Param := 'Offset: $' + IntToHex(tag.OffSet,4) + '(' + IntToStr(tag.OffSet) + ')';
               end;
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadSanyoMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
     SANYO: Array[1..6] of Char;
   iOffset: Cardinal;
     i,j,l: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
       Tag: TTag;
      s,s2: string;
    Create: boolean;

procedure AssignValue;
var
 writeOffset: Cardinal;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
 if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
 else
  Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset));
end;

procedure CaseElse;
var
 s:string;
begin
 s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
 ParamName := '';
 Param := s;
end;

begin
  MakerNoteinformation := 'Sanyo Maker Note information';
  Write(' ------------- Sanyo MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;
  Write('OffSet = $' + IntToHex(OffSet,4) + ' (' + IntToStr(OffSet) + ')       Off0 = $' + IntToHex(Off0,4) + ' (' + IntToStr(Off0) + ')');
  Seek(F, OffSet + Off0);
  BlockRead(F, SANYO, SizeOf(SANYO));
  Write(IntToHex(FilePos(F) - SizeOf(SANYO), 8) + ': '+SANYO);
  Seek(F, FilePos(F) + 2);
  iOffset := Off0;//mFile.Position;
  If SANYO <> 'SANYO'#0 then Exit;
  BlockRead(F, Entries, 2);
  if Align=$4D4D then Entries := InvertHex2(Entries);
  Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
//    if tag.TagID = $1015 then asm int 3 end;
    Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
//    if tag.TagID = $0200 then Param := '3,7,8,8';
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0200: begin
                ParamName := 'SpecialMode';
                s2 := Param;
                try
                 s := Param;
                 j := StrToInt(Copy(s,1,Pos(',',s)-1));
                 case j of
                  0: Param := 'normal';
                  2: Param := 'fast';
                  3: begin
                       Param := 'panorama';
                       Delete(s,1,Pos(',',s));
                       Param := Param + ' (#' + Copy(s,1,Pos(',',s)-1) + ') ';
                       Delete(s,1,Pos(',',s));
                       l := StrToInt(s);
                       case l of
                         1: Param := Param + 'left to right';
                         2: Param := Param + 'right to left';
                         3: Param := Param + 'bottom to top';
                         4: Param := Param + 'top to bottom';
                       end;
                      end;
                 end;
                except
                 Param := s2;
                end;
               end;
       $0201: begin
                ParamName := 'JPEGqualityResolution';
                w1 := Param;
                case bb[0] of
                 0: Param := 'very low';
                 1: Param := 'low';
                 2: Param := 'medium low';
                 3: Param := 'medium';
                 4: Param := 'medium high';
                 5: Param := 'high';
                 6: Param := 'very high';
                 7: Param := 'super high';
                else CaseElse;
                end;
                logParam := bb[0];
                AssignValue;
                ParamName := 'JPEGqualityDetail';
                case bb[1] of
                 0: Param := 'normal';
                 1: Param := 'fine';
                 2: Param := 'super fine';
                else CaseElse;
                end;
                logParam := bb[1];
               end;
       $0202: begin
                ParamName := 'Macro'; Pn := 3;
                case Param of
                 0: Param := 'normal';
                 1: Param := 'macro';
                 2: Param := 'view';
                 3: Param := 'manual';
                else CaseElse;
                end;
               end;
       $0204: begin
                ParamName := 'DigitalZoom';
                Param := DoubleToStr(RationalToDouble(Param),0) + ' X';
               end;
       $0207: begin ParamName := 'SoftwareRelease'; end;
       $0208: begin ParamName := 'PictInfo'; end;
       $0209: begin
                ParamName := 'CameraID';
                s := Param;
                Param := '';
                while length(s)>0 do
                 Param := Param+Chr(ExtractDigit(s));
               end;
       $020E: begin
                ParamName := 'SequentialShotMethod';
                case Param of
                 0: Param := 'none';
                 1: Param := 'standart';
                 2: Param := 'best';
                 3: Param := 'adjust exposure';
                else CaseElse;
                end;
               end;
       $020F: begin
                ParamName := 'WideRange';
                case Param of
                 0: Param := 'off';
                 1: Param := 'on';
                else CaseElse;
                end;
               end;
       $0210: begin
                ParamName := 'ColourAdjustmentMode';
                case Param of
                 0: Param := 'off';
                else Param := 'colour adjustment mode used';
                end;
               end;
       $0213: begin
                ParamName := 'QuickShot';
                case Param of
                 0: Param := 'off';
                 1: Param := 'on';
                else CaseElse;
                end;
               end;
       $0214: begin
                ParamName := 'SelfTimer';
                case Param of
                 0: Param := 'off';
                 1: Param := 'on';
                else CaseElse;
                end;
               end;
       $0216: begin
                ParamName := 'VoiceMemo';
                case Param of
                 0: Param := 'off';
                 1: Param := 'on';
                else CaseElse;
                end;
               end;
       $0217: begin
                ParamName := 'RecordShutterRelease';
                case Param of
                 0: Param := 'record whilst held';
                 1: Param := 'press to start, press to stop';
                else CaseElse;
                end;
               end;
       $0218: begin
                ParamName := 'FlickerReduce';
                case Param of
                 0: Param := 'off';
                 1: Param := 'on';
                else CaseElse;
                end;
               end;
       $0219: begin
                ParamName := 'OpticalZoom';
                case Param of
                 0: Param := 'disabled';
                 1: Param := 'enabled';
                else CaseElse;
                end;
               end;
       $021B: begin
                ParamName := 'DigitalZoom';
                case Param of
                 0: Param := 'disabled';
                 1: Param := 'enabled';
                else CaseElse;
                end;
               end;
       $021D: begin
                ParamName := 'LightSourceSpecial';
                case Param of
                 0: Param := 'off';
                 1: Param := 'on';
                else CaseElse;
                end;
               end;
       $021E: begin
                ParamName := 'Resaved';
                case Param of
                 0: Param := 'no';
                 1: Param := 'yes';
                else CaseElse;
                end;
               end;
       $021F: begin
                ParamName := 'SceneSelect';
                case Param of
                 0: Param := 'off';
                 1: Param := 'sport';
                 2: Param := 'tv';
                 3: Param := 'night';
                 4: Param := 'user 1';
                 5: Param := 'user 2';
                else CaseElse;
                end;
               end;
       $0223: begin
                ParamName := 'ManualFocalDistance';
               end;
       $0224: begin
                ParamName := 'SequentialShotInterval';
                case Param of
                 0: Param := '5 frames/sec';
                 1: Param := '10 frames/sec';
                 2: Param := '15 frames/sec';
                 3: Param := '20 frames/sec';
                else CaseElse;
                end;
               end;
       $0225: begin
                ParamName := 'FlashMode';
                case Param of
                 0: Param := 'auto';
                 1: Param := 'force';
                 2: Param := 'disabled';
                 3: Param := 'red eye';
                else CaseElse;
                end;
               end;
       $0E00: begin
                ParamName := 'PrintM';
                Param := 'Offset: $' + IntToHex(tag.OffSet,4) + '(' + IntToStr(tag.OffSet) + ')';
               end;
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadCasioMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
type
 TGetTagMode = (mode1,mode2);
var
     CASIO: Array[1..4] of Char;
GetTagMode: TGetTagMode;
   iOffset: Cardinal;
     i,j,l: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
       Tag: TTag;
      s,s2: string;
    Create: boolean;

procedure AssignValue;
var
 writeOffset: Cardinal;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
 if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
 else
  Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset));
end;

procedure CaseElse;
var
 s:string;
begin
 s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
 ParamName := '';
 Param := s;
end;

procedure GetTagValue1;
var
  s,s1: string;
     j: cardinal;
begin
 ParamName := '';
 Pn := 0;
 Param := Null;
 logParam := Null;
 Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
 logParam := Param;
 case tag.TagID of
   $0001: begin
            ParamName := 'RecordingMode';
            case Param of
             1: Param := 'Single Shutter';
             2: Param := 'Panorama';
             3: Param := 'Night Scene';
             4: Param := 'Portrait';
             5: Param := 'Landscape';
            else CaseElse;
            end;
           end;
   $0003: begin
            ParamName := 'ThumbnailSize';
           end;
   $0004: begin
            ParamName := 'ThumbnailOffset';
           end;
   $2000: begin
            ParamName := '';
            Param := IntToStr(tag.Count) + ' bytes ($' + IntToHex(tag.Count, 8) + ')';
           end;
   $2022: begin
            ParamName := 'ObjectDistance';
            s := string(Param) + ' mm';
            Param := s;
           end;
   $2034: begin
            ParamName := 'FlashDistance';
            case Param of
             0: Param := 'Off';
            else CaseElse;
            end;
           end;
   $3000: begin
            ParamName := 'RecMode';
            case Param of
             2: Param := 'Normal mode';
            else CaseElse;
            end;
           end;
   $3002: begin
            ParamName := 'Quality';
            case Param of
             3: Param := 'Fine';
            else CaseElse;
            end;
           end;
   $3003: begin
            ParamName := 'FocusMode';
            case Param of
             3: Param := 'Auto';
            else CaseElse;
            end;
           end;
   $3006: begin
            ParamName := 'TimeZone';
           end;
   $3007: begin
            ParamName := 'BestshotMode';
            case Param of
             0: Param := 'Off';
            else CaseElse;
            end;
           end;
   $3015: begin
            ParamName := 'ColorMode';
            case Param of
             0: Param := 'Off';
            else CaseElse;
            end;
           end;
   $3016: begin
            ParamName := 'Enhancement';
            case Param of
             0: Param := 'Off';
            else CaseElse;
            end;
           end;
   $3017: begin
            ParamName := 'Filter';
            case Param of
             0: Param := 'Off';
            else CaseElse;
            end;
           end;
   $3014: begin
            ParamName := 'CCDsensitivity';
           end;
 end;
end;

procedure GetTagValue2;
var
 s: string;
 j: cardinal;
begin
 ParamName := '';
 Pn := 0;
 Param := Null;
 logParam := Null;
 Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
 logParam := Param;
 case tag.TagID of
   $0001: begin
            ParamName := 'RecordingMode';
            case Param of
             1: Param := 'Single Shutter';
             2: Param := 'Panorama';
             3: Param := 'Night Scene';
             4: Param := 'Portrait';
             5: Param := 'Landscape';
            else CaseElse;
            end;
           end;
   $0002: begin
            ParamName := 'Quality';
            case Param of
             1: Param := 'Economy';
             2: Param := 'Normal';
             3: Param := 'Fine';
            else CaseElse;
            end;
           end;
   $0003: begin
            ParamName := 'FocusingMode';
            case Param of
             2: Param := 'Macro';
             3: Param := 'Auto';
             4: Param := 'Manual';
             5: Param := 'Infinity';
             6: Param := 'Multi-area AF';
            else CaseElse;
            end;
           end;
   $0004: begin
            ParamName := 'FlashMode';
            case Param of
             1: Param := 'Auto';
             2: Param := 'On';
             3: Param := 'Off';
             4: Param := 'Red eye reduction';
            else CaseElse;
            end;
           end;
   $0005: begin
            ParamName := 'FlashIntensity';
            case Param of
             11: Param := 'Weak';
             13: Param := 'Normal';
             15: Param := 'Strong';
            else CaseElse;
            end;
           end;
   $0006: begin
            ParamName := 'ObjectDistance';
            s := string(Param);
            Param := s + ' mm';
           end;
   $0007: begin
            ParamName := 'WhiteBalance';
            case Param of
             1: Param := 'Auto';
             2: Param := 'Tungsten';
             3: Param := 'Daylight';
             4: Param := 'Fluorescent';
             5: Param := 'Shade';
             129: Param := 'Manual';
            else CaseElse;
            end;
           end;
   $000A: begin
            ParamName := 'DigitalZoom';
            case Param of
             $10000: Param := 'Off';
             $10001: Param := '2X';
            else CaseElse;
            end;
           end;
   $000B: begin
            ParamName := 'Sharpness';
            case Param of
             0: Param := 'Normal';
             1: Param := 'Soft';
             2: Param := 'Hard';
            else CaseElse;
            end;
           end;
   $000C: begin
            ParamName := 'Contrast';
            case Param of
             0: Param := 'Normal';
             1: Param := 'Soft';
             2: Param := 'Hard';
            else CaseElse;
            end;
           end;
   $000D: begin
            ParamName := 'Saturation';
            case Param of
             0: Param := 'Normal';
             1: Param := 'Soft';
             2: Param := 'Hard';
            else CaseElse;
            end;
           end;
   $0014: begin
            ParamName := 'CCDsensitivity';
           end;
   $0016: begin
            ParamName := 'Enhancement';
            case Param of
             0: Param := 'On';
             1: Param := 'Off';
            else CaseElse;
            end;
           end;
   $0017: begin
            ParamName := 'Filter';
            case Param of
             0: Param := 'On';
             1: Param := 'Off';
            else CaseElse;
            end;
           end;
   $0018: begin
            ParamName := 'FocusArea';
            case Param of
             1: Param := 'Center';
            else CaseElse;
            end;
           end;
   $0019: begin
            ParamName := 'FlashIntensity';
            case Param of
             1: Param := 'Normal';
            else CaseElse;
            end;
           end;
 end;
end;

begin
  MakerNoteinformation := 'Casio Maker Note information';
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;
  Write(' -------------- Casio MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  Write('OffSet = $' + IntToHex(OffSet,4) + ' (' + IntToStr(OffSet) + ')       Off0 = $' + IntToHex(Off0,4) + ' (' + IntToStr(Off0) + ')');
  Seek(F, OffSet + Off0);
  BlockRead(F, CASIO, SizeOf(CASIO));
  iOffset := Off0;
  If UpperCase(CASIO) = 'QVC'#0 then
   begin
    Write(IntToHex(FilePos(F) - SizeOf(CASIO), 8) + ': ' + CASIO);
    GetTagMode := mode1;
    Seek(F, FilePos(F) + 2);
   end
  else
   begin
    Seek(F, OffSet + Off0);
    GetTagMode := mode2;
   end;
  BlockRead(F, Entries, 2);
  if Align=$4D4D then Entries := InvertHex2(Entries);
  case GetTagMode of
   mode1: Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries) + '   (mode 1)');
   mode2: Write(IntToHex(FilePos(F) - 2, 8)+': Entries: ' + IntToStr(Entries) + '   (mode 2)');
  end;
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
    case GetTagMode of
     mode1: GetTagValue1;
     mode2: GetTagValue2;
    end;

// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadOlympusMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
     OLIMP: Array[1..6] of Char;
   iOffset: Cardinal;
     i,j,l: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
       Tag: TTag;
      s,s2: string;
    Create: boolean;

procedure AssignValue;
var
 writeOffset: Cardinal;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
 if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
 else
  Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset));
end;

procedure CaseElse;
var
 s:string;
begin
 s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
 ParamName := '';
 Param := s;
end;

begin
  MakerNoteinformation := 'Olympus Maker Note information';
  Write(' ------------- Olympus MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;
  Seek(F, OffSet + Off0);
  BlockRead(F, OLIMP, SizeOf(OLIMP));
  Write(IntToHex(FilePos(F) - 6, 8) + ': '+OLIMP);
  Seek(F, FilePos(F) + 2);
  iOffset := Off0;//mFile.Position;
  If OLIMP<>'OLYMP'#0 then Exit;
  BlockRead(F, Entries, 2);
  if Align=$4D4D then Entries := InvertHex2(Entries);
  Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
//    if tag.TagID = $1015 then asm int 3 end;
    Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
//    if tag.TagID = $0200 then Param := '3,7,8,8';
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0200: begin
                ParamName := 'SpecialMode';
                s2 := Param;
                try
                 s := Param;
                 j := StrToInt(Copy(s,1,Pos(',',s)-1));
                 case j of
                  0: Param := 'Normal';
                  2: Param := 'Fast';
                  3: begin
                       Param := 'Panorama';
                       Delete(s,1,Pos(',',s));
                       Param := Param + ' (#' + Copy(s,1,Pos(',',s)-1) + ') ';
                       Delete(s,1,Pos(',',s));
                       l := StrToInt(s);
                       case l of
                         1: Param := Param + 'left to right';
                         2: Param := Param + 'right to left';
                         3: Param := Param + 'bottom to top';
                         4: Param := Param + 'top to bottom';
                       end;
                      end;
                 end;
                except
                 Param := s2;
                end;
               end;
       $0201: begin
                ParamName := 'JPEGquality'; Pn := 2;
                case Param of
                 1: Param := 'SQ';
                 2: Param := 'HQ';
                 3: Param := 'SHQ';
                else CaseElse;
                end;
               end;
       $0202: begin
                ParamName := 'Macro'; Pn := 3;
                case Param of
                 0: Param := 'Off';
                 1: Param := 'On';
                else CaseElse;
                end;
               end;
       $0204: begin
                ParamName := 'DigiZoom'; Pn := 5;
                Param := DoubleToStr(RationalToDouble(Param),0) + ' X';
               end;
       $0207: begin ParamName := 'SoftwareRelease'; Pn := 8; end;
       $0208: begin
                ParamName := 'PictInfo';
               end;
       $0209: begin
                ParamName := 'CameraID'; Pn := 10;
                s := Param;
                Param := '';
                while length(s)>0 do
                 Param := Param+Chr(ExtractDigit(s));
               end;
       $0302: begin
                ParamName := 'OneTouchWB';
                case Param of
                 0: Param := 'Off';
                 1: Param := 'On';
                else CaseElse;
                end;
               end;
//       $0F00: begin ParamName := 'DataDump'; Pn := 11; end;
       $100D: begin ParamName := 'ZoomPosition'; Pn := 12; end;
       $101A: begin
                ParamName := 'SerialNumber';
                Param := TrimRight(Param);
               end;
       $1015: begin ParamName := 'WhiteBalance'; end;
       $102E: begin ParamName := 'Width'; Pn := 13; end;
       $102F: begin ParamName := 'Height'; Pn := 14; end;
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadMinoltaMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
   iOffset: Cardinal;
     i,j,l: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
       Tag: TTag;
      s,s2: string;
    Create: boolean;
 TIFFHeader:TTIFFHeader;
// ��������������� ���������� ����������� ��������� ����
  Interval: boolean;         // Tag: $0003 subtag: 7
  FileNumberMemory: boolean; // Tag: $0003 subtag: 27

procedure AssignValue;
var
 writeOffset: Cardinal;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
 if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
 else
  Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset));
end;

procedure AssignSubValue;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName,Pn,Tag.TagType,Param, False);
    Write('             '+IntToStr(j)+' '+ParamName+' = "'+string(Param)+'" ('+string(logParam)+')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ', sub ' + IntToStr(j) + ',' + IntToStr(Tag.TagType) + ')', Pn, Tag.TagType, Param, True);
     Write('             '+IntToStr(j)+' (!) Unknown = '+string(Param))
    end
   else
    Write('             '+IntToStr(j)+' (!) Unknown')
end;

procedure CaseElse;
var
 s:string;
begin
 s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
 ParamName := '';
 Param := s;
end;

begin
  Interval := false;
  FileNumberMemory := false;
  MakerNoteinformation := 'Minolta Maker Note information';
  Write(' ------------- Minolta MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;
  Seek(F, OffSet + Off0);

  iOffset := Off0;
  BlockRead(F, Entries, 2);
  if Align=$4D4D then Entries := InvertHex2(Entries);
  Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align = Motorola then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
//    if tag.TagID = $1015 then asm int 3 end;
    Param := ReadValue(tag.TagType, tag.Count, tag.OffSet, iOffset, Align);
//    if tag.TagID = $0200 then Param := '3,7,8,8';
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0000: begin
                ParamName := 'MakerNoteVersion';
                Param := ReadValue(2,tag.Count,tag.OffSet,iOffset, Intel);
               end;
       $0001,
       $0003: begin
                case tag.TagID of
                 $0001:  ParamName := 'CameraSettingsOld';
                 $0003:  ParamName := 'CameraSettingsNew';
                end;
                Param := IntToStr(tag.Count) + ' bytes';
                AssignValue;
                Param := ReadValue(tag.TagType, tag.Count, tag.OffSet, iOffset, Align);
                Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':  (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(tag.OffSet) + ' offset=$' + IntToHex(tag.OffSet, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
                s := Param;
                j := 1;
                while (length(s) <> 0){ and (j < 54)} do
                  begin
                   Param := ExtractDigit(s);
                   logParam := Param;
                   ParamName := '';
                   case j of
                     2: begin
                         ParamName := 'ExposureMode';
                         case Param of
                          0: Param := 'P mode';
                          1: Param := 'A mode';
                          2: Param := 'S mode';
                          3: Param := 'M mode';
                         else CaseElse;
                         end;
                        end;
                     3: begin
                         ParamName := 'FlashMode';
                         case Param of
                          0: Param := 'fill-flash';
                          1: Param := 'red-eye reduction';
                          2: Param := 'rear flash sync';
                          3: Param := 'wireless';
                         else CaseElse;
                         end;
                        end;
                     4: begin
                         ParamName := 'WhiteBalance';
                         case Param of
                          0: Param := 'auto';
                          1: Param := 'daylight';
                          2: Param := 'cloudy';
                          3: Param := 'tungsten';
                          5: Param := 'custom';
                          7: Param := 'fluorescent';
                          8: Param := 'fluorescent 2';
                          11: Param := 'custom 2';
                          12: Param := 'custom 3';
                         else CaseElse;
                         end;
                        end;
                     5: begin
                         ParamName := 'ImageSize';
                         case Param of
                          0: Param := '2560 x 1920 (2048x1536)';
                          1: Param := '1600 x 1200';
                          2: Param := '1280 x 960';
                          3: Param := '640 x 480';
                         else CaseElse;
                         end;
                        end;
                     6: begin
                         ParamName := 'ImageQuality';
                         case Param of
                          0: Param := 'raw';
                          1: Param := 'super fine';
                          2: Param := 'fine';
                          3: Param := 'standart';
                          4: Param := 'economy';
                          5: Param := 'extra fine';
                         else CaseElse;
                         end;
                        end;
                     7: begin
                         ParamName := 'DriveMode';
                         case Param of
                          0: Param := 'single';
                          1: Param := 'continuous';
                          2: Param := 'self-timer';
                          4: Param := 'bracketing';
                          5: begin Param := 'interval'; Interval := true; end;
                          6: Param := 'UHS continuous';
                          7: Param := 'HS continuous';
                         else CaseElse;
                         end;
                        end;
                     8: begin
                         ParamName := 'MeteringMode';
                         case Param of
                          0: Param := 'multi-segment';
                          1: Param := 'center weighted';
                          2: Param := 'spot';
                         else CaseElse;
                         end;
                        end;
                     9: begin
                         ParamName := 'FilmSpeed';
                         if Param > 0 then
                          begin
                           w1 := StrToInt(Param);
                           Param := DoubleToStr(w1/8-1,0);
                           AssignSubValue;
                           ParamName := 'ISO';
                           Param := DoubleToStr(Power(2,(w1/8-1))*3.125 ,0);
                          end;
                        end;
                     10: begin
                          ParamName := 'ShutterSpeed';
                          if Param > 0 then
                           begin
                            w1 := StrToInt(Param);
                            Param := DoubleToStr(Power(2,(48-w1)/8),0);
                           end;
                         end;
                     11: begin
                          ParamName := 'Aperture';
                          if Param > 0 then
                           begin
                            w1 := StrToInt(Param);
                            Param := DoubleToStr(Power(2,w1/16-0.5),0);
                           end;
                         end;
                     12: begin
                          ParamName := 'MacroMode';
                          case Param of
                           0: Param := 'Off';
                           1: Param := 'On';
                          else CaseElse;
                          end;
                         end;
                     13: begin
                          ParamName := 'DigitalZoom';
                          case Param of
                           0: Param := 'Off';
                           1: Param := 'electronic magnification was used';
                           2: Param := 'digital zoom 2x';
                          else CaseElse;
                          end;
                         end;
                     14: begin
                          ParamName := 'ExposureCompensation';
                          if Param > 0 then
                           begin
                            w1 := StrToInt(Param);
                            Param := DoubleToStr(w1/3-2,0);
                           end;
                          s2 := string(Param) + ' EV';
                          Param := s2;
                         end;
                     15: begin
                          ParamName := 'BracketStep';
                          case Param of
                           0: Param := '1/3 EV';
                           1: Param := '2/3 EV';
                           2: Param := '1';
                          else CaseElse;
                          end;
                         end;
                     17: begin
                          ParamName := '';
                          if Interval then
                           begin
                            ParamName := 'IntervalLength';
                            s2 := IntToStr(Param + 1) + ' min';
                            Param := s2;
                           end;
                         end;
                     18: begin
                          ParamName := '';
                          if Interval then
                           begin
                            ParamName := 'IntervalNumber';
                            s2 := IntToStr(Param) + ' frames';
                            Param := s2;
                           end;
                         end;
                     19: begin
                          ParamName := '';
                          if Param > 0 then
                           begin
                            ParamName := 'FocalLength(mm)';
                            w1 := StrToInt(Param);
                            Param := DoubleToStr(w1/256,0);
                            AssignSubValue;
                            ParamName := 'FocalLength(35mm)';
                            Param := DoubleToStr(w1/256*3.9333,0);
                           end;
                         end;
                     20: begin
                          ParamName := 'FocusDistance';
                          if Param > 0 then
                           begin
                            w1 := StrToInt(Param);
                            Param := DoubleToStr(w1/1000,5) + ' m';
                           end
                          else
                           Param := 'infinity';
                         end;
                     21: begin
                          ParamName := 'FlashFired';
                          case Param of
                           0: Param := 'No';
                           1: Param := 'Yes';
                          else CaseElse;
                          end;
                         end;
                     22: begin
                          ParamName := '';
                          if Param > 0 then
                           begin
                            asm int 3 end;
                            ParamName := 'Date';
                            w1 := StrToInt(Param);
                            s2 := '-' + DoubleToStr(w1/65536,0);
                            s2 := '-' + DoubleToStr(w1/256-w1/65536*256,0) + s2;
                            s2 := DoubleToStr(w1 mod 256,0) + s2;
                            Param := s2;
                           end;
                         end;
                     23: begin
                          ParamName := '';
                          if Param > 0 then
                           begin
                            asm int 3 end;
                            ParamName := 'Time';
                            w1 := StrToInt(Param);
                            s2 := ':' + DoubleToStr(w1/65536,0);
                            s2 := ':' + DoubleToStr(w1/256-w1/65536*256,0) + s2;
                            s2 := DoubleToStr(w1 mod 256,0) + s2;
                            Param := s2;
                           end;
                         end;
                     24: begin
                          ParamName := '';
                          if Param > 0 then
                           begin
                            ParamName := 'MaxAperture';
                            w1 := StrToInt(Param);
                            Param := DoubleToStr(Power(2,(w1/16-0.5)),0);
                           end
                         end;
                     27: begin
                          ParamName := 'FileNumberMemory';
                          case Param of
                           0: Param := 'Off';
                           1: begin Param := 'On'; FileNumberMemory := true; end;
                          else CaseElse;
                          end;
                         end;
                     28: begin
                          if FileNumberMemory then
                           ParamName := 'LastFileNumber'
                          else
                           ParamName := '';
                         end;
                     29: begin
                          ParamName := 'WhiteBalanceRed';
                          w1 := StrToInt(Param);
                          Param := DoubleToStr(w1/256,0);
                         end;
                     30: begin
                          ParamName := 'WhiteBalanceGreen';
                          w1 := StrToInt(Param);
                          Param := DoubleToStr(w1/256,0);
                         end;
                     31: begin
                          ParamName := 'WhiteBalanceBlue';
                          w1 := StrToInt(Param);
                          Param := DoubleToStr(w1/256,0);
                         end;
                     32: begin
                          ParamName := 'Saturation';
                          s2 := Param - 3;
                          Param := s2;
                         end;
                     33: begin
                          ParamName := 'Contrast';
                          s2 := Param - 3;
                          Param := s2;
                         end;
                     34: begin
                          ParamName := 'Sharpness';
                          case Param of
                           0: Param := 'Hard';
                           1: Param := 'Normal';
                           2: Param := 'Soft';
                          else CaseElse;
                          end;
                         end;
                     35: begin
                          ParamName := 'SubjectProgram';
                          case Param of
                           0: Param := 'none';
                           1: Param := 'portrait';
                           2: Param := 'text';
                           3: Param := 'night portrait';
                           4: Param := 'sunset';
                           5: Param := 'sports action';
                          else CaseElse;
                          end;
                         end;
                     36: begin
                          ParamName := 'FlashCompensation';
                          w1 := StrToInt(Param);
                          Param := DoubleToStr((w1-6)/3,0) + ' EV';
                         end;
                     37: begin
                          ParamName := 'ISOSetting';
                          case Param of
                           0: Param := '100';
                           1: Param := '200';
                           2: Param := '400';
                           3: Param := '800';
                           4: Param := 'auto';
                          else CaseElse;
                          end;
                         end;
                     38: begin
                          ParamName := 'CameraModel';
                          case Param of
                           0: Param := 'DiMAGE 7';
                           1: Param := 'DiMAGE 5';
                           2: Param := 'DiMAGE S304';
                           3: Param := 'DiMAGE S404';
                           4: Param := 'DiMAGE 7i';
                           5: Param := 'DiMAGE 7Hi';
                          else CaseElse;
                          end;
                         end;
                     39: begin
                          ParamName := 'IntervalMode';
                          case Param of
                           0: Param := 'still image';
                           1: Param := 'time-lapse movie';
                          else CaseElse;
                          end;
                         end;
                     40: begin
                          ParamName := 'FolderName';
                          case Param of
                           0: Param := 'Standart form';
                           1: Param := 'Data form';
                          else CaseElse;
                          end;
                         end;
                     41: begin
                          ParamName := 'ColorMode';
                          case Param of
                           0: Param := 'natural color';
                           1: Param := 'black&white';
                           2: Param := 'vivid color';
                           3: Param := 'solarization';
                           4: Param := 'AdobeRGB';
                          else CaseElse;
                          end;
                         end;
                     42: begin
                          ParamName := 'ColorFilter';
                          s2 := Param - 3;
                          Param := s2;
                         end;
                     43: begin
                          ParamName := 'BWFilter';
                         end;
                     44: begin
                          ParamName := 'InternalFlash';
                          case Param of
                           0: Param := 'no';
                           1: Param := 'Fired';
                          else CaseElse;
                          end;
                         end;
                     45: begin
                          ParamName := 'BrightnessValue';
                          w1 := StrToInt(Param);
                          Param := DoubleToStr(w1/8-6,0);
                         end;
                     46: begin
                          ParamName := 'SpotFocusPointX';
                         end;
                     47: begin
                          ParamName := 'SpotFocusPointY';
                         end;
                     48: begin
                          ParamName := 'WideFocusZone';
                          case Param of
                           0: Param := 'no zone';
                           1: Param := 'center zone (horizontal orientation)';
                           2: Param := 'center zone (vertical orientation)';
                           3: Param := 'left zone';
                           4: Param := 'right zone';
                          else CaseElse;
                          end;
                         end;
                     49: begin
                          ParamName := 'FocusMode';
                          case Param of
                           0: Param := 'AF';
                           1: Param := 'MF';
                          else CaseElse;
                          end;
                         end;
                     50: begin
                          ParamName := 'FocusArea';
                          case Param of
                           0: Param := 'wide focus';
                           1: Param := 'spot focus';
                          else CaseElse;
                          end;
                         end;
                     51: begin
                          ParamName := 'DECPosition';
                          case Param of
                           0: Param := 'exposure';
                           1: Param := 'contrast';
                           2: Param := 'saturation';
                           3: Param := 'filter';
                          else CaseElse;
                          end;
                         end;
                     52: begin
                          ParamName := 'ColorProfile';
                          case Param of
                           0: Param := 'not';
                           1: Param := 'embedded';
                          else CaseElse;
                          end;
                         end;
                     53: begin
                          ParamName := 'DataImprint';
                          case Param of
                           0: Param := 'none';
                           1: Param := 'yyyy/mm/dd';
                           2: Param := 'mm/dd/hr:min';
                           3: Param := 'text';
                           4: Param := 'text + id#';
                          else CaseElse;
                          end;
                         end;
                   else
                   end;
                   Tag.TagType := 1;
                   AssignSubValue;
                   j := j + 1;
                  end;
                Continue;
               end;
       $0010: begin
                ParamName := '';
                Param := IntToStr(tag.Count) + ' bytes (probably focus and exposure)';
               end;
       $0020: begin
                ParamName := '';
                Param := IntToStr(tag.Count) + ' bytes';
               end;
       $0040: begin
                ParamName := 'CompressedImageSize';
               end;
       $0081: begin
                ParamName := 'Thumbnail';
               end;
       $0088: begin
                ParamName := 'ThumbnailOffset';
               end;
       $0089: begin
                ParamName := 'ThumbnailLength';
               end;
       $0100: begin
                ParamName := '';
               end;
       $0101: begin
                ParamName := 'ColorMode';
                case Param of
                 0: Param := 'natural color';
                 1: Param := 'black&white';
                 2: Param := 'vivid color';
                 3: Param := 'solarization';
                 4: Param := 'AdobeRGB';
                else CaseElse;
                end;
               end;
       $0102: begin
                ParamName := 'ImageQuality';
                case Param of
                 0: Param := 'raw';
                 1: Param := 'super fine';
                 2: Param := 'fine';
                 3: Param := 'standart';
                 5: Param := 'extra fine';
                else CaseElse;
                end;
               end;
       $0103: begin
                ParamName := 'ImageQuality';
                case Param of
                 0: Param := 'raw';
                 1: Param := 'super fine';
                 2: Param := 'fine';
                 3: Param := 'standart';
                 5: Param := 'extra fine';
                else CaseElse;
                end;
               end;
       $0E00: begin
                ParamName := 'PrintM';
                Param := IntToStr(tag.Count) + ' bytes';
               end;
       $0F00: begin
                ParamName := '';
               end;
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadFujifilmMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
  FUJIFILM: Array[1..8] of Char;
   iOffset: Cardinal;
         i: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
       Tag: TTag;
         s: string;
    Create: boolean;

procedure AssignValue;
var
 writeOffset: Cardinal;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
 writeOffset := tag.OffSet;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
 else
  Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':'+' (!) TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(writeOffset));
end;

procedure CaseElse;
var
 s:string;
begin
 s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
 ParamName := '';
 Param := s;
end;

begin
  MakerNoteinformation := 'Fujifilm Maker Note information';
  Write(' ------------- Fujifilm MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;
  Seek(F, OffSet + Off0);
  BlockRead(F, FUJIFILM, SizeOf(FUJIFILM));
  Write(IntToHex(FilePos(F) - 8, 8) + ': '+FUJIFILM);
  If FUJIFILM<>'FUJIFILM' then Exit;
  BlockRead(F, iOffset, 4);
  Write(IntToHex(FilePos(F) - 4, 8) + ': pointer to first IFD entry: ' + IntToHex(iOffset, 8));
  iOffset := iOffset + OffSet;
//  iOffset := Off0;//mFile.Position;
  BlockRead(F, Entries, 2);
  Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// �� ����������� ����� � ����� ���� ���� ���� 'Motorola' ������������
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
//    if tag.TagID = $1000 then asm int 3 end;
    Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, $4949);
//    if tag.TagID = $0200 then Param := '3,7,8,8';
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0000: begin
                ParamName := 'version';
                s := string(Param);
                if (s[1]='4') and (s[2]='8') and (s[3]=',') and (s[11]='8') and (s[10]='4') and (s[9]=',') then
                 begin
                  Delete(s,1,3); Delete(s,Length(s)-2,3);
                  s := IntToStr(StrToInt(Copy(s,1,Pos(',',s)-1))-48)+','+IntToStr(StrToInt(Copy(s,Pos(',',s)+1,Length(s)-Pos(',',s)))-48);
                  Param := s;
                 end;
               end;
       $1000: begin ParamName := 'Quality'; end;
       $1001: begin
                ParamName := 'Sharpness';
                case Param of
                 1,2: Param := 'SOFT';
                 3: Param := 'NORMAL';
                 4,5: Param := 'HARD';
                else CaseElse;
                end;
               end;
       $1002: begin
                ParamName := 'WhiteBalance';
                case Param of
                 0: Param := 'AUTO';
                 256: Param := 'DAYLIGHT';
                 512: Param := 'CLOUDY';
                 768: Param := 'DAYLIGHTCOLOR-FLUORESCENCE';
                 769: Param := 'DAYWHITECOLOR-FLUORESCENCE';
                 770: Param := 'WHITE-FLUORESCENCE';
                 1024: Param := 'INCANDENSCENSE';
                 3840: Param := 'CUSTOM';
                else CaseElse;
                end;
               end;
       $1003: begin
                ParamName := 'Color';
                case Param of
                 0: Param := 'NORMAL';
                 256: Param := 'HIGH';
                 512: Param := 'LOW';
                else CaseElse;
                end;
               end;
       $1004: begin
                ParamName := 'Tone';
                case Param of
                 0: Param := 'NORMAL';
                 256: Param := 'HIGH';
                 512: Param := 'LOW';
                else CaseElse;
                end;
               end;
       $1010: begin
                ParamName := 'FlashMode';
                case Param of
                 0: Param := 'AUTO';
                 1: Param := 'ON';
                 2: Param := 'OFF';
                 3: Param := 'RED EYE';
                else CaseElse;
                end;
               end;
       $1011: begin
                ParamName := 'FlashStrength';
               end;
       $1020: begin
                ParamName := 'Macro';
                case Param of
                 0: Param := 'OFF';
                 1: Param := 'ON';
                else CaseElse;
                end;
               end;
       $1021: begin
                ParamName := 'FocusMode';
                case Param of
                 0: Param := 'AUTO';
                 1: Param := 'MANUAL';
                else CaseElse;
                end;
               end;
       $1030: begin
                ParamName := 'SlowSync';
                case Param of
                 0: Param := 'OFF';
                 1: Param := 'ON';
                else CaseElse;
                end;
               end;
       $1031: begin
                ParamName := 'PictureMode';
                case Param of
                 0: Param := 'AUTO';
                 1: Param := 'PORTRAIT';
                 2: Param := 'LANDSCAPE';
                 4: Param := 'SPORTS';
                 5: Param := 'NIGHT';
                 6: Param := 'PROGRAM';
                 256: Param := 'Aperture prior';
                 512: Param := 'Shutter prior';
                 768: Param := 'MANUAL';
                else CaseElse;
                end;
               end;
       $1100: begin
                ParamName := 'ContTake/Bracket';
                case Param of
                 0: Param := 'OFF';
                 1: Param := 'ON';
                else CaseElse;
                end;
               end;
       $1300: begin
                ParamName := 'BlurWarning';
                case Param of
                 0: Param := 'NO';
                 1: Param := 'YES';
                else CaseElse;
                end;
               end;
       $1301: begin
                ParamName := 'FocusWarning';
                case Param of
                 0: Param := 'Auto Focus good';
                 1: Param := 'Out of focus';
                else CaseElse;
                end;
               end;
       $1302: begin
                ParamName := 'AEwarning';
                case Param of
                 0: Param := 'AE good';
                 1: Param := 'Over exposure';
                else CaseElse;
                end;
               end;
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadNikonMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
type
 TGetTagMode = (mode1,mode2);
var
     NIKON: Array[1..6] of Char;
GetTagMode: TGetTagMode;
         i: integer;
   Entries: Word;
        Pn: Byte;
        w1: Word;
        bb: array[0..1] of Byte absolute w1;
   iOffset: Cardinal;
        c1: Cardinal;
        ww: array[0..1] of Word absolute c1;
 ParamName: string;
     Param: Variant;
  logParam: Variant;
       Tag: TTag;
 TIFFHeader:TTIFFHeader;
    Create: boolean;

procedure AssignValue;
var
 writeOffset: Cardinal;
begin
// ���� �� ������� �� ������� ���������
 if not Create then
  if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
     ((UnknownsVisible = true) and ((Param <> Null))) then
   if FExifData.Add(MakerNoteinformation) then
    Create := true;
 if Align=$4D4D then writeOffset := InvertHex4(tag.OffSet) else writeOffset := tag.OffSet;
// ��������� ��������� � ������� ���������� � ���
 if Param <> Null then
  if ParamName <> '' then
   begin
    FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if UnknownsVisible = true then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset) + ' offset=$' + IntToHex(writeOffset, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')')
 else
  Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(writeOffset));
end;

procedure CaseElse;
var
 s:string;
begin
 s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
 ParamName := '';
 Param := s;
end;

procedure GetTagValue1;
begin
 ParamName := '';
 Pn := 0;
 Param := Null;
 logParam := Null;
 Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
 logParam := Param;
 case tag.TagID of
  $0003: begin
           ParamName := 'Quality';
           case Integer(Param) of
             1: Param := 'VGA BASIC';
             2: Param := 'VGA NORMAL';
             3: Param := 'VGA FINE';
             4: Param := 'SXGA BASIC';
             5: Param := 'SXGA NORMAL';
             6: Param := 'SXGA FINE';
             7: Param := '?XGA BASIC';
             8: Param := '?XGA NORMAL';
             9: Param := '?XGA FINE';
            10: Param := 'UXGA BASIC';
            11: Param := 'UXGA NORMAL';
            12: Param := 'UXGA FINE';
           else CaseElse;
           end;
          end;
  $0004: begin
           ParamName := 'ColorMode';
           case Integer(Param) of
             1: Param := 'COLOR';
             2: Param := 'MONOHCROME';
           else CaseElse;
           end;
          end;
  $0005: begin
           ParamName := 'ImageAdjustment';
           case Integer(Param) of
             0: Param := 'NORMAL';
             1: Param := 'BRIGHT+';
             2: Param := 'BRIGHT-';
             3: Param := 'CONTRAST+';
             4: Param := 'CONTRAST-';
           else CaseElse;
           end;
          end;
  $0006: begin
           ParamName := 'CCDsensitivity';
           case Integer(Param) of
             0: Param := 'ISO 80';
             2: Param := 'ISO 160';
             4: Param := 'ISO 320';
             5: Param := 'ISO 100';
           else CaseElse;
           end;
          end;
  $0007: begin
           ParamName := 'WhiteBalance';
           case Integer(Param) of
             0: Param := 'AUTO';
             1: Param := 'PRESET';
             2: Param := 'DAYLIGHT';
             3: Param := 'INCANDESCENSE';
             4: Param := 'FLUORESCENSE';
             5: Param := 'CLOUDY';
             6: Param := 'SPEEDLIGHT';
           else CaseElse;
           end;
          end;
  $0008: begin
           ParamName := 'Focus';
           if Param = '1/0' then Param := 'INFINITE';
          end;
  $000A: begin
           ParamName := 'DigitalZoom';
           Param := DoubleToStr(RationalToDouble(Param),0) + 'X';
          end;
  $000B: begin
           ParamName := 'Converter';
           if Param = '1' then Param := 'FISHEYE'
          end;
 end;
end;

procedure GetTagValue2;
var
  s,s1: string;
     j: cardinal;

begin
 ParamName := '';
 Pn := 0;
 Param := Null;
 logParam := Null;
 Param := ReadValue(tag.TagType,tag.Count,tag.OffSet,iOffset, Align);
 logParam := Param;
 case tag.TagID of
   $0001: begin
            ParamName := 'version'; Pn := 1;
            s := string(Param);
            if (s[1]='4') and (s[2]='8') and (s[3]=',') and (s[11]='8') and (s[10]='4') and (s[9]=',') then
             begin
              Delete(s,1,3); Delete(s,Length(s)-2,3);
              s := IntToStr(StrToInt(Copy(s,1,Pos(',',s)-1))-48)+','+IntToStr(StrToInt(Copy(s,Pos(',',s)+1,Length(s)-Pos(',',s)))-48);
              Param := s;
             end;
           end;
   $0002: begin
            ParamName := 'ISO'; Pn := 1;
            s := Param;
            Delete(s, length(s)-1, 2);
            Param := s;
           end;
   $0003: begin
            ParamName := 'Color mode'; Pn := 1;
           end;
   $0004: begin
            ParamName := 'Quality'; Pn := 2;
           end;
   $0005: begin
            ParamName := 'WhiteBalance'; Pn := 2;
           end;
   $0006: begin
            ParamName := 'ImageSharpening'; Pn := 2;
           end;
   $0007: begin
            ParamName := 'FocusMode'; Pn := 2;
           end;
   $0008: begin
            ParamName := 'FlashSetting'; Pn := 2;
           end;
   $000B: begin
            ParamName := ' WhiteBalanceBiasValue'; Pn := 2;
           end;
   $000F: begin
            ParamName := 'ISOSelection'; Pn := 2;
           end;
   $0080: begin
            ParamName := 'ImageAdjustment?'; Pn := 2;
           end;
   $0081: begin
            ParamName := 'ImageAdjustment'; Pn := 2;
           end;
   $0082: begin
            ParamName := 'Adapter'; Pn := 2;
           end;
   $0084: begin
            ParamName := 'LensInfo'; Pn := 2;
            s := string(Param) + ',';
            Param := '(';
            j := 1;
            while Length(s) > 0 do
             begin
              s1 := Copy(s,1,Pos(',',s)-1);
              Delete(s, 1, Pos(',', s));
              Param := Param + DoubleToStr(RationalToDouble(s1),0);
              if Length(s) > 0 then
               case j of
                1: Param := Param + '-';
                2: Param := Param + '),(';
                3: Param := Param + '-';
               end;
              j := j + 1;
             end;
            Param := Param + ')';
           end;
   $0085: begin
            ParamName := 'ManualFocusDistance'; Pn := 2;
           end;
   $0086: begin
            ParamName := 'DigitalZoom';
            Param := DoubleToStr(RationalToDouble(Param),0) + 'X';
           end;
   $0088: begin
            ParamName := 'AFFocusPosition'; Pn := 2;
            if Param = '0,0,0,0' then Param := 'CENTER';
            if Param = '0,1,0,0' then Param := 'TOP';
            if Param = '0,2,0,0' then Param := 'BOTTOM';
            if Param = '0,3,0,0' then Param := 'LEFT';
            if Param = '0,4,0,0' then Param := 'RIGHT';
           end;
   $008D: begin
            ParamName := 'ColorSpace'; Pn := 2;
           end;
   $0092: begin
            ParamName := 'HueAdjustment'; Pn := 2;
           end;
   $0095: begin
            ParamName := 'NoiseReduction'; Pn := 2;
           end;
//   $0010: begin ParamName := 'DataDump'; Pn := 2; end;
   $0202: begin
            ParamName := 'Macro'; Pn := 3;
            case Param of
             0: Param := 'Normal';
             1: Param := 'Macro';
            else CaseElse;
            end;
           end;
   $0204: begin
            ParamName := 'DigiZoom'; Pn := 5;
            {
            if Param=0 then Param := 'Off';
            }
           end;
   $0207: begin ParamName := 'SoftwareRelease'; Pn := 8; end;
   $0208: begin ParamName := 'PictInfo'; Pn := 9; end;
   $0209: begin
            ParamName := 'CameraID'; Pn := 10;
            s := Param;
            Param := '';
            while length(s)>0 do
             Param := Param+Chr(ExtractDigit(s));
           end;
//   $0F00: begin ParamName := 'DataDump'; Pn := 11; end;
   $100D: begin ParamName := 'ZoomPosition'; Pn := 12; end;
   $102E: begin ParamName := 'Width'; Pn := 13; end;
   $102F: begin ParamName := 'Height'; Pn := 14; end;
 end;
end;

begin
  MakerNoteinformation := 'Nikon Maker Note information';
  GetTagMode := mode2;
  if FExifData.GetByName(MakerNoteinformation) = Nil then
   Create := false
  else
   Create := true;
  Write(' -------------- Nikon MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  Seek(F, OffSet + Off0);
  BlockRead(F, NIKON, SizeOf(NIKON));
  Write(IntToHex(FilePos(F) - 6, 8) + ': '+NIKON);
  Seek(F, FilePos(F) + 4);
  iOffset := FilePos(F);
  If UpperCase(NIKON) = 'NIKON'#0 then
   begin
    BlockRead(F, TIFFHeader, 8);
    if (TIFFHeader.Align <> $4D4D) and (TIFFHeader.Align <> $4949) then
     begin
      Seek(F, iOffset - 2);
      GetTagMode := mode1;
     end;
    if TIFFHeader.Align=$4D4D then
     begin
      TIFFHeader.bytes := InvertHex2(TIFFHeader.bytes);
      TIFFHeader.OffsetIDF0 := InvertHex4(TIFFHeader.OffsetIDF0);
     end;
   end
  else
   begin
    Seek(F, OffSet + Off0);
    iOffset := Off0;
   end;

  BlockRead(F, Entries, 2);
  if Align=$4D4D then Entries := InvertHex2(Entries);
  case GetTagMode of
   mode1: Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries) + '   (mode 1)');
   mode2: Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(Entries) + '   (mode 2)');
  end;
  for i := 1 to Entries do
   begin
// ������ ��������� ���
    BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align=$4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType<1) or (tag.TagType>12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
// ������ ��������
    case GetTagMode of
     mode1: GetTagValue1;
     mode2: GetTagValue2;
    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadCanonMakerNote(const OffSet, Off0, Leng: Cardinal; const Align: Word);
var
         i,j: integer;
     Entries: Word;
          Pn: Byte;
          w1: Word;
          bb: array[0..1] of Byte absolute w1;
          c1: Cardinal;
          ww: array[0..1] of Word absolute c1;
       r1,r2: Single;
   ParamName: string;
       Param: Variant;
    logParam: Variant;
         Tag: TTag;
        s,ss: string;
      Create: boolean; // ������� ������� ���������

 procedure AssignValue;
 begin
  // ���� �� ������� �� ������� ���������
  if not Create then
   if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
      ((UnknownsVisible = true) and ((Param <> Null))) then
    if FExifData.Add(MakerNoteinformation) then
     Create := true;
  // ��������� ��������� � ������� ���������� � ���
  if Param <> Null then
   if ParamName <> '' then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem(ParamName, Pn, Tag.TagType, Param, False);
     Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(tag.OffSet) + ' offset=$' + IntToHex(tag.OffSet, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
    end
   else
    if UnknownsVisible = true then
     begin
      FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(Tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
      Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':'+' (!) TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(tag.OffSet));
     end
    else
     Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':'+' (!) TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(tag.OffSet));
 //  Write(IntToHex(mFile.Position-SizeOf(tag),8)+':'+' (!) TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(tag.OffSet)+' offset=$'+IntToHex(tag.OffSet,4)+') = "'+string(Param)+'" ('+string(logParam)+')');
 end;

 procedure AssignSubValue;
 begin
 // ���� �� ������� �� ������� ���������
  if not Create then
   if ((UnknownsVisible = false) and (Param <> Null) and (ParamName <> '')) or
      ((UnknownsVisible = true) and ((Param <> Null))) then
    if FExifData.Add(MakerNoteinformation) then
     Create := true;
 // ��������� ��������� � ������� ���������� � ���
  if Param <> Null then
   if ParamName <> '' then
    begin
     FExifData.GetByName(MakerNoteinformation).AddItem(ParamName,Pn,Tag.TagType,Param, False);
     Write('             '+IntToStr(j)+' '+ParamName+' = "'+string(Param)+'" ('+string(logParam)+')');
    end
   else
    if UnknownsVisible = true then
     begin
      FExifData.GetByName(MakerNoteinformation).AddItem('($' + IntToHex(Tag.TagID, 4) + ', sub ' + IntToStr(j) + ',' + IntToStr(Tag.TagType) + ')', Pn, Tag.TagType, Param, True);
      Write('             '+IntToStr(j)+' (!) Unknown = '+string(Param))
     end
    else
     Write('             '+IntToStr(j)+' (!) Unknown')
 end;

 procedure CaseElse;
 var
  s:string;
 begin
  s := '(Tag $'+IntToHex(tag.TagID,4)+') '+'(!) '+IntToStr(Param);
  ParamName := '';
  Param := s;
 end;

begin
  MakerNoteinformation := 'Canon Maker Note information';
  if FExifData.GetByName(MakerNoteinformation) = nil then
   Create := false
  else
   Create := true;
  Write(' -------------- Canon MakerNote Information (MakerNote pointer) -------------------------------------------------------------');
  Seek(F, OffSet + Off0);
  BlockRead(F, Entries, 2);
  if Align=$4D4D then Entries := InvertHex2(Entries);
  Write(IntToHex(FilePos(F) - 2,8) + ': Entries: ' + IntToStr(Entries));
  for i := 1 to Entries do
   begin
    // ������ ��������� ���
    BlockRead(F, tag, 12);
    // ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
    if Align = $4D4D then
     begin
      tag.TagID := InvertHex2(tag.TagID);
      tag.TagType := InvertHex2(tag.TagType);
      tag.Count := InvertHex4(tag.Count);
     end;
    // ��� ���� ����� ���� ������ �� 1 � �� 12
    if (tag.TagType < 1) or (tag.TagType > 12) then
     begin
      Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
      Break;
     end;
    // ������ ��������
    Param := ReadValue(tag.TagType, tag.Count, tag.OffSet, off0, Align);
    logParam := Param;
    ParamName := '';
    case tag.TagID of
       $0001: begin
                Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':  (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(tag.OffSet) + ' offset=$' + IntToHex(tag.OffSet, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
                s := Param;
                j := 1;
                Param := StrToInt(Copy(s, 1, Pos(',', s) - 1));
                Delete(s, 1, Pos(',', s));
                if Param <> Tag.Count * 2 then Write(' count ' + IntToStr(Tag.Count * 2) + ' <> ' + IntToStr(Param));
                 while length(s) <> 0 do
                  begin
                   Param := ExtractDigit(s);
                   logParam := Param;
                   ParamName := '';
                   case j of
                     1: begin
                         ParamName := 'MacroMode'; Pn := 1;
                         case Param of
                          1: Param := 'macro';
                          2: Param := 'off';
                         else CaseElse;
                         end;
                        end;
                     2: begin
                         ParamName := 'Self-timer'; Pn := 1;
                         if Param=0 then Param := 'off'
                         else
                          begin
                           Param := IntToStr(Trunc(Param/10))+' Sec';
                          end;
                        end;
                     3: begin
                         ParamName := 'Quality'; Pn := 1;
                         case Param of
                          2: Param := 'normal';
                          3: Param := 'fine';
                          5: Param := 'superfine';
                         else CaseElse;
                         end;
                        end;
                     4: begin
                         ParamName := 'FlashMode'; Pn := 1;
                         case Param of
                           0: Param := 'flash not fired';
                           1: Param := 'auto';
                           2: Param := 'on';
                           3: Param := 'red-eye reduction';
                           4: Param := 'slow synchro';
                           5: Param := 'auto + red-eye reduction';
                           6: Param := 'on + red-eye reduction';
                          16: Param := 'external flash';
                         else CaseElse;
                         end;
                        end;
                     5: begin
                         ParamName := 'ContinuousDriveMode'; Pn := 1;
                         case Param of
                           0: Param := 'single';
                           1: Param := 'continuous';
                           2: Param := 'self timer';
                         else CaseElse;
                         end;
                        end;
                     7: begin
                         ParamName := 'FocusMode'; Pn := 1;
                         case Param of
                           0: Param := 'One-Shot';
                           1: Param := 'AI Servo';
                           2: Param := 'AI Focus';
                           3: Param := 'manual 3';
                           4: Param := 'Single';
                           5: Param := 'Continuous';
                           6: Param := 'manual 6';
                         else CaseElse;
                         end;
                        end;
                    10: begin
                         ParamName := 'ImageSize'; Pn := 1;
                         case Param of
                           0: Param := 'large';
                           1: Param := 'medium';
                           2: Param := 'small';
                         else CaseElse;
                         end;
                        end;
                    11: begin
                         ParamName := 'EasyShootingMode'; Pn := 1;
                         case Param of
                           0: Param := 'Full Auto';
                           1: Param := 'Manual';
                           2: Param := 'Landscape';
                           3: Param := 'Fast Shutter';
                           4: Param := 'Slow Shutter';
                           5: Param := 'Night';
                           6: Param := 'B&W';
                           7: Param := 'Sepia';
                           8: Param := 'Portrait';
                           9: Param := 'Sports';
                          10: Param := 'Macro / Close-Up';
                          11: Param := 'Pan Focus';
                         else CaseElse;
                         end;
                        end;
                    12: begin
                         ParamName := 'DigitalZoom'; Pn := 1;
                         case Param of
                           0: Param := 'off';
                           1: Param := '2x';
                           2: Param := '4x';
                         else CaseElse;
                         end;
                        end;
                    13: begin
                         ParamName := 'Contrast'; Pn := 1;
                         case Param of
                          $ffff: Param := 'low';
                          $0000: Param := 'normal';
                          $0001: Param := 'high';
                         else CaseElse;
                         end;
                        end;
                    14: begin
                         ParamName := 'Saturation'; Pn := 1;
                         case Param of
                          $ffff: Param := 'low';
                          $0000: Param := 'normal';
                          $0001: Param := 'high';
                         else CaseElse;
                         end;
                        end;
                    15: begin
                         ParamName := 'Sharpness'; Pn := 1;
                         case Param of
                          $ffff: Param := 'low';
                          $0000: Param := 'normal';
                          $0001: Param := 'high';
                         else CaseElse;
                         end;
                        end;
                    16: begin
                         ParamName := 'ISO'; Pn := 1;
                         case Param of
                           0: ParamName := '';
                          15: Param := 'auto';
                          16: Param := '50';
                          17: Param := '100';
                          18: Param := '200';
                          19: Param := '400';
                         else CaseElse;
                         end;
                        end;
                    17: begin
                         ParamName := 'MeteringMode'; Pn := 1;
                         case Param of
                          3: Param := 'Evaluative';
                          4: Param := 'Partial';
                          5: Param := 'Center-weighted';
                         else CaseElse;
                         end;
                        end;
                    18: begin
                         ParamName := 'FocusType'; Pn := 1;
                         case Param of
                          0: Param := 'manual';
                          1: Param := 'auto';
                          3: Param := 'close-up (macro)';
                          8: Param := 'locked (pan mode)';
                         else CaseElse;
                         end;
                        end;
                    19: begin
                         ParamName := 'AFpointSelected'; Pn := 1;
                         case Param of
                              0: ParamName := '';
                          $3000: Param := 'none';
                          $3001: Param := 'auto-selected';
                          $3002: Param := 'right';
                          $3003: Param := 'center';
                          $3004: Param := 'left';
                         else CaseElse;
                         end;
                        end;
                    20: begin
                         ParamName := 'ExposureMode'; Pn := 1;
                         case Param of
                          0: Param := 'Easy shooting';
                          1: Param := 'Program';
                          2: Param := 'Tv-priority';
                          3: Param := 'Av-priority';
                          4: Param := 'Manual';
                          5: Param := 'A-DEP';
                         else CaseElse;
                         end;
                        end;
                    23: begin
                         ParamName := ''; Pn := 0;
                         AssignSubValue;
                         j := j + 1;
                         r2 := Param;
                         Param := ExtractDigit(s);
                         logParam := Param;
                         AssignSubValue;
                         j := j + 1;
                         r1 := Param;
                         Param := ExtractDigit(s);
                         logParam := Param;
                         if Param <> 0 then
                          begin
                           if (r1 mod Param <> 0) or (r2 mod Param <> 0) then
                            Param := DoubleToStr(r1 / Param, 1) + '-' + DoubleToStr(r2/Param, 1) + ' (mm)'
                           else
                            Param := IntToStr(Trunc(r1 div Param)) + '-' + IntToStr(Trunc(r2 div Param)) + ' (mm)';
                          end;
                         ParamName := 'FocalLengthOfLens'; Pn := 1;
                        end;
                    28: begin
                         ParamName := 'FlashActivity'; Pn := 1;
                         case Param of
                          0: Param := 'not fire';
                          1: Param := 'fired';
                         else CaseElse;
                         end;
                        end;
                    29: begin
                         ParamName := 'FlashDetail'; Pn := 1; ss := '';
                         if (Param and 16384) {14bit} then ss := ss+'(external E-TTL) ';
                         if (Param and 8192) {13bit} then ss := ss+'(internal flash) ';
                         if (Param and 2048) {11bit} then ss := ss+'(FP sync used) ';
                         if (Param and 128) {7bit} then ss := ss+'(2nd("rear")-curtain sync used) ';
                         if (Param and 8) {4bit} then ss := ss+'(FP sync enabled) ';
                         if ss='' then ParamName := '' else Param := ss;
                        end;
                    32: begin
                         ParamName := 'FocusMode'; Pn := 1;
                         case Param of
                              0: Param := 'Single';
                              1: Param := 'Continuous';
                         else CaseElse;
                         end;
                        end;

                   else
                   end;
                   Tag.TagType := 1;
                   AssignSubValue;
                   j := j + 1;
                  end;
                Continue;
               end;
       $0004: begin
                Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':  (TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(tag.OffSet)+' offset=$'+IntToHex(tag.OffSet,4)+') = "'+string(Param)+'" ('+string(logParam)+')');
                s := Param;
                j := 1;
                Param := StrToInt(Copy(s,1,Pos(',',s)-1));
                Delete(s,1,Pos(',',s));
                if Param=Tag.Count*2 then
                 while length(s) <> 0 do
                  begin
                   Param := ExtractDigit(s);
                   logParam := Param;
                   ParamName := '';
                   case j of
                     7: begin
                         ParamName := 'WhiteBalance'; Pn := 1;
                         case Param of
                          0: Param := 'auto';
                          1: Param := 'Sunny';
                          2: Param := 'Cloudy';
                          3: Param := 'Tungsten';
                          4: Param := 'Flourescent';
                          5: Param := 'Flash';
                          6: Param := 'Custom';
                         else CaseElse;
                         end;
                        end;
                     9: begin
                         ParamName := 'SequenceNumber'; Pn := 1;
                         if Param<>0 then Param := string(Param)+' (continuous mode)';
                        end;
                    14: begin
                         ParamName := 'AFpointUsed'; Pn := 1; ss := '';
                         if (Param and 1) {0bit} then ss := 'right';
                         if (Param and 2) {1bit} then ss := 'center';
                         if (Param and 4) {2bit} then  ss := 'left';
                         if ss='' then ParamName := '' else Param := ss;
                        end;
                    15: begin
                         ParamName := 'FlashBias'; Pn := 1;
                         case Param of
                          $FFC0: Param := '-2 EV';
                          $FFCC: Param := '-1.67 EV';
                          $FFD0: Param := '-1.50 EV';
                          $FFD4: Param := '-1.33 EV';
                          $FFE0: Param := '-1 EV';
                          $FFEC: Param := '-0.67 EV';
                          $FFF0: Param := '-0.50 EV';
                          $FFF4: Param := '-0.33 EV';
                          $0000: Param := '0 EV';
                          $000C: Param := '0.33 EV';
                          $0010: Param := '0.50 EV';
                          $0014: Param := '0.67 EV';
                          $0020: Param := '1 EV';
                          $002C: Param := '1.33 EV';
                          $0030: Param := '1.50 EV';
                          $0034: Param := '1.67 EV';
                          $0040: Param := '2 EV';
                         else CaseElse;
                         end;
                        end;
                    19: begin
                         ParamName := 'SubjectDistance'; Pn := 1;
                        end;

                   else
                   end;
                   Tag.TagType := 1;
                   AssignSubValue;
                   j := j+1;
                  end;
                Continue;
               end;
       $0006: begin ParamName := 'ImageType'; Pn := 1; end;
       $0007: begin ParamName := 'Firmware version'; Pn := 2; end;
       $0008: begin
                ParamName := 'ImageNumber'; Pn := 2;
                s := string(Param);
                if (Length(s))>4 then
                 begin
                  Insert('-',s,Length(s)-3);
                  Param := s;
                 end;
               end;
       $0009: begin ParamName := 'OwnerName'; Pn := 2; end;
       $000C: begin
                ParamName := 'CameraSerialNumber'; Pn := 2;
{  �� ������� ����� ��� ���� ��������
                c1 := Param;
                ss := IntToHex(ww[1],2);
                while Length(ss)<4 do ss := '0'+ss;
                Param := ss+'-';
                ss := IntToStr(ww[0]);
                while Length(ss)<5 do ss := '0'+ss;
                Param := Param+ss;
}                
               end;
       $000F: begin
                Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':  (TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(tag.OffSet)+' offset=$'+IntToHex(tag.OffSet,4)+') = "'+string(Param)+'" ('+string(logParam)+')');
                s := Param;
                j := 1;
                Param := StrToInt(Copy(s,1,Pos(',',s)-1));
                Delete(s,1,Pos(',',s));
                if Param <> Tag.Count * 2 then Write(' count ' + IntToStr(Tag.Count * 2) + ' <> ' + IntToStr(Param));
                 while length(s) <> 0 do
                  begin
                   Param := ExtractDigit(s);
                   ParamName := '';
                   w1 := integer(Param);
                   logParam := bb[0];
                   case bb[1] of
                     1: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':LongExposureNoiseReduction'; Pn := 1;
                         case bb[0] of
                          0: Param := 'off';
                          1: Param := 'on';
                         else CaseElse;
                         end;
                        end;
                     2: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':Shutter/AE-lock buttons'; Pn := 1;
                         case bb[0] of
                          0: Param := 'AF/AE lock';
                          1: Param := 'AE lock/AF';
                          2: Param := 'AF/AF lock';
                          3: Param := 'AE+release/AE+AF';
                         else CaseElse;
                         end;
                        end;
                     3: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':MirrorLockup'; Pn := 1;
                         case bb[0] of
                          0: Param := 'Disable';
                          1: Param := 'Enable';
                         else CaseElse;
                         end;
                        end;
                     4: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':Tv/AvAndExposureLevel'; Pn := 1;
                         case bb[0] of
                          0: Param := '1/2 stop';
                          1: Param := '1/3 stop';
                         else CaseElse;
                         end;
                        end;
                     5: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':AF-AssistLight'; Pn := 1;
                         case bb[0] of
                          0: Param := 'On (auto)';
                          1: Param := 'Off';
                          2: Param := 'Only Ext Flash';
                         else CaseElse;
                         end;
                        end;
                     6: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':ShutterSpeedInAVMode'; Pn := 1;
                         case bb[0] of
                          0: Param := 'auto';
                          1: Param := 'fixed';
                         else CaseElse;
                         end;
                        end;
                     7: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':AEBSequence/AutoCancellation'; Pn := 1;
                         case bb[0] of
                          0: Param := '0, -, + / Enabled';
                          1: Param := '0, -, + / Disabled';
                          2: Param := '-, 0, + / Enabled';
                          3: Param := '-, 0, + / Disabled';
                         else CaseElse;
                         end;
                        end;
                     8: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':ShutterCurtainSync'; Pn := 1;
                         case bb[0] of
                          0: Param := 'First curtain';
                          1: Param := 'Second curtain';
                         else CaseElse;
                         end;
                        end;
                     9: begin
                         ParamName := 'Fn'+IntToStr(bb[1])+':LensAFstopButtonFnSwitch'; Pn := 1;
                         case bb[0] of
                          0: Param := 'AF stop';
                          1: Param := 'Operate AF';
                          2: Param := 'Lock AE and start timer';
                         else CaseElse;
                         end;
                        end;
                     10: begin
                          ParamName := 'Fn'+IntToStr(bb[1])+':AutoReductionOfFillFlash'; Pn := 1;
                          case bb[0] of
                           0: Param := 'Enable';
                           1: Param := 'Disable';
                          else CaseElse;
                          end;
                         end;
                     11: begin
                          ParamName := 'Fn'+IntToStr(bb[1])+':MenuButtonReturnPosition'; Pn := 1;
                          case bb[0] of
                           0: Param := 'Top';
                           1: Param := 'Previous (volatile)';
                           2: Param := 'Previous';
                          else CaseElse;
                          end;
                         end;
                     12: begin
                          ParamName := 'Fn'+IntToStr(bb[1])+':SETButtonFuncWhenShooting'; Pn := 1;
                          case bb[0] of
                           0: Param := 'Not assigned';
                           1: Param := 'Change quality';
                           2: Param := 'Change ISO speed';
                           3: Param := 'Select parameters';
                          else CaseElse;
                          end;
                         end;
                     13: begin
                          ParamName := 'Fn'+IntToStr(bb[1])+':SensorCleaning'; Pn := 1;
                          case bb[0] of
                           0: Param := 'Disable';
                           1: Param := 'Enable';
                          else CaseElse;
                          end;
                         end;
                     14: begin
                          ParamName := 'Fn'+IntToStr(bb[1])+':SuperimposedDisplay'; Pn := 1;
                          case bb[0] of
                           0: Param := 'Enable';
                           1: Param := 'Disable';
                          else CaseElse;
                          end;
                         end;
                     15: begin
                          ParamName := 'Fn'+IntToStr(bb[1])+':Fn15.ShutterReleaseW/oCFcard'; Pn := 1;
                          case bb[0] of
                           0: Param := 'Possible w/out CF card';
                           1: Param := 'Not possible';
                          else CaseElse;
                          end;
                         end;

                   else
                   end;
                   Tag.TagType := 1;
                   AssignSubValue;
                   j := j+1;
                  end;
                Continue;
               end;

    end;
// ��������� ��������� � ������� ���������� � ���
    AssignValue;
   end;
end;

procedure TExif.ReadFromFile;
//const ori: Array[1..8] of string=('Normal','Mirrored','Rotated 180','Rotated 180, mirrored','Rotated 90 left, mirrored','Rotated 90 right','Rotated 90 right, mirrored','Rotated 90 left');
var
            PIM: Array[1..8] of Char;
            APP: TAPP;
  JFIF_APP_Exif: Cardinal;
        JFIFrec: TJFIFrec;
         Marker: TJFIFMarker;
           off0: Cardinal; //Null Exif Offset
            tag: TTag;
       Position: Cardinal;
            i,j: Integer;
        NextIFD: Cardinal;
       NextIFD2: Cardinal;
           Pn,b: Byte;
      ParamName: string;
          Param: Variant;
       logParam: Variant;
         GroupP: TParamList;
          s, ss: string;

 procedure AssignValue;
 begin
  if Pos(#0#0#0, ParamName) = 1 then
   begin
    Delete(ParamName, 1, 3);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': => ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + '  offset=$' + IntToHex(tag.OffSet, 4));
   end
  else
  if (Param <> Null) and (ParamName <> '') then
   begin
    if GroupP <> Nil then GroupP.AddItem(ParamName, Pn, Tag.TagType, Param, false);
    Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ': ' + ParamName + ' (TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(tag.OffSet) + ' offset=$' + IntToHex(tag.OffSet, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
   end
  else
   if Param<>Null then
    begin
     if UnknownsVisible = true then
      begin
       if GroupP <> Nil then GroupP.AddItem('($' + IntToHex(Tag.TagID, 4) + ',' + IntToStr(Tag.TagType) + ',' + IntToStr(Tag.Count) + ',$' + IntToHex(Tag.OffSet, 1) + ')', Pn, Tag.TagType, Param, True);
       Write(IntToHex(FilePos(F) - SizeOf(tag), 8) + ':' + ' (!) TagID $' + IntToHex(tag.TagID, 4) + ' Type=' + IntToStr(tag.TagType) + ' count=' + IntToStr(tag.Count) + ' offset=' + IntToStr(tag.OffSet) + ' offset=$' + IntToHex(tag.OffSet, 4) + ') = "' + string(Param) + '" (' + string(logParam) + ')');
      end
     else
      Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':'+' (!) TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(tag.OffSet)+' offset=$'+IntToHex(tag.OffSet,4)+') = "'+string(Param)+'" ('+string(logParam)+')');
    end
   else
    Write(IntToHex(FilePos(F)-SizeOf(tag),8)+':'+' (!) TagID $'+IntToHex(tag.TagID,4)+' Type='+IntToStr(tag.TagType)+' count='+IntToStr(tag.Count)+' offset='+IntToStr(tag.OffSet));
 end;

 procedure ReadJFIF;
 var
   Save: cardinal;
   s: string;
   b: Byte;
 begin
   Save := FilePos(F);
   BlockRead(F, JFIFrec, SizeOf(JFIFrec));
   s := '';
   repeat
    BlockRead(F, b, SizeOf(b));
    s := s + Chr(b);
   until b = 0;
   Write(IntToHex(FilePos(F) - SizeOf(JFIFrec), 8) + ': JFIF APP' + IntToStr(Marker.Marker.Number - $E0) + ': Marker: $' + IntToHex(InvertHex2(JFIFrec.Marker.Word), 2) + '(' + IntToStr(JFIFrec.Marker.Number) + ')   Length: ' + IntToStr(InvertHex2(JFIFrec.Length)) + ' ($' + IntToHex(InvertHex2(JFIFrec.Length), 2) + ')   Identifier: ' + s);
   FJFIFmarker := true;
   Seek(F, Save);
 end;

 function ReadJPEGHeader: Boolean;
 Var
   i: int64;
   s: string;
 // 2 bytes SOI marker. FF D8 (Start Of Image)
 // 2 bytes EOI marker. FF D9 (End Of Image)
 begin
 // Every JPEG file starts from binary value '0xFFD8',
 // ends by binary value '0xFFD9'
   Result := false;
   Seek(F, 0);
   BlockRead(F, SOI.Identifier, 2);
   if SOI.Identifier=$D8FF then //Is this Jpeg begin
    begin
     SOI.Offset := 0;
     i := FileSize(F) - 2;
     EOI.Identifier := 0;
     while (EOI.Identifier<>$D9FF) and (i>2) do
      begin
       Seek(F, i);
       BlockRead(F, EOI.Identifier, 2);
       i := i-1;
      end;
     if EOI.Identifier=$D9FF then
      begin
       FTypeOfFile := tfJPEG; //Is this Jpeg end
       Write('File format: JPEG');
       log.Add('FileSize: '+IntToStr(FileSize(F))+' ($'+IntToHex(FileSize(F),8)+')');
       log.Add('00000000: SOI');
       i := i+1; EOI.Offset := i;
       s := IntToHex(i,4);
       while length(s) < 8 do s := '0' + s;
       if i < (FileSize(F)-3) then
        log.Add(s + ': EOI  (-' + IntToStr((FileSize(F) - 3) - i) + ')')
       else log.Add(s + ': EOI (end of file)');
       Result := true;
      end;
    end;
 end;

 function ReadTIFFHeader(var TIFFHeader: TTIFFHeader): Boolean;
 begin
   Result := false;
   Seek(F, 0);
   BlockRead(F, TIFFHeader, 8);
   if (TIFFHeader.Align=$4949) and (TIFFHeader.bytes=$002A) then //Is this Intel align
    begin
     FTypeOfAlignTIFF := 'intel';
     FTypeOfFile := tfTIFF;
     Seek(F, TIFFHeader.OffsetIDF0);
     Result := true;
    end;
   if (TIFFHeader.Align=$4D4D) and (TIFFHeader.bytes=$2A00) then //Is this Motorola align
    begin
     FTypeOfAlignTIFF := 'motorola';
     FTypeOfFile := 'TIFF';
     Seek(F, TIFFHeader.OffsetIDF0);
     Result := true;
    end;
   if Result then
    try
     Write('File format: TIFF');
     Write('00000000: TIFF Align $'+IntToHex(TIFFHeader.Align,2)+' 2 bytes = $'+IntToHex(TIFFHeader.bytes,4)+' OffSet = $'+IntToHex(TIFFHeader.OffsetIDF0,4)+' OffSet = '+IntToStr(TIFFHeader.OffsetIDF0));
    except
    end;
 end;

 procedure WriteNoExif;
 begin
  Write('No Exif Header in this file!');
 end;

begin
  FTickCount := GetTickCount;
  FLogTickCount := GetTickCount;
  Init;
  Pn := 0;
  FLog.Clear;
  FValidExif := False;
  Write('Opening: ' + FFileName);
  {$I-}
  AssignFile(F, FFileName);
  FileMode := fmOpenRead;
  Reset(F, 1);
  {$I+}
  if IOResult <> 0 then
   begin
    Write('File not found');
    Exit;
   end;

  if FileSize(F) < 8 then Exit;

  JFIF_APP_Exif := 0;

  if ReadJPEGHeader then
   begin
// ������ ��������� JPEG
    Seek(F, 2);
    i := 0;
    repeat
     i := i + 1;
     Position := FilePos(F);
     if FilePos(F) < FileSize(F) - 1 then
      BlockRead(F, Marker, SizeOf(Marker));
     Marker.Length := InvertHex2(Marker.Length);
     s := '';
     Repeat
      BlockRead(F, b, SizeOf(b));
      s := s + Chr(b);
     Until b = 0;
     Seek(F, FilePos(F) - SizeOf(Marker));

     if (Marker.Marker.FF <> $FF) or
        (
        (Marker.Marker.FF = $FF) and
        (Marker.Marker.Number - $E0 < 0)
        ) then
      Break;

     if (Marker.Marker.Number = $E1) and (s = 'Exif'#0) then
      JFIF_APP_Exif := Position;

     Seek(F, Position);
     ReadJFIF;

     Seek(F, Position + 2 + Marker.Length);
    Until i = 15;
    if JFIF_APP_Exif = 0 then
     begin
      WriteNoExif;
      CloseFile(F);
      Exit;
     end;
    Seek(F, JFIF_APP_Exif);
    BlockRead(F, APP, SizeOf(APP));
    Off0 := FilePos(F) - 10;
    if APP.TIFFHeader.Align = $4D4D then
     begin
      APP.TIFFHeader.bytes := InvertHex2(APP.TIFFHeader.bytes);
      APP.TIFFHeader.OffsetIDF0 := InvertHex4(APP.TIFFHeader.OffsetIDF0);
      APP.Entries := InvertHex2(APP.Entries);
     end;
   end
  else
   if ReadTIFFHeader(APP.TIFFHeader) then
    begin
// ������ ��������� TIFF
     BlockRead(F, APP.Entries, 2);
     off0 := 0;
     if APP.TIFFHeader.Align=$4D4D then APP.Entries := InvertHex2(APP.Entries);
     Write(IntToHex(FilePos(F)-2,8)+': Numbers of Entries '+IntToStr(APP.Entries));
    end
   else
    begin
     WriteNoExif;
     CloseFile(F);
     Exit;
    end;

// ������� ���������� �� APP ---------------
  Write('   ||   APP: MarkerPrefix: $'+IntToHex(APP.MarkerPrefix,2)+'     APP# $'+IntToHex(APP.Number,2)+'     Length: '+IntToStr(APP.Length)+' ($'+IntToHex(APP.Length,2)+')');
  Write('   ||        TIFF Align $'+IntToHex(APP.TIFFHeader.Align,2)+'    2 bytes = $'+IntToHex(APP.TIFFHeader.bytes,4)+'    OffSet = '+IntToStr(APP.TIFFHeader.OffsetIDF0)+' ($'+IntToHex(APP.TIFFHeader.OffsetIDF0,4)+')');
  Write('   ||        Numbers of Entries '+IntToStr(APP.Entries)+' ($'+IntToHex(APP.Entries,2)+')');
// -----------------------------------------
  FValidExif := True;

//............................................................
//............................................................
//  SetLength(FileArray, FileSize(F));
//  for i := 0 to FileSize(F) - 1 do  FileArray[i] := 46;
//............................................................
//............................................................

  Write(' -------------- 0th IFD for Primary Image Data -------------------------------------------------------------');
// ---------------------------
// ������ �������� ����������.
// ---------------------------
 Ch := '#'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if APP.Entries > 0 then
   if FExifData.Add('Main information') then
    for i := 1 to APP.Entries do
     begin
      // ������ ��������� ���
      BlockRead(F, tag, 12);
      // ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
      if APP.TIFFHeader.Align=$4D4D then
       begin
        tag.TagID := InvertHex2(tag.TagID);
        tag.TagType := InvertHex2(tag.TagType);
        tag.Count := InvertHex4(tag.Count);
       end;
      GroupP := FExifData.GetByName('Main information');
      GetTagValue(Tag, APP.TIFFHeader.Align, Off0, ParamName, Param, logParam);
      AssignValue;
     end;
  // ������ ��������� �� ��������� IFD -----------------------
  BlockRead(F, NextIFD, 4);
  if APP.TIFFHeader.Align=$4D4D then NextIFD := InvertHex4(NextIFD);
  Write(IntToHex(FilePos(F)-4,8)+': Next IDF OffSet = '+IntToStr(NextIFD));

// ---------------------------------
// ������ �������������� ����������.
// ---------------------------------
 Ch := '%'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if IFDpointer > 0 then
   begin
    Seek(F, IFDpointer + Off0);
    BlockRead(F, APP.Entries, 2);
    if APP.TIFFHeader.Align = $4D4D then
     APP.Entries := InvertHex2(APP.Entries);
    Write(' -------------- SUB Information (IFD pointer) -------------------------------------------------------------');
    Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(APP.Entries));
    if APP.Entries > 0 then
     if FExifData.Add('Additional information') then
      for i := 1 to APP.Entries do
       begin
        BlockRead(F, tag, 12);
        if APP.TIFFHeader.Align = $4D4D then
         begin
          tag.TagID := InvertHex2(tag.TagID);
          tag.TagType := InvertHex2(tag.TagType);
          tag.Count := InvertHex4(tag.Count);
         end;
        // ��� ���� ����� ���� ������ �� 1 � �� 12
        if (tag.TagType < 1) or (tag.TagType > 12) then
         begin
          Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
          Break;
         end;
        // ������ ��������
        GroupP := FExifData.GetByName('Additional information');
        GetTagValue(Tag, APP.TIFFHeader.Align, Off0, ParamName, Param, logParam);
        AssignValue;
       end;
    // ������ ��������� �� ��������� IFD
    BlockRead(F, NextIFD2, 4);
    if APP.TIFFHeader.Align = $4D4D then
     NextIFD2 := InvertHex4(NextIFD2);
    Write('Next IDF OffSet = ' + IntToStr(NextIFD2));
   end;

// ----------------------
// ������ GPS ����������.
// ----------------------
 Ch := '&'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if GPSpointer > 0 then
   begin
//    asm int 3 end; // ����� ��������
//    Write('!!!!!  Found GPSpointer information !!!!!'+#13+'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    Seek(F, GPSpointer + Off0);
    BlockRead(F, APP.Entries, 2);
    if APP.TIFFHeader.Align = $4D4D then APP.Entries := InvertHex2(APP.Entries);
    Write(IntToHex(FilePos(F), 8) + ': ------ GPS information ------------------------------------------------------------------------------------');
    Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(APP.Entries));
//    ShowMessage(IntToStr(FilePos(f))+'  '+IntToStr(IFDpointer));
    if APP.Entries > 0 then
     if FExifData.Add('GPS information') then
      for i := 1 to APP.Entries do
       begin
// ������ ��������� ���
        BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
        if APP.TIFFHeader.Align = $4D4D then
         begin
          tag.TagID := InvertHex2(tag.TagID);
          tag.TagType := InvertHex2(tag.TagType);
          tag.Count := InvertHex4(tag.Count);
         end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
        if (tag.TagType < 1) or (tag.TagType > 12) then
         begin
          Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
          Break;
         end;
// ������ ��������
        GroupP := FExifData.GetByName('GPS information');
        GetTagValue(Tag, APP.TIFFHeader.Align, Off0, ParamName, Param, logParam);
        AssignValue;
       end;
{
// ������ ��������� �� ��������� IFD
    mFile.Read(NextIFD2,4);
    if APP.TIFFHeader.Align=$4D4D then NextIFD2 := InvertHex4(NextIFD2);
    Write('Next IDF OffSet = '+IntToStr(NextIFD2));
}
   end;

// ---------------------------------
// ������ MakerNote ����������.
// ---------------------------------
 Ch := '0'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if MakerNote.Length > 0 then
   repeat
    If MakerNote.Owner = 'CANON' then
     begin
      ReadCanonMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'NIKON' then
     begin
      ReadNikonMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'OLYMPUS' then
     begin
      ReadOlympusMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'FUJIFILM' then
     begin
      ReadFujifilmMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'CASIO' then
     begin
      ReadCasioMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'SANYO' then
     begin
      ReadSanyoMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'MINOLTA' then
     begin
      ReadMinoltaMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'SONY' then
     begin
      ReadSonyMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    If MakerNote.Owner = 'LEICA' then
     begin
      ReadLeicaMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
      Break;
     end;
    ReadUnknownMakerNote(MakerNote.OffSet,Off0,MakerNote.Length,APP.TIFFHeader.Align);
   until true;

// ----------------------
// ������ Print Image Matching ����������.
// ----------------------
 Ch := '&'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if PrintIM.Length > 0 then
   begin
    Seek(F, PrintIM.OffSet + Off0);
    BlockRead(F, PIM, Length(PIM));
    if PIM = 'PrintIM'#0 then
     if FExifData.Add('Print Image Matching') then
      begin
       BlockRead(F, PIM, 5);
       ParamName := 'Version';
       if (PIM[1] = '0') and (PIM[4] = '0') then
        begin
         s := Copy(string(PIM), 2, 2);
         Insert(',', s, 2);
         Param := s;
         GroupP := FExifData.GetByName('Print Image Matching');
         AssignValue;
        end;
       BlockRead(F, APP.Entries, 2);
       if APP.TIFFHeader.Align = $4D4D then APP.Entries := InvertHex2(APP.Entries);
       Write(IntToHex(FilePos(F), 8) + ': ------ Print image matching information ------------------------------------------------------------------------------------');
       Write(IntToHex(FilePos(F) - 2, 8) + ': Entries: ' + IntToStr(APP.Entries));
       if APP.Entries > 0 then
        for i := 1 to APP.Entries do
         begin
// ������ ������ �� ���� �� ���������� !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         end;
     end;
{
// ������ ��������� �� ��������� IFD
    mFile.Read(NextIFD2,4);
    if APP.TIFFHeader.Align=$4D4D then NextIFD2 := InvertHex4(NextIFD2);
    Write('Next IDF OffSet = '+IntToStr(NextIFD2));
}
   end;


// ----------------------------------------
// ������ ������ IFD (IMAGE FILE DIRECTORY)
// ----------------------------------------
 Ch := '@'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  if NextIFD > 0 then
   begin
    Seek(F, NextIFD + Off0);
    BlockRead(F, APP.Entries, 2);
    if APP.TIFFHeader.Align=$4D4D then APP.Entries := InvertHex2(APP.Entries);
    Write(IntToHex(FilePos(F),8)+': ------ 1th IFD for Thumbnail Data -------------------------------------------------------------');
    Write(IntToHex(FilePos(F)-2,8)+': Entries: '+IntToStr(APP.Entries));
//    ShowMessage(IntToStr(FilePos(f))+'  '+IntToStr(IFDpointer));
    if APP.Entries>0 then
     if FExifData.Add('Thumbnail information') then
      for i := 1 to APP.Entries do
       begin
// ������ ��������� ���
        BlockRead(F, tag, 12);
// ����������� ����� � ����� ���� ���� 'Motorola' ������������ (����� OffSet, ��� �����)
        if APP.TIFFHeader.Align=$4D4D then
         begin
          tag.TagID := InvertHex2(tag.TagID);
          tag.TagType := InvertHex2(tag.TagType);
          tag.Count := InvertHex4(tag.Count);
         end;
// ��� ���� ����� ���� ������ �� 1 � �� 12
        if (tag.TagType<1) or (tag.TagType>12) then
         begin
          Write('!!!!!!!!!! ERROR TagType !!!!!!!!!!!!');
          Break;
         end;
// ������ ��������
        GroupP := FExifData.GetByName('Thumbnail information');
        GetTagValue(Tag, APP.TIFFHeader.Align, Off0, ParamName, Param, logParam);
        AssignValue;
       end;
// ������ ��������� �� ��������� IFD
    BlockRead(F, NextIFD2, 4);
    if APP.TIFFHeader.Align=$4D4D then NextIFD2 := InvertHex4(NextIFD2);
    Write('Next IDF OffSet = '+IntToStr(NextIFD2));
   end;

// ----------------------------------------
// ������ THUMBNAIL
// ----------------------------------------
 Ch := 'T'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  If (FThumbnail.Offset<>0) and (FThumbnail.Length<>0) then
   begin
    j := FilePos(F);
    SetLength(FThumbnail.Image,FThumbnail.Length);
    Seek(F, FThumbnail.Offset + Off0);
    BlockRead(F, FThumbnail.Image[0], FThumbnail.Length);
    Seek(F, j);
   end;

// ----------------------------------------
// ������ Shuter count (������ � RAW ������ 20D � 30D �� ����������� ������)
// ----------------------------------------
 Ch := 'C'; // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

 if (FTypeOfFile = tfTIFF) and (FFileExtention = AnsiUpperCase('.CR2')) then
  if ExifData.FCount > 0 then
   if ExifData.GetByName('Main information').Count > 0 then
    begin
     if (ExifData.GetByName('Main information').GetByName('Model') = 'Canon EOS 20D') or
        (ExifData.GetByName('Main information').GetByName('Model') = 'Canon EOS 30D') then
     if FExifData.Add('ShuterCount') then
      begin
      j := FilePos(F);
      GroupP := FExifData.GetByName('ShuterCount');
      if ExifData.GetByName('Main information').GetByName('Model') = 'Canon EOS 20D' then
       begin
        Seek(F, $95D);
        ParamName := 'ShuterCount 20D';
       end
      else
       begin
        Seek(F, $943);
        ParamName := 'ShuterCount 30D';
       end;
      BlockRead(F, i, 2);
      i := InvertHex2(i);
      Param := i;
      AssignValue;
      Seek(F, j);
     end;
    end;
// ---------------------------------

  Write('.........................................................................................................................................................................................................................................................');
  Write('End of read (' + IntToStr(GetTickCount - FTickCount) + 'ms)');
  CloseFile(F);
{
  j := Length(FileArray) - 11;
  for i := Length(FileArray) - 11 downto 0 do
   if FileArray[i] = 46 then
    j := j - 1
   else
    Break;
  SetLength(FileArray, j + 10);
}
end;

procedure TExifGrid.SetFileNameVisible(Value: boolean);
begin
// if Value then Form1.Output2.Lines.Add('true') else Form1.Output2.Lines.Add('false');
 if FFileNameVisible <> Value then
  begin
   PopupMenu.Items.Find(PM_SHOWFILENAME_ITEM).Checked := Value;
   FFileNameVisible := Value;
   FillGrid;
   TopRow := FixedRows;
   Refresh(Self);
  end;
end;

procedure TExifGrid.WMLButtonDown(var Message: TWMLButtonDown);
begin
 if TypeOf(MouseCoord(Message.XPos, Message.YPos).X, MouseCoord(Message.XPos, Message.YPos).Y) in ([Value, UnknownValue]) then
  inherited
// Write('WMLButtonDown: X='+IntToStr(Message.XPos)+' Y='+IntToStr(Message.YPos));
end;

// ==================================== ParamList =====
function TParamList.GetByName(ParamName: string): Variant;
var
  i: integer;
begin
 for i := 0 to Count - 1 do
  if Items[i].Name = ParamName then
   begin
    result := Items[i].Value;
    Exit;
   end;
 result := '';
end;

procedure TParamList.AddItem(Name: string; Number, ParamType: Byte; Value: Variant; Unknown: boolean);
var
 AItem: TParamItem;
begin
 AItem := TParamItem.Create;
 AItem.Name := Name;
 AItem.Number := Number;
 AItem.ParamType := ParamType;
 AItem.Value := Value;
 AItem.Unknown := Unknown;
 AItem.Group := Self;
 inherited Add(AItem);
end;

function TParamList.GetItem(Index: Integer): TParamItem;
begin
 try
  Result := inherited Items[Index] as TParamItem;
 except
  asm int 3 end;
 end;
end;
// ------------------------------------ ParamList -----

procedure TExifGrid.WMNCPaint(var Msg: TWMNCPaint);
begin
 inherited;
end;

procedure TExif.Write(s: string);
var
 TickCount: Cardinal;
begin
 TickCount := GetTickCount;
 FLog.Add(FormatDateTime('hh:nn:ss:zzzz', Now) + ' (' + IntToStr(TickCount - FLogTickCount) + ')| ' + s);
 FLogTickCount := TickCount;
end;

{ TCollections }

function TParamListCollections.Add(iName: string): boolean;
var
  i: integer;
begin
 result := false;
 if iName = '' then Exit;
// ��������� ���� �� ��� ����� ���
 for i := 0 to Length(FTitles)-1 do
  if FTitles[i].FName = iName then Exit;
// ��������� ����� �������
 SetLength(FTitles, Length(FTitles) + 1);
// ������� �������
 FTitles[Length(FTitles) - 1] := TParamList.Create;
// ����������� ��� ���������� ��������
 FTitles[Length(FTitles) - 1].FName := iName;
 FCount := FCount + 1;
 result := true;
end;

constructor TParamListCollections.Create;
begin
 FCount := 0;
end;

destructor TParamListCollections.Destroy;
var
 i: integer;
begin
 for i :=  0 to FCount - 1 do
  Items[i].Free;
end;

function TParamListCollections.FindTag(Tag: string): TParamItem;
var
 i, j: integer;
begin
 result := nil;
 for i := 0 to FCount - 1 do
  for j := 0 to Items[i].Count - 1 do
   if Items[i].Items[j].Name = Tag then
    begin
     result := Items[i].Items[j];
     Exit;
    end;
end;

function TParamListCollections.GetByName(iName: string): TParamList;
var
 i: integer;
begin
 result := nil;
 for i := 0 to Length(FTitles) - 1 do
  if FTitles[i].FName = iName then
   begin
    result := FTitles[i];
    Break;
   end;
end;

function TParamListCollections.GetIndexByName(iName: string): integer;
var
 i: integer;
begin
 result := -1;
 for i := 0 to Length(FTitles) - 1 do
  if FTitles[i].FName = iName then
   begin
    result := i;
    break;
   end;
end;

function TParamListCollections.GetItem(Index: integer): TParamList;
begin
 result := FTitles[Index];
end;

function TParamListCollections.GetSubItemsCount: integer;
var
 i,j: integer;
begin
 j := 0;
 for i := 0 to Length(FTitles)-1 do
  j := j + FTitles[i].Count;
 result := FCount+j;
end;

procedure TExifGrid.SetUnknownVisible(Value: boolean);
begin
 if FUnknownVisible <> Value then
  begin
   PopupMenu.Items.Find(PM_SHOWUNKNOWN_ITEM).Checked := Value;
   FUnknownVisible := Value;
   FillGrid;
   TopRow := FixedRows;
   Refresh(Self);
  end;
end;

function TParamListCollections.GetUnknownCount: integer;
var
 i,j,l: integer;
begin
 l := 0;
 for i := 0 to Length(FTitles)-1 do
  for j := 0 to FTitles[i].Count-1 do
   if FTitles[i].Items[j].Unknown = true then  l := l + 1;
 result := l;
end;

function TExifGrid.SelectCell(ACol, ARow: Longint): Boolean;
begin
 result := true;
 if TypeOf(ACol, ARow) = NoExif then Exit;

 if (FSelectedRow.Name <> string(FCells[ARow].Name)) or (FSelectedRow.Value <> string(FCells[ARow].Value)) then
  begin
   FSelectedRow.Name := FCells[ARow].Name;
   FSelectedRow.Value := FCells[ARow].Value;
   if Assigned(FOnSelectCell) then FOnSelectCell(Self, ACol, ARow, FSelectedRow);
  end;
end;

{ TSelectedRow }

procedure TSelectedRow.Clear;
begin
 Name := '';
 Value := '';
end;

procedure TExifGrid.WMMouseMove(var Message: TWMMouseMove);
begin
 inherited;
// MouseMove([], Message.XPos, Message.YPos);
end;

procedure TParamList.AddFrom(Value: TParamItem);
var
 AItem: TParamItem;
begin
 AItem := TParamItem.Create;
 AItem.Name := Value.Name;
 AItem.Number := Value.Number;
 AItem.ParamType := Value.ParamType;
 AItem.Value := Value.Value;
 AItem.Unknown := Value.Unknown;
 AItem.Group := Self;
 inherited Add(AItem);
end;

function TParamList.Compare(Value1, Value2: TParamItem; WithValue: boolean): boolean;
begin
 result := false;
 case WithValue of
  true :
   if (Value1.Name = Value2.Name) and
      (Value1.ParamType = Value2.ParamType) and
      (Value1.Value = Value2.Value) and
      (Value1.Unknown = Value2.Unknown) and
      (Value1.Group.FName = Value2.Group.FName) then
    result := true;
  false :
   if (Value1.Name = Value2.Name) and
      (Value1.ParamType = Value2.ParamType) and
      (Value1.Unknown = Value2.Unknown) and
      (Value1.Group.FName = Value2.Group.FName) then
    result := true
 end;
end;

function TParamList.Find(Value: TParamItem): integer;
var
 i: integer;
begin
 result := -1;
 for i :=  0 to Count - 1 do
  if Compare(Items[i], Value, false) then
   begin
    result := i;
    exit;
   end;
end;

destructor TExifGrid.Destroy;
begin
 if (FSelfExif) and (Assigned(FExif)) then FExif.Free;
 FSelectedRow.Free;
 FPlus.Free;
 FIndentions.Free;
 FColors.Free;
 FPopupMenu.Free;
 inherited;
end;

destructor TExif.Destroy;
begin
 FLog.Free;
 FExifData.Free;
end;
{
destructor TParamList.Destroy;
var
 i: integer;
begin
// for i := 0 to Count - 1 do
//  Items[i].Free;
 inherited;
end;
}
function TExifGrid.GetFirstColumnWidth: integer;
begin
 result := ColWidths[0];
end;

procedure TExifGrid.SetFirstColumnWidth(const Value: integer);
begin
 ColWidths[0] := Value;
 SetColumnsSize;
end;

procedure TExifGrid.SetExif(const Value: TExif);
begin
 if (FSelfExif) and (Assigned(FExif)) then FExif.Free;
 FSelfExif := false;
 FExif := Value;
 FFileName := FExif.FFileName;
 GetExifData(false);
 Write('(TExifGrid.SetExif) SetFileName: ColWidths[0]=' + IntToStr(ColWidths[0]));
end;

procedure TExif.ReplaceFileName(iFileName: Ansistring);
begin
 if FileExists(iFileName) then
  begin
   FFileName := iFileName;
   FFileExtention := ExtractFileExt(iFileName);
  end
 else
  Write('File: "' + iFileName + '" not found');
end;

end.
