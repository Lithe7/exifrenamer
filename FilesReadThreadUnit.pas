unit FilesReadThreadUnit;

interface

uses
  Classes, ComCtrls, StdCtrls, Exif;

type
  TFilesReadThread = class(TThread)
  private
    FDir: AnsiString;
    Exif: TExif;
    FFullFileName: string;
    // For ListView ----------------
    FFileName: string;
    FFileCreatedDateTime: TDateTime;
    FFileExifDateTime: string;
    FFileExifDateTimeOriginal: string;
    FFileExifDateTimeDigitized: string;
    FFileModifaedDateTime: TDateTime;
    // -----------------------------
    FTickCount: int64;
    FAllFilesCount: integer;
    FFileCurrent: integer; // ������� �������������� ������ (���� ������ � ��������)

    FMainForm: TObject;

    {$IFDEF LOGMODE}
    LogStr: string;
//    procedure LogAdd_;
    {$ENDIF LOGMODE}

    procedure UpdateListView;
    procedure UpdateProgress;
    procedure UpdateProgressFinish;
    procedure ClearListView;
    function  GetNumberOfFiles(iDir: string; WithSubDirs: boolean = false): integer;
    procedure SetAllFilesCount;
  protected
    procedure MySynchronize(AMethod: TThreadMethod; s: string);
    procedure Execute; override;
  public
    constructor Create(Dir: AnsiString; MainForm: TObject); reintroduce;
  private
    {$IFDEF LOGMODE}
    procedure UpdateLog;
    procedure WriteLog(Msg: string);
//    procedure LogAdd_;
    {$ENDIF LOGMODE}
  end;

implementation

uses SysUtils, Windows, MainFormUnit, ConstUnit, ShellAPI, Graphics, Forms, Messages;

{ TFilesReadThred }

constructor TFilesReadThread.Create(Dir: AnsiString; MainForm: TObject);
begin
 inherited Create(true);
// FreeOnTerminate := true;
 FreeOnTerminate := false;
 FDir := Dir;
 FMainForm := MainForm;
end;

function TFilesReadThread.GetNumberOfFiles(iDir: string; WithSubDirs: boolean = false): integer;

 function FilesCount(iDir: string): integer;
 // ����������� �������
 var
  iSr: TSearchRec;
 begin
  result := 0;
  try
   if DirectoryExists(iDir) then
    begin
     if FindFirst(IncludeTrailingPathDelimiter(iDir) + '*.*', faAnyFile, iSr) = 0 then
      repeat
       if (iSr.Name <> '.') and
          (iSr.Name <> '..') then
        if WithSubDirs and
           ((iSr.Attr and faDirectory) <> 0) and
           ((iSr.Attr and faVolumeID) = 0) then
         result := FilesCount(IncludeTrailingPathDelimiter(iDir) + iSr.Name)
        else
         if not (((iSr.Attr and faDirectory) <> 0) and
            ((iSr.Attr and faVolumeID) = 0)) then
          result := result + 1;
      until FindNext(iSr) <> 0;
      SysUtils.FindClose(iSr);
    end;
  except
   result := -1;
  end;
 end;

begin
 result := FilesCount(iDir); // ����������� �������
end;

procedure TFilesReadThread.UpdateListView;
type
 TIconInfo = packed record
              Extention: string[255];
              ImageIndex: byte;
             end;
var
 IconInfo: array of TIconInfo;
 ShFileInfo: TShFileInfo;
 SmallIcon: TIcon;
 i: integer;

 function GetIconInfo(Ext: string): byte;
 var
  i: byte;
 begin
 // ���� �� ������ ���������� FF
  result := $FF;
  if Length(IconInfo) = 0 then Exit;
  for i := 0 to Length(IconInfo) - 1 do
   if Uppercase(IconInfo[i].Extention) = Uppercase(Ext) then
    result := IconInfo[i].ImageIndex;
 end;

 function AddIconInfo(Ext: string; Icon: TIcon): integer;
 begin
  SetLength(IconInfo, Length(IconInfo) + 1);
  IconInfo[Length(IconInfo) - 1].Extention := Uppercase(Ext);
  IconInfo[Length(IconInfo) - 1].ImageIndex := SmallImagesList.AddIcon(Icon);
  result := IconInfo[Length(IconInfo) - 1].ImageIndex;
 end;

begin
 SetLength(IconInfo, 0);
 MainFormUnit.FileListView.Items.BeginUpdate;
 with MainFormUnit.FileListView.Items.Add do
  begin
   Caption := FFileName;
   Data := Exif;
   SubItems.Add(DateTimeToStr(FFileCreatedDateTime, FS));
   SubItems.Add(DateTimeToStr(FFileModifaedDateTime, FS));
   SubItems.Add(FFileExifDateTime);
   SubItems.Add(FFileExifDateTimeOriginal);
   SubItems.Add(FFileExifDateTimeDigitized);

   // �������� ������ ������ ��� ���������� �����
   i := GetIconInfo(ExtractFileExt(FFileName));
   if i = $FF then
    begin
     ShGetFileInfo(Pchar(IncludeTrailingPathDelimiter(FDir) + FFileName), 0, ShFileInfo, SizeOf(ShFileInfo), SHGFI_ICON + SHGFI_SMALLICON);
     SmallIcon := TIcon.Create;
     SmallIcon.Handle := ShFileInfo.hIcon;
     ImageIndex := AddIconInfo(ExtractFileExt(FFileName), SmallIcon);
     SmallIcon.Free;
    end
   else
    ImageIndex := i;

   MainFormUnit.ListFilesCount := MainFormUnit.ListFilesCount + 1;
  end;
 MainFormUnit.FileListView.Items.EndUpdate;
end;

procedure TFilesReadThread.MySynchronize(AMethod: TThreadMethod; s: string);
begin
 {$IFDEF LOGMODE}WriteLog(s);{$ENDIF LOGMODE}
 Synchronize(AMethod);
end;

procedure TFilesReadThread.Execute;
var
 SearchRec: TSearchRec;
 SystemTime: TSystemTime;
 TimeZoneInformation: TTimeZoneInformation;
 j: int64;
 FileCurrent: integer; // ������� �������������� ������ (������ � Exif)
begin
 j := GetTickCount;
 // ���������� �����
 FTickCount := j;
 // ������� FileListView
 if Terminated then exit else
  MySynchronize(ClearListView, 'ClearListView');
 // ������ ���������� ������ � ����� � ���������� � ���������� ����������
 if Terminated then exit else
  MySynchronize(SetAllFilesCount, 'SetAllFilesCount');
 // ��������� ���� � ���
 {$IFDEF LOGMODE}WriteLog('����: ' + FDir);{$ENDIF LOGMODE}
 FileCurrent := 0;
 // �������� ������� ��� ������ � �����
 if FindFirst(IncludeTrailingPathDelimiter(FDir) + '*.*', faAnyFile, SearchRec) = 0 then
  try
   repeat
    if Terminated then exit;
    // ��������� ��� ����� � ���
    {$IFDEF LOGMODE}WriteLog('- ����: ' + SearchRec.Name);{$ENDIF LOGMODE}
    if ((SearchRec.Attr and faDirectory) = 0) and  ((SearchRec.Attr and faVolumeID) = 0) and  ((SearchRec.Attr and faSymLink) = 0) then
     begin
      FFullFileName := IncludeTrailingPathDelimiter(FDir) + SearchRec.Name;
      FFileName := SearchRec.Name;
      if Terminated then exit;
      FFileCurrent := FFileCurrent + 1;
      Exif := TExif.Create; // ������ ����������� � ������ FileListView ��� ������� ����� � ��������� ������ � ���
                            // ����� �������� �� ����������� ������ ���� �������� � FileListView ����� �� �� ����� �����
      Exif.UnknownsVisible := false;
      Exif.FileName := FFullFileName;

      if (Exif.TypeOfFile = tfJPEG) or (Exif.TypeOfFile = tfTIFF) then
       begin
        FileCurrent := FileCurrent + 1;
        // ����������� ���� �������� ����� � TDateTime
        // ����� ������� ������� � �������� FileTimeToLocalFileTime ������ �� ���������� �� ��� ������!!!
        FileTimeToSystemTime(SearchRec.FindData.ftCreationTime, SystemTime);

        GetTimeZoneInformation(TimeZoneInformation);
        SystemTimeToTzSpecificLocalTime(@TimeZoneInformation, SystemTime, SystemTime);

        FFileCreatedDateTime := SystemTimeToDateTime(SystemTime);
        // ����������� ���� ����������� ����� � TDateTime
        FileTimeToSystemTime(SearchRec.FindData.ftLastWriteTime, SystemTime);
        SystemTimeToTzSpecificLocalTime(@TimeZoneInformation, SystemTime, SystemTime);
        FFileModifaedDateTime := SystemTimeToDateTime(SystemTime);

        if Exif.ValidExif then
         begin
          if Exif.ExifData.GetByName('Main information') <> nil then
           FFileExifDateTime := PhotoStrDateToStr(Exif.ExifData.GetByName('Main information').GetByName('DateTime'))
          else
           FFileExifDateTime := NOT_HAVE;
          if Exif.ExifData.GetByName('Additional information') <> nil then
           FFileExifDateTimeOriginal := PhotoStrDateToStr(Exif.ExifData.GetByName('Additional information').GetByName('DateTimeOriginal'))
          else
           FFileExifDateTimeOriginal := NOT_HAVE;
          if Exif.ExifData.GetByName('Additional information') <> nil then
           FFileExifDateTimeDigitized := PhotoStrDateToStr(Exif.ExifData.GetByName('Additional information').GetByName('DateTimeDigitized'))
          else
           FFileExifDateTimeDigitized := NOT_HAVE;
        end
       else
        begin
         FFileExifDateTime := NO_EXIF;
         FFileExifDateTimeOriginal := NO_EXIF;
         FFileExifDateTimeDigitized := NO_EXIF;
        end;

        if Terminated then exit else
         MySynchronize(UpdateListView, 'UpdateListView');
        if FileCurrent = 1 then
         PostMessage(MainFormUnit.FileListView.Handle, WM_KEYDOWN, VK_DOWN, 0);
       end;
      if Terminated then exit else
       MySynchronize(UpdateProgress, 'UpdateProgress');
     end;
    {$IFDEF LOGMODE}WriteLog('- ���� ��������');{$ENDIF LOGMODE}
   until FindNext(SearchRec) <> 0;
  finally
   {$IFDEF LOGMODE}WriteLog('finally');{$ENDIF LOGMODE}
   SysUtils.FindClose(SearchRec);
  end;
// if FAllFilesCount > 0 then
//  s := IntToStr(GetTickCount - FTickCount) + 'ms  ' + IntToStr(FAllFilesCount) + '�� ' + IntToStr((GetTickCount - FTickCount) div FAllFilesCount) + 'ms'
// else
//  s := '0';
 if Terminated then exit else
  MySynchronize(UpdateProgressFinish, 'UpdateProgressFinish');
 {$IFDEF LOGMODE}WriteLog('������ ������ ���������.');{$ENDIF LOGMODE}
end;

{$IFDEF LOGMODE}
procedure TFilesReadThread.WriteLog(Msg: string);
begin
 LogStr := Msg;
 Synchronize(UpdateLog);
end;

procedure TFilesReadThread.UpdateLog;
begin
 LogAdd('     ===== TFilesReadThread: ' + LogStr);
end;
{$ENDIF LOGMODE}

procedure TFilesReadThread.ClearListView;
begin
 MainFormUnit.FileListView.Clear;
end;

procedure TFilesReadThread.UpdateProgress;
begin
 TExifDateMainForm(FMainForm).ReadProgress(Self, psRunning, Round(FFileCurrent * 100 / FAllFilesCount), 'Reading...');
 PreviewStatusBar.Files := FileListView.Items.Count;
 FileViewStatusBar.Panels[0].Text := IntToStr(FileListView.Items.Count) + '/' + IntToStr(FileListView.Items.Count) + '/' + IntToStr(FAllFilesCount);
end;

procedure TFilesReadThread.UpdateProgressFinish;
begin
 FileViewStatusBar.Panels[0].Text := IntToStr(FileListView.ItemIndex + 1) + '/' + IntToStr(FileListView.Items.Count);
 TExifDateMainForm(FMainForm).ReadProgress(Self, psEnding, 100, '');
 FileViewStatusBar.Reading := false;
end;

procedure TFilesReadThread.SetAllFilesCount;
begin
 FAllFilesCount := GetNumberOfFiles(FDir);
 MainFormUnit.AllFilesCount := FAllFilesCount;
end;

end.

