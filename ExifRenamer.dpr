program ExifRenamer;

//{$DEFINE LOGMODE}

{%ToDo 'ExifRenamer.todo'}

uses
  Forms in 'Forms.pas',
  Windows,
  XPMan,
  MainFormUnit in 'MainFormUnit.pas',
  Exif in 'Exif.pas',
  ConstUnit in 'ConstUnit.pas',
  VerInfo in 'VERINFO.PAS',
  TB97 in 'TB97.pas',
  DockFormUnit in 'DockFormUnit.pas',
  ExifGridUnit in 'ExifGridUnit.pas',
  MyRegistry in 'MyRegistry.pas',
  RenameFormUnit in 'RenameFormUnit.pas',
  LitheColorUnit in 'LitheColorUnit.pas',
  DataUnit in 'DataUnit.pas',
  FilesReadThreadUnit in 'FilesReadThreadUnit.pas';

{$R *.res}

var
 Handle1: LongInt;
 Handle2: LongInt;
 i: byte;

begin
  Application.Free;
  Application := TExifApplication.Create(nil);
  Application.Initialize;
  Handle1 := FindWindow('TExifDateMainForm',nil);
  if Handle1 = 0 then
   begin
    Application.Title := 'Exif Renamer';
    Application.ShowHint := true;
    Application.HintPause := 500;
    Application.HintHidePause := 5000;
    Application.ShowHint := true;
    ExifDateMainForm := TExifDateMainForm.CreateNew(Application);
    TExifApplication(Application).SetMainForm(ExifDateMainForm);
    ExifDateMainForm.FormCreate(Application);
    ExifDateMainForm.Visible := true;
    ExifDateMainForm.FormActivate(nil);
    Application.Run;
   end
  else
   begin
    Handle2 := GetWindow(Handle1,GW_OWNER);
    For i:=0 to 2 do
     begin
      ShowWindow(Handle2,SW_HIDE);
      sleep(80);
      ShowWindow(Handle2,SW_RESTORE);
      sleep(80);
     end;
    SetForegroundWindow(Handle1);
   end;

 ExifDateMainForm.Free;
end.
