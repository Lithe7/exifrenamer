unit MainFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ExifGridUnit, Exif, StdCtrls, Menus, ShellAPI, Gauges,
  ComCtrls, ToolWin, DockFormUnit, ShellCtrls, Grids, ConstUnit, Jpeg, Buttons,
  TB97, TB97Tlbr,  TB97Ctls, ExtDlgs, ActnMan, ActnColorMaps, GraphicEx,
  CommCtrl, FileCtrl, VerInfo, ShlObj, MyRegistry, RenameFormUnit, ActnList,
  LitheColorUnit, LitheMenus, DataUnit, FilesReadThreadUnit;

type

  TStatisticsItem = packed record
                     Name: ShortString;
                     Value: integer;
                    end;

  TStatisticList = array of TStatisticsItem;

  TToolBarDock = class(TDock97);

  TToolBar = class(TToolBar97);

  TToolBarButton = class(TToolbarButton97);

  TToolBarSeparator = class(TToolbarSep97);

  TExifGridLeftDockPanel = class(TPanel);

  TExifGridRightDockPanel = class(TPanel);

  TFoldersDockPanel = class(TPanel);

  TThumbnailImage = class(TImage)
  private
    FThumbnail: boolean;
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
  published
    property Thumbnail: boolean read FThumbnail write FThumbnail;
  end;

  TExifDateSplitter = class(TSplitter);

  TFileListView = class(TListView)
  private
    ColumnToSort: Integer;
    procedure Clear; override;
    procedure WMMouseWheel(var Message: TWMMouseWheel); message WM_MOUSEWHEEL;
    procedure Next;
    procedure Prev;
  end;

  TStatShape = class(TShape)
  public
    TopIndent: integer;
    Max: integer;
    LeftIndent: integer;
    IndentColor: TColor;
    Color: TColor;
    procedure Paint; override;
  end;

  TExifProgressBar = class(TProgressBar);

  TPreviewImage = class(TImage)
  private
    FFit: boolean;
    FFileName: string;
    FCanResize: boolean;
    FMove: boolean;
    FX0: integer;
    FY0: integer;
    TIFFGraphic: TGraphic;
    procedure MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ShowPreview(const Item: TListItem);
    procedure PreviewLoadProgress(Sender: TObject; Stage: TProgressStage; PercentDone: Byte; RedrawNow: Boolean; const R: TRect; const Msg: String);
    procedure SetCanResize(Value: boolean);
    procedure SetFit(Value: boolean);
  protected
//    FParentWidth: integer;
//    FParentHeight: integer;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property CanResize: boolean read FCanResize write SetCanResize;
    property Fit: boolean read FFit write SetFit;
  end;

  TPreviewStatusBar = class(TStatusBar)
    constructor Create(AOwner: TComponent); override;
  protected
    CountFiles: integer;
    ItemIndex: integer;
  private
    ExifDateTime: string;
    ExifDateTimeOriginal: string;
    ExifDateTimeDigitized: string;
    TypeOfFile: string[4];
    SizeOfFile: longword;
    loaded: longword;
    FileName: string;
    procedure StatusBarResize(Sender: TObject);
    procedure InitStatusBar;
    procedure StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure ShowParam;
    procedure SetCountFiles(Count: integer);
    procedure SetItemIndex(Index: integer);
  published
    property  Files: integer read CountFiles write SetCountFiles;
    property  FileIndex: integer read ItemIndex write SetItemIndex;
  end;

  TFileViewStatusBar = class(TStatusBar)
    constructor Create(AOwner: TComponent); override;
  private
    FFocused: (sbFileView, sbExifGrid);
    FReading: Boolean;
    procedure StatusBarResize(Sender: TObject);
    procedure InitStatusBar;
    procedure StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure ShowParam;
  published
    property  Reading: boolean read FReading write FReading;
  end;

  TStatForm = class(TForm)
    Panel: TPanel;
    ScrollBox: TScrollBox;
    StatMemo: TMemo;
    Button: TButton;
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); virtual;
    procedure FormCreate(Sender: TObject);
    procedure MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    FX0: integer;
    FY0: integer;
    MouseScrollOn: boolean;
    procedure ButtonClick(Sender: TObject);
  end;

  TExifDateMainForm = class(TForm)
    Panel1: TPanel;
    Timer1: TTimer;
    procedure PreviewPanelResize(Sender: TObject);
    procedure ExitProgram(Sender: TObject);
    procedure CreateMainMenu;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    function  ComputeDockingRect(Sender: TObject; var DockRect: TRect; MousePos: TPoint): TAlign;
    procedure ExifGridDockPanelGetSiteInfo(Sender: TObject; DockClient: TControl; var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
    procedure ExifGridDockPanelDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ExifGridDockPanelDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure ExifGridDockPanelUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
    procedure ShowExifGridDockPanel(APanel: TPanel; MakeVisible: Boolean; iWidth: Integer);
    procedure FoldersDockPanelGetSiteInfo(Sender: TObject; DockClient: TControl; var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
    procedure FoldersDockPanelDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FoldersDockPanelDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure FoldersDockPanelUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
    procedure ShowFoldersDockPanel(APanel: TPanel; MakeVisible: Boolean);
{*} procedure FolderTreeViewChange(Sender: TObject; Node: TTreeNode);
{*} procedure FolderTreeViewChangeOld(Sender: TObject; Node: TTreeNode);
    procedure DisableFormForReadingFiles;
    procedure EnableFormForReadingFiles;
    procedure FileListViewCreateColumns;
    procedure InitFS;
    procedure FileListViewSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure ShowExifGridForm(Sender: TObject);
    procedure ShowFoldersForm(Sender: TObject);
    procedure ShowThumbnailForm(Sender: TObject);
    procedure MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ShowThumbnail;
    procedure NoThumbnailPaint(Img: TImage);
    procedure CreateToolBar(iToolBar: TToolBar);
    procedure OpenFolder(Sender: TObject);
    procedure OpenURL(Sender: TObject);
    procedure NextFileListView(Sender: TObject);
    procedure PrevFileListView(Sender: TObject);
    procedure FileListViewCompare(Sender: TObject; Item1, Item2: TListItem; Data: Integer; var Compare: Integer);
    procedure FileListViewColumnClick(Sender: TObject; Column: TListColumn);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SwitchToPicture(Sender: TObject);
    procedure ResizePreviewImage(Sender: TObject);
    procedure FileListViewCustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure FileListViewCustomDrawSubItem(Sender: TCustomListView; Item: TListItem; SubItem: Integer; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure ResizeThumbnailForm;
    procedure ReadProgress(Sender: TObject; Stage: TProgressStage; PercentDone: Byte; const Msg: String);
    procedure ExifGridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FileListViewMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ExifGridSelectCell(Sender: TObject; ACol, ARow: Integer; SelectItem: TSelectedRow);
    procedure Rename(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ProgressFormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    procedure WMClose(var Message:TMessage); Message WM_Close;
    procedure WMNCPaint(var Msg: TWMNCPaint); message WM_NCPAINT;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  private
    procedure ShellChangeNotifierChange;
    procedure CreateFileListViewPopupMenu(iPopupMenu: TPopupMenu);
    // ������ ������������ ���� --------------------------
    procedure FileListViewSelectAll(Sender: TObject);
    procedure FileListViewSelectWithExif(Sender: TObject);
    procedure FileListViewSelectWithExifDateTimeWithoutOriginal(Sender: TObject);
    procedure FileListViewSelectWithExifDateTimeOriginal(Sender: TObject);
    // -------------------------- ������ ������������ ����
    function  GetNumberOfFiles(iDir: string; WithSubDirs: boolean = false): integer;
    function  GetSubDirs(iDir: string): boolean;
    procedure ReadStatistics(Sender: TObject);
    procedure FilesReadThredOnTerminate(Sender: TObject);
  public
    procedure EnablerNextPrevButton;
    procedure Timer1Timer(Sender: TObject);
    procedure TempClickToolBarButton(Sender: TObject);
  end;

var
  Version: string;

//  FilesExifList: TFilesExifList;

{$IFDEF LOGMODE}
  TempPanel: TPanel;
  LogMemo: TMemo;
  LogSplitter: TSplitter;
{$ENDIF LOGMODE}

  Sch: cardinal;
  Cancel: boolean; // ���������� ���������� ��� ������ �����-���� ��������

  ToolBarColor: TColor = $ECE9D8;
  MenuColor: TColor = clWhite;
  MenuGutterColor: TColor = $ECE9D8;
  MenuSelectColor: TColor = $ECE9D8;

  FS: TFormatSettings;     // ������ ���� � ������� ��� ����������� � ListView
  ExifDateMainForm: TExifDateMainForm;
  ExifGridForm: TExifGridForm;
  FoldersForm: TFolderDockForm;
  FolderTreeView: TShellTreeView;
  ShellChangeNotifier: TShellChangeNotifier;
  ThumbnailForm: TThumbnailDockForm;
  ThumbnailImage: TThumbnailImage;
  MainPanel: TPanel;
  SwitchPanel: TPanel;
// .............................................................................
  //
  ModelLabel: TLabel;
  ShutterSpeedValueLabel: TLabel;
  ApertureValueLabel: TLabel;
  ISOValueLabel: TLabel;
  LensValueLabel: TLabel;
  //
  PreviewPanel: TPanel;
  PreviewImage: TPreviewImage;
  PreviewProgressBar: TExifProgressBar;
  PreviewStatusBar: TPreviewStatusBar;
// .............................................................................
  LoadImageDone: boolean;
  ExifGridLeftDockPanel: TExifGridLeftDockPanel;
  ExifGridRightDockPanel: TExifGridRightDockPanel;
  ExifGridLeftDockPanelSplitter: TExifDateSplitter;
  ExifGridRightDockPanelSplitter: TExifDateSplitter;
  FoldersDockPanel: TFoldersDockPanel;
  FoldersDockPanelSplitter: TExifDateSplitter;
  DirPanel: TPanel;
  DirLabel: TLabel;
// .............................................................................
  ToolBarDock: TToolBarDock;
  ToolBarImageList: TImageList;
  ToolBar: TToolBar;
  OpenToolBarButton: TToolBarButton;
  SwitchToolBarButton: TToolBarButton;
  PrevToolBarButton: TToolBarButton;
  NextToolBarButton: TToolBarButton;
  ResizeToolBarButton: TToolBarButton;
  RenameToolBarButton: TToolBarButton;
// .............................................................................
  ActionNext: TAction;
  ActionPrev: TAction;
  ActionSwitch: TAction;
  ActionOpen: TAction;
  ActionResize: TAction;
  ActionRename: TAction;
  ActionViewFolders: TAction;
  ActionViewThumbnail: TAction;
  ActionViewExifGrid: TAction;
  ActionShowStatistics: TAction;

// .............................................................................
  MainMenu: TMainMenu;
  FileListViewPopupMenu: TPopupMenu;
  ActionsNextItem: TMenuItem;
  ActionsPrevItem: TMenuItem;
  ActionsSwichItem: TMenuItem;
  ActionsRenameItem: TMenuItem;
//  ActionsNextItem: TMenuItem;

  ViewFoldersItem: TMenuItem;
  ViewThumbnailItem: TMenuItem;
  ViewExifItem: TMenuItem;
  VersionItem: TMenuItem;
// .............................................................................
  FileViewStatusBar: TFileViewStatusBar;
  FileViewProgressBar: TExifProgressBar;
// .............................................................................
  SmallImagesList: TImageList;
  FileListView: TFileListView;
// .............................................................................
  FilesReadThred: TFilesReadThread;
  FilesReadThredCount: integer; // ���������� ������� ������
  AllFilesCount: integer;       // ����� ������ � ��������
  ListFilesCount: integer;      // ������ � ������

procedure LoadJPEGFromRes(TheJPEG: string; ThePicture: TBitmap);
function Dir(iDir: string): string;
function PhotoStrDateToStr(Str:String):String;
function DoubleToStr(V: Double; Count: byte):string;
function FileSizeToStr(Size: longword): string;
function ExifFileSize(Name: string): longword;

{$IFDEF LOGMODE}
procedure LogAdd(s: string);
{$ENDIF LOGMODE}

implementation

uses Types;

{$R ToolBarRes.res}

{$IFDEF LOGMODE}
procedure LogAdd(s: string);
begin
 LogMemo.Lines.Add(FormatDateTime('hh:nn.ss.zzzz', Now) + ' | ' + s);
end;
{$ENDIF LOGMODE}

procedure LoadBitmapFromRes(Name: string; Bitmap: TBitmap);
var
  Res: TResourceStream;
begin
 try
  Res := TResourceStream.Create(HInstance, PChar(Name), PChar('TOOL'));
  Bitmap.LoadFromStream(Res);
  Res.Free;
 except
 end; 
end;

procedure LoadJPEGFromRes(TheJPEG: string; ThePicture: TBitmap);
var
  ResHandle: THandle;
  MemHandle: THandle;
  MemStream: TMemoryStream;
  ResPtr   : PByte;
  ResSize  : Longint;
  JPEGImage: TJPEGImage;
begin
 try
  try
   ResHandle := FindResource(hInstance, PChar(TheJPEG), 'JPEG');
   MemHandle := LoadResource(hInstance, ResHandle);
   ResPtr    := LockResource(MemHandle);
   MemStream := TMemoryStream.Create;
   JPEGImage := TJPEGImage.Create;
   ResSize := SizeOfResource(hInstance, ResHandle);
   MemStream.SetSize(ResSize);
   MemStream.Write(ResPtr^, ResSize);
   FreeResource(MemHandle);
   MemStream.Seek(0, 0);
   JPEGImage.LoadFromStream(MemStream);
   ThePicture.Width := JPEGImage.Width;
   ThePicture.Height := JPEGImage.Height;
   ThePicture.Canvas.Draw(0, 0, JPEGImage);
  except
  end;
 finally
  JPEGImage.Free;
  MemStream.Free;
 end;
end;

function PhotoStrDateToStr(Str: string): string;
var
 lFS: TFormatSettings;
 d: TDateTime;
begin
 lFS.DateSeparator := ':';
 lFS.TimeSeparator := ':';
 lFS.ShortDateFormat := 'yyyy/mm/dd hh:nn:ss';
 lFS.LongDateFormat := 'yyyy/mm/dd hh:nn:ss';
 lFS.ShortTimeFormat := 'hh:nn:ss';
 lFS.LongTimeFormat := 'hh:nn:ss';
 lFS.TwoDigitYearCenturyWindow := 10;
 try
  d := StrToDateTime(Str, lFS);
 except
  result := 'ERROR!';
  exit;
 end;
 if d > 0 then
  result := FormatDateTime(FS.ShortDateFormat + ' ' + FS.ShortTimeFormat, d)
 else
  result := 'not have';
end;

procedure TExifDateMainForm.FileListViewCreateColumns;
begin
 with FileListView.Columns.Add do
  begin
   Width := 165;
   Caption := 'Name';
  end;
 with FileListView.Columns.Add do
  begin
   Width := 120;
   Caption := 'Created';
  end;
 with FileListView.Columns.Add do
  begin
   Width := 120;
   Caption := 'Modified';
  end;
 with FileListView.Columns.Add do
  begin
   Width := 120;
   Caption := 'Exif DateTime';
  end;
 with FileListView.Columns.Add do
  begin
   Width := 120;
   Caption := 'Exif Original';
  end;
 with FileListView.Columns.Add do
  begin
   Width := 120;
   Caption := 'Exif Digitized';
  end;
end;

procedure CallBack(wnd: hWnd; uMsg: UINT; lParam, lpData: LParam) stdcall;
begin
  SendMessage(wnd, BFFM_ENABLEOK, 0, 1);
end;

procedure TExifDateMainForm.OpenFolder(Sender: TObject);
var
 bi: TBrowseInfo;
 s: PChar;
 PIDL, ResPIDL: PItemIDList;
begin
{
  SHGetSpecialFolderLocation(ExifDateMainForm.Handle, CSIDL_DESKTOP, PIDL);
  s := StrAlloc(128);
  bi.hwndOwner := ExifDateMainForm.Handle;
  bi.pszDisplayName := s;
  bi.lpszTitle := 'Select folder';
  bi.pidlRoot := PIDL;
  bi.lpfn := addr(CallBack);
  ResPidl := SHBrowseForFolder(BI);
  SHGetPathFromIDList(ResPidl, s);
  ExifDateMainForm.Caption := s;
}
{
 OpenPictureDialog := TOpenPictureDialog.Create(Nil);
 OpenPictureDialog.Filter := 'JPEG Image Files|*.jpg;*jpeg|TIFF image files|*.tif;*.tiff|All supporting files|*.jpg;*.jpeg;*.tif;*.tiff';
 OpenPictureDialog.FilterIndex := 4;
 OpenPictureDialog.Options := [ofReadOnly,ofOverwritePrompt,ofHideReadOnly,ofEnableSizing];
 OpenPictureDialog.Title := 'Select file';
 if OpenPictureDialog.Execute then
  begin
   FolderTreeView.Path := ExtractFileDir(OpenPictureDialog.FileName);
   FileListView.Selected := FileListView.FindCaption(0, ExtractFileName(OpenPictureDialog.FileName),false,false,false);
  end;
}  
end;

procedure TPreviewStatusBar.StatusBarResize(Sender: TObject);
var
 i, j: integer;
begin
 if Panels.Count < 8 then Exit;
 Panels[0].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[0].Text) + 14;
 Panels[2].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[2].Text) + 14;
 Panels[3].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[3].Text) + 14;
 Panels[4].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[4].Text) + 14;
 Panels[5].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[5].Text) + 14;
 Panels[6].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[6].Text) + 14;
 Panels[7].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[7].Text) + 14;
 Panels[1].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[1].Text) + 40;
 j := 0;
 for i := 0 to (Sender as TStatusBar).Panels.Count - 2 do
  j := j + (Sender as TStatusBar).Panels[i].Width;
 Panels[(Sender as TStatusBar).Panels.Count - 1].Width := (Sender as TStatusBar).ClientWidth - j;

 j := 0;
 for i := 0 to Panels.Count - 2 do j := j + Panels[i].Width;
 if PreviewProgressBar <> nil then
  PreviewProgressBar.SetBounds(j, Height div 2 - PreviewProgressBar.Height div 2 + 1, Panels[Panels.Count - 1].Width - 3, PreviewProgressBar.Height);
end;

constructor TFileViewStatusBar.Create(AOwner: TComponent);
begin
 inherited;
 OnResize := StatusBarResize;
end;

procedure TFileViewStatusBar.StatusBarResize(Sender: TObject);
var
 i, j: integer;
begin
 if Panels.Count < 8 then Exit;
 if Panels[0].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[0].Text) + 14 then
  Panels[0].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[0].Text) + 14;
 if Panels[2].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[2].Text) + 14 then
  Panels[2].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[2].Text) + 14;
 if Panels[3].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[3].Text) + 14 then
  Panels[3].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[3].Text) + 14;
 if Panels[4].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[4].Text) + 14 then
  Panels[4].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[4].Text) + 14;
 if Panels[5].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[5].Text) + 14 then
  Panels[5].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[5].Text) + 14;
 if Panels[6].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[6].Text) + 14 then
  Panels[6].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[6].Text) + 14;
 if Panels[7].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[7].Text) + 14 then
  Panels[7].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[7].Text) + 14;
 if Panels[1].Width <> (Sender as TStatusBar).Canvas.TextWidth(Panels[1].Text) + 40 then
  Panels[1].Width := (Sender as TStatusBar).Canvas.TextWidth(Panels[1].Text) + 40;
 j := 0;
 for i := 0 to (Sender as TStatusBar).Panels.Count - 2 do
  j := j + (Sender as TStatusBar).Panels[i].Width;
 if Panels[(Sender as TStatusBar).Panels.Count - 1].Width <> (Sender as TStatusBar).ClientWidth - j then
  Panels[(Sender as TStatusBar).Panels.Count - 1].Width := (Sender as TStatusBar).ClientWidth - j;

 j := 0;
 for i := 0 to Panels.Count - 2 do j := j + Panels[i].Width;
 if FileViewProgressBar <> nil then
  if FileViewProgressBar.Width <> Panels[Panels.Count - 1].Width - 3 then
   FileViewProgressBar.SetBounds(j, Height div 2 - FileViewProgressBar.Height div 2 + 1, Panels[Panels.Count - 1].Width - 3, FileViewProgressBar.Height);
end;

procedure TFileListView.Clear;
var
 i: integer;
begin
 // ������� ������� TExif ����������� � ������ ������
 for i := 0 to Items.Count - 1 do
  begin
   TExif(Items[i].Data).Free;
   Items[i].Data := nil;
  end;
 // ������� ��� ListView
 inherited;
end;

procedure TFileListView.Next;
begin
 if FileListView.Items.Count > 0 then
  if FileListView.ItemIndex < FileListView.Items.Count - 1 then
   FileListView.Perform(WM_KEYDOWN, VK_DOWN, 0);
end;

procedure TFileListView.Prev;
begin
 if FileListView.Items.Count > 0 then
  if FileListView.ItemIndex > 0 then
   FileListView.Perform(WM_KEYDOWN, VK_UP, 0);
end;

procedure TExifDateMainForm.SwitchToPicture(Sender: TObject);
begin
 if SwitchPanel.Visible then
  begin
   LoadBitmapFromRes('TOOLBARLIST', SwitchToolBarButton.Glyph);
   ActionSwitch.Caption := SWITCHTOLIST_ITEM;
   PreviewPanel.Visible := true;
   ActionResize.Enabled := true;
   PreviewStatusBar.Visible := true;
   FileViewStatusBar.Visible := false;
   SwitchPanel.Visible := false;
   if FileListView.Items.Count > 0 then
    PreviewImage.ShowPreview(FileListView.Selected);
  end
 else
  begin
   LoadBitmapFromRes('TOOLBARPIC', SwitchToolBarButton.Glyph);
   ActionSwitch.Caption := SWITCHTOPICTURE_ITEM;
   PreviewPanel.Visible := false;
   PreviewStatusBar.Visible := false;
   FileViewStatusBar.Visible := true;
   ActionResize.Enabled := false;
   SwitchPanel.Visible := true;
   FileViewStatusBar.ShowParam;
   FileListView.SetFocus;
  end
end;

procedure TExifDateMainForm.ResizePreviewImage(Sender: TObject);
begin
 if PreviewImage.Fit then
  begin
   PreviewImage.Fit := false;
   ActionResize.Caption := REDUCED_ITEM;
   PreviewImage.Align := alNone;
   PreviewImage.Proportional := false;
   PreviewImage.Cursor := crHandPoint;
   LoadBitmapFromRes('TOOLBAR100', ResizeToolBarButton.Glyph);
  end
 else
  begin
   PreviewImage.Fit := true;
   ActionResize.Caption := ENLARGE_ITEM;
   PreviewImage.Align := alClient;
   PreviewImage.Proportional := true;
   PreviewImage.Cursor := crDefault;
//   PreviewPanel.Align := alClient;
   LoadBitmapFromRes('TOOLBAR1002', ResizeToolBarButton.Glyph);
  end;
 PreviewImage.Invalidate;
end;

procedure TExifDateMainForm.EnableFormForReadingFiles;
begin
 ActionSwitch.Enabled := true;
 ActionRename.Enabled := true;
 ActionShowStatistics.Enabled := true;
end;

procedure TExifDateMainForm.EnablerNextPrevButton;
begin
 ActionPrev.Enabled := true;
 ActionNext.Enabled := true;
 if FileListView.Items.Count < 2 then
  begin
   ActionPrev.Enabled := false;
   ActionNext.Enabled := false;
  end;

 if FileListView.Items.Count > 1 then
  if FileListView.ItemIndex = 0 then
   ActionPrev.Enabled := false;

 if FileListView.Items.Count > 1 then
  if FileListView.ItemIndex = FileListView.Items.Count - 1 then
   ActionNext.Enabled := false;

 if PreviewProgressBar <> nil then
  begin
   ActionPrev.Enabled := false;
   ActionNext.Enabled := false;
  end;
end;

procedure TExifDateMainForm.TempClickToolBarButton(Sender: TObject);
begin
 try
  PreviewImage.Free;
 except
 end
end;

procedure TExifDateMainForm.Rename(Sender: TObject);

 procedure EnableControls(en: boolean);
 begin
  FolderTreeView.Enabled := en;
  ExifGrid.Enabled := en;
  FileListView.Enabled := en;
 end;

begin
 {$IFDEF LOGMODE}LogAdd('-= Rename =-');{$ENDIF LOGMODE}
 RenameForm := TRenameForm.CreateNew(Self);
 try
  EnableControls(false);
  RenameForm.Caption := 'Renaming files';
  if RenameForm.ReadCommonTags then
   if RenameForm.ShowModal = mrOk then
    begin
//     ShowMessage('N');
//     FolderTreeView.Enabled := true;
//     FolderTreeViewChange(FolderTreeView, FolderTreeView.Selected);
    end
   else
  else
   if not RenameForm.Cancel then
    ShowMessage('Not found common tags!');
 finally
  RenameForm.Free;
  EnableControls(true);
  FileListView.SetFocus;
 end;
 {$IFDEF LOGMODE}LogAdd('-= EndRename =-');{$ENDIF LOGMODE}
end;

procedure TExifDateMainForm.CreateToolBar(iToolBar: TToolBar);
var
 ToolBarButton: TToolBarButton;
 ToolBarSeparator: TToolBarSeparator;
 ToolButtonWidth, ToolButtonHeight: byte;
 Color: TColor;
begin
 iToolBar.Color := ToolBarColor;

 Color := iToolBar.Color;

 ToolButtonWidth := 50;
 ToolButtonHeight := 50;

 {Open}ToolBarButton := TToolBarButton.Create(ToolBar);
 {Open}ToolBarButton.Parent := iToolBar;
 {Open}ToolBarButton.Action := ActionOpen;
 {Open}ToolBarButton.DisplayMode := dmBoth;
 {Open}ToolBarButton.DisplayMode := dmGlyphOnly;
 {Open}ToolBarButton.Height := ToolButtonHeight;
 {Open}ToolBarButton.Width := ToolButtonWidth;
 {Open}ToolBarButton.NoBorder := true;
 LoadBitmapFromRes('TOOLBAROPEN', {Open}ToolBarButton.Glyph);
 {Open}ToolBarButton.NumGlyphs := 3;
 {Open}ToolBarButton.Color := Color;

 SwitchToolBarButton := TToolBarButton.Create(ToolBar);
 SwitchToolBarButton.Parent := iToolBar;
 SwitchToolBarButton.Action := ActionSwitch;
 SwitchToolBarButton.DisplayMode := dmBoth;
 SwitchToolBarButton.DisplayMode := dmGlyphOnly;
 SwitchToolBarButton.Height := ToolButtonHeight;
 SwitchToolBarButton.Width := ToolButtonWidth;
 SwitchToolBarButton.WordWrap := true;
 SwitchToolBarButton.Enabled := false;
 SwitchToolBarButton.NoBorder := true;
 LoadBitmapFromRes('TOOLBARPIC', SwitchToolBarButton.Glyph);
 SwitchToolBarButton.NumGlyphs := 3;
 SwitchToolBarButton.Color := Color;

 ToolBarSeparator := TToolBarSeparator.Create(ToolBar);
 ToolBarSeparator.Parent := iToolBar;

 {Prev}ToolBarButton := TToolBarButton.Create(ToolBar);
 {Prev}ToolBarButton.Parent := iToolBar;
 {Prev}ToolBarButton.Action := ActionPrev;
 {Prev}ToolBarButton.Height := ToolButtonHeight;
 {Prev}ToolBarButton.Width := ToolButtonWidth;
 {Prev}ToolBarButton.Enabled := false;
 {Prev}ToolBarButton.DisplayMode := dmGlyphOnly;
 {Prev}ToolBarButton.OnClick := PrevFileListView;
 {Prev}ToolBarButton.Caption := 'Prev';
 {Prev}ToolBarButton.NoBorder := true;
 LoadBitmapFromRes('TOOLBARPREV', {Prev}ToolBarButton.Glyph);
 {Prev}ToolBarButton.NumGlyphs := 3;
 {Prev}ToolBarButton.Color := Color;

 {Next}ToolBarButton := TToolBarButton.Create(ToolBar);
 {Next}ToolBarButton.Parent := iToolBar;
 {Next}ToolBarButton.Action := ActionNext;
 {Next}ToolBarButton.Height := ToolButtonHeight;
 {Next}ToolBarButton.Width := ToolButtonWidth;
 {Next}ToolBarButton.Enabled := false;
 {Next}ToolBarButton.DisplayMode := dmGlyphOnly;
 {Next}ToolBarButton.OnClick := NextFileListView;
 {Next}ToolBarButton.Caption := 'Next';
 {Next}ToolBarButton.NoBorder := true;
 LoadBitmapFromRes('TOOLBARNEXT', {Next}ToolBarButton.Glyph);
 {Next}ToolBarButton.NumGlyphs := 3;
 {Next}ToolBarButton.Color := Color;

 ToolBarSeparator := TToolBarSeparator.Create(ToolBar);
 ToolBarSeparator.Parent := iToolBar;

 ResizeToolBarButton := TToolBarButton.Create(ToolBar);
 ResizeToolBarButton.Parent := iToolBar;
 ResizeToolBarButton.Action := ActionResize;
 ResizeToolBarButton.Height := ToolButtonHeight;
 ResizeToolBarButton.Width := ToolButtonWidth;
 ResizeToolBarButton.WordWrap := true;
 ResizeToolBarButton.DisplayMode := dmGlyphOnly;
 ResizeToolBarButton.NoBorder := true;
 LoadBitmapFromRes('TOOLBAR1002', ResizeToolBarButton.Glyph);
 ResizeToolBarButton.NumGlyphs := 3;
 ResizeToolBarButton.Color := Color;

 ToolBarSeparator := TToolBarSeparator.Create(ToolBar);
 ToolBarSeparator.Parent := iToolBar;

 ToolBarButton := TToolBarButton.Create(ToolBar);
 ToolBarButton.Parent := iToolBar;
 ToolBarButton.Action := ActionRename;
 ToolBarButton.Height := ToolButtonHeight;
 ToolBarButton.Width := ToolButtonWidth;
 ToolBarButton.WordWrap := true;
 ToolBarButton.DisplayMode := dmGlyphOnly;
 ToolBarButton.NoBorder := true;
 LoadBitmapFromRes('TOOLBARRENAME', ToolBarButton.Glyph);
 ToolBarButton.NumGlyphs := 3;
 ToolBarButton.Color := Color;

 ToolBarButton := TToolBarButton.Create(ToolBar);
 ToolBarButton.Parent := iToolBar;
 ToolBarButton.Action := ActionShowStatistics;
 ToolBarButton.Height := ToolButtonHeight;
 ToolBarButton.Width := ToolButtonWidth;
 ToolBarButton.WordWrap := true;
 ToolBarButton.DisplayMode := dmGlyphOnly;
 ToolBarButton.NoBorder := true;
 LoadBitmapFromRes('TOOLBARSTATISTICS', ToolBarButton.Glyph);
 ToolBarButton.NumGlyphs := 3;
 ToolBarButton.Color := Color;

// ReadStatistics
end;

procedure TExifDateMainForm.DisableFormForReadingFiles;
// ������� �������� ����� � ��������� ������ ������
begin
 ActionSwitch.Enabled := false;
 ActionRename.Enabled := false;
 ActionShowStatistics.Enabled := false;
end;

procedure TPreviewImage.PreviewLoadProgress(Sender: TObject; Stage: TProgressStage; PercentDone: Byte; RedrawNow: Boolean; const R: TRect; const Msg: String);
var
  X,i: Integer;
  s: string[4];
  PanelIndex: Byte;
begin
 try
 with PreviewStatusBar do
  begin
   PanelIndex := PreviewStatusBar.Panels.Count - 1;
   case Stage of
    psStarting: begin
                 for i := 2 to PanelIndex do Panels[i].Text :='';
                 if PreviewProgressBar <> nil then PreviewProgressBar.Free;
                 PreviewProgressBar := TExifProgressBar.Create(nil);
                 PreviewProgressBar.Parent := PreviewStatusBar;
                 PreviewProgressBar.Height := PreviewStatusBar.ClientHeight - 5;
                 PreviewProgressBar.BorderWidth := 0;
                 PreviewProgressBar.Color := ChangeColor(PreviewStatusBar.Color, false, 50);
                 PreviewProgressBar.Smooth := true;
                 PreviewProgressBar.Max := 100;
                 if Msg <> '' then PreviewStatusbar.Panels[PanelIndex - 1].Text := Msg
                 else PreviewStatusbar.Panels[PanelIndex - 1].Text := 'Transfering...';
                 X := 0;
                 for i := 0 to PanelIndex - 1 do X := X + Panels[i].Width;
                 PreviewProgressBar.SetBounds(X, PreviewStatusBar.Height div 2 - PreviewProgressBar.Height div 2 + 1, Panels[PanelIndex].Width - PreviewStatusBar.ClientHeight, PreviewProgressBar.Height);
                 SetWindowLong(PreviewProgressBar.Handle, GWL_EXSTYLE, DWORD(GetWindowLong(PreviewProgressBar.Handle, GWL_EXSTYLE)) and (not WS_EX_STATICEDGE));
                 SendMessage(PreviewProgressBar.Handle, PBM_SETBARCOLOR, 0, ChangeColor(PreviewStatusBar.Color, false, 0));
                 PreviewProgressBar.Show;
                 ExifDateMainForm.EnablerNextPrevButton;
                 Application.ProcessMessages;
                end;
    psEnding:  begin
                 PreviewStatusbar.loaded := GetTickCount - PreviewStatusbar.loaded;
                 PreviewStatusbar.ShowParam;
                 PreviewProgressBar.Free;
                 PreviewProgressBar := nil;
                 ExifDateMainForm.EnablerNextPrevButton;
//                 if (Self.Parent.Width <> FParentWidth) or (Self.Parent.Height <> FParentHeight) then
                 Application.ProcessMessages;
                 Invalidate;
                end;
    psRunning: begin
                 if PreviewImage = nil then asm int 3 end;
                 PreviewProgressBar.Position := PercentDone;
                 PreviewProgressBar.Update;
//                 Application.ProcessMessages;
                end;
   end;
  end;
 except
 end;
end;

procedure TExifDateMainForm.ResizeThumbnailForm;
begin
 if ThumbnailForm.HostDockSite = nil then
  if ThumbnailImage.Thumbnail then
   begin
    ThumbnailForm.ClientWidth := ThumbnailImage.Picture.Width + 20;
    ThumbnailForm.ClientHeight := ThumbnailImage.Picture.Height + 20;
    ThumbnailForm.Color := clWhite;
   end
  else
   begin
//    ThumbnailForm.Width := 500;
//    ThumbnailForm.Height := 500;
   end
end;

procedure TPreviewStatusBar.StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
var
 Icon: TIcon;
 iRect: TRect;
 ShFileInfo: TShFileInfo;
 X: integer;
begin
 case Panel.Index of
  0,
  2,
  3,
  4,
  5,
  6: X := (Rect.Left + (Rect.Right - Rect.Left) div 2) - StatusBar.Canvas.TextWidth(Panel.Text) div 2;
  7: X := Rect.Left + 3;
 end;
 case Panel.Index of
  1: begin
       try
        Icon := TIcon.Create;
        ShGetFileInfo(Pchar(Dir(FolderTreeView.Path) + FileListView.Selected.Caption), 0, ShFileInfo, SizeOf(ShFileInfo), SHGFI_ICON + SHGFI_SMALLICON);
        Icon.Handle := ShFileInfo.hIcon;
        iRect.Left := Rect.Left + 3;
        iRect.Top := Rect.Top;
        iRect.Bottom := Rect.Bottom;
        iRect.Right := Rect.Bottom;
        StatusBar.Canvas.StretchDraw(iRect, Icon);
       except
       end;
       StatusBar.Canvas.TextRect(Classes.Rect(Rect.Left + Rect.Bottom, Rect.Top, Rect.Right, Rect.Bottom), Rect.Left + Rect.Bottom + 5, Rect.Bottom - StatusBar.Canvas.TextHeight(' ') - 2, Panel.Text);
       Icon.Free;
      end;
  8: StatusBar.Canvas.TextRect(Rect, Rect.Left + 3, Rect.Bottom - StatusBar.Canvas.TextHeight(' ') - 2, MinimizeName(Panel.Text, StatusBar.Canvas, Rect.Right - Rect.Left - StatusBar.ClientHeight));
 else begin
       StatusBar.Canvas.TextRect(Rect, X, Rect.Bottom - StatusBar.Canvas.TextHeight(' ') - 2, Panel.Text);
      end;
 end;
 StatusBar.Canvas.Pen.Color := ChangeColor(StatusBar.Color, false, 50);
 if (Panel.Index < StatusBar.Panels.Count - 1) and (Panel.Text <> '') then
  begin
   StatusBar.Canvas.MoveTo(Rect.Right - 1, Rect.Top + 2);
   StatusBar.Canvas.LineTo(Rect.Right - 1, Rect.Bottom - 2);
  end;
end;

procedure TFileViewStatusBar.StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
var
 Icon: TIcon;
 iRect: TRect;
 ShFileInfo: TShFileInfo;
 X: integer;
begin
 StatusBar.Canvas.Brush.Color := StatusBar.Color;
 case Panel.Index of
  0,
  2,
  3,
  4,
  5,
  6: X := (Rect.Left + (Rect.Right - Rect.Left) div 2) - StatusBar.Canvas.TextWidth(Panel.Text) div 2;
  7: X := Rect.Left + 3;
 end;
 case Panel.Index of
   3,4,5: if FFocused = sbExifGrid then
            StatusBar.Canvas.Font.Color := $900000    // ����� �����
           else
            StatusBar.Canvas.Font.Color := $004000;   // ����� �������
 else
  StatusBar.Canvas.Font.Color := clDefault;
 end;
 case Panel.Index of
  1: begin
       if FileListView.SelCount > 0 then
        try
         Icon := TIcon.Create;
         ShGetFileInfo(Pchar(Dir(FolderTreeView.Path) + FileListView.Selected.Caption), 0, ShFileInfo, SizeOf(ShFileInfo), SHGFI_ICON + SHGFI_SMALLICON);
         Icon.Handle := ShFileInfo.hIcon;
         iRect.Left := Rect.Left + 3;
         iRect.Top := Rect.Top;
         iRect.Bottom := Rect.Bottom;
         iRect.Right := Rect.Bottom;
         StatusBar.Canvas.StretchDraw(iRect, Icon);
         StatusBar.Canvas.TextRect(Classes.Rect(Rect.Left + Rect.Bottom, Rect.Bottom - StatusBar.Canvas.TextHeight(' ') - 1, Rect.Right, Rect.Bottom), Rect.Left + Rect.Bottom + 5, Rect.Bottom - StatusBar.Canvas.TextHeight(' ') - 2, Panel.Text);
         Icon.Free;
        except
        end;
      end;
  8: StatusBar.Canvas.TextRect(Rect, Rect.Left + 3, Rect.Bottom - StatusBar.Canvas.TextHeight(' ') - 2, MinimizeName(Panel.Text, StatusBar.Canvas, Rect.Right - Rect.Left - StatusBar.ClientHeight));
 else begin
       StatusBar.Canvas.TextRect(Rect, X, Rect.Bottom - StatusBar.Canvas.TextHeight(' ') - 2, Panel.Text);
      end;
 end;
 StatusBar.Canvas.Pen.Color := ChangeColor(StatusBar.Color, false, 50);
 if (Panel.Index < StatusBar.Panels.Count - 1) and (Panel.Text <> '') then
  begin
   StatusBar.Canvas.MoveTo(Rect.Right - 1, Rect.Top + 2);
   StatusBar.Canvas.LineTo(Rect.Right - 1, Rect.Bottom - 2);
  end;
end;

procedure TPreviewStatusBar.InitStatusBar;
var
 i,j: integer;
begin
 Self.OnDrawPanel := StatusBarDrawPanel;
 with Self.Panels.Add do
  begin
   width := 50;
   style := psOwnerDraw;
   bevel := pbNone;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 60;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 60;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 100;
  end;
 with Self.Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 200;
  end;
 j := 0;
 for i:= 0 to Self.Panels.Count - 2 do
  j := j + Self.Panels[i].Width;
 Self.Panels[Self.Panels.Count - 1].Width := Self.ClientWidth - j - 5;
end;

procedure TFileViewStatusBar.InitStatusBar;
var
 i,j: integer;
begin
 OnDrawPanel := StatusBarDrawPanel;
 with Panels.Add do
  begin
   width := 50;
   style := psOwnerDraw;
   bevel := pbNone;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 60;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 60;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 80;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 100;
  end;
 with Panels.Add do
  begin
   style := psOwnerDraw;
   bevel := pbNone;
   Width := 200;
  end;
 j := 0;
 for i:= 0 to Panels.Count - 2 do
  j := j + Panels[i].Width;
 Panels[Panels.Count - 1].Width := ClientWidth - j - 5;
end;

procedure TExifDateMainForm.Timer1Timer(Sender: TObject);
{$IFDEF LOGMODE}
var
 s, ss: string;
{$ENDIF LOGMODE}
begin
{$IFDEF LOGMODE}
 try
 TempPanel.Caption := IntToStr(FoldersDockPanel.DockClientCount);
//  if ThumbnailImage.Picture.Graphic.Empty then TempPanel.Caption := 'Empty' else TempPanel.Caption := 'Picture';
 except
 end;
// TempPanel.Caption := IntToStr(GetTickCount);
{$ENDIF LOGMODE}
end;

procedure TExifDateMainForm.ExifGridSelectCell(Sender: TObject; ACol, ARow: Integer; SelectItem: TSelectedRow);
begin
 if FileViewStatusBar <> nil then FileViewStatusBar.ShowParam;
end;

procedure TExifDateMainForm.ExifGridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
{$IFDEF LOGMODE}
 try
  if FileViewStatusBar.FReading then
   TempPanel.Caption := IntToStr(StrToInt(TempPanel.Caption) + 1);
 except
  TempPanel.Caption := '0';
 end;
{$ENDIF LOGMODE}
 if Assigned(ExifGrid) then
  if Assigned(ExifGrid.Exif) then
   if ExifGrid.Exif.ValidExif then
    if (Length(ExifGrid.SelectedRow.Name) > 0) and (Length(ExifGrid.SelectedRow.Value) > 0) then
     begin
      FileViewStatusBar.FFocused := sbExifGrid;
      if not FileViewStatusBar.FReading then
       FileViewStatusBar.ShowParam;
     end;
end;

procedure TExifDateMainForm.FileListViewMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
// if not (Sender as TWinControl).Focused then (Sender as TWinControl).SetFocus;
 FileViewStatusBar.FFocused := sbFileView;
 FileViewStatusBar.ShowParam;
end;

procedure TExifDateMainForm.FormResize(Sender: TObject);
begin
 if Assigned(PreviewImage) then PreviewImage.Invalidate;
end;

procedure TExifDateMainForm.FormCreate(Sender: TObject);
var
 i: integer;
 Panel4,
 Panel2,
 Panel3: TPanel;
 JPEGPicture: TPicture;
 BitMap: TBitMap;
 Canv: TCanvas;
 b: boolean;
begin
// GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, FormatSettings);

 FilesReadThredCount := 0;
// DoubleBuffered := true;

 Cancel := false;
// FilesExifList := TFilesExifList.Create;

 ActionNext := TAction.Create(Application);
 ActionNext.Caption := NEXT_ITEM;
 ActionNext.OnExecute := NextFileListView;

 ActionPrev := TAction.Create(Application);
 ActionPrev.Caption := PREV_ITEM;
 ActionPrev.OnExecute := PrevFileListView;

 ActionOpen := TAction.Create(Application);
 ActionOpen.Caption := OPEN_ITEM;
 ActionOpen.OnExecute := OpenFolder;
 ActionOpen.Enabled := false;

 ActionSwitch := TAction.Create(Application);
 ActionSwitch.Caption := SWITCHTOPICTURE_ITEM;
 ActionSwitch.OnExecute := SwitchToPicture;

 ActionResize := TAction.Create(Application);
 ActionResize.Caption := ENLARGE_ITEM;
 ActionResize.Enabled := false;
 ActionResize.OnExecute := ResizePreviewImage;

 ActionViewFolders := TAction.Create(Application);
 ActionViewFolders.Caption := SHOWFOLDERS_ITEM;
 ActionViewFolders.OnExecute := ShowFoldersForm;
 ActionViewFolders.Checked := true;
 ActionViewFolders.Enabled := false;

 ActionViewThumbnail := TAction.Create(Application);
 ActionViewThumbnail.Caption := SHOWTHUMBNAIL_ITEM;
 ActionViewThumbnail.OnExecute := ShowThumbnailForm;
 ActionViewThumbnail.Checked := true;
 ActionViewThumbnail.Enabled := false;

 ActionViewExifGrid := TAction.Create(Application);
 ActionViewExifGrid.Caption := SHOWEXIFGRID_ITEM;
 ActionViewExifGrid.OnExecute := ShowExifGridForm;
 ActionViewExifGrid.Checked := true;
 ActionViewExifGrid.Enabled := false;

 ActionRename := TAction.Create(Application);
 ActionRename.Caption := RENAME_ITEM;
 ActionRename.OnExecute := Rename;
 
 ActionShowStatistics := TAction.Create(Application);
 ActionShowStatistics.Caption := STATISTICS_ITEM;
 ActionShowStatistics.OnExecute := ReadStatistics;
 ActionShowStatistics.Checked := true;
 ActionShowStatistics.Enabled := true;

// Timer1 := TTimer.Create(self);
// Timer1.Interval := 100;
// Timer1.OnTimer := Timer1Timer;

// ����������� ����� ������� ...................................................
 MyReg.RootKey := HKEY_CURRENT_USER;
 MyReg.Key := 'Software\Lithe\ExifDateManager';

// ��������� ���� (maximized, normal, ...) .....................................
 if MyReg.ReadFromReg('Maximized', b) then
  WindowState := wsMaximized;
// ������ ......................................................................
 if MyReg.ReadFromReg('Width', i) then
  Width := i
 else
  begin
   Width := Screen.Width div 5 * 4;
   MyReg.WriteToReg('Width', Width);
  end;
// ������ ......................................................................
 if MyReg.ReadFromReg('Height', i) then
  Height := i
 else
  begin
   Height := Screen.Height div 5 * 4;
   MyReg.WriteToReg('Height', Height);
  end;
// ���������� �� ��� ������� ...................................................
 if MyReg.ReadFromReg('Left', i) then
  Left := i
 else
  begin
   Left := Screen.Width div 2 - Width div 2;
   MyReg.WriteToReg('Left', Left);
  end;
// ���������� �� ��� ������� ...................................................
 if MyReg.ReadFromReg('Top', i) then
  Top := i
 else
  begin
   Top := Screen.Height div 2 - Height div 2;
   MyReg.WriteToReg('Top', Top);
  end;

// ����������� ����� (�������� �� DFM) .........................................
 Caption := Application.Title;
 DragKind := dkDock;
 Font.Charset := DEFAULT_CHARSET;
 Font.Color := clWindowText;
 Font.Height := -11;
 Font.Name := 'MS Sans Serif';
 Font.Style := [];
 OldCreateOrder := False;
 Position := poDesigned;//poScreenCenter;
 KeyPreview := true;
 BorderStyle := bsSizeable;
 FormStyle := fsNormal;
 OnKeyDown := FormKeyDown;
 OnResize := FormResize;
// ����������� ������������ � ��������� ������ ���� � ������� ..................
 InitFS;

{$IFDEF LOGMODE}
// ������� � ����������� �� ������ ��������������� ������ ......................
 TempPanel := TPanel.Create(Self);
 TempPanel.Parent := Self;
 TempPanel.Height := 250;
 TempPanel.Align := alTop;
 LogSplitter := TSplitter.Create(Self);
 LogSplitter.Parent := Self;
 LogSplitter.Top := TempPanel.Top + TempPanel.Height;
 LogSplitter.Align := alTop;
 LogSplitter.Height := 3;
 LogSplitter.Color := clRed;
 LogMemo := TMemo.Create(TempPanel);
 LogMemo.Parent := TempPanel;
 LogMemo.Align := alClient;
 LogMemo.Width := TempPanel.ClientWidth div 3;
 LogMemo.ScrollBars := ssBoth;
{$ENDIF LOGMODE}

// ������� � ����������� File List Status Bar ..................................
 FileViewStatusBar := TFileViewStatusBar.Create(Self);
 FileViewStatusBar.Parent := Self;
 FileViewStatusBar.Height := 21;
 FileViewStatusBar.SizeGrip := false;
 FileViewStatusBar.InitStatusBar;
// ������� � ����������� Preview Status Bar ....................................
 PreviewStatusBar := TPreviewStatusBar.Create(Self);
 PreviewStatusBar.Parent := Self;
 PreviewStatusBar.Height := 21;
 PreviewStatusBar.Visible := false;
 PreviewStatusBar.InitStatusBar;
// ������� � ����������� ������ DOCK ��� TOOLBAR ...............................
 ToolBarDock := TToolBarDock.Create(ExifDateMainForm);
 ToolBarDock.Parent := Self;
 ToolBarDock.Position := dpBottom;
 ToolBarDock.Color := ToolBarColor;
// ������� � ����������� ������� DOCK ��� TOOLBAR ..............................
 ToolBarDock := TToolBarDock.Create(Self);
 ToolBarDock.Parent := Self;
 ToolBarDock.Position := dpTop;
 ToolBarDock.Color := ToolBarColor;
// ������� � ����������� TOOLBAR ...............................................
 ToolBar := TToolBar.Create(Self);
 ToolBar.Parent := Self;
 ToolBar.ManualDock(ToolBarDock);
 CreateToolBar(ToolBar);
// ������� � ����������� �������� ������ .......................................
 MainPanel := TPanel.Create(Self);
 MainPanel.Parent := Self;
 MainPanel.Align := alclient;
 MainPanel.BevelOuter := bvNone;
// ������� � ����������� ������ ������ ��� �������� �� ��� ExifGrid ............
 ExifGridRightDockPanel := TExifGridRightDockPanel.Create(MainPanel);
 ExifGridRightDockPanel.Parent := MainPanel;
 ExifGridRightDockPanel.Align := alRight;
 ExifGridRightDockPanel.Width := 0;
 ExifGridRightDockPanel.BevelOuter := bvNone;
 ExifGridRightDockPanel.OnGetSiteInfo := ExifGridDockPanelGetSiteInfo;
 ExifGridRightDockPanel.OnDockOver := ExifGridDockPanelDockOver;
 ExifGridRightDockPanel.OnDockDrop := ExifGridDockPanelDockDrop;
 ExifGridRightDockPanel.OnUnDock := ExifGridDockPanelUnDock;
 ExifGridRightDockPanel.DockSite := true;
 ExifGridRightDockPanel.DragMode := dmManual;
 ExifGridRightDockPanel.DragKind := dkDock;
// ������� � ����������� ������ DOCK ��� TOOLBAR ...............................
 ToolBarDock := TToolBarDock.Create(ExifDateMainForm);
 ToolBarDock.Parent := MainPanel;
 ToolBarDock.Position := dpRight;
 ToolBarDock.Color := ToolBarColor;
// ������� � ����������� ������ �������� �� ������� ����� ������� ExifGrid .....
 ExifGridRightDockPanelSplitter := TExifDateSplitter.Create(MainPanel);
 ExifGridRightDockPanelSplitter.Parent := MainPanel;
 ExifGridRightDockPanelSplitter.Align := alRight;
 ExifGridRightDockPanelSplitter.Width := 5;
 ExifGridRightDockPanelSplitter.Visible := false;
// ������� � ����������� ����� ������ ��� �������� �� ��� ExifGrid .............
 ExifGridLeftDockPanel := TExifGridLeftDockPanel.Create(MainPanel);
 ExifGridLeftDockPanel.Parent := MainPanel;
 ExifGridLeftDockPanel.Align := alLeft;
 ExifGridLeftDockPanel.Width := 0;
 ExifGridLeftDockPanel.BevelOuter := bvNone;
 ExifGridLeftDockPanel.OnGetSiteInfo := ExifGridDockPanelGetSiteInfo;
 ExifGridLeftDockPanel.OnDockOver := ExifGridDockPanelDockOver;
 ExifGridLeftDockPanel.OnDockDrop := ExifGridDockPanelDockDrop;
 ExifGridLeftDockPanel.OnUnDock := ExifGridDockPanelUnDock;
 ExifGridLeftDockPanel.DockSite := true;
 ExifGridLeftDockPanel.DragMode := dmManual;
 ExifGridLeftDockPanel.DragKind := dkDock;
// ������� � ����������� ����� �������� �� ������� ����� ������� ExifGrid ......
 ExifGridLeftDockPanelSplitter := TExifDateSplitter.Create(MainPanel);
 ExifGridLeftDockPanelSplitter.Parent := MainPanel;
 ExifGridLeftDockPanelSplitter.Left := ExifGridLeftDockPanel.Left + ExifGridLeftDockPanel.Width + 1;
 ExifGridLeftDockPanelSplitter.Align := alLeft;
 ExifGridLeftDockPanelSplitter.Width := 5;
 ExifGridLeftDockPanelSplitter.Visible := false;
// ������� � ����������� ����� DOCK ��� TOOLBAR ..............................
 ToolBarDock := TToolBarDock.Create(MainPanel);
 ToolBarDock.Parent := MainPanel;
 ToolBarDock.Position := dpLeft;
 ToolBarDock.Color := ToolBarColor;
// �������, ����������� � ����������� ����� � ExifGrid .........................
 ExifGridForm := TExifGridForm.CreateNew(Self); //??
 ExifGridForm.Width := 300;
 ExifGridForm.Color := clWhite;
 ExifGridForm.ManualDock(ExifGridRightDockPanel, nil, alClient);
// ������� � �����������  ���� ExifGrid ........................................
 ExifGrid := TExifGrid.Create(ExifGridForm);
 ExifGrid.Parent := ExifGridForm;
 ExifGrid.BorderStyle := bsNone;
 ExifGrid.Indentions.Indention := 1;
 ExifGrid.Indentions.TitleIndention := 18;
 ExifGrid.AutoCatchFocus := false;
 ExifGrid.Colors.FileNameBgColor := ChangeColor(ToolBarColor, true, 30);
 ExifGrid.Colors.CursorColor := ChangeColor(ToolBarColor, true, 30);
 ExifGrid.Colors.CursorFrameColor := ChangeColor(ToolBarColor, false, 50);
 ExifGrid.UnknownVisible := false;
 ExifGrid.Align := alClient;
 ExifGrid.LogAlwaysVisible := true;       // ???????????????????????????????????
 ExifGrid.OnMouseMove := ExifGridMouseMove;
 ExifGrid.OnSelectCell := ExifGridSelectCell;
// ���������� ����������� ����� � ExifGrid .....................................
 ExifGridForm.Show;

// ������ ExifGrid .............................................................
 if MyReg.ReadFromReg('ExifGridRightDockPanelWidth', i) then
  ExifGridRightDockPanel.Width := i
 else
  begin
   ExifGridRightDockPanel.Width := Width div 3;
   MyReg.WriteToReg('ExifGridRightDockPanelWidth', ExifGridRightDockPanel.Width);
  end;
// ������� � ����������� ��������������� ������ ................................
 PreviewPanel := TPanel.Create(MainPanel);
 PreviewPanel.Parent := MainPanel;
 PreviewPanel.Color := clBlack;
 PreviewPanel.Align := alClient;
 PreviewPanel.Visible := false;
 PreviewPanel.BevelOuter := bvNone;
 PreviewPanel.OnResize := PreviewPanelResize;
// ������� � ����������� Preview Image .......................................
 PreviewImage := TPreviewImage.Create(PreviewPanel);
 PreviewImage.Parent := PreviewPanel;
 PreviewImage.Align := alClient;
 PreviewImage.Center := true;
 PreviewImage.AutoSize := true;
 PreviewImage.IncrementalDisplay := true;
 PreviewImage.Proportional := true;
 PreviewImage.Transparent := false;

 ModelLabel := TLabel.Create(Self);
 ModelLabel.Parent := PreviewPanel;
 ModelLabel.Left := 10;
 ModelLabel.Top := 10;
 ModelLabel.Width := 200;
 ModelLabel.Caption := '';
 ModelLabel.Font.Size := 16;
 ModelLabel.Font.Color := clWhite;
 ModelLabel.Transparent := false;
 ModelLabel.Color := clBlack;

 ShutterSpeedValueLabel := TLabel.Create(Self);
 ShutterSpeedValueLabel.Parent := PreviewPanel;
 ShutterSpeedValueLabel.Left := 10;
 ShutterSpeedValueLabel.Top := 40;
 ShutterSpeedValueLabel.Width := 200;
 ShutterSpeedValueLabel.Caption := '';
 ShutterSpeedValueLabel.Font.Size := 16;
 ShutterSpeedValueLabel.Font.Color := clWhite;
 ShutterSpeedValueLabel.Transparent := false;
 ShutterSpeedValueLabel.Color := clBlack;

 ApertureValueLabel := TLabel.Create(Self);
 ApertureValueLabel.Parent := PreviewPanel;
 ApertureValueLabel.Left := 10;
 ApertureValueLabel.Top := 70;
 ApertureValueLabel.Width := 200;
 ApertureValueLabel.Caption := '';
 ApertureValueLabel.Font.Size := 16;
 ApertureValueLabel.Font.Color := clWhite;
 ApertureValueLabel.Transparent := false;
 ApertureValueLabel.Color := clBlack;

 ISOValueLabel := TLabel.Create(Self);
 ISOValueLabel.Parent := PreviewPanel;
 ISOValueLabel.Left := 10;
 ISOValueLabel.Top := 100;
 ISOValueLabel.Width := 200;
 ISOValueLabel.Caption := '';
 ISOValueLabel.Font.Size := 16;
 ISOValueLabel.Font.Color := clWhite;
 ISOValueLabel.Transparent := false;
 ISOValueLabel.Color := clBlack;

 LensValueLabel := TLabel.Create(Self);
 LensValueLabel.Parent := PreviewPanel;
 LensValueLabel.Left := 10;
 LensValueLabel.Top := 130;
 LensValueLabel.Width := 200;
 LensValueLabel.Caption := '';
 LensValueLabel.Font.Size := 16;
 LensValueLabel.Font.Color := clWhite;
 LensValueLabel.Transparent := false;
 LensValueLabel.Color := clBlack;

// ������� � ����������� ��������������� ������ ................................
 SwitchPanel := TPanel.Create(MainPanel);
 SwitchPanel.Parent := MainPanel;
 SwitchPanel.Align := alClient;
 SwitchPanel.BevelOuter := bvNone;
// ������� � ����������� ��������������� ������ ................................
 Panel2 := TPanel.Create(SwitchPanel);
 Panel2.Parent := SwitchPanel;
 Panel2.Align := alTop;
 Panel2.Height := 0;
 Panel2.BevelOuter := bvNone;
// ������� � ����������� ����� ������ ��� �������� �� ��� FolderForm � Thumbnail .....
 FoldersDockPanel := TFoldersDockPanel.Create(Panel2);
 FoldersDockPanel.Parent := Panel2;
 FoldersDockPanel.Align := alClient;
 FoldersDockPanel.BevelOuter := bvNone;
 FoldersDockPanel.OnDockOver := FoldersDockPanelDockOver;
 FoldersDockPanel.OnGetSiteInfo := FoldersDockPanelGetSiteInfo;
 FoldersDockPanel.OnDockDrop := FoldersDockPanelDockDrop;
 FoldersDockPanel.OnUnDock := FoldersDockPanelUnDock;
 FoldersDockPanel.DockSite := true;
 FoldersDockPanel.DragMode := dmManual;
 FoldersDockPanel.DragKind := dkDock;
// ������� � ����������� �������� �� ������� ����� ������� Folders .............
 FoldersDockPanelSplitter := TExifDateSplitter.Create(SwitchPanel);
 FoldersDockPanelSplitter.Parent := SwitchPanel;
 FoldersDockPanelSplitter.Top := Panel2.Top + Panel2.Height + 1;
 FoldersDockPanelSplitter.Align := alTop;
 FoldersDockPanelSplitter.Height := 5;
 FoldersDockPanelSplitter.Visible := false;
// �������, ����������� � ����������� ����� � Folders.......................
 FoldersForm := TFolderDockForm.CreateNew(Self);
 FoldersForm.Width := FoldersDockPanel.Width div 2;
 FoldersForm.ManualDock(FoldersDockPanel, nil, alClient);
 FoldersForm.Caption := 'Folders';
 FoldersForm.Show;
// ������� � ����������� ������ ��������� ......................................
 FolderTreeView := TShellTreeView.Create(FoldersForm);
 FolderTreeView.Parent := FoldersForm;
 FolderTreeView.BorderStyle := bsNone;
 FolderTreeView.Align := alClient;
 FolderTreeView.ObjectTypes := [otFolders];
 FolderTreeView.AutoExpand := true;
 FolderTreeView.AutoRefresh := true;
 FolderTreeView.HideSelection := false;
 FolderTreeView.Name := 'FolderTreeView';
// FolderTreeView.OnChange := FolderTreeViewChange;
// FolderTreeView.OnMouseMove := MouseMove;

 ShellChangeNotifier := TShellChangeNotifier.Create(FoldersForm);
 ShellChangeNotifier.NotifyFilters := [nfFileNameChange{, nfDirNameChange, nfAttributeChange, nfSizeChange, nfWriteChange, nfSecurityChange}];
 ShellChangeNotifier.WatchSubTree := false;

// ShellChangeNotifier.OnChange := ShellChangeNotifierChange;

// �������, ����������� � ����������� ����� � Thumbnail.....................
 ThumbnailForm := TThumbnailDockForm.CreateNew(Self); //??
 ThumbnailForm.Width := FoldersDockPanel.Width div 2;
 ThumbnailForm.BorderStyle := bsToolWindow;

 {ThumbnailForm.Color := clBlack;}
 ThumbnailForm.ManualDock(FoldersDockPanel, nil, alRight);
 ThumbnailForm.Show;
// ������� � ����������� Thumbnail Image .......................................
 ThumbnailImage := TThumbnailImage.Create(ThumbnailForm);
 ThumbnailImage.Parent := ThumbnailForm;
 ThumbnailImage.Name := 'ThumbnailImage';
 ThumbnailImage.Align := alClient;
 ThumbnailImage.Center := true;
 ThumbnailImage.AutoSize := true;
 ThumbnailImage.Transparent := false;
 NoThumbnailPaint(ThumbnailImage);
// ������� � ����������� TOOLBARDOCK ...........................................
 ToolBarDock := TToolBarDock.Create(SwitchPanel);
 ToolBarDock.Parent := SwitchPanel;
 ToolBarDock.Align := alTop;
 ToolBarDock.Position := dpTop;
 ToolBarDock.Color := ToolBarColor;
// ������� � ����������� ��������������� ������ ................................
 Panel3 := TPanel.Create(SwitchPanel);
 Panel3.Parent := SwitchPanel;
 Panel3.Align := alClient;
 Panel3.BevelOuter := bvNone;
// ������� � ����������� DIR ������ ...........................................
 DirPanel := TPanel.Create(Panel3);
 DirPanel.Parent := Panel3;
 DirPanel.Align := alTop;
 DirPanel.BevelOuter := bvRaised;
 DirPanel.BevelInner := bvLowered;
 DirPanel.Height := 21;
// ������� � ����������� DIR label .............................................
 DirLabel := TLabel.Create(DirPanel);
 DirLabel.Parent := DirPanel;
 DirLabel.Transparent := true;
 DirLabel.Top := DirPanel.ClientRect.Top + (DirPanel.ClientHeight div 2) - (DirLabel.Canvas.TextHeight('A') div 2);
 DirLabel.Left := 10 + DirPanel.ClientRect.Left;
 DirLabel.Width := DirPanel.ClientWidth - 20;
// ������� � ����������� popup ���� FileListView ...............................
 FileListViewPopupMenu := TPopupMenu.Create(Self);
 CreateFileListViewPopupMenu(FileListViewPopupMenu);
// ������� � ����������� FileListView ..........................................
 FileListView := TFileListView.Create(Panel3);
 FileListView.Top := DirPanel.Top + DirPanel.Height + 1;
 FileListView.Parent := Panel3;
 FileListView.Align := alClient;
 FileListView.BorderStyle := bsNone;
 FileListView.ViewStyle := vsReport;
 FileListView.RowSelect := true;
 FileListView.FlatScrollBars := false;
 FileListView.DoubleBuffered := true;
 FileListView.ReadOnly := true;
 FileListView.HideSelection := false;
 FileListView.MultiSelect := true;
 FileListView.PopupMenu := FileListViewPopupMenu;
// .................................................................
 FileListView.OwnerDraw := false;
// FileListView.OnCustomDrawItem := FileListViewCustomDrawItem;
// FileListView.OnCustomDrawSubItem := FileListViewCustomDrawSubItem;
// .................................................................
 FileListView.OnSelectItem := FileListViewSelectItem;
 FileListView.OnColumnClick := FileListViewColumnClick;
 FileListView.OnCompare := FileListViewCompare;
 FileListView.OnMouseMove := FileListViewMouseMove;
 FileListViewCreateColumns;
 SmallImagesList := TImageList.Create(FileListView);
 FileListView.SmallImages := SmallImagesList;
// ������� ������� ���� ����� ..................................................
 CreateMainMenu;
{$IFDEF LOGMODE}LogAdd('������ ���������');{$ENDIF LOGMODE}
end;

procedure TExifDateMainForm.FormActivate(Sender: TObject);
var
 s: string;
 i: integer;
 PM: TPopupMenu;
 pp, cp: TTreeNode;

 function GetPrevPath(Path: string): string;
 var
  i: integer;
 begin
  result := '';
  if Pos('\', Path) > 0 then
   for i := Length(Path) downto 0 do
    if Path[i] = '\' then
     break
    else
  else
   exit;
  result := Copy(Path, 0, i - 1);
  MyReg.WriteToReg('CurrentDir', Path);
 end;

begin
// ExifGrid.FileName := 'e:\180_8027_RT16.TIF';
 // ��������� ����������� ���� ������ ���������
 SendMessage(FolderTreeView.Handle, WM_SETREDRAW, 0, 0);
 try
// .............................................................................
// ������ � ��������
// .............................................................................
// ��������� ���������� ���� ��� ����� �����
 if MyReg.ReadFromReg('CurrentDir', s) then
  repeat
   if DirectoryExists(s) then
    begin
     FolderTreeView.UseShellImages := true;
//    FolderTreeView.RightClickSelect := true;
     PM := TPopupMenu.Create(FolderTreeView);
     FolderTreeView.PopupMenu := PM;
     FolderTreeView.OnChange := nil;
     FolderTreeView.Path := s;
     Application.ProcessMessages;
//     FolderTreeView.Refresh(FolderTreeView.Selected);
     FolderTreeViewChange(FolderTreeView, FolderTreeView.Selected);
     FolderTreeView.OnChange := FolderTreeViewChange;
     Application.ProcessMessages;
     Break;
    end
   else
    s := GetPrevPath(s);
  until Pos('\', s) = 0
 else
  MyReg.WriteToReg('CurrentDir', FolderTreeView.Path);

 // ����������� ���������� ������� ������ ��������� �� �������� ������ ����
 if Assigned(FolderTreeView.Selected) then
  begin
   cp := FolderTreeView.Selected;
   pp := cp;
   for i := 1 to Round(FolderTreeView.Height / 30) do
    if cp <> nil then
     begin
      pp := cp;
      cp := cp.GetPrevVisible;
     end;
   FolderTreeView.TopItem := pp;
  end;
 finally
  // �������� ����������� ������ ��������� � �������������� ����
  SendMessage(FolderTreeView.Handle, WM_SETREDRAW, 1, 0);
  RedrawWindow(FolderTreeView.Handle, nil, 0, RDW_INVALIDATE or RDW_UPDATENOW or RDW_ALLCHILDREN);
 end;
end;

procedure TExifDateMainForm.FileListViewCustomDrawItem(Sender: TCustomListView;
                                                       Item: TListItem;
                                                       State: TCustomDrawState;
                                                   var DefaultDraw: Boolean);
var
 i: integer;
 Icon: TIcon;
 ShFileInfo: TShFileInfo;
 s: string;
begin
// .............................................................................
 s := '';
 if cdsSelected	in State then s := s + 'cdsSelected';
 if cdsGrayed	in State then s := s + ', ' + 'cdsGrayed';
 if cdsDisabled in State then s := s + ', ' + 'cdsDisabled';
 if cdsChecked in State then s := s + ', ' + 'cdsChecked';
 if cdsFocused in State then s := s + ', ' + 'cdsFocused';
 if cdsDefault in State then s := s + ', ' + 'cdsDefault';
 if cdsHot in State then s := s + ', ' + 'cdsHot';
 if cdsMarked in State then s := s + ', ' + 'cdsMarked';
 if cdsIndeterminate in State then s := s + ', ' + 'cdsIndeterminate';
// Memo.Lines.Add(s);
// .............................................................................
 if (Sender as TListView).Columns.Count = 0 then Exit;
 with Sender.Canvas do
  begin
   if (Item.Selected) then
    begin
     Brush.Color := clHighlight;
     Font.Color := clWhite;
    end
   else
    begin
     Brush.Color := clWindow;
     Font.Color := clBlack;
    end;
//..............................................................................
   FillRect(Item.DisplayRect(drSelectBounds));
//..............................................................................
   Icon := TIcon.Create;
   ShGetFileInfo(Pchar(Dir(FolderTreeView.Path) + Item.Caption), 0, ShFileInfo, SizeOf(ShFileInfo), SHGFI_ICON + SHGFI_SMALLICON);
   Icon.Handle := ShFileInfo.hIcon;
   StretchDraw(Item.DisplayRect(drIcon), Icon);
   Icon.Free;
//..............................................................................
   Font.Color := clBlue;
   TextRect(Item.DisplayRect(drLabel), Item.Position.X + 20, Item.Position.Y + (Item.DisplayRect(drLabel).Bottom - Item.DisplayRect(drLabel).Top) div 2 - TextHeight(Item.Caption) div 2, Item.Caption);
// .............................................................................
   for i := 0 to Item.SubItems.Count - 1 do
    FileListViewCustomDrawSubItem(Sender, Item, i, State, DefaultDraw);
  end;
 DefaultDraw := false;
end;

procedure TExifDateMainForm.FileListViewCustomDrawSubItem(Sender: TCustomListView;
                                                          Item: TListItem;
                                                          SubItem: Integer;
                                                          State: TCustomDrawState;
                                                      var DefaultDraw: Boolean);
var
 i, X: integer;
 iRect: TRect;
begin
 Sch := Sch + 1;
// Memo.Lines.Add(IntToStr(Sch) + ' DrawSubItem ' + IntToStr(SubItem));
 with Sender.Canvas do
  begin
   if (Sender as TListView).Columns.Count < SubItem then exit;
   X := Item.Position.X;
   for i := 0 to SubItem do
    X := X + Item.ListView.Column[i].Width;
   if Item.Selected then
    Brush.Color := clHighlight;

   iRect := Item.DisplayRect(drBounds);
   iRect.Left := X;
   iRect.Right := X + Sender.Column[SubItem].Width;
//   ShowMessage(IntToStr(SubItem));
   if SubItem = (Sender as TListView).Columns.Count - 2 then
    iRect.Right := iRect.Right - 5;

//   Brush.Color := clRed;
   FillRect(iRect);
   Font.Color := clRed;
   TextOut(iRect.Left, iRect.Top + (iRect.Bottom - iRect.Top) div 2 - TextHeight(' ') div 2, Item.SubItems[SubItem]);
//   TextRect(iRect, 0, (iRect.Bottom - iRect.Top) div 2 - TextHeight(' ') div 2, Item.SubItems[SubItem]);
   DefaultDraw := false;
  end;
end;

procedure TExifDateMainForm.ExitProgram(Sender: TObject);
var
 WindowPlacement: TWindowPlacement;
begin
 GetWindowPlacement(Handle, @WindowPlacement);

 if WindowState = wsMaximized then
  MyReg.WriteToReg('Maximized', true)
 else
  begin
   MyReg.DelValue('Maximized');
   MyReg.WriteToReg('Left', WindowPlacement.rcNormalPosition.Left);
   MyReg.WriteToReg('Top', WindowPlacement.rcNormalPosition.Top);
   MyReg.WriteToReg('Width', WindowPlacement.rcNormalPosition.Right - WindowPlacement.rcNormalPosition.Left);
   MyReg.WriteToReg('Height', WindowPlacement.rcNormalPosition.Bottom - WindowPlacement.rcNormalPosition.Top);
  end;
 MyReg.WriteToReg('ExifGridRightDockPanelWidth', ExifGridRightDockPanel.Width);
// ��� ���������� � �������� ExifGrid ������� ��� Access Violation
 ShowExifGridForm(Sender);
// ...............................................................
 Application.Terminate;
end;

procedure TExifDateMainForm.ShowFoldersForm(Sender: TObject);
begin
 if not FoldersForm.Showing then
  begin
   FoldersForm.ManualDock(FoldersDockPanel, nil, alLeft);
   FoldersForm.Show;
   if FoldersForm.Showing then
    begin
     ActionViewFolders.Checked := true;
     ActionViewFolders.Enabled := false;
    end;
  end;
end;

procedure TExifDateMainForm.ShowThumbnailForm(Sender: TObject);
begin
 if not ThumbnailForm.Showing then
  begin
   ThumbnailForm.ManualDock(FoldersDockPanel, nil, alRight);
   ThumbnailForm.Show;
   if ThumbnailForm.Showing then
    begin
     ActionViewThumbnail.Checked := true;
     ActionViewThumbnail.Enabled := false;
    end;
  end;
end;

procedure TExifDateMainForm.ShowExifGridForm(Sender: TObject);
begin
 if not ExifGridForm.Showing then
  begin
   ExifGridForm.ManualDock(ExifGridRightDockPanel, nil, alClient);
   ExifGridRightDockPanel.Width := ClientWidth div 3;
   ExifGridForm.Show;
   if ExifGridForm.Showing then
    begin
     ActionViewExifGrid.Checked := true;
     ActionViewExifGrid.Enabled := false;
    end;
  end;
end;

procedure TExifDateMainForm.CreateMainMenu;
var
 MenuItem: TMenuItem;
 VersionInfo: TVersionInfo;
 VersionLitheMenu: TLitheVersionMenu;
 MenuItemInfo: tagMENUITEMINFO;
begin
// ������� ������� ���� ������� ����� ..........................................
 MainMenu := TMainMenu.Create(ExifDateMainForm);
 MainMenu.OwnerDraw := true;
// .............................................................................
// ������� ����� FILE ��������� ���� ...........................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := FILE_ITEM;
 MainMenu.Items.Add(MenuItem);
// ������� ����� OPEN ������� FILE .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Action := ActionOpen;
 MainMenu.Items.Find(FILE_ITEM).Add(MenuItem);
// ������� SEPARATOR � ������� FILE .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := SEPARATOR_ITEM;
 MainMenu.Items.Find(FILE_ITEM).Add(MenuItem);
// ������� ����� EXIT ������� FILE .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := EXIT_ITEM;
 MenuItem.OnClick := ExitProgram;
 MainMenu.Items.Find(FILE_ITEM).Add(MenuItem);
// .............................................................................
// ������� ����� VIEW ��������� ���� ...........................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := VIEW_ITEM;
 MainMenu.Items.Add(MenuItem);
// ������� ����� NEXT_ITEM ������� VIEW .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Action := ActionNext;
 MainMenu.Items.Find(VIEW_ITEM).Add(MenuItem);
// ������� ����� PREV_ITEM ������� VIEW .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Action := ActionPrev;
 MainMenu.Items.Find(VIEW_ITEM).Add(MenuItem);
// ������� SEPARATOR ������� VIEW .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := SEPARATOR_ITEM;
 MainMenu.Items.Find(VIEW_ITEM).Add(MenuItem);
// ������� ����� SWITCH_ITEM ������� VIEW .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Action := ActionSwitch;
 MainMenu.Items.Find(VIEW_ITEM).Add(MenuItem);
// ������� SEPARATOR ������� VIEW .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := SEPARATOR_ITEM;
 MainMenu.Items.Find(VIEW_ITEM).Add(MenuItem);
// ������� ����� RESIZE_ITEM ������� VIEW .............................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Action := ActionResize;
 MainMenu.Items.Find(VIEW_ITEM).Add(MenuItem);
// .............................................................................
// ������� ����� WINDOW ��������� ���� ...........................................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := WINDOW_ITEM;
 MainMenu.Items.Add(MenuItem);
// ������� ����� FOLDERS ������� VIEW .............................................
 ViewFoldersItem := TMenuItem.Create(MainMenu);
 ViewFoldersItem.Action := ActionViewFolders;
 MainMenu.Items.Find(WINDOW_ITEM).Add(ViewFoldersItem);
// ������� ����� THUMBNAIL ������� VIEW .............................................
 ViewThumbnailItem := TMenuItem.Create(MainMenu);
 ViewThumbnailItem.Action := ActionViewThumbnail;
 MainMenu.Items.Find(WINDOW_ITEM).Add(ViewThumbnailItem);
// ������� ����� EXIFGRID ������� VIEW .............................................
 ViewExifItem := TMenuItem.Create(MainMenu);
 ViewExifItem.Action := ActionViewExifGrid;
 MainMenu.Items.Find(WINDOW_ITEM).Add(ViewExifItem);
// .............................................................................
// ������� ����� EXIF ��������� ���� ...........................................
// MainMenu.Items.Add(ExifGrid.PopupMenu.Items);
// .............................................................................

 LitheMenu.MenuColor := MenuColor;
 LitheMenu.GutterColor := ChangeColor(MenuGutterColor, false, 20);
 LitheMenu.SelectedColor := MenuSelectColor;
 LitheMenu.SelLightColor := MenuColor;
 LitheMenu.Add(MainMenu, nil);

// ������� ���� ������ .........................................................
// .............................................................................
 VersionItem := TMenuItem.Create(MainMenu);
 VersionInfo := TVersionInfo.Create(Application.ExeName);
// VersionItem.Caption := 'http://exifrenamer.ucoz.com/         ' + VersionInfo.FileVersion; // http://exifrenamer.ucoz.com/ � 21.06.2013
 VersionItem.Caption := VersionInfo.FileVersion; // ��� URL � 31.07.2020
 VersionItem.OnClick := OpenURL;
 VersionInfo.Free;
 MainMenu.Items.Add(VersionItem);

 VersionLitheMenu := TLitheVersionMenu.Create;
 VersionLitheMenu.Add(nil, VersionItem);

 MenuItemInfo.fMask := MIIM_FTYPE;
 MenuItemInfo.cbSize := SizeOf(MenuItemInfo);
 if GetMenuItemInfo(MainMenu.Handle, MainMenu.Items.Count - 1, true, MenuItemInfo) then
  begin
   MenuItemInfo.fType := MenuItemInfo.fType or MFT_RIGHTJUSTIFY;
   MenuItemInfo.fMask := MIIM_FTYPE;
   SetMenuItemInfo(MainMenu.Handle, MainMenu.Items.Count - 1, true, MenuItemInfo);
  end;

end;

procedure TExifDateMainForm.FileListViewSelectAll(Sender: TObject);
begin
 SendMessage(FileListView.Handle, WM_SETREDRAW, 0, 0);
 try
  FileListView.SelectAll;
 finally
  SendMessage(FileListView.Handle, WM_SETREDRAW, 1, 0);
  RedrawWindow(FileListView.Handle, nil, 0, RDW_ERASE or RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN);
 end;
end;

procedure TExifDateMainForm.FileListViewSelectWithExif(Sender: TObject);
var
 i: integer;
begin
 SendMessage(FileListView.Handle, WM_SETREDRAW, 0, 0);
 try
  if FileListView.Items.Count > 0 then
   for i := 0 to FileListView.Items.Count - 1 do
    if FileListView.Items[i].SubItems[2] <> NO_EXIF then
     FileListView.Items[i].Selected := true
    else
     FileListView.Items[i].Selected := false;
 finally
  SendMessage(FileListView.Handle, WM_SETREDRAW, 1, 0);
  RedrawWindow(FileListView.Handle, nil, 0, RDW_ERASE or RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN);
 end;
end;

procedure TExifDateMainForm.FileListViewSelectWithExifDateTimeOriginal(Sender: TObject);
var
 i: integer;
begin
 SendMessage(FileListView.Handle, WM_SETREDRAW, 0, 0);
 try
  if FileListView.Items.Count > 0 then
   for i := 0 to FileListView.Items.Count - 1 do
    if (TExif(FileListView.Items[i].Data).ValidExif) and
       (Assigned(TExif(FileListView.Items[i].Data).ExifData.FindTag('DateTimeOriginal'))) then
      FileListView.Items[i].Selected := true
    else
     FileListView.Items[i].Selected := false;
 finally
  SendMessage(FileListView.Handle, WM_SETREDRAW, 1, 0);
  RedrawWindow(FileListView.Handle, nil, 0, RDW_ERASE or RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN);
 end;
end;

procedure TExifDateMainForm.FileListViewSelectWithExifDateTimeWithoutOriginal(Sender: TObject);
var
 i: integer;
begin
 SendMessage(FileListView.Handle, WM_SETREDRAW, 0, 0);
 try
  if FileListView.Items.Count > 0 then
   for i := 0 to FileListView.Items.Count - 1 do
    if (TExif(FileListView.Items[i].Data).ValidExif) and
       (not Assigned(TExif(FileListView.Items[i].Data).ExifData.FindTag('DateTimeOriginal'))) and
       (Assigned(TExif(FileListView.Items[i].Data).ExifData.FindTag('DateTime'))) then
      FileListView.Items[i].Selected := true
    else
     FileListView.Items[i].Selected := false;
 finally
  SendMessage(FileListView.Handle, WM_SETREDRAW, 1, 0);
  RedrawWindow(FileListView.Handle, nil, 0, RDW_ERASE or RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN);
 end;
end;

procedure TExifDateMainForm.CreateFileListViewPopupMenu(iPopupMenu: TPopupMenu);
var
 MenuItem: TMenuItem;
begin
// ������� ����� ������������ ���� ..........................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := 'Select all';
 MenuItem.OnClick := FileListViewSelectAll;
 iPopupMenu.Items.Add(MenuItem);
// ������� ����� ������������ ���� ..........................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := 'Select files with exif';
 MenuItem.OnClick := FileListViewSelectWithExif;
 iPopupMenu.Items.Add(MenuItem);
// ������� ����� ������������ ���� ..........................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := 'Select files with exif DateTime (without DateTimeOriginal)';
 MenuItem.OnClick := FileListViewSelectWithExifDateTimeWithoutOriginal;
 iPopupMenu.Items.Add(MenuItem);
// ������� ����� ������������ ���� ..........................
 MenuItem := TMenuItem.Create(MainMenu);
 MenuItem.Caption := 'Select files with exif DateTimeOriginal';
 MenuItem.OnClick := FileListViewSelectWithExifDateTimeOriginal;
 iPopupMenu.Items.Add(MenuItem);
end;

procedure TExifDateMainForm.InitFS;
begin
 FS.CurrencyFormat := CurrencyFormat;
 FS.NegCurrFormat := NegCurrFormat;
 FS.ThousandSeparator := ThousandSeparator;
 FS.DecimalSeparator := DecimalSeparator;
 FS.CurrencyDecimals := CurrencyDecimals;
 FS.DateSeparator := DateSeparator;
 FS.TimeSeparator := TimeSeparator;
 FS.ListSeparator := ListSeparator;
 FS.CurrencyString := CurrencyString;
 FS.ShortDateFormat := ShortDateFormat;
 FS.LongDateFormat := LongDateFormat;
 FS.TimeAMString := TimeAMString;
 FS.TimePMString := TimePMString;
// FS.ShortTimeFormat := ShortTimeFormat;
 FS.ShortTimeFormat := 'hh:mm:ss';
// FS.LongTimeFormat := LongTimeFormat;
 FS.LongTimeFormat := 'hh:mm:ss';
// FS.TwoDigitYearCenturyWindow := 10;
{
 FS.CurrencyFormat := 1;
 FS.NegCurrFormat := 5;
 FS.ThousandSeparator := #0;
 FS.DecimalSeparator := '.';
 FS.CurrencyDecimals := 2;
 FS.DateSeparator := '.';
 FS.TimeSeparator := ':';
 FS.ListSeparator := '+';
 FS.CurrencyString := '�.';
 FS.ShortDateFormat := 'dd/mm/yyyy';
 FS.LongDateFormat := 'dd/mm/yyyy';
 FS.TimeAMString := 'am';
 FS.TimePMString := 'pm';
 FS.ShortTimeFormat := 'hh:nn:ss';
 FS.LongTimeFormat := 'hh:nn:ss';
 FS.TwoDigitYearCenturyWindow := 10;
}
end;

function Dir(iDir: string): string;
begin
 result := iDir;
 if Length(iDir) = 0 then Exit;
 if iDir[Length(iDir)] <> '\' then result := iDir + '\';
end;

procedure TExifDateMainForm.ShellChangeNotifierChange;
begin
// FolderTreeViewChange(FolderTreeView, FolderTreeView.Selected);
end;

function TExifDateMainForm.GetNumberOfFiles(iDir: string; WithSubDirs: boolean = false): integer;
var
 ProgressShow: boolean;
 Count: integer;
 Tick: Longword;

 procedure ReadDir(iDir: string);
 // ����������� �������
 var
  iSr: TSearchRec;
 begin
  try
   if DirectoryExists(iDir) then
    begin

     if FindFirst(Dir(iDir) + '*.*', faAnyFile, iSr) = 0 then
      repeat
       // ���� ������ ���������� ����� ������� �������� ������ � progressbar
       if not ProgressShow then
        if (GetTickCount - Tick > 100) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
         if not ProgressForm.Visible then
          begin
           ProgressForm.Show;
           ProgressShow := true;
          end;
       // .....................

       if (iSr.Name <> '.') and
          (iSr.Name <> '..') then
        if WithSubDirs and
           ((iSr.Attr and faDirectory) <> 0) and
           ((iSr.Attr and faVolumeID) = 0) then
         ReadDir(Dir(Dir(iDir) + iSr.Name))
        else
         Count := Count + 1;
       ProgressForm.Panel.Caption := 'Find files: ' + IntToStr(Count);
       Application.ProcessMessages;
       if Cancel then break;
      until FindNext(iSr) <> 0;
      FindClose(iSr);
    end;
   except
    Count := -1;
   end;
 end;

begin
 if Cancel then exit;
 Count := 0;
 ProgressShow := false;
 // �������� ����� .............................................................
 Tick := GetTickCount;
 ProgressForm := TProgressForm.CreateNew(Self);
 try
  ProgressForm.ProgressBar.Min := 0;
  ProgressForm.ProgressBar.Max := 2;
  ProgressForm.ProgressBar.Position := 0;
  ProgressForm.ProgressBar.Visible := false;
  ProgressForm.OnKeyDown := ProgressFormKeyDown;
  ProgressForm.Panel.Caption := 'Find files: 0';
  ProgressForm.Caption := 'Reading files...';
  //.............................................................................
  ReadDir(iDir); // ����������� �������
  //.............................................................................
 finally
  ProgressForm.Close;
  ProgressForm.Free;
 end;
 //.............................................................................
 result := Count;
end;

function TExifDateMainForm.GetSubDirs(iDir: string): boolean;
var
 ProgressShow: boolean;
 Count: integer;
 Tick: Longword;
 iSr: TSearchRec;

 function FindSubDir(iDir: string): boolean;
 // ����������� �������
 begin
  result := false;
  try
   if DirectoryExists(iDir) then
    begin
     if FindFirst(Dir(iDir) + '*.*', faDirectory, iSr) = 0 then
      repeat
       // ���� ������ ���������� ����� ������� �������� ������ � progressbar
       if not ProgressShow then
        if (GetTickCount - Tick > 100) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
         if not ProgressForm.Visible then
          begin
           ProgressForm.Show;
           ProgressShow := true;
          end;
       // .....................

       if (iSr.Name <> '.') and
          (iSr.Name <> '..') then
        if ((iSr.Attr and faDirectory) <> 0) and
           ((iSr.Attr and faVolumeID) = 0) then
         begin
          result := true;
          exit;
         end
        else
         Count := Count + 1;
       ProgressForm.Panel.Caption := 'Find: ' + IntToStr(Count);
       Application.ProcessMessages;
       if Cancel then break;
      until FindNext(iSr) <> 0;
      FindClose(iSr);
    end;
   except
    Count := -1;
   end;
 end;

begin
 if Cancel then exit;
 result := false;
 ProgressShow := false;
 // �������� ����� .............................................................
 Tick := GetTickCount;
 ProgressForm := TProgressForm.CreateNew(Self);
 try
  ProgressForm.ProgressBar.Min := 0;
  ProgressForm.ProgressBar.Max := 2;
  ProgressForm.ProgressBar.Position := 0;
  ProgressForm.ProgressBar.Visible := false;
  ProgressForm.OnKeyDown := ProgressFormKeyDown;
  ProgressForm.Panel.Caption := 'Find: 0';
  ProgressForm.Caption := 'Find subdirectories...';
  //.............................................................................
  result := FindSubDir(iDir);
  //.............................................................................
 finally
  ProgressForm.Close;
  ProgressForm.Free;
 end;
 //.............................................................................
end;
(*
procedure TExifDateMainForm.ReadStatistics(Sender: TObject);
type
 TypeOfStatistics = (Aperture, ExposureTime, FocalLength);
const
 TabInc: LongInt = 80;
// HeightBand = 200;      // ������ ������ ����� ����������
// Interval = 15;         // �������� ����� �������� ����������� (�� �����������)
 TextValuesHeight = 20; // ������ ��������� ������ �������� (��� ������������)
 BandTitleHeight = 20;  // ������ ��������� ����������
 FirstBandTop = 4;      // ���������� ������ ������ ������ (����� ���������)
 LeftIndent = 20;       // ������ �����
 ShapeLeftIndent = 40;  // ������ �����
 IntervalBand = 10;     // �������� ����� �������� ���������� (�� ���������)
 MinHeight = 1;         // ����������� ������ �������� �����������
 ApertureWidth = 35;    // ������ �������� ����������� ���������� ���������
 ExposureWidth = 40;    // ������ �������� ����������� ���������� ��������
 FocalLengthWidth = 40; // ������ �������� ����������� ���������� ��������� ����������
var
 Interval: integer;{ = 15 }        // �������� ����� �������� ����������� (�� �����������)
 HeightBand: integer;{ = 200;}      // ������ ������ ����� ����������
 T: integer;
 L: integer;

 ApertureStatistics: TStatisticList;
 ExposureTimeStatistics: TStatisticList;
 FocalLengthStatistics: TStatisticList;
 CurrentAperture: string;
 CurrentExposureTime: string;
 CurrentFocalLength: string;

 FT: FILETIME;
 LFT: FILETIME;
 FileHandle: Integer;
 iExif: TExif;
 FileName: string;
 FileCount: integer;
 iSr: TSearchRec; // ��� ������� FindFirst
 SubDirs: boolean;
 AllFileCount: integer;
 ExifFiles: integer;

 i: integer;
 b: boolean;
 Form: TStatForm;
 _Label: TLabel;
 _Shape: TShape;
 _StatShape: TStatShape;
 _Max, _Min: integer;
 Tick: Longword;
 WindowList: pointer;

 function GetNumberFromApertureValue(value: string): double;
 begin
  result := -1;
  if length(value) = 0 then
   exit;
  if pos('F', value) <> 0 then
   try
    result := StrToFloat(copy(value, 3, length(value) - 2));
   except
    result := -1;
   end;
 end;

 function GetNumberFromExposureTimeValue(value: string): double;
 var
  s: string;
 begin
  s := value;
  result := -1;
  if length(s) = 0 then
   exit;
  if pos(' Sec', s) <> 0 then
   delete(s, pos(' Sec', s), 4);
  try
   if pos('/', s) <> 0 then
    result := RationalToDouble(s)
   else
    result := StrToFloat(s)
  except
   result := -1;
  end;
 end;

 function GetNumberFromFocalLengthValue(value: string): double;
 var
  s: string;
 begin
  s := value;
  result := -1;
  if length(s) = 0 then
   exit;
  if pos(' (mm)', s) <> 0 then
   delete(s, pos(' (mm)', s), 5);
  try
   result := StrToFloat(s)
  except
   result := -1;
  end;
 end;

 procedure SortStatistics(ts: TypeOfStatistics; var iStatistics: TStatisticList);
 var
  i, j: integer;
  d, dd: double;
  Item: TStatisticsItem;
 begin
  if Length(iStatistics) > 1 then
   for i := 0 to Length(iStatistics) - 1 do
    for j := 0 to Length(iStatistics) - 2 do
     begin
      case ts of
       Aperture:
        begin
         d := GetNumberFromApertureValue(iStatistics[j].Name);
         dd := GetNumberFromApertureValue(iStatistics[j + 1].Name);
        end;
       ExposureTime:
        begin
         d := GetNumberFromExposureTimeValue(iStatistics[j].Name);
         dd := GetNumberFromExposureTimeValue(iStatistics[j + 1].Name);
        end;
       FocalLength:
        begin
         d := GetNumberFromFocalLengthValue(iStatistics[j].Name);
         dd := GetNumberFromFocalLengthValue(iStatistics[j + 1].Name);
        end;
      end;
      if d > dd then
       begin
        Item := iStatistics[j];
        iStatistics[j] := iStatistics[j + 1];
        iStatistics[j + 1] := Item;
       end;
     end;
 end;

 procedure ReadStatisticsFromDir(iDir: string; iSubDirs: boolean = true);
 const
  NotData = 'N/A';
 var
  i: integer;
  sr: TSearchRec;
 begin
  if Cancel then exit;
  if DirectoryExists(iDir) then
   begin
    if FindFirst(Dir(iDir) + '*.*', faAnyFile, sr) = 0 then
     begin
      repeat
       // ���� ������ ���������� ����� ������� �������� ������ � progressbar
       if (GetTickCount - Tick > 100) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
        if not ProgressForm.Visible then
         ProgressForm.Show;
       // .....................
       if (sr.Name <> '.') and
          (sr.Name <> '..') then
        if ((sr.Attr and faDirectory) = 0) and
           ((sr.Attr and faVolumeID) = 0) and
           ((sr.Attr and faSymLink) = 0) then
         try
          iExif := TExif.Create;
          FileName := Dir(iDir) + sr.Name;
          iExif.FileName := FileName;
          Application.ProcessMessages;
          if (iExif.TypeOfFile = tfJPEG) or (iExif.TypeOfFile = tfTIFF) then
           if iExif.ValidExif then
            begin
             // ��������� ���������
             CurrentAperture := '';
             if iExif.ExifData.GetByName('Additional information') <> nil then
              CurrentAperture := iExif.ExifData.GetByName('Additional information').GetByName('FNumber');
             // ��������� ��������
             CurrentExposureTime := '';
             if iExif.ExifData.GetByName('Additional information') <> nil then
              CurrentExposureTime := iExif.ExifData.GetByName('Additional information').GetByName('ExposureTime');
             // ��������� ��������� ����������
             CurrentFocalLength := '';
             if iExif.ExifData.GetByName('Additional information') <> nil then
              CurrentFocalLength := iExif.ExifData.GetByName('Additional information').GetByName('FocalLength');
             if iExif.ExifData.GetByName('Main information') <> nil then
              if iExif.ExifData.GetByName('Main information').GetByName('Model') = 'Canon PowerShot G2' then
               CurrentFocalLength := IntToStr(Trunc(GetNumberFromFocalLengthValue(CurrentFocalLength) * 4.8571428571428571428571428571429)) + ' (mm)';
//            end;
//          Application.ProcessMessages;
//          iExif.Free;

             // �������������� ������ ���������
             if CurrentAperture = '' then
              CurrentAperture := NotData;
             b := false;
             for i := 0 to Length(ApertureStatistics) - 1 do
              if ApertureStatistics[i].Name = CurrentAperture then
               begin
                ApertureStatistics[i].Value := ApertureStatistics[i].Value + 1;
                b := true;
               end;
             if not b then
              begin
               SetLength(ApertureStatistics, Length(ApertureStatistics) + 1);
               ApertureStatistics[Length(ApertureStatistics) - 1].Name := CurrentAperture;
               ApertureStatistics[Length(ApertureStatistics) - 1].Value := 1;
              end;
             // �������������� ������ ��������
             if CurrentExposureTime = '' then
              CurrentExposureTime := NotData;
             b := false;
             for i := 0 to Length(ExposureTimeStatistics) - 1 do
              if ExposureTimeStatistics[i].Name = CurrentExposureTime then
               begin
                ExposureTimeStatistics[i].Value := ExposureTimeStatistics[i].Value + 1;
                b := true;
               end;
             if not b then
              begin
               SetLength(ExposureTimeStatistics, Length(ExposureTimeStatistics) + 1);
               ExposureTimeStatistics[Length(ExposureTimeStatistics) - 1].Name := CurrentExposureTime;
               ExposureTimeStatistics[Length(ExposureTimeStatistics) - 1].Value := 1;
              end;
             // �������������� ������ ��������� ����������
             if CurrentFocalLength = '' then
              CurrentFocalLength := NotData;
             b := false;
             for i := 0 to Length(FocalLengthStatistics) - 1 do
              if FocalLengthStatistics[i].Name = CurrentFocalLength then
               begin
                FocalLengthStatistics[i].Value := FocalLengthStatistics[i].Value + 1;
                b := true;
               end;
             if not b then
              begin
               SetLength(FocalLengthStatistics, Length(FocalLengthStatistics) + 1);
               FocalLengthStatistics[Length(FocalLengthStatistics) - 1].Name := CurrentFocalLength;
               FocalLengthStatistics[Length(FocalLengthStatistics) - 1].Value := 1;
              end;
             ExifFiles := ExifFiles + 1; // ���������� ������ � EXIF (��� ������ � ����������)
            end;
          Application.ProcessMessages;
          iExif.Free;
          FileCount := FileCount + 1; // ���������� ������ �����
         except
         end
        else
         if (iSubDirs) and ((sr.Attr and faDirectory) <> 0) then
          ReadStatisticsFromDir(Dir(Dir(iDir) + sr.Name));
       ProgressForm.ProgressBar.Position := FileCount;
       FileViewStatusBar.Panels[0].Text := 'reading: ' + IntToStr(FileCount) + '(' + IntToStr(ExifFiles) + ')/' + IntToStr(AllFileCount);
       Application.ProcessMessages;
       if Cancel then break;
      until FindNext(sr) <> 0;
      FindClose(sr);
     end
   end;
 end;

 procedure MaxMin(iStatistics: TStatisticList);
 // ����� ������������ � ������������� �������� � ���������� ��������� �� �������� ������
 var
  i: integer;
 begin
  _Max := Low(integer);
  _Min := High(integer);
  if Length(iStatistics) > 0 then
   for i := 0 to Length(iStatistics) - 1 do
    begin
     if _Min > iStatistics[i].Value then
      _Min := iStatistics[i].Value;
     if _Max < iStatistics[i].Value then
      _Max := iStatistics[i].Value;
    end;
  if _Max = Low(integer) then _Max := 0;
  if _Min = High(integer) then _Min := 0;

  if _Max > 150 then
   while _Max mod 100 <> 0 do
    _Max := _Max + 1
  else
   if (_Max > 10) and (_Max <= 150) then
    while _Max mod 10 <> 0 do
     _Max := _Max + 1
   else
    if _Max < 5 then
     _Max := 4
    else
     if _Max <= 10 then
      _Max := 10;
 end;

begin
 // ���������� ������������ �� �����������
 SubDirs := false;
 if GetSubDirs(FolderTreeView.Path) then
  SubDirs := Application.MessageBox('Read subdirectories?', 'Read statistic', MB_YESNO) = IDYES;

 Cancel := false;
 // �������� ������� ����������
 SetLength(ApertureStatistics, 0);
 SetLength(ExposureTimeStatistics, 0);
 SetLength(FocalLengthStatistics, 0);
 // ---------------------------
 FileCount := 0;
 AllFileCount := GetNumberOfFiles(FolderTreeView.Path, SubDirs);
 ExifFiles := 0;
 with FileViewStatusBar do
  for i := 2 to FileViewStatusBar.Panels.Count - 1 do Panels[i].Text :='';
 FileViewStatusBar.Panels[0].Width := 200;
// FileViewStatusBar.Panels[0].Text := 'reading: ' + IntToStr(ExifFiles) + '/' + IntToStr(AllFileCount);

 if Cancel then exit;
 try
  WindowList := DisableTaskWindows(0);
  // �������� ����� ............................................................
  Tick := GetTickCount;
  ProgressForm := TProgressForm.CreateNew(Self);
  ProgressForm.ProgressBar.Min := 0;
  ProgressForm.ProgressBar.Max := AllFileCount;
  ProgressForm.ProgressBar.Position := 0;
  ProgressForm.OnKeyDown := ProgressFormKeyDown;
  ProgressForm.Caption := 'Reading statistics...';

  ReadStatisticsFromDir(FolderTreeView.Path, SubDirs);

  if not Cancel then
   begin
    with ProgressForm.ProgressBar do
     Position := Max;

    if FileListView.Items.Count > 0 then
     FileListViewSelectItem(FileListView, FileListView.Selected, true);

    if (Length(ApertureStatistics) > 0) or
       (Length(ExposureTimeStatistics) > 0) or
       (Length(FocalLengthStatistics) > 0) then
     begin
      // ������� ����� ��� ����������� ����������
      Form := TStatForm.CreateNew(Self);

      ////////////////////////////////////////////
      // ������� ��������� ��������� ���������� //
      ////////////////////////////////////////////

      with Form.StatMemo do
       begin
        SendMessage(Form.StatMemo.Handle, EM_SetTabStops, 1, Longint(@TabInc));
        // ���������
        Lines.clear;
        if SubDirs then
         Lines.Add(' Dir: "' + FolderTreeView.Path + '" with subdirectories')
        else
         Lines.Add(' Dir: "' + FolderTreeView.Path + '" without subdirectories');
        Lines.Add('  Files: ' + IntToStr(ExifFiles) + ' (' + IntToStr(AllFileCount) + ')');
        Lines.Add('');
        // ��������� ���������� ��������
        Lines.Add(' Aperture statistics (' + IntToStr(Length(ApertureStatistics)) + ')');
        Lines.Add('');
        SortStatistics(Aperture, ApertureStatistics);
        for i := 0 to Length(ApertureStatistics) - 1 do
         Lines.Add(' ' + #9 + ApertureStatistics[i].Name + #9 + IntToStr(ApertureStatistics[i].Value));
        Lines.Add('');
        Lines.Add('');
        // ��������� ���������� ��������
        Lines.Add(' ExposureTime statistics (' + IntToStr(Length(ExposureTimeStatistics)) + ')');
        Lines.Add('');
        SortStatistics(ExposureTime, ExposureTimeStatistics);
        for i := 0 to Length(ExposureTimeStatistics) - 1 do
         Lines.Add(' ' + #9 + ExposureTimeStatistics[i].Name + #9 + IntToStr(ExposureTimeStatistics[i].Value));
        Lines.Add('');
        Lines.Add('');
        // ��������� ���������� �������� ����������
        Lines.Add(' FocalLength statistics (' + IntToStr(Length(FocalLengthStatistics)) + ')');
        Lines.Add('');
        SortStatistics(FocalLength, FocalLengthStatistics);
        for i := 0 to Length(FocalLengthStatistics) - 1 do
         Lines.Add(' ' + #9 + FocalLengthStatistics[i].Name + #9 + IntToStr(FocalLengthStatistics[i].Value));
        // .....................
       end;

      with Form do
       begin
      /////////////////////////////
      // ������ �������� ������� //
      /////////////////////////////
      HeightBand := (ScrollBox.ClientHeight - FirstBandTop - IntervalBand * 4) div 3;
      // ��������� (�������)
      _Label := TLabel.Create(Panel);
      _Label.Parent := Panel;
      _Label.Left := 10;
      _Label.Top := 5;
      _Label.Font.Size := 10;
      _Label.Font.Style := [fsBold];
      _Label.Caption := 'Dir: "' + FolderTreeView.Path + '"';
      if SubDirs then
       _Label.Caption := _Label.Caption + ' with subdirectories'
      else
       _Label.Caption := _Label.Caption + ' without subdirectories';
      // ��������� (���������� ������)
      _Label := TLabel.Create(Panel);
      _Label.Parent := Panel;
      _Label.Left := 10;
      _Label.Top := 25;
      _Label.Font.Size := 10;
      _Label.Font.Style := [fsBold];
      _Label.Caption := 'Files: ' + IntToStr(ExifFiles) + ' (' + IntToStr(AllFileCount) + ')';
      _Label.ShowHint := true;
      _Label.Hint := 'Files with EXIF / All files';
      //////////////////////////////////////////////////////////////////////////
      // ��������� ���������� �������� /////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      SortStatistics(Aperture, ApertureStatistics);
      MaxMin(ApertureStatistics);

      Interval := {20;//}Round((ScrollBox.ClientWidth - (Length(ApertureStatistics) * ApertureWidth) - LeftIndent * 2 - ShapeLeftIndent) / (Length(ApertureStatistics) + 1));
      if Interval < 5 then Interval := 5;
      if Interval > 40 then Interval := 40;
      // ��������� ���������� ���������
      _Label := TLabel.Create(ScrollBox);
      _Label.Parent := ScrollBox;
      _Label.Left := 10;
      _Label.Top := FirstBandTop;
      _Label.Font.Size := 9;
      _Label.Font.Style := [fsBold, fsUnderline];
      _Label.Caption := 'Aperture statistics  (' + IntToStr(Length(ApertureStatistics)) + ')';

      _StatShape := TStatShape.Create(ScrollBox);
      _StatShape.Parent := ScrollBox;
      _StatShape.TopIndent := TextValuesHeight;
      _StatShape.Max := _Max;
      _StatShape.Color := clWhite;
      _StatShape.IndentColor := ScrollBox.Color;
      _StatShape.LeftIndent := ShapeLeftIndent;
      _StatShape.Top := FirstBandTop + BandTitleHeight;
      _StatShape.Left := LeftIndent;
      _StatShape.Width := ShapeLeftIndent + Length(ApertureStatistics) * (ApertureWidth + Interval) + Interval;
      _StatShape.Height := HeightBand - BandTitleHeight - TextValuesHeight;
      for i := 0 to Length(ApertureStatistics) - 1 do
       begin
        // �������� ��������� (��������� ����� ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        if ApertureStatistics[i].Name <> 'N/A' then
         begin
          delete(ApertureStatistics[i].Name, 2, 1);
          ApertureStatistics[i].Name[1] := 'f';
         end;
        L := LeftIndent + ShapeLeftIndent + Interval + i * (ApertureWidth + Interval);
        _Label.Caption := ApertureStatistics[i].Name;
        _Label.Font.Style := [fsBold];
//        _Label.Font.
        _Label.AutoSize := true;
        _Label.Top := FirstBandTop + HeightBand - _Label.Height;
        _Label.Left := L + Round((ApertureWidth / 2) - (_Label.Width / 2));
        // ������� �����������
        _Shape := TShape.Create(ScrollBox);
        _Shape.Parent := ScrollBox;
        _Shape.Left := L;
        _Shape.Height := MinHeight + Round((HeightBand - BandTitleHeight - TextValuesHeight * 2 - MinHeight) / _Max * ApertureStatistics[i].Value);
        _Shape.Top := FirstBandTop + HeightBand - TextValuesHeight - _Shape.Height;
        _Shape.Width := ApertureWidth;
        _Shape.Brush.Color := clBlue;
        _Shape.Pen.Color := clBlue;
        // ���������� (��������� ������ ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        _Label.Caption := ' ' + IntToStr(ApertureStatistics[i].Value) + ' ';
        _Label.Font.Style := [fsBold];
        _Label.Font.Color := clBlue;
        _Label.AutoSize := true;
        _Label.Top := _Shape.Top - _Label.Height;
        _Label.Left := L + (ApertureWidth div 2) - (_Label.Width div 2);
        _Label.Color := clWhite;
        _Label.Transparent := false;
       end;
      //////////////////////////////////////////////////////////////////////////
      // ��������� ���������� �������� /////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      SortStatistics(ExposureTime, ExposureTimeStatistics);
      MaxMin(ExposureTimeStatistics);

      Interval := {20;//}Round((ScrollBox.ClientWidth - (Length(ExposureTimeStatistics) * ExposureWidth) - LeftIndent * 2 - ShapeLeftIndent) / (Length(ExposureTimeStatistics) + 1));
      if Interval < 5 then Interval := 5;
      if Interval > 40 then Interval := 40;
      // ��������� ���������� ��������
      _Label := TLabel.Create(ScrollBox);
      _Label.Parent := ScrollBox;
      _Label.Left := 10;
      _Label.Top := FirstBandTop + HeightBand + IntervalBand;
      _Label.Font.Size := 9;
      _Label.Font.Style := [fsBold, fsUnderline];
      _Label.Caption := 'Exposure time statistics  (' + IntToStr(Length(ExposureTimeStatistics)) + ')';

      _StatShape := TStatShape.Create(ScrollBox);
      _StatShape.Parent := ScrollBox;
      _StatShape.TopIndent := TextValuesHeight;
      _StatShape.Max := _Max;
      _StatShape.Color := clWhite;
      _StatShape.IndentColor := ScrollBox.Color;
      _StatShape.LeftIndent := ShapeLeftIndent;
      _StatShape.Top := FirstBandTop + HeightBand + IntervalBand + BandTitleHeight;
      _StatShape.Left := LeftIndent;
      _StatShape.Width := ShapeLeftIndent + Length(ExposureTimeStatistics) * (ExposureWidth + Interval) + Interval;
      _StatShape.Height := HeightBand - BandTitleHeight - TextValuesHeight;
      for i := 0 to Length(ExposureTimeStatistics) - 1 do
       begin
        // �������� �������� (��������� ����� ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        if ExposureTimeStatistics[i].Name <> 'N/A' then
         delete(ExposureTimeStatistics[i].Name, length(ExposureTimeStatistics[i].Name) - 3, 4);
        L := LeftIndent + ShapeLeftIndent + Interval + i * (ExposureWidth + Interval);
        _Label.Caption := ExposureTimeStatistics[i].Name;
        _Label.Font.Style := [fsBold];
        _Label.AutoSize := true;
        _Label.Top := FirstBandTop + HeightBand + IntervalBand + HeightBand - _Label.Height;
        _Label.Left := L + Round((ExposureWidth / 2) - (_Label.Width / 2));
        // ������� �����������
        _Shape := TShape.Create(ScrollBox);
        _Shape.Parent := ScrollBox;
        _Shape.Left := L;
        _Shape.Height := MinHeight + Round((HeightBand - BandTitleHeight - TextValuesHeight * 2 - MinHeight) / _Max * ExposureTimeStatistics[i].Value);
        _Shape.Top := FirstBandTop + HeightBand + IntervalBand + HeightBand - TextValuesHeight - _Shape.Height;
        _Shape.Width := ExposureWidth;
        _Shape.Brush.Color := clBlue;
        _Shape.Pen.Color := clBlue;
        // ���������� (��������� ������ ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        _Label.Caption := ' ' + IntToStr(ExposureTimeStatistics[i].Value) + ' ';
        _Label.Font.Style := [fsBold];
        _Label.Font.Color := clBlue;
        _Label.AutoSize := true;
        _Label.Top := _Shape.Top - _Label.Height;
        _Label.Left := L + (ExposureWidth div 2) - (_Label.Width div 2);
        _Label.Color := clWhite;
        _Label.Transparent := false;
       end;
      //////////////////////////////////////////////////////////////////////////
      // ��������� ���������� �������� ���������� //////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      SortStatistics(FocalLength, FocalLengthStatistics);
      MaxMin(FocalLengthStatistics);

      Interval := {20;//}Round((ScrollBox.ClientWidth - (Length(FocalLengthStatistics) * FocalLengthWidth) - LeftIndent * 2 - ShapeLeftIndent) / (Length(FocalLengthStatistics) + 1));
      if Interval < 5 then Interval := 5;
      if Interval > 40 then Interval := 40;
      // ��������� ���������� �������� ����������
      _Label := TLabel.Create(ScrollBox);
      _Label.Parent := ScrollBox;
      _Label.Left := 10;
      _Label.Top := FirstBandTop + (HeightBand + IntervalBand) * 2;
      _Label.Font.Size := 9;
      _Label.Font.Style := [fsBold, fsUnderline];
      _Label.Caption := 'Focal length statistics, mm  (' + IntToStr(Length(FocalLengthStatistics)) + ')';

      _StatShape := TStatShape.Create(ScrollBox);
      _StatShape.Parent := ScrollBox;
      _StatShape.TopIndent := TextValuesHeight;
      _StatShape.Max := _Max;
      _StatShape.Color := clWhite;
      _StatShape.IndentColor := ScrollBox.Color;
      _StatShape.LeftIndent := ShapeLeftIndent;
      _StatShape.Top := FirstBandTop + (HeightBand + IntervalBand) * 2 + BandTitleHeight;
      _StatShape.Left := LeftIndent;
      _StatShape.Width := ShapeLeftIndent + Length(FocalLengthStatistics) * (FocalLengthWidth + Interval) + Interval;
      _StatShape.Height := HeightBand - BandTitleHeight - TextValuesHeight;
      for i := 0 to Length(FocalLengthStatistics) - 1 do
       begin
        // �������� ��������� ���������� (��������� ����� ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        if FocalLengthStatistics[i].Name <> 'N/A' then
         delete(FocalLengthStatistics[i].Name, length(FocalLengthStatistics[i].Name) - 3, 4);
        L := LeftIndent + ShapeLeftIndent + Interval + i * (FocalLengthWidth + Interval);
        _Label.Caption := FocalLengthStatistics[i].Name;
        _Label.Font.Style := [fsBold];
        _Label.AutoSize := true;
        _Label.Top := FirstBandTop + (HeightBand + IntervalBand) * 2 + HeightBand - _Label.Height;
        _Label.Left := L + Round((FocalLengthWidth / 2) - (_Label.Width / 2));
        // ������� �����������
        _Shape := TShape.Create(ScrollBox);
        _Shape.Parent := ScrollBox;
        _Shape.Left := L;
        _Shape.Height := MinHeight + Round((HeightBand - BandTitleHeight - TextValuesHeight * 2 - MinHeight) / _Max * FocalLengthStatistics[i].Value);
        _Shape.Top := FirstBandTop + (HeightBand + IntervalBand) * 2 + HeightBand - TextValuesHeight - _Shape.Height;
        _Shape.Width := FocalLengthWidth;
        _Shape.Brush.Color := clBlue;
        _Shape.Pen.Color := clBlue;
        // ���������� (��������� ������ ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        _Label.Caption := ' ' + IntToStr(FocalLengthStatistics[i].Value) + ' ';
        _Label.Font.Style := [fsBold];
        _Label.Font.Color := clBlue;
        _Label.AutoSize := true;
        _Label.Top := _Shape.Top - _Label.Height;
        _Label.Left := L + (FocalLengthWidth div 2) - (_Label.Width div 2);
        _Label.Color := clWhite;
        _Label.Transparent := false;
       end;
  {
      // ������� memo
      StatMemo := TMemo.Create(ScrollBox);
      StatMemo.Parent := ScrollBox;
      StatMemo.Align := alClient;
      StatMemo.ScrollBars := ssVertical;
      StatMemo.ReadOnly := true;

      SendMessage(StatMemo.Handle, EM_SetTabStops, 1, Longint(@TabInc));
  }
      // ���������
  //    StatMemo.Lines.clear;
  //    StatMemo.Lines.Add('Dir: ' + FolderTreeView.Path);
  //    StatMemo.Lines.Add(' files: ' + IntToStr(AllFileCount));
  //    StatMemo.Lines.Add('');
  {
      // ��������� ���������� ��������
      StatMemo.Lines.Add('Aperture statistics:');
      StatMemo.Lines.Add('');
      SortStatistics(Aperture, ApertureStatistics);
      for i := 0 to Length(ApertureStatistics) - 1 do
       StatMemo.Lines.Add(ApertureStatistics[i].Name + #9 + IntToStr(ApertureStatistics[i].Value));
      StatMemo.Lines.Add('');
      StatMemo.Lines.Add('');
      // ��������� ���������� ��������
      StatMemo.Lines.Add('ExposureTime statistics:');
      StatMemo.Lines.Add('');
      SortStatistics(Exposure, ExposureTimeStatistics);
      for i := 0 to Length(ExposureTimeStatistics) - 1 do
       StatMemo.Lines.Add(ExposureTimeStatistics[i].Name + #9 + IntToStr(ExposureTimeStatistics[i].Value));
      StatMemo.Lines.Add('');
      StatMemo.Lines.Add('');
      // ��������� ���������� �������� ����������
      StatMemo.Lines.Add('FocalLength statistics:');
      StatMemo.Lines.Add('');
      SortStatistics(Focal, FocalLengthStatistics);
      for i := 0 to Length(FocalLengthStatistics) - 1 do
       StatMemo.Lines.Add(FocalLengthStatistics[i].Name + #9 + IntToStr(FocalLengthStatistics[i].Value));
  }
       end;
      // .....................
      Form.ShowModal;
      Form.Free;
     end
    else
     ShowMessage('�� ������ ����������� ����� �� �������!');
   end;
 finally
  if FileListView.Items.Count > 0 then
   FileListViewSelectItem(FileListView, FileListView.Selected, true);
  ProgressForm.Close;
  ProgressForm.Free;
  EnableTaskWindows(WindowList);
  SetActiveWindow(Self.Handle);
 end;
end;
*)
procedure TExifDateMainForm.ReadStatistics(Sender: TObject);
type
 TypeOfStatistics = (Aperture, ExposureTime, FocalLength);
const
 TabInc: LongInt = 80;
// HeightBand = 200;      // ������ ������ ����� ����������
// Interval = 15;         // �������� ����� �������� ����������� (�� �����������)
 TextValuesHeight = 20; // ������ ��������� ������ �������� (��� ������������)
 BandTitleHeight = 20;  // ������ ��������� ����������
 FirstBandTop = 4;      // ���������� ������ ������ ������ (����� ���������)
 LeftIndent = 20;       // ������ �����
 ShapeLeftIndent = 40;  // ������ �����
 IntervalBand = 10;     // �������� ����� �������� ���������� (�� ���������)
 MinHeight = 1;         // ����������� ������ �������� �����������
 ApertureWidth = 35;    // ������ �������� ����������� ���������� ���������
 ExposureWidth = 40;    // ������ �������� ����������� ���������� ��������
 FocalLengthWidth = 40; // ������ �������� ����������� ���������� ��������� ����������
var
 StatFilesExifList: TFilesExifList;

 Interval: integer;{ = 15 }        // �������� ����� �������� ����������� (�� �����������)
 HeightBand: integer;{ = 200;}      // ������ ������ ����� ����������
 T: integer;
 L: integer;

 ApertureStatistics: TStatisticList;
 ExposureTimeStatistics: TStatisticList;
 FocalLengthStatistics: TStatisticList;
 CurrentAperture: string;
 CurrentExposureTime: string;
 CurrentFocalLength: string;

 FT: FILETIME;
 LFT: FILETIME;
 FileHandle: Integer;
 iExif: TExif;
 FileName: string;
 FileCount: integer;
 iSr: TSearchRec; // ��� ������� FindFirst
 SubDirs: boolean;
 AllFileCount: integer;
 ExifFiles: integer;

 i: integer;
 b: boolean;
 Form: TStatForm;
 _Label: TLabel;
 _Shape: TShape;
 _StatShape: TStatShape;
 _Max, _Min: integer;
 Tick: Longword;
 WindowList: pointer;

 function GetNumberFromApertureValue(value: string): double;
 begin
  result := -1;
  if length(value) = 0 then
   exit;
  if pos('F', value) <> 0 then
   try
    result := StrToFloat(copy(value, 3, length(value) - 2));
   except
    result := -1;
   end;
 end;

 function GetNumberFromExposureTimeValue(value: string): double;
 var
  s: string;
 begin
  s := value;
  result := -1;
  if length(s) = 0 then
   exit;
  if pos(' Sec', s) <> 0 then
   delete(s, pos(' Sec', s), 4);
  try
   if pos('/', s) <> 0 then
    result := RationalToDouble(s)
   else
    result := StrToFloat(s)
  except
   result := -1;
  end;
 end;

 function GetNumberFromFocalLengthValue(value: string): double;
 var
  s: string;
 begin
  s := value;
  result := -1;
  if length(s) = 0 then
   exit;
  if pos(' (mm)', s) <> 0 then
   delete(s, pos(' (mm)', s), 5);
  try
   result := StrToFloat(s)
  except
   result := -1;
  end;
 end;

 procedure SortStatistics(ts: TypeOfStatistics; var iStatistics: TStatisticList);
 var
  i, j: integer;
  d, dd: double;
  Item: TStatisticsItem;
 begin
  if Length(iStatistics) > 1 then
   for i := 0 to Length(iStatistics) - 1 do
    for j := 0 to Length(iStatistics) - 2 do
     begin
      case ts of
       Aperture:
        begin
         d := GetNumberFromApertureValue(iStatistics[j].Name);
         dd := GetNumberFromApertureValue(iStatistics[j + 1].Name);
        end;
       ExposureTime:
        begin
         d := GetNumberFromExposureTimeValue(iStatistics[j].Name);
         dd := GetNumberFromExposureTimeValue(iStatistics[j + 1].Name);
        end;
       FocalLength:
        begin
         d := GetNumberFromFocalLengthValue(iStatistics[j].Name);
         dd := GetNumberFromFocalLengthValue(iStatistics[j + 1].Name);
        end;
      end;
      if d > dd then
       begin
        Item := iStatistics[j];
        iStatistics[j] := iStatistics[j + 1];
        iStatistics[j + 1] := Item;
       end;
     end;
 end;

 procedure ReadStatisticsFromDir(iDir: string; iSubDirs: boolean = true);
 const
  NotData = 'N/A';
 var
  i: integer;
  sr: TSearchRec;
  FromMem: boolean;
 begin
  if Cancel then exit;
  // ���� ������� ��������� � ��������
  if iDir = FolderTreeView.Path then
   FromMem := true // ��������� � �������� ������
  else
   FromMem := false;
  if DirectoryExists(iDir) then
   begin
    if FindFirst(Dir(iDir) + '*.*', faAnyFile, sr) = 0 then
     begin
      repeat
       // ���� ������ ���������� ����� ������� �������� ������ � progressbar
       if (GetTickCount - Tick > 100) and (ProgressForm.ProgressBar.Position < ProgressForm.ProgressBar.Max div 2) then
        if not ProgressForm.Visible then
         ProgressForm.Show;
       // .....................
       if (sr.Name <> '.') and
          (sr.Name <> '..') then
        if ((sr.Attr and faDirectory) = 0) and
           ((sr.Attr and faVolumeID) = 0) and
           ((sr.Attr and faSymLink) = 0) then
         try
          iExif := TExif.Create;
          FileName := Dir(iDir) + sr.Name;
//          if FromMem then
//           for i := 0 to
          iExif.FileName := FileName;
          Application.ProcessMessages;
          if (iExif.TypeOfFile = tfJPEG) or (iExif.TypeOfFile = tfTIFF) then
           if iExif.ValidExif then
            begin
             // ��������� ���������
             CurrentAperture := '';
             if iExif.ExifData.GetByName('Additional information') <> nil then
              CurrentAperture := iExif.ExifData.GetByName('Additional information').GetByName('FNumber');
             // ��������� ��������
             CurrentExposureTime := '';
             if iExif.ExifData.GetByName('Additional information') <> nil then
              CurrentExposureTime := iExif.ExifData.GetByName('Additional information').GetByName('ExposureTime');
             // ��������� ��������� ����������
             CurrentFocalLength := '';
             if iExif.ExifData.GetByName('Additional information') <> nil then
              CurrentFocalLength := iExif.ExifData.GetByName('Additional information').GetByName('FocalLength');
             if iExif.ExifData.GetByName('Main information') <> nil then
              if iExif.ExifData.GetByName('Main information').GetByName('Model') = 'Canon PowerShot G2' then
               CurrentFocalLength := IntToStr(Trunc(GetNumberFromFocalLengthValue(CurrentFocalLength) * 4.8571428571428571428571428571429)) + ' (mm)';
//            end;
//          Application.ProcessMessages;
//          iExif.Free;

             // �������������� ������ ���������
             if CurrentAperture = '' then
              CurrentAperture := NotData;
             b := false;
             for i := 0 to Length(ApertureStatistics) - 1 do
              if ApertureStatistics[i].Name = CurrentAperture then
               begin
                ApertureStatistics[i].Value := ApertureStatistics[i].Value + 1;
                b := true;
               end;
             if not b then
              begin
               SetLength(ApertureStatistics, Length(ApertureStatistics) + 1);
               ApertureStatistics[Length(ApertureStatistics) - 1].Name := CurrentAperture;
               ApertureStatistics[Length(ApertureStatistics) - 1].Value := 1;
              end;
             // �������������� ������ ��������
             if CurrentExposureTime = '' then
              CurrentExposureTime := NotData;
             b := false;
             for i := 0 to Length(ExposureTimeStatistics) - 1 do
              if ExposureTimeStatistics[i].Name = CurrentExposureTime then
               begin
                ExposureTimeStatistics[i].Value := ExposureTimeStatistics[i].Value + 1;
                b := true;
               end;
             if not b then
              begin
               SetLength(ExposureTimeStatistics, Length(ExposureTimeStatistics) + 1);
               ExposureTimeStatistics[Length(ExposureTimeStatistics) - 1].Name := CurrentExposureTime;
               ExposureTimeStatistics[Length(ExposureTimeStatistics) - 1].Value := 1;
              end;
             // �������������� ������ ��������� ����������
             if CurrentFocalLength = '' then
              CurrentFocalLength := NotData;
             b := false;
             for i := 0 to Length(FocalLengthStatistics) - 1 do
              if FocalLengthStatistics[i].Name = CurrentFocalLength then
               begin
                FocalLengthStatistics[i].Value := FocalLengthStatistics[i].Value + 1;
                b := true;
               end;
             if not b then
              begin
               SetLength(FocalLengthStatistics, Length(FocalLengthStatistics) + 1);
               FocalLengthStatistics[Length(FocalLengthStatistics) - 1].Name := CurrentFocalLength;
               FocalLengthStatistics[Length(FocalLengthStatistics) - 1].Value := 1;
              end;
             ExifFiles := ExifFiles + 1; // ���������� ������ � EXIF (��� ������ � ����������)
            end;
          Application.ProcessMessages;
          iExif.Free;
          FileCount := FileCount + 1; // ���������� ������ �����
         except
         end
        else
         if (iSubDirs) and ((sr.Attr and faDirectory) <> 0) then
          ReadStatisticsFromDir(Dir(Dir(iDir) + sr.Name));
       ProgressForm.ProgressBar.Position := FileCount;
       FileViewStatusBar.Panels[0].Text := 'reading: ' + IntToStr(FileCount) + '(' + IntToStr(ExifFiles) + ')/' + IntToStr(AllFileCount);
       Application.ProcessMessages;
       if Cancel then break;
      until FindNext(sr) <> 0;
      FindClose(sr);
     end
   end;
 end;

 procedure MaxMin(iStatistics: TStatisticList);
 // ����� ������������ � ������������� �������� � ���������� ��������� �� �������� ������
 var
  i: integer;
 begin
  _Max := Low(integer);
  _Min := High(integer);
  if Length(iStatistics) > 0 then
   for i := 0 to Length(iStatistics) - 1 do
    begin
     if _Min > iStatistics[i].Value then
      _Min := iStatistics[i].Value;
     if _Max < iStatistics[i].Value then
      _Max := iStatistics[i].Value;
    end;
  if _Max = Low(integer) then _Max := 0;
  if _Min = High(integer) then _Min := 0;

  if _Max > 150 then
   while _Max mod 100 <> 0 do
    _Max := _Max + 1
  else
   if (_Max > 10) and (_Max <= 150) then
    while _Max mod 10 <> 0 do
     _Max := _Max + 1
   else
    if _Max < 5 then
     _Max := 4
    else
     if _Max <= 10 then
      _Max := 10;
 end;

 procedure SetMouseMessages(Contr: TControl);
 begin
//   Contr.OnMouseDown := ScrollBox.OnMouseDown;
//   Contr.OnMouseUp := ScrollBox.OnMouseUp;
//   Contr.OnMouseMove := ScrollBox.OnMouseMove;
 end;

begin
 StatFilesExifList := TFilesExifList.Create;
 // ���������� ������������ �� �����������
 SubDirs := false;
 if GetSubDirs(FolderTreeView.Path) then
  SubDirs := Application.MessageBox('Read subdirectories?', 'Read statistic', MB_YESNO) = IDYES;

 Cancel := false;
 // �������� ������� ����������
 SetLength(ApertureStatistics, 0);
 SetLength(ExposureTimeStatistics, 0);
 SetLength(FocalLengthStatistics, 0);
 // ---------------------------
 FileCount := 0;
 AllFileCount := GetNumberOfFiles(FolderTreeView.Path, SubDirs);
 ExifFiles := 0;
 with FileViewStatusBar do
  for i := 2 to FileViewStatusBar.Panels.Count - 1 do Panels[i].Text :='';
 FileViewStatusBar.Panels[0].Width := 200;
// FileViewStatusBar.Panels[0].Text := 'reading: ' + IntToStr(ExifFiles) + '/' + IntToStr(AllFileCount);

 if Cancel then exit;
 try
  WindowList := DisableTaskWindows(0);
  // �������� ����� ............................................................
  Tick := GetTickCount;
  ProgressForm := TProgressForm.CreateNew(Self);
  ProgressForm.ProgressBar.Min := 0;
  ProgressForm.ProgressBar.Max := AllFileCount;
  ProgressForm.ProgressBar.Position := 0;
  ProgressForm.OnKeyDown := ProgressFormKeyDown;
  ProgressForm.Caption := 'Reading statistics...';

  ReadStatisticsFromDir(FolderTreeView.Path, SubDirs);
  
  if not Cancel then
   begin
    with ProgressForm.ProgressBar do
     Position := Max;

    if FileListView.Items.Count > 0 then
     FileListViewSelectItem(FileListView, FileListView.Selected, true);

    if (Length(ApertureStatistics) > 0) or
       (Length(ExposureTimeStatistics) > 0) or
       (Length(FocalLengthStatistics) > 0) then
     begin
      // ������� ����� ��� ����������� ����������
      Form := TStatForm.CreateNew(Self);

      ////////////////////////////////////////////
      // ������� ��������� ��������� ���������� //
      ////////////////////////////////////////////

      with Form.StatMemo do
       begin
        SendMessage(Form.StatMemo.Handle, EM_SetTabStops, 1, Longint(@TabInc));
        // ���������
        Lines.clear;
        if SubDirs then
         Lines.Add(' Dir: "' + FolderTreeView.Path + '" with subdirectories')
        else
         Lines.Add(' Dir: "' + FolderTreeView.Path + '" without subdirectories');
        Lines.Add('  Files: ' + IntToStr(ExifFiles) + ' (' + IntToStr(AllFileCount) + ')');
        Lines.Add('');
        // ��������� ���������� ��������
        Lines.Add(' Aperture statistics (' + IntToStr(Length(ApertureStatistics)) + ')');
        Lines.Add('');
        SortStatistics(Aperture, ApertureStatistics);
        for i := 0 to Length(ApertureStatistics) - 1 do
         Lines.Add(' ' + #9 + ApertureStatistics[i].Name + #9 + IntToStr(ApertureStatistics[i].Value));
        Lines.Add('');
        Lines.Add('');
        // ��������� ���������� ��������
        Lines.Add(' ExposureTime statistics (' + IntToStr(Length(ExposureTimeStatistics)) + ')');
        Lines.Add('');
        SortStatistics(ExposureTime, ExposureTimeStatistics);
        for i := 0 to Length(ExposureTimeStatistics) - 1 do
         Lines.Add(' ' + #9 + ExposureTimeStatistics[i].Name + #9 + IntToStr(ExposureTimeStatistics[i].Value));
        Lines.Add('');
        Lines.Add('');
        // ��������� ���������� �������� ����������
        Lines.Add(' FocalLength statistics (' + IntToStr(Length(FocalLengthStatistics)) + ')');
        Lines.Add('');
        SortStatistics(FocalLength, FocalLengthStatistics);
        for i := 0 to Length(FocalLengthStatistics) - 1 do
         Lines.Add(' ' + #9 + FocalLengthStatistics[i].Name + #9 + IntToStr(FocalLengthStatistics[i].Value));
        // .....................
       end;

      with Form do
       begin
      /////////////////////////////
      // ������ �������� ������� //
      /////////////////////////////
      HeightBand := (ScrollBox.ClientHeight - FirstBandTop - IntervalBand * 4) div 3;
      // ��������� (�������)
      _Label := TLabel.Create(Panel);
      _Label.Parent := Panel;
      _Label.Left := 10;
      _Label.Top := 5;
      _Label.Font.Size := 10;
      _Label.Font.Style := [fsBold];
      _Label.Caption := 'Dir: "' + FolderTreeView.Path + '"';
      if SubDirs then
       _Label.Caption := _Label.Caption + ' with subdirectories'
      else
       _Label.Caption := _Label.Caption + ' without subdirectories';
      // ��������� (���������� ������)
      _Label := TLabel.Create(Panel);
      _Label.Parent := Panel;
      _Label.Left := 10;
      _Label.Top := 25;
      _Label.Font.Size := 10;
      _Label.Font.Style := [fsBold];
      _Label.Caption := 'Files: ' + IntToStr(ExifFiles) + ' (' + IntToStr(AllFileCount) + ')';
      _Label.ShowHint := true;
      _Label.Hint := 'Files with EXIF / All files';
      //////////////////////////////////////////////////////////////////////////
      // ��������� ���������� �������� /////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      SortStatistics(Aperture, ApertureStatistics);
      MaxMin(ApertureStatistics);

      Interval := {20;//}Round((ScrollBox.ClientWidth - (Length(ApertureStatistics) * ApertureWidth) - LeftIndent * 2 - ShapeLeftIndent) / (Length(ApertureStatistics) + 1));
      if Interval < 5 then Interval := 5;
      if Interval > 40 then Interval := 40;
      // ��������� ���������� ���������
      _Label := TLabel.Create(ScrollBox);
      _Label.Parent := ScrollBox;
      _Label.Left := 10;
      _Label.Top := FirstBandTop;
      _Label.Font.Size := 9;
      _Label.Font.Style := [fsBold, fsUnderline];
      _Label.Caption := 'Aperture statistics  (' + IntToStr(Length(ApertureStatistics)) + ')';
      _Label.OnMouseDown := ScrollBox.OnMouseDown;
      _Label.OnMouseUp := ScrollBox.OnMouseUp;
      _Label.OnMouseMove := ScrollBox.OnMouseMove;

      _StatShape := TStatShape.Create(ScrollBox);
      _StatShape.Parent := ScrollBox;
      _StatShape.TopIndent := TextValuesHeight;
      _StatShape.Max := _Max;
      _StatShape.Color := clWhite;
      _StatShape.IndentColor := ScrollBox.Color;
      _StatShape.LeftIndent := ShapeLeftIndent;
      _StatShape.Top := FirstBandTop + BandTitleHeight;
      _StatShape.Left := LeftIndent;
      _StatShape.Width := ShapeLeftIndent + Length(ApertureStatistics) * (ApertureWidth + Interval) + Interval;
      _StatShape.Height := HeightBand - BandTitleHeight - TextValuesHeight;
      for i := 0 to Length(ApertureStatistics) - 1 do
       begin
        // �������� ��������� (��������� ����� ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        if ApertureStatistics[i].Name <> 'N/A' then
         begin
          delete(ApertureStatistics[i].Name, 2, 1);
          ApertureStatistics[i].Name[1] := 'f';
         end;
        L := LeftIndent + ShapeLeftIndent + Interval + i * (ApertureWidth + Interval);
        _Label.Caption := ApertureStatistics[i].Name;
        _Label.Font.Style := [fsBold];
//        _Label.Font.
        _Label.AutoSize := true;
        _Label.Top := FirstBandTop + HeightBand - _Label.Height;
        _Label.Left := L + Round((ApertureWidth / 2) - (_Label.Width / 2));
        // ������� �����������
        _Shape := TShape.Create(ScrollBox);
        _Shape.Parent := ScrollBox;
        _Shape.Left := L;
        _Shape.Height := MinHeight + Round((HeightBand - BandTitleHeight - TextValuesHeight * 2 - MinHeight) / _Max * ApertureStatistics[i].Value);
        _Shape.Top := FirstBandTop + HeightBand - TextValuesHeight - _Shape.Height;
        _Shape.Width := ApertureWidth;
        _Shape.Brush.Color := clBlue;
        _Shape.Pen.Color := clBlue;
        // ���������� (��������� ������ ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        _Label.Caption := ' ' + IntToStr(ApertureStatistics[i].Value) + ' ';
        _Label.Font.Style := [fsBold];
        _Label.Font.Color := clBlue;
        _Label.AutoSize := true;
        _Label.Top := _Shape.Top - _Label.Height;
        _Label.Left := L + (ApertureWidth div 2) - (_Label.Width div 2);
        _Label.Color := clWhite;
        _Label.Transparent := false;
       end;
      //////////////////////////////////////////////////////////////////////////
      // ��������� ���������� �������� /////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      SortStatistics(ExposureTime, ExposureTimeStatistics);
      MaxMin(ExposureTimeStatistics);

      Interval := {20;//}Round((ScrollBox.ClientWidth - (Length(ExposureTimeStatistics) * ExposureWidth) - LeftIndent * 2 - ShapeLeftIndent) / (Length(ExposureTimeStatistics) + 1));
      if Interval < 5 then Interval := 5;
      if Interval > 40 then Interval := 40;
      // ��������� ���������� ��������
      _Label := TLabel.Create(ScrollBox);
      _Label.Parent := ScrollBox;
      _Label.Left := 10;
      _Label.Top := FirstBandTop + HeightBand + IntervalBand;
      _Label.Font.Size := 9;
      _Label.Font.Style := [fsBold, fsUnderline];
      _Label.Caption := 'Exposure time statistics  (' + IntToStr(Length(ExposureTimeStatistics)) + ')';

      _StatShape := TStatShape.Create(ScrollBox);
      _StatShape.Parent := ScrollBox;
      _StatShape.TopIndent := TextValuesHeight;
      _StatShape.Max := _Max;
      _StatShape.Color := clWhite;
      _StatShape.IndentColor := ScrollBox.Color;
      _StatShape.LeftIndent := ShapeLeftIndent;
      _StatShape.Top := FirstBandTop + HeightBand + IntervalBand + BandTitleHeight;
      _StatShape.Left := LeftIndent;
      _StatShape.Width := ShapeLeftIndent + Length(ExposureTimeStatistics) * (ExposureWidth + Interval) + Interval;
      _StatShape.Height := HeightBand - BandTitleHeight - TextValuesHeight;
      for i := 0 to Length(ExposureTimeStatistics) - 1 do
       begin
        // �������� �������� (��������� ����� ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        if ExposureTimeStatistics[i].Name <> 'N/A' then
         delete(ExposureTimeStatistics[i].Name, length(ExposureTimeStatistics[i].Name) - 3, 4);
        L := LeftIndent + ShapeLeftIndent + Interval + i * (ExposureWidth + Interval);
        _Label.Caption := ExposureTimeStatistics[i].Name;
        _Label.Font.Style := [fsBold];
        _Label.AutoSize := true;
        _Label.Top := FirstBandTop + HeightBand + IntervalBand + HeightBand - _Label.Height;
        _Label.Left := L + Round((ExposureWidth / 2) - (_Label.Width / 2));
        // ������� �����������
        _Shape := TShape.Create(ScrollBox);
        _Shape.Parent := ScrollBox;
        _Shape.Left := L;
        _Shape.Height := MinHeight + Round((HeightBand - BandTitleHeight - TextValuesHeight * 2 - MinHeight) / _Max * ExposureTimeStatistics[i].Value);
        _Shape.Top := FirstBandTop + HeightBand + IntervalBand + HeightBand - TextValuesHeight - _Shape.Height;
        _Shape.Width := ExposureWidth;
        _Shape.Brush.Color := clBlue;
        _Shape.Pen.Color := clBlue;
        // ���������� (��������� ������ ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        _Label.Caption := ' ' + IntToStr(ExposureTimeStatistics[i].Value) + ' ';
        _Label.Font.Style := [fsBold];
        _Label.Font.Color := clBlue;
        _Label.AutoSize := true;
        _Label.Top := _Shape.Top - _Label.Height;
        _Label.Left := L + (ExposureWidth div 2) - (_Label.Width div 2);
        _Label.Color := clWhite;
        _Label.Transparent := false;
       end;
      //////////////////////////////////////////////////////////////////////////
      // ��������� ���������� �������� ���������� //////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      SortStatistics(FocalLength, FocalLengthStatistics);
      MaxMin(FocalLengthStatistics);

      Interval := {20;//}Round((ScrollBox.ClientWidth - (Length(FocalLengthStatistics) * FocalLengthWidth) - LeftIndent * 2 - ShapeLeftIndent) / (Length(FocalLengthStatistics) + 1));
      if Interval < 5 then Interval := 5;
      if Interval > 40 then Interval := 40;
      // ��������� ���������� �������� ����������
      _Label := TLabel.Create(ScrollBox);
      _Label.Parent := ScrollBox;
      _Label.Left := 10;
      _Label.Top := FirstBandTop + (HeightBand + IntervalBand) * 2;
      _Label.Font.Size := 9;
      _Label.Font.Style := [fsBold, fsUnderline];
      _Label.Caption := 'Focal length statistics, mm  (' + IntToStr(Length(FocalLengthStatistics)) + ')';

      _StatShape := TStatShape.Create(ScrollBox);
      _StatShape.Parent := ScrollBox;
      _StatShape.TopIndent := TextValuesHeight;
      _StatShape.Max := _Max;
      _StatShape.Color := clWhite;
      _StatShape.IndentColor := ScrollBox.Color;
      _StatShape.LeftIndent := ShapeLeftIndent;
      _StatShape.Top := FirstBandTop + (HeightBand + IntervalBand) * 2 + BandTitleHeight;
      _StatShape.Left := LeftIndent;
      _StatShape.Width := ShapeLeftIndent + Length(FocalLengthStatistics) * (FocalLengthWidth + Interval) + Interval;
      _StatShape.Height := HeightBand - BandTitleHeight - TextValuesHeight;
      for i := 0 to Length(FocalLengthStatistics) - 1 do
       begin
        // �������� ��������� ���������� (��������� ����� ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        if FocalLengthStatistics[i].Name <> 'N/A' then
         delete(FocalLengthStatistics[i].Name, length(FocalLengthStatistics[i].Name) - 3, 4);
        L := LeftIndent + ShapeLeftIndent + Interval + i * (FocalLengthWidth + Interval);
        _Label.Caption := FocalLengthStatistics[i].Name;
        _Label.Font.Style := [fsBold];
        _Label.AutoSize := true;
        _Label.Top := FirstBandTop + (HeightBand + IntervalBand) * 2 + HeightBand - _Label.Height;
        _Label.Left := L + Round((FocalLengthWidth / 2) - (_Label.Width / 2));
        // ������� �����������
        _Shape := TShape.Create(ScrollBox);
        _Shape.Parent := ScrollBox;
        _Shape.Left := L;
        _Shape.Height := MinHeight + Round((HeightBand - BandTitleHeight - TextValuesHeight * 2 - MinHeight) / _Max * FocalLengthStatistics[i].Value);
        _Shape.Top := FirstBandTop + (HeightBand + IntervalBand) * 2 + HeightBand - TextValuesHeight - _Shape.Height;
        _Shape.Width := FocalLengthWidth;
        _Shape.Brush.Color := clBlue;
        _Shape.Pen.Color := clBlue;
        // ���������� (��������� ������ ��� ���������)
        _Label := TLabel.Create(ScrollBox);
        _Label.Parent := ScrollBox;
        _Label.Caption := ' ' + IntToStr(FocalLengthStatistics[i].Value) + ' ';
        _Label.Font.Style := [fsBold];
        _Label.Font.Color := clBlue;
        _Label.AutoSize := true;
        _Label.Top := _Shape.Top - _Label.Height;
        _Label.Left := L + (FocalLengthWidth div 2) - (_Label.Width div 2);
        _Label.Color := clWhite;
        _Label.Transparent := false;
       end;
  {
      // ������� memo
      StatMemo := TMemo.Create(ScrollBox);
      StatMemo.Parent := ScrollBox;
      StatMemo.Align := alClient;
      StatMemo.ScrollBars := ssVertical;
      StatMemo.ReadOnly := true;

      SendMessage(StatMemo.Handle, EM_SetTabStops, 1, Longint(@TabInc));
  }
      // ���������
  //    StatMemo.Lines.clear;
  //    StatMemo.Lines.Add('Dir: ' + FolderTreeView.Path);
  //    StatMemo.Lines.Add(' files: ' + IntToStr(AllFileCount));
  //    StatMemo.Lines.Add('');
  {
      // ��������� ���������� ��������
      StatMemo.Lines.Add('Aperture statistics:');
      StatMemo.Lines.Add('');
      SortStatistics(Aperture, ApertureStatistics);
      for i := 0 to Length(ApertureStatistics) - 1 do
       StatMemo.Lines.Add(ApertureStatistics[i].Name + #9 + IntToStr(ApertureStatistics[i].Value));
      StatMemo.Lines.Add('');
      StatMemo.Lines.Add('');
      // ��������� ���������� ��������
      StatMemo.Lines.Add('ExposureTime statistics:');
      StatMemo.Lines.Add('');
      SortStatistics(Exposure, ExposureTimeStatistics);
      for i := 0 to Length(ExposureTimeStatistics) - 1 do
       StatMemo.Lines.Add(ExposureTimeStatistics[i].Name + #9 + IntToStr(ExposureTimeStatistics[i].Value));
      StatMemo.Lines.Add('');
      StatMemo.Lines.Add('');
      // ��������� ���������� �������� ����������
      StatMemo.Lines.Add('FocalLength statistics:');
      StatMemo.Lines.Add('');
      SortStatistics(Focal, FocalLengthStatistics);
      for i := 0 to Length(FocalLengthStatistics) - 1 do
       StatMemo.Lines.Add(FocalLengthStatistics[i].Name + #9 + IntToStr(FocalLengthStatistics[i].Value));
  }
       end;
      // .....................
      Form.ShowModal;
      Form.Free;
     end
    else
     ShowMessage('�� ������ ����������� ����� �� �������!');
   end;
 finally
  if FileListView.Items.Count > 0 then
   FileListViewSelectItem(FileListView, FileListView.Selected, true);
  ProgressForm.Close;
  ProgressForm.Free;
  EnableTaskWindows(WindowList);
  SetActiveWindow(Self.Handle);
  StatFilesExifList.Free;
 end;
end;

procedure TExifDateMainForm.FilesReadThredOnTerminate(Sender: TObject);
// ����������� � �������� ������ (������������� �� �����)
begin
// InterlockedDecrement(FilesReadThredCount);
 Dec(FilesReadThredCount);
 EnableFormForReadingFiles;
// Sender := nil;
 {$IFDEF LOGMODE}LogAdd('FilesReadThredOnTerminate');{$ENDIF LOGMODE}
 {$IFDEF LOGMODE}LogAdd('FilesReadThredCount = ' + IntToStr(FilesReadThredCount));{$ENDIF LOGMODE}
end;

procedure TExifDateMainForm.FolderTreeViewChange(Sender: TObject; Node: TTreeNode);
// ������� ������� ����� ��� ������ ������ �� ��������
begin
{$IFDEF LOGMODE}LogAdd('------------------------------------------------------------------------------');{$ENDIF LOGMODE}
{$IFDEF LOGMODE}LogAdd('-= FolderTreeViewChange =-');{$ENDIF LOGMODE}
 // ������������� � ������� ������ �����
 {$IFDEF LOGMODE}LogAdd('FilesReadThredCount = ' + IntToStr(FilesReadThredCount));{$ENDIF LOGMODE}
 if Assigned(FilesReadThred) then
  try
   {$IFDEF LOGMODE}LogAdd('������������� � ������� ������ �����...');{$ENDIF LOGMODE}
   Application.ProcessMessages;
   FilesReadThred.Terminate;
   {$IFDEF LOGMODE}LogAdd('  ...����... ' + IntToStr(FilesReadThredCount));{$ENDIF LOGMODE}
   while FilesReadThredCount > 0 do
    begin
     Application.ProcessMessages;
//     {$IFDEF LOGMODE}LogAdd('  ...����... ' + IntToStr(FilesReadThredCount));{$ENDIF LOGMODE}
     if Application.Terminated then
      halt;
    end;
   {$IFDEF LOGMODE}LogAdd('  ...���������... ' + IntToStr(FilesReadThredCount));{$ENDIF LOGMODE}
//   FilesReadThred.WaitFor;
   FreeAndNil(FilesReadThred);
   {$IFDEF LOGMODE}LogAdd('...���������� � ������� ������ �����');{$ENDIF LOGMODE}
  except
   FilesReadThred := nil;
   ShowMessage('������ �������� ������!');
   {$IFDEF LOGMODE}LogAdd('������ �������� ������!');{$ENDIF LOGMODE}
  end;
 DisableFormForReadingFiles;
 // ������� � ����������� ����� ����� ������ ������
 {$IFDEF LOGMODE}LogAdd('������� � ����������� ����� ����� ������ ������');{$ENDIF LOGMODE}
 {$IFDEF LOGMODE}LogAdd(' �������: ' + (Sender as TShellTreeView).Path);{$ENDIF LOGMODE}
 FileViewStatusBar.FReading := true;
 ShellChangeNotifier.Root := (Sender as TShellTreeView).Path;
 MyReg.WriteToReg('CurrentDir', (Sender as TShellTreeView).Path);
 if ExifGrid = nil then Exit;
 DirLabel.Caption := (Sender as TShellTreeView).Path;
 if not PreviewPanel.Visible then ActionSwitch.Enabled := false;
 ReadProgress(Self, psStarting, 0, 'Reading...');
 FileListView.Items.Clear;
 SmallImagesList.Clear;
 ThumbnailImage.Picture := nil;
 if PreviewPanel.Visible then PreviewImage.Picture := nil;
 ExifGrid.FileName := '';
 Application.ProcessMessages; // ????

 AllFilesCount := 0;
 ListFilesCount := 0;
 FilesReadThred := TFilesReadThread.Create((Sender as TShellTreeView).Path, Self);
 Inc(FilesReadThredCount);
// Caption := IntToStr(FilesReadThredCount);
 FilesReadThred.OnTerminate := FilesReadThredOnTerminate;
 FilesReadThred.Resume;
end;

procedure TExifDateMainForm.FolderTreeViewChangeOld(Sender: TObject; Node: TTreeNode);
// ������� ������ ������ �� ��������
type
 TIconInfo = packed record
              Extention: string[255];
              ImageIndex: byte;
             end;
var
 IconInfo: array of TIconInfo;
 FT: FILETIME;
 LFT: FILETIME;
 FileHandle: Integer;
 ST: SYSTEMTIME;
 iExif: TExif;
 sr: TSearchRec;
 FileName: string;
 FileCount: integer;
 AllFileCount: integer;
 CurrentFile: integer;
 ShFileInfo: TShFileInfo;
 SmallIcon: TIcon;
 i: integer;

 function GetIconInfo(Ext: string): byte;
 var
  i: byte;
 begin
 // ���� �� ������ ���������� FF
  result := $FF;
  if Length(IconInfo) = 0 then Exit;
  for i := 0 to Length(IconInfo) - 1 do
   if Uppercase(IconInfo[i].Extention) = Uppercase(Ext) then
    result := IconInfo[i].ImageIndex;
 end;

 function AddIconInfo(Ext: string; Icon: TIcon): integer;
 begin
  SetLength(IconInfo, Length(IconInfo) + 1);
  IconInfo[Length(IconInfo) - 1].Extention := Uppercase(Ext);
  IconInfo[Length(IconInfo) - 1].ImageIndex := SmallImagesList.AddIcon(Icon);
  result := IconInfo[Length(IconInfo) - 1].ImageIndex;
 end;

begin
 Cancel := false;
 FileViewStatusBar.FReading := true;
 ShellChangeNotifier.Root := (Sender as TShellTreeView).Path;
 MyReg.WriteToReg('CurrentDir', (Sender as TShellTreeView).Path);
 Application.ProcessMessages; // ????
 FileCount := 0;
 if ExifGrid = nil then Exit;
 DirLabel.Caption := (Sender as TShellTreeView).Path;
 if not PreviewPanel.Visible then ActionSwitch.Enabled := false;
 FileListView.Enabled := false;   //   ????????
 ReadProgress(Self, psStarting, 0, 'Reading...');
 FileListView.Items.Clear;
 SetLength(IconInfo, 0);
 SmallImagesList.Clear;
 ThumbnailImage.Picture := nil;
 if PreviewPanel.Visible then PreviewImage.Picture := nil;
 ExifGrid.FileName := '';
 AllFileCount := GetNumberOfFiles((Sender as TShellTreeView).Path);
 CurrentFile := 1;
 Application.ProcessMessages; // ????
 try
  if FindFirst(Dir((Sender as TShellTreeView).Path) + '*.*', faAnyFile, sr) = 0 then
   begin
    repeat
     EnablerNextPrevButton;
     if ((sr.Attr and faDirectory) = 0) and  ((sr.Attr and faVolumeID) = 0) and  ((sr.Attr and faSymLink) = 0) then
      begin
       ReadProgress(Self, psRunning, Round(CurrentFile * 100 / AllFileCount), 'Reading...');
       // ������� ������
       iExif := TExif.Create;
       FileName := Dir((Sender as TShellTreeView).Path) + sr.Name;
       // ������ EXIF �� �����
       iExif.FileName := FileName;
       Application.ProcessMessages;
       if ((iExif.TypeOfFile = tfJPEG) or (iExif.TypeOfFile = tfTIFF)) and (FileExists(Dir((Sender as TShellTreeView).Path) + sr.Name)) then
          begin
           // ���� ����������� � EXIF, ���������� �������.
           // ��������� � ������.
//           FilesExifList.Add(iExif);
           with FileListView.Items.Add do
            begin
             Data := iExif;
             FileCount := FileCount + 1;
             Caption := ExtractFileName(sr.Name);
             FileHandle := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
             if FileHandle > 0 then
              GetFileTime(FileHandle, @FT, NIL, NIL)
             else
              begin
               ShowMessage('File: ' + FileName + ' not open');
               break;
              end;
             FileTimeToLocalFileTime(FT, LFT);
             FileTimeToSystemTime(LFT, ST);
             SubItems.Add(DateTimeToStr(SystemTimeToDateTime(ST), FS));
             Application.ProcessMessages;
             SubItems.Add(DateTimeToStr(FileDateToDateTime(FileAge(FileName)), FS));
             FileClose(FileHandle);
             if iExif.ValidExif then
              begin
               if iExif.ExifData.GetByName('Main information') <> nil then
                SubItems.Add(PhotoStrDateToStr(iExif.ExifData.GetByName('Main information').GetByName('DateTime')))
               else
                SubItems.Add(NOT_HAVE);
               if iExif.ExifData.GetByName('Additional information') <> nil then
                SubItems.Add(PhotoStrDateToStr(iExif.ExifData.GetByName('Additional information').GetByName('DateTimeOriginal')))
               else
                SubItems.Add(NOT_HAVE);
               if iExif.ExifData.GetByName('Additional information') <> nil then
                SubItems.Add(PhotoStrDateToStr(iExif.ExifData.GetByName('Additional information').GetByName('DateTimeDigitized')))
               else
                SubItems.Add(NOT_HAVE);
               Application.ProcessMessages;
              end
             else
              begin
               SubItems.Add(NO_EXIF);
               SubItems.Add(NO_EXIF);
               SubItems.Add(NO_EXIF);
               Application.ProcessMessages;
              end;
             // �������� ������ ������ ��� ���������� �����
             i := GetIconInfo(ExtractFileExt(sr.Name));
             if i = $FF then
              begin
               ShGetFileInfo(Pchar(Dir((Sender as TShellTreeView).Path) + sr.Name), 0, ShFileInfo, SizeOf(ShFileInfo), SHGFI_ICON + SHGFI_SMALLICON);
               SmallIcon := TIcon.Create;
               SmallIcon.Handle := ShFileInfo.hIcon;
               ImageIndex := AddIconInfo(ExtractFileExt(sr.Name), SmallIcon);
               SmallIcon.Free;
              end
             else
              ImageIndex := i;
            end;
          end
         else
          // ���� ��� EXIF ��� �� ����������
          // ����������� ��������� ����� ������
          iExif.Free;
       PreviewStatusBar.Files := FileListView.Items.Count;
       FileViewStatusBar.Panels[0].Text := IntToStr(FileListView.Items.Count) + '/' + IntToStr(FileListView.Items.Count) + '/' + IntToStr(AllFileCount);
       Application.ProcessMessages;
       if FileCount = 1 then
        begin
         FileListView.Perform(WM_KEYDOWN, VK_DOWN, 0);
         Application.ProcessMessages;
//         FileCount := FileCount + 1;
         ActionSwitch.Enabled := true;
        end;
//       iExif.Free;
      end;
     Application.ProcessMessages;
     if Application.Terminated then
      break;
     CurrentFile := CurrentFile + 1;
    until FindNext(sr) <> 0;
    FileViewStatusBar.Panels[0].Text := IntToStr(FileListView.ItemIndex + 1) + '/' + IntToStr(FileListView.Items.Count);
    ReadProgress(Self, psRunning, 100, 'Reading...');
    FindClose(sr);
    FileListView.Enabled:=true;
   end
  else
   ThumbnailImage.Picture:=nil;
 except
//  ShowMessage('ShellTVChange except: ' + sr.Name);
  FileListView.Enabled:=true;
 end;
 ReadProgress(Self, psEnding, 100, '');
 FileViewStatusBar.FReading := false;
end;

procedure TExifDateMainForm.FileListViewSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  if (Selected) {and assigned(Item)} then
   if (Sender as TFileListView).SelCount = 1 then
    begin
     if ExifGrid <> nil then
      begin
       ActionPrev.Enabled := false;
       ActionNext.Enabled := false;
       if PreviewPanel.Visible then
        PreviewImage.Visible := false;
       ExifGrid.Visible := false;
       ThumbnailImage.Visible := false;
       ThumbnailImage.Picture := nil;
       ExifGrid.Exif := TExif(Item.Data);
       Application.ProcessMessages;
       if ExifGrid.Exif.ValidExif then
        begin
         // Model
         if Assigned(ExifGrid.Exif.ExifData.FindTag('Model')) then
          begin
           ModelLabel.Caption := ' ' + ExifGrid.Exif.ExifData.FindTag('Model').Value + ' ';
           ModelLabel.Visible := true;
          end
         else
          begin
           ModelLabel.Caption := '';
           ModelLabel.Visible := false;
          end;
         // ShutterSpeedValue
         if Assigned(ExifGrid.Exif.ExifData.FindTag('ShutterSpeedValue')) then
          begin
           ShutterSpeedValueLabel.Caption := ' ' + ExifGrid.Exif.ExifData.FindTag('ShutterSpeedValue').Value + ' ';
           ShutterSpeedValueLabel.Visible := true;
          end
         else
          begin
           ShutterSpeedValueLabel.Visible := false;
           ShutterSpeedValueLabel.Caption := '';
          end;
         // ApertureValue
         if Assigned(ExifGrid.Exif.ExifData.FindTag('ApertureValue')) then
          begin
           ApertureValueLabel.Caption := ' ' + ExifGrid.Exif.ExifData.FindTag('ApertureValue').Value + ' ';
           ApertureValueLabel.Visible := true;
          end
         else
          begin
           ApertureValueLabel.Visible := false;
           ApertureValueLabel.Caption := '-';
          end;
         // ISO
         if Assigned(ExifGrid.Exif.ExifData.FindTag('ISO')) then
          begin
           ISOValueLabel.Caption := ' ISO ' + ExifGrid.Exif.ExifData.FindTag('ISO').Value + ' ';
           ISOValueLabel.Visible := true;
          end
         else
          if Assigned(ExifGrid.Exif.ExifData.FindTag('ISOSpeedRatings')) then
           begin
            ISOValueLabel.Caption := ' ISO ' + ExifGrid.Exif.ExifData.FindTag('ISOSpeedRatings').Value + ' ';
            ISOValueLabel.Visible := true;
           end
          else
           begin
            ISOValueLabel.Visible := false;
            ISOValueLabel.Caption := '-';
           end;
         // Lens
         if Assigned(ExifGrid.Exif.ExifData.FindTag('FocalLength')) then
          begin
           LensValueLabel.Caption := '';
           if Length(ExifGrid.Exif.ExifData.FindTag('FocalLength').Value) > 5 then
            LensValueLabel.Caption := copy(ExifGrid.Exif.ExifData.FindTag('FocalLength').Value, 1, Length(ExifGrid.Exif.ExifData.FindTag('FocalLength').Value) - 5);
           if (Assigned(ExifGrid.Exif.ExifData.FindTag('FocalLengthOfLens'))) and
              (Length(ExifGrid.Exif.ExifData.FindTag('FocalLengthOfLens').Value) > 5) then
            LensValueLabel.Caption := ' ' + LensValueLabel.Caption + ' (' + copy(ExifGrid.Exif.ExifData.FindTag('FocalLengthOfLens').Value, 1, Length(ExifGrid.Exif.ExifData.FindTag('FocalLengthOfLens').Value) - 5) + ') '
           else
            LensValueLabel.Caption := LensValueLabel.Caption + ' mm';
           LensValueLabel.Visible := true;
          end
         else
          begin
           LensValueLabel.Visible := false;
           LensValueLabel.Caption := ' ';
          end;
        end;
      end;
     ShowThumbnail;
     Application.ProcessMessages;
     ExifGrid.Visible := true;
     ActionSwitch.Enabled := true;
     ThumbnailImage.Visible := true;
     FileViewStatusBar.ShowParam;
     if PreviewPanel.Visible then
      begin
       PreviewImage.ShowPreview(Item);
       PreviewImage.Visible := true;
      end;
     FileViewStatusBar.Panels[0].Text := IntToStr(FileListView.ItemIndex + 1) + '/' + IntToStr(FileListView.Items.Count);
     FileViewStatusBar.Panels[1].Text := ExifGrid.Exif.TypeOfFile;
     FileViewStatusBar.Panels[2].Text := FileSizeToStr(ExifFileSize(Dir(FolderTreeView.Path) + Item.Caption));
     FileViewStatusBar.ShowParam;
     EnablerNextPrevButton;
    end
   else
    begin
     if ExifGrid <> nil then
      begin
       FileViewStatusBar.Panels[0].Text := IntToStr(FileListView.ItemIndex + 1) + '/' + IntToStr(FileListView.SelCount) + '/' + IntToStr(FileListView.Items.Count);
       ActionPrev.Enabled := false;
       ActionNext.Enabled := false;
       ActionSwitch.Enabled := false;
       if PreviewPanel.Visible then
        PreviewImage.Visible := false;
       ThumbnailImage.Visible := false;
       ThumbnailImage.Picture := nil;
       ExifGrid.FileName := '';
      end;
    end
  else
end;

function DoubleToStr(V: Double; Count: byte):string;
Var
 j: byte;
 i: Cardinal;
 D: Double;
 b: boolean;
begin
 b:=false;
 Result:='';
 if v<0 then
  begin
   Result:='-';
   d:=Abs(V);
  end
 else
  d:=v;
 i := Trunc(d);
 Result:=Result+IntToStr(i);
 if d - i > 0 then
  begin
   Result := Result + ',';
   b := true;
   d:=(d - i) * 10 + 0.1;
   i := Trunc(d);
   Result:=Result+IntToStr(i);
   if Count > 0 then Count := Count - 1;
  end;
 if Count > 0 then
  for j:=1 to Count do
   begin
    if (b = false) and (j = 1) and (Copy(result,length(result),1) <> ',') then
     begin
      Result := Result + ',';
      b:=true;
     end;
    d:=(d - i) * 10 + 0.1;
    if (d <> 0) or (j = 1) then
     begin
      i := Trunc(d);
      Result:=Result+IntToStr(i);
     end
   end;
end;

function FileSizeToStr(Size: longword): string;
begin
 if Size < 1024 then  result := IntToStr(Size)+ ' bytes';
 if (Size > 1024) and (Size < 1048576) then result := DoubleToStr(Size / 1024, 1)+ ' Kb';
 if Size > 1048576 then result := DoubleToStr(Size / 1048576, 1)+ ' Mb';
end;

function ExifFileSize(Name: string): longword;
var
  F: File;
begin
 if FileExists(Name) then
  begin
   result := 0;
   AssignFile(F, Name);
   try
    Reset(F, 1);
    result := FileSize(F);
    CloseFile(F);
   except
    CloseFile(F);
   end;
  end;
end;

procedure TPreviewImage.ShowPreview(const Item: TListItem);
var
  Start: DWORD;
  GraphicClass: TGraphicExGraphicClass;
  Width, Height: integer;
  kWidth,kHeight: real;
  Stream: TMemoryStream;
begin
 if (FFileName = Dir(FolderTreeView.Path) + Item.Caption) and
    (not Self.Picture.Graphic.Empty) then
  Exit;
 Picture := nil;
 ActionResize.Enabled := false;
 FFileName := Dir(FolderTreeView.Path) + Item.Caption;
 Stream := TMemoryStream.Create;
 Stream.LoadFromFile(FFileName);
 Screen.Cursor := crHourGlass;
 PreviewStatusbar.Files := FileListView.Items.Count;
 PreviewStatusbar.FileIndex := FileListView.ItemIndex + 1;
 Left := 0;
 Top := 0;
 try
  try
   LoadImageDone := false;
   PreviewStatusbar.loaded := GetTickCount;
   Start := PreviewStatusbar.loaded;
   GraphicClass := FileFormatList.GraphicFromContent(Stream);
   if GraphicClass = nil then
    begin
// ������ �������� � ������� JPEG
     Self.IncrementalDisplay := true;
     Self.OnProgress := PreviewLoadProgress;
     PreviewStatusbar.TypeOfFile := tfJPEG;
     Self.Picture.LoadFromFile(FFileName);
    end
   else
    begin
// ������ �������� � ������� TIFF
     TIFFGraphic := GraphicClass.Create;
     TIFFGraphic.OnProgress := PreviewLoadProgress;
     PreviewStatusbar.TypeOfFile := tfTIFF;
     TIFFGraphic.LoadFromStream(Stream);
     Self.Picture.Graphic := TIFFGraphic;
     PreviewStatusbar.loaded := GetTickCount - Start;
    end;
   if (PreviewImage.Picture.Width < PreviewPanel.ClientWidth) and
      (PreviewImage.Picture.Height < PreviewPanel.ClientHeight) then
    PreviewImage.CanResize := false
   else
    PreviewImage.CanResize := true;
   PreviewStatusbar.ExifDateTime := Item.SubItems[2];
   PreviewStatusbar.ExifDateTimeOriginal := Item.SubItems[3];
   PreviewStatusbar.ExifDateTimeDigitized := Item.SubItems[4];
   PreviewStatusbar.SizeOfFile := ExifFileSize(FFileName);
   PreviewStatusbar.FileName := FFileName;
   PreviewStatusbar.ShowParam;
  except
   FFileName := '';
   Self.Picture := nil;
   ExifDateMainForm.EnablerNextPrevButton;
  end
 finally
  Screen.Cursor := crDefault;
 end;
end;

Procedure TExifDateMainForm.ShowThumbnail;
Var
 MS: TMemoryStream;
 JPG: TJPEGImage;
 s:string;
begin
 ThumbnailImage.Thumbnail := false;
 if (ExifGrid.Exif.ValidExif) and (ExifGrid.Exif.Thumbnail.Length > 0) then
  begin
   MS := TMemoryStream.Create;
   MS.Write(ExifGrid.Exif.Thumbnail.Image[0], ExifGrid.Exif.Thumbnail.Length);
   JPG := TJPEGImage.Create;
   try
    MS.Position := 0;
    JPG.LoadFromStream(MS);
    ThumbnailImage.Picture.Bitmap.Assign(JPG);
   finally
    JPG.Free;
    MS.Free;
   end;
// SetJPEGOptions
   if (ThumbnailImage.Picture.Graphic is TJPEGImage) then
    with TJPEGImage(ThumbnailImage.Picture.Graphic) do
     begin
      PixelFormat := jf24Bit;
      Scale := jsFullSize;
      Grayscale := false;
      Performance := jpBestQuality;
      ProgressiveDisplay := true;
     end;
   ThumbnailImage.IncrementalDisplay := true;
   ThumbnailImage.Thumbnail := true;
  end
 else
  NoThumbnailPaint(ThumbnailImage);
 ResizeThumbnailForm;
end;

procedure TExifDateMainForm.NoThumbnailPaint(Img: TImage);
var
 s: string;
begin
 s := 'No Thumbnail';
 Img.Canvas.Brush := Img.Parent.Brush;
 Img.Canvas.TextRect(Img.ClientRect, Img.Left + Img.ClientWidth div 2 - Img.Canvas.TextWidth(s) div 2, Img.Top + Img.ClientHeight div 2 - Img.Canvas.TextHeight(s) div 2, s);
end;

function TExifDateMainForm.ComputeDockingRect(Sender: TObject; var DockRect: TRect; MousePos: TPoint): TAlign;
var
 DockLeftRect,
 DockRightRect: TRect;
begin
 Result := alNone;

 DockLeftRect.TopLeft := (Sender as TWinControl).ClientRect.TopLeft;
 DockLeftRect.BottomRight := Point((Sender as TWinControl).ClientWidth div 2, (Sender as TWinControl).ClientHeight);

 DockRightRect.TopLeft := Point((Sender as TWinControl).ClientWidth div 2, (Sender as TWinControl).Top);
 DockRightRect.BottomRight := (Sender as TWinControl).ClientRect.BottomRight;

 if PtInRect(DockLeftRect, MousePos) then
  begin
   Result := alLeft;
   DockRect := DockLeftRect;
  end
 else
  if PtInRect(DockRightRect, MousePos) then
   begin
    Result := alRight;
    DockRect := DockRightRect;
   end;
   
 if Result = alNone then Exit;

 DockRect.TopLeft := (Sender as TWinControl).ClientToScreen(DockRect.TopLeft);
 DockRect.BottomRight := (Sender as TWinControl).ClientToScreen(DockRect.BottomRight);
end;

procedure TExifDateMainForm.ExifGridDockPanelGetSiteInfo(Sender: TObject;
  DockClient: TControl; var InfluenceRect: TRect; MousePos: TPoint;
  var CanDock: Boolean);
begin
  CanDock := DockClient is TExifGridForm;
end;

procedure TExifDateMainForm.ExifGridDockPanelDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  ARect: TRect;
  iWidth: integer;
begin
 Accept := (Source.Control is TExifGridForm);
 if Accept then
  begin
{
    if (Source.Control as TExifGridForm).ClientWidth > ExifDateMainForm.ClientWidth div 2 then
     iWidth := ExifDateMainForm.ClientWidth div 2
    else
     iWidth := (Source.Control as TExifGridForm).ClientWidth;
}
   iWidth := ExifDateMainForm.ClientWidth div 4;
   if (Sender is TExifGridRightDockPanel) then
    begin
     ARect.TopLeft := (Sender as TPanel).ClientToScreen(Point( - iWidth, 0));
     ARect.BottomRight := (Sender as TPanel).ClientToScreen(Point(0, (Sender as TPanel).Height));
    end
   else
    begin
     ARect.TopLeft := (Sender as TPanel).ClientToScreen(Point(0, 0));
     ARect.BottomRight := (Sender as TPanel).ClientToScreen(Point(iWidth, (Sender as TPanel).Height));
    end;
   Source.DockRect := ARect;
  end;
end;

procedure TExifDateMainForm.ExifGridDockPanelDockDrop(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer);
var
 iWidth: integer;
begin
// iWidth := Source.DockRect.Right - Source.DockRect.Left;
 iWidth := ExifDateMainForm.ClientWidth div 9;
 if (Sender as TPanel).DockClientCount = 1 then
  ShowExifGridDockPanel(Sender as TPanel, True, iWidth);
 (Sender as TPanel).DockManager.ResetBounds(True);
end;

procedure TExifDateMainForm.ExifGridDockPanelUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
  if (Sender as TPanel).DockClientCount = 1 then
   ShowExifGridDockPanel(Sender as TPanel, False, 0);
end;

procedure TExifDateMainForm.ShowExifGridDockPanel(APanel: TPanel; MakeVisible: Boolean; iWidth: Integer);
begin
 if not MakeVisible and (APanel.VisibleDockClientCount > 1) then Exit;
 if (APanel is TExifGridLeftDockPanel) then
  ExifGridLeftDockPanelSplitter.Visible := MakeVisible;
 if (APanel is TExifGridRightDockPanel) then
  ExifGridRightDockPanelSplitter.Visible := MakeVisible;
 if MakeVisible then
  if iWidth > ExifDateMainForm.ClientWidth div 2 then
   APanel.Width := ExifDateMainForm.ClientWidth div 2
  else
   APanel.Width := iWidth
 else
  APanel.Width := 0;
end;

procedure TExifDateMainForm.FoldersDockPanelGetSiteInfo(Sender: TObject;
  DockClient: TControl; var InfluenceRect: TRect; MousePos: TPoint;
  var CanDock: Boolean);
begin
 CanDock := (DockClient is TFolderDockForm) and ((Sender as TPanel).DockClientCount = 0);
end;

procedure TExifDateMainForm.FoldersDockPanelDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  ARect: TRect;
begin
 Accept := (Source.Control is TThumbnailDockForm) or (Source.Control is TFolderDockForm);
 if Accept then
  if (Sender as TPanel).DockClientCount > 0 then
   begin
    if ComputeDockingRect(Sender, ARect, Point(X, Y)) <> alNone then
     Source.DockRect := ARect
   end
  else
   begin
    ARect.TopLeft := (Sender as TPanel).ClientToScreen(Point(0, 0));
    ARect.BottomRight := (Sender as TPanel).ClientToScreen(Point((Sender as TPanel).ClientWidth, ExifDateMainForm.ClientHeight div 3));
    Source.DockRect := ARect;
   end;
end;

procedure TExifDateMainForm.FoldersDockPanelDockDrop(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer);
var
  i: integer;
  ARect: TRect;
  Ctrl: TControl;
begin
 i := (Sender as TPanel).DockClientCount;
 if (Sender as TPanel).DockClientCount > 0 then
  ShowFoldersDockPanel(Sender as TPanel, True);
{
 asm int 3 end;
 if (Sender as TPanel).DockClientCount = 2 then
  if (Sender as TPanel).DockClients[0].Left = 0 then
   begin
    Left := ((Sender as TPanel).DockClients[0] as TFolderForm);
    Right := ((Sender as TPanel).DockClients[1] as TFolderForm);
   end
  else
   begin
    Left := ((Sender as TPanel).DockClients[1] as TFolderForm);
    Right := ((Sender as TPanel).DockClients[0] as TFolderForm);
   end;

 if (Sender as TPanel).DockClientCount = 2 then
  if (Sender as TPanel).DockClients[0].Left < (Sender as TPanel).DockClients[1].Left then
   begin
    (Sender as TPanel).DockClients[0].Width := (Sender as TPanel).ClientWidth div 3 * 2 - 3;
    (Sender as TPanel).DockClients[1].Left := (Sender as TPanel).DockClients[0].Left + (Sender as TPanel).DockClients[0].Width + 3;
    (Sender as TPanel).DockClients[1].Width := (Sender as TPanel).ClientWidth div 3;
   end
  else
   begin
    (Sender as TPanel).DockClients[1].Width := (Sender as TPanel).ClientWidth div 3 * 2 - 3;
    (Sender as TPanel).DockClients[0].Left := (Sender as TPanel).DockClients[1].Left + (Sender as TPanel).DockClients[1].Width + 3;
    (Sender as TPanel).DockClients[0].Width := (Sender as TPanel).ClientWidth div 3;
   end;
}
 if (Sender is TThumbnailDockForm) and
    (not ThumbnailImage.Picture.Graphic.Empty) then
  begin
   (Sender as TPanel).UndockHeight := ThumbnailImage.Picture.Height;
   (Sender as TPanel).UndockWidth := ThumbnailImage.Picture.Width;
  end;
 (Sender as TPanel).DockManager.ResetBounds(true);
end;

procedure TExifDateMainForm.FoldersDockPanelUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
 if (Sender as TPanel).DockClientCount = 1 then
  ShowFoldersDockPanel(Sender as TPanel, False);
 if Client is TDockForm then Client.Width := (ClientWidth div 3) * 2;
end;

procedure TExifDateMainForm.ShowFoldersDockPanel(APanel: TPanel; MakeVisible: Boolean);
begin
 if not MakeVisible and (APanel.VisibleDockClientCount > 1) then Exit;
 FoldersDockPanelSplitter.Visible := MakeVisible;
 if MakeVisible then
  APanel.Parent.Height := ClientHeight div 3
 else
  APanel.Parent.Height := 0;
end;

procedure TExifDateMainForm.MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
///// if not (Sender as TWinControl).Focused then (Sender as TWinControl).SetFocus;
end;

{ TThumbnailImage }

procedure TThumbnailImage.WMPaint(var Msg: TWMPaint);
begin
 inherited;
end;


procedure TExifDateMainForm.FileListViewCompare(Sender: TObject; Item1, Item2: TListItem; Data: Integer; var Compare: Integer);
var
 d1,d2: TDateTime;
begin
 case FileListView.ColumnToSort of
  0: Compare := CompareStr(Uppercase(Item1.Caption),Uppercase(Item2.Caption));
  else
   begin
    try
     d1 := 0;
     d1 := StrToDateTime(Item1.SubItems[FileListView.ColumnToSort - 1], FS);
    except
     Compare := 1;
     Exit;
    end;
    try
     d2 := 0;
     d2 := StrToDateTime(Item2.SubItems[FileListView.ColumnToSort - 1], FS);
    except
     Compare := -1;
     Exit;
    end;
    if d1 < d2 then Compare := -1;
    if d1 > d2 then Compare := 1;
    if d1 = d2 then Compare := 0;
   end;
 end;
end;

procedure TExifDateMainForm.FileListViewColumnClick(Sender: TObject; Column: TListColumn);
begin
 FileListView.ColumnToSort := Column.Index;
 (Sender as TCustomListView).AlphaSort;
end;

procedure TExifDateMainForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 case Key of
  27: ExitProgram(Sender);
  33: begin
        PrevFileListView(Sender);
        Key := 0;
       end;
  34: begin
        NextFileListView(Sender);
        Key := 0;
       end;
  13: begin
        if (FileListView.Focused) or (PreviewPanel.Visible) then
         SwitchToPicture(Sender);
       end;
 end;
end;

{ TPreviewStatusBar }

procedure TPreviewStatusBar.ShowParam;
begin
 Panels[0].Text := IntToStr(ItemIndex) + '/' + IntToStr(CountFiles);
 Panels[1].Text := TypeOfFile;
 Panels[2].Text := FileSizeToStr(SizeOfFile);
 Panels[3].Text := Format('%d x %d', [PreviewImage.Picture.Width, PreviewImage.Picture.Height]);
 Panels[4].Text := ExifDateTime;
 Panels[5].Text := ExifDateTimeOriginal;
 Panels[6].Text := ExifDateTimeDigitized;
 Panels[7].Text := 'load time: ' + DoubleToStr((Loaded) / 1000, 2) + ' s';
 Panels[8].Text := FileName;
 StatusBarResize(Self);
end;

constructor TPreviewStatusBar.Create(AOwner: TComponent);
begin
 inherited;
 OnResize := StatusBarResize;
end;

procedure TPreviewStatusBar.SetCountFiles(Count: integer);
begin
 CountFiles := Count;
 ShowParam;
end;

procedure TPreviewStatusBar.SetItemIndex(Index: integer);
begin
 ItemIndex := Index;
 ShowParam;
end;

{ TPreviewImage }

constructor TPreviewImage.Create(AOwner: TComponent);
begin
 inherited;
 FFit := true;
 OnMouseMove := MouseMove;
 OnMouseDown := MouseDown;
 OnMouseUp := MouseUp;
end;

procedure TPreviewImage.MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
 NewX, NewY: integer;
begin
 if FMove then
  begin
   NewX := Self.Left + X - FX0;
   NewY := Self.Top + Y - FY0;
   if NewX > 0 then NewX := 0;
   if NewY > 0 then NewY := 0;

   if NewX + Self.Width < Self.Parent.ClientWidth then
    NewX := Self.Parent.ClientWidth - Self.Width;
   if NewY + Self.Height < Self.Parent.ClientHeight then
    NewY := Self.Parent.ClientHeight - Self.Height;

//   if NewX + SelfWidth > Self.Parent.ClientWidth then NewX :=
//   TempPanel.Caption := IntToStr(NewX + Self.Width)+ '      ' + IntToStr(NewY + Self.Height);
   Self.SetBounds(NewX, NewY, Self.Width, Self.Height);
  end;
end;

procedure TFileViewStatusBar.ShowParam;
var
 i: integer;
begin
 if not FReading then
  begin
   case FFocused of
    sbExifGrid: begin
                  if FileViewStatusbar.Panels[3].Text <> ExifGrid.SelectedRow.Name then
                   FileViewStatusbar.Panels[3].Text := ExifGrid.SelectedRow.Name;
                  if FileViewStatusbar.Panels[4].Text <> ExifGrid.SelectedRow.Value then
                   FileViewStatusbar.Panels[4].Text := ExifGrid.SelectedRow.Value;
                  if FileViewStatusbar.Panels[5].Text <> '' then FileViewStatusbar.Panels[5].Text := '';
                 end;
    sbFileView: if FileListView.Items.Count > 0 then
                  if FileListView.Selected <> nil then
                   begin
                    FileViewStatusbar.Panels[3].Text := FileListView.Selected.SubItems[2];
                    FileViewStatusbar.Panels[4].Text := FileListView.Selected.SubItems[3];
                    FileViewStatusbar.Panels[5].Text := FileListView.Selected.SubItems[4];
                   end;
   else
    if FileViewStatusbar.Panels[3].Text <> '' then FileViewStatusbar.Panels[3].Text := '';
    if FileViewStatusbar.Panels[4].Text <> '' then FileViewStatusbar.Panels[4].Text := '';
    if FileViewStatusbar.Panels[5].Text <> '' then FileViewStatusbar.Panels[5].Text := '';
   end;
   for i := 6 to FileViewStatusBar.Panels.Count - 1 do FileViewStatusbar.Panels[i].Text := '';
   StatusBarResize(Self);
  end;
end;

procedure TPreviewImage.MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 FX0 := X;
 FY0 := Y;
 FMove := true;
end;

procedure TPreviewImage.MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 FMove := false;
end;

procedure TExifDateMainForm.PreviewPanelResize(Sender: TObject);
begin
 if PreviewImage.Left + PreviewImage.Width < PreviewPanel.ClientWidth then
  PreviewImage.Left := PreviewPanel.ClientWidth - PreviewImage.Width;
 if PreviewImage.Top + PreviewImage.Height < PreviewPanel.ClientHeight then
  PreviewImage.Top := PreviewPanel.ClientHeight - PreviewImage.Height;
 if (PreviewImage.Picture.Width < PreviewPanel.ClientWidth) and
    (PreviewImage.Picture.Height < PreviewPanel.ClientHeight) then
  PreviewImage.CanResize := false
 else
  PreviewImage.CanResize := true;
end;

procedure TPreviewImage.SetCanResize(Value: boolean);
begin
 FCanResize := Value;
 ActionResize.Enabled := value;
 if (FFit = false) and (Value = false) then
  PreviewImage.Align := alClient;
 if (FFit = false) and (Value = true) then
  PreviewImage.Align := alNone;
end;

procedure TPreviewImage.SetFit(Value: boolean);
begin
 FFit := Value;
end;

procedure TExifDateMainForm.ReadProgress(Sender: TObject; Stage: TProgressStage; PercentDone: Byte; const Msg: String);
var
  X,i: Integer;
  s: string[4];
  PanelIndex: Byte;
begin
 with FileViewStatusBar do
  begin
    PanelIndex := FileViewStatusBar.Panels.Count - 1;
    case Stage of
      psStarting:
        begin
          for i := 2 to PanelIndex do Panels[i].Text :='';
          if FileViewProgressBar <> nil then FileViewProgressBar.Free;
          FileViewProgressBar := TExifProgressBar.Create(FileViewStatusBar);
          FileViewProgressBar.Parent := FileViewStatusBar;
          FileViewProgressBar.Height := FileViewStatusBar.ClientHeight - 5;
          FileViewProgressBar.BorderWidth := 0;
          FileViewProgressBar.Color := ChangeColor(FileViewStatusBar.Color, false, 50);
          FileViewProgressBar.Smooth := true;
          FileViewProgressBar.Max := 100;
          FileViewStatusbar.Panels[PanelIndex - 1].Text := Msg;
          X := 0;
          for i := 0 to PanelIndex - 1 do X := X + Panels[i].Width;
          FileViewProgressBar.SetBounds(X, FileViewStatusBar.Height div 2 - FileViewProgressBar.Height div 2 + 1, Panels[PanelIndex].Width - 3, FileViewProgressBar.Height);
          SetWindowLong(FileViewProgressBar.Handle, GWL_EXSTYLE, DWORD(GetWindowLong(FileViewProgressBar.Handle, GWL_EXSTYLE)) and (not WS_EX_STATICEDGE));
          SendMessage(FileViewProgressBar.Handle, PBM_SETBARCOLOR, 0, ChangeColor(FileViewStatusBar.Color, false, 0));
          FileViewProgressBar.Show;
          Application.ProcessMessages;
        end;
      psEnding:
        begin
          FileViewStatusbar.ShowParam;
          FileViewProgressBar.Free;
          FileViewProgressBar := nil;
        end;
      psRunning:
        begin
          FileViewStatusbar.Panels[FileViewStatusBar.Panels.Count - 2].Text := Msg;
          FileViewStatusbar.Resize;
          FileViewProgressBar.Position := PercentDone;
          FileViewProgressBar.Update;
          Application.ProcessMessages;
        end;
    end;
  end;
end;

{ TFileListView }

procedure TExifDateMainForm.NextFileListView(Sender: TObject);
begin
 if Assigned(FileListView) then FileListView.Next;
end;

procedure TExifDateMainForm.PrevFileListView(Sender: TObject);
begin
 if Assigned(FileListView) then FileListView.Prev;
end;

procedure TFileListView.WMMouseWheel(var Message: TWMMouseWheel);
begin
 if Message.WheelDelta < 0 then
  Next
 else
  Prev;
end;

procedure TExifDateMainForm.WMNCPaint(var Msg: TWMNCPaint);
var
  dc: hDc;
  Pen: hPen;
  OldPen: hPen;
  OldBrush: hBrush;
begin
  inherited;
(*  
//  Exit;
  dc := GetWindowDC(Handle);
  msg.Result := 1;
  Pen := CreatePen(PS_SOLID, 1, MainLightColor);
  OldPen := SelectObject(dc, Pen);
  OldBrush := SelectObject(dc, GetStockObject(NULL_BRUSH));

  MoveToEx(dc, 0, Self.Height - 1, nil);
  LineTo(dc, 0, 0);
  LineTo(dc, Self.Width - 1, 0);

  MoveToEx(dc, 1, Self.Height - 2, nil);
  LineTo(dc, 1, 1);
  LineTo(dc, Self.Width - 2, 1);

  Pen := CreatePen(PS_SOLID, 1, MainShadowColor);
  OldPen := SelectObject(dc, Pen);

  MoveToEx(dc, Self.Width - 1, 0, nil);
  LineTo(dc, Self.Width - 1, Self.Height - 1);
  LineTo(dc, 0, Self.Height - 1);

  MoveToEx(dc, Self.Width - 2, 1, nil);
  LineTo(dc, Self.Width - 2, Self.Height - 2);
  LineTo(dc, 1, Self.Height - 2);

  Pen := CreatePen(PS_SOLID, 1, MainColor);
  OldPen := SelectObject(dc, Pen);

  Rectangle(dc, 2, 2, Self.Width - 2, Self.Height - 2);
  Rectangle(dc, 3, 3, Self.Width - 3, Self.Height - 3);

//  Rectangle(dc, 0,0, Self.Width, Self.Height);
  SelectObject(dc, OldBrush);
  SelectObject(dc, OldPen);
  DeleteObject(Pen);
  ReleaseDC(Handle, Canvas.Handle);
*)
end;

procedure TExifDateMainForm.WMClose(var Message: TMessage);
begin
 ExitProgram(Application);
end;

procedure TExifDateMainForm.WMSysCommand(var Message: TWMSysCommand);
begin
 inherited;
end;

{ TStatShape }

procedure TStatShape.Paint;
var
  X, Y, W, H: Integer;
  hs, ws: integer;
  R: TRect;
  s: string;
begin
  with Canvas do
   begin
    Pen.Style := psSolid;
    Pen.Color := clBlack;
    Brush.Color := Color;
    X := LeftIndent + Pen.Width div 2;
    Y := Pen.Width div 2;
    W := Width - Pen.Width + 1 - LeftIndent;
    H := Height - Pen.Width + 1;
    if Pen.Width = 0 then
     begin
      Dec(W);
      Dec(H);
     end;
//    Rectangle(X - LeftIndent, Y, X + W, Y + H);
    Rectangle(X, Y, X + W, Y + H);
    //..........................................................................
    Brush.Color := IndentColor;
    Font.Size := 9;
    // ��������
    s := IntToStr(Max);
    hs := DrawText(Handle, PChar(s), Length(s), R, DT_LEFT or DT_TOP or DT_WORDBREAK or DT_NOPREFIX or DT_CALCRECT);
    ws := R.Right - R.Left;
    TextOut(LeftIndent - ws - 5, TopIndent - hs div 2, s);
    // ��������
    s := IntToStr(Max div 2);
    hs := DrawText(Handle, PChar(s), Length(s), R, DT_LEFT or DT_TOP or DT_WORDBREAK or DT_NOPREFIX or DT_CALCRECT);
    ws := R.Right - R.Left;
    TextOut(LeftIndent - ws - 5, TopIndent + (Height - TopIndent) div 2 - hs div 2, s);
    // �������
    s := '0';
    hs := DrawText(Handle, PChar(s), Length(s), R, DT_LEFT or DT_TOP or DT_WORDBREAK or DT_NOPREFIX or DT_CALCRECT);
    ws := R.Right - R.Left;
    TextOut(LeftIndent - ws - 5, Height - hs, s);
    //..........................................................................
    MoveTo(LeftIndent + Pen.Width - 3, TopIndent); LineTo(LeftIndent + Pen.Width + 3, TopIndent);
    MoveTo(LeftIndent + Pen.Width - 3, TopIndent + (Height - TopIndent) div 4); LineTo(LeftIndent + Pen.Width + 3, TopIndent + (Height - TopIndent) div 4);
    MoveTo(LeftIndent + Pen.Width - 3, TopIndent + (Height - TopIndent) div 2); LineTo(LeftIndent + Pen.Width + 3, TopIndent + (Height - TopIndent) div 2);
    MoveTo(LeftIndent + Pen.Width - 3, TopIndent + (Height - TopIndent) div 2 + (Height - TopIndent) div 4); LineTo(LeftIndent + Pen.Width + 3, TopIndent + (Height - TopIndent) div 2 + (Height - TopIndent) div 4);
    Pen.Style := psDot;
    Pen.Color := clSilver;
    MoveTo(LeftIndent + Pen.Width, TopIndent); LineTo(Width - Pen.Width, TopIndent);
    MoveTo(LeftIndent + Pen.Width, TopIndent + (Height - TopIndent) div 4); LineTo(Width - Pen.Width, TopIndent + (Height - TopIndent) div 4);
    MoveTo(LeftIndent + Pen.Width, TopIndent + (Height - TopIndent) div 2); LineTo(Width - Pen.Width, TopIndent + (Height - TopIndent) div 2);
    MoveTo(LeftIndent + Pen.Width, TopIndent + (Height - TopIndent) div 2 + (Height - TopIndent) div 4); LineTo(Width - Pen.Width, TopIndent + (Height - TopIndent) div 2 + (Height - TopIndent) div 4);
   end;
end;

procedure TExifDateMainForm.ProgressFormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 Cancel := Key = 27;
end;

procedure TExifDateMainForm.OpenURL(Sender: TObject);
begin
 ShellExecute(0, 'open', 'http://exifrenamer.ucoz.com/', nil, nil, SW_SHOW)   // http://exifrenamer.ucoz.com/ � 21.06.2013
end;

{ TStatForm }

constructor TStatForm.CreateNew(AOwner: TComponent; Dummy: Integer = 0);
begin
 inherited;
 FormCreate(Self);
end;

procedure TStatForm.FormCreate(Sender: TObject);
begin
 MouseScrollOn := false;
 Caption := 'Statistics';
 Width := Screen.WorkAreaWidth - Round(Screen.WorkAreaWidth * 5 /100);
 Height := Screen.WorkAreaHeight - Round(Screen.WorkAreaHeight * 5 /100);
 Position := poMainFormCenter;
 WindowState := wsMaximized;
 BorderIcons := [biSystemMenu, biMinimize, biMaximize];
 BorderStyle := bsSizeable;
 Color := $00FCF4ED;
 Font.Charset := DEFAULT_CHARSET;
 Font.Color := clWindowText;
 Font.Height := -11;
 Font.Name := 'MS Sans Serif';
 Font.Style := [];
 OldCreateOrder := False;
// OnCreate := FormCreate;
 PixelsPerInch := 96;
 DockSite := false;
 KeyPreview := true;
 FormStyle := fsNormal;
 // ������� Panel
 Panel := TPanel.Create(Self);
 Panel.Parent := Self;
 Panel.Align := alTop;
 Panel.Height := 44;
 Panel.BevelOuter := bvNone;
 Panel.Color := $00FCF4ED;
 // ������� ScrollBox
 ScrollBox := TScrollBox.Create(Self);
 ScrollBox.Parent := Self;
 ScrollBox.Align := alClient;
 ScrollBox.Visible := true;
 ScrollBox.HorzScrollBar.Tracking := true;
 ScrollBox.VertScrollBar.Tracking := true;
 ScrollBox.Color := $00FCF4ED;
 ScrollBox.BorderStyle := bsNone;
 ScrollBox.OnMouseDown := MouseDown;
 ScrollBox.OnMouseUp := MouseUp;
 ScrollBox.OnMouseMove := MouseMove;
 // ������� memo
 StatMemo := TMemo.Create(Self);
 StatMemo.Parent := Self;
 StatMemo.Align := alClient;
 StatMemo.ScrollBars := ssVertical;
 StatMemo.ReadOnly := true;
 StatMemo.Visible := false;
 StatMemo.Font.Charset := DEFAULT_CHARSET;
 StatMemo.Font.Color := clWindowText;
 StatMemo.Font.Size := 11;
 StatMemo.Font.Name := 'MS Sans Serif';
 StatMemo.Font.Style := [fsBold];
 // ������ ������������ ���������� � ������������ ���� Memo.Visible := true/false
 Button := TButton.Create(Self);
 Button.Parent := Self;
 Button.Anchors := [akTop,akRight];
 Button.Width := 90;
 Button.Left := ClientWidth - Button.Width - 30;
 Button.Top := 10;
 Button.Caption := 'TextView';
 Button.OnClick := ButtonClick;
end;

procedure TStatForm.ButtonClick(Sender: TObject);
begin
 StatMemo.Visible := not StatMemo.Visible;
 Panel.Visible := not StatMemo.Visible;
 if StatMemo.Visible then
  Button.Caption := 'Histogram view'
 else
  Button.Caption := 'Text view'
end;

procedure TExifDateMainForm.FormDestroy(Sender: TObject);
begin
// FilesExifList.Free;
// FilesExifList := nil;
end;

procedure TStatForm.MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
 if MouseScrollOn then
  begin
   ScrollBox.HorzScrollBar.Position := ScrollBox.HorzScrollBar.Position - (X - FX0);
   ScrollBox.VertScrollBar.Position := ScrollBox.VertScrollBar.Position - (Y - FY0);
   FX0 := X;
   FY0 := Y;
  end;
end;

procedure TStatForm.MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Cursor := crHandPoint;
 FX0 := X;
 FY0 := Y;
 MouseScrollOn := true;
end;

procedure TStatForm.MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 MouseScrollOn := false;
 Cursor := crDefault;
end;

end.
