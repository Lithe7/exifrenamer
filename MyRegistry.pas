{$R+}
unit MyRegistry;

interface

uses Windows, Registry, SysUtils, Forms, Variants, Dialogs;

type
  TMyReg = class(TObject)
  private
    FKey : string;
    FRootKey : HKEY;
  protected
  public
    constructor Create;
    destructor Destroy; virtual;

    function ReadFromReg(Name : string; var Value : string) : Boolean; overload;
    function ReadFromReg(Name : string; var Value : integer) : Boolean; overload;
    function ReadFromReg(Name : string; var Value : boolean) : Boolean; overload;
    function ReadFromReg(Name : string; var Value : TDateTime) : Boolean; overload;

    function WriteToReg(name : string; value : Variant) : boolean;
    
    function DelValue(Name : string) : boolean;
  published
    property Key : string read FKey write FKey;
    property RootKey : HKEY read FRootKey write FRootKey;
  end;

var
 MyReg : TMyReg;

implementation

constructor TMyReg.Create;
begin
 inherited;
end;

function TMyReg.DelValue(Name: string): boolean;
var
 Reg : TRegistry;
begin
 result := false;
 try
   Reg := TRegistry.Create;
   with Reg do
    begin
     RootKey := Self.FRootKey;
     if OpenKey(FKey, false) then
      result := DeleteValue(Name);
     free;
    end;
  except
   result := false;
  end;
end;

destructor TMyReg.Destroy;
begin
 inherited;
end;

Function TMyReg.ReadFromReg(Name: string; var value: string): boolean;
var
 Reg: TRegistry;
begin
 result := false;
 try
   Reg := TRegistry.create;
   with Reg do
    begin
     RootKey := Self.FRootKey;
     if OpenKey(FKey, false) then
      if ValueExists(Name) then
       begin
        value := ReadString(Name);
        result := true;
       end;
     free;
    end;
  except
   result := false;
  end;
end;

Function TMyReg.ReadFromReg(Name: string; var value: boolean): boolean;
var
 Reg: TRegistry;
begin
 result := false;
 try
   Reg := TRegistry.Create;
   with Reg do
    begin
     RootKey := Self.FRootKey;
     if OpenKey(FKey, false) then
      begin
       value := ReadBool(Name);
       result := true;
      end;
     free;
    end;
  except
   result := false;
  end;
end;

Function TMyReg.ReadFromReg(Name:string; var value : integer) : boolean;
var
 Reg : TRegistry;
begin
 result := false;
 try
   Reg := TRegistry.create;
   with Reg do
    begin
     RootKey := Self.FRootKey;
     if OpenKey(FKey, false) then
      begin
       value := ReadInteger(Name);
       result := true;
      end;
     free;
    end;
  except
   result := false;
  end;
end;

Function TMyReg.ReadFromReg(Name:string; var value : TDateTime) : boolean;
var
 Reg : TRegistry;
begin
 result := false;
 try
   Reg := TRegistry.Create;
   with Reg do
    begin
     RootKey := Self.FRootKey;
     if OpenKey(FKey, false) then
      begin
       value := ReadDate(Name);
       result := true;
      end;
     free;
    end;
  except
   result := false;
  end;
end;

function TMyReg.WriteToReg(Name : string; value : variant) : boolean;
var
 Reg : TRegistry;
begin
 try
   Reg := TRegistry.Create;
   with Reg do
    begin
     LazyWrite := false;
     RootKey := Self.FRootKey;
     OpenKey(FKey, true);
     Case VarType(Value) of
      varBoolean  : WriteBool(Name, value);
      varInteger  : WriteInteger(Name, value);
      varString   : WriteString(Name, value);
      varDate     : WriteDate(Name, value);
      varShortInt : WriteInteger(Name, value);
     end;
     free;
    end;
   result := true;
  except
   result := false;
  end;
end;

initialization

 MyReg := TMyReg.Create;

finalization

 MyReg.Free;

end.
