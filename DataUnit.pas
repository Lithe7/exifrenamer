unit DataUnit;

interface

uses Contnrs, Exif;

type

 TFilesExifList = class(TObjectList)
  private
    function GetItems(Index: Integer): TExif;
    procedure SetItems(Index: Integer; const Value: TExif);
  public
   property Items[Index: Integer]: TExif read GetItems write SetItems; default;
 end;

implementation

{ FilesExifList }

function TFilesExifList.GetItems(Index: Integer): TExif;
begin
 result := TExif(inherited GetItem(Index))
end;

procedure TFilesExifList.SetItems(Index: Integer; const Value: TExif);
begin
 inherited SetItem(Index, Value);
end;

end.
