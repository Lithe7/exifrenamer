unit LitheColorUnit;

interface

uses Graphics;

function ChangeColor(iColor : TColor; Up : boolean; iDifference : Byte) : TColor;

implementation

function ChangeColor(iColor : TColor; Up : boolean; iDifference : Byte) : TColor;
type
 TRGBColor = record
              R,G,B,S : Byte;
             end;
var
  iC : TColor;
  RGBColor : TRGBColor absolute iC;
begin
 iC := ColorToRGB(iColor);
 Case Up of
  true  : begin
           if RGBColor.R + iDifference < 255 then
            RGBColor.R := RGBColor.R + iDifference
           else
            RGBColor.R := 255;
           if RGBColor.G + iDifference < 255 then
            RGBColor.G := RGBColor.G + iDifference
           else
            RGBColor.G := 255;
           if RGBColor.B + iDifference < 255 then
            RGBColor.B := RGBColor.B + iDifference
           else
            RGBColor.B := 255;
          end;
  false : begin
           if RGBColor.R - iDifference > 0 then
            RGBColor.R := RGBColor.R - iDifference
           else
            RGBColor.R := 0;
           if RGBColor.G - iDifference > 0 then
            RGBColor.G := RGBColor.G - iDifference
           else
            RGBColor.G := 0;
           if RGBColor.B - iDifference > 0 then
            RGBColor.B := RGBColor.B - iDifference
           else
            RGBColor.B := 0;
          end;
 end;
 result := iC;
end;

end.
 